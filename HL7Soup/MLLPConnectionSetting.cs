using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	[Serializable]
	public class MLLPConnectionSetting : IConnectionSetting
	{
		private char[] frameStart = new char[] { '\v' };

		private char[] frameEnd = new char[] { '\u001C', '\r' };

		public HL7Soup.ConnectionDirection ConnectionDirection
		{
			get;
			set;
		}

		public string ConnectionTypeName
		{
			get
			{
				return "MLLP";
			}
		}

		public string Details
		{
			get
			{
				return string.Concat(this.Server, ":", this.Port);
			}
		}

		public string Encoding
		{
			get;
			set;
		}

		public string ErrorMessage
		{
			get;
			set;
		}

		public char[] FrameEnd
		{
			get
			{
				return this.frameEnd;
			}
			set
			{
				this.frameEnd = value;
			}
		}

		public char[] FrameStart
		{
			get
			{
				return this.frameStart;
			}
			set
			{
				this.frameStart = value;
			}
		}

		public bool LoadApplicationAccept
		{
			get;
			set;
		}

		public bool LoadApplicationError
		{
			get;
			set;
		}

		public bool LoadApplicationReject
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public string RejectMessage
		{
			get;
			set;
		}

		public bool RelayMessage
		{
			get;
			set;
		}

		public string RelayName
		{
			get;
			set;
		}

		public bool ReturnApplicationAccept
		{
			get;
			set;
		}

		public bool ReturnApplicationError
		{
			get;
			set;
		}

		public bool ReturnApplicationReject
		{
			get;
			set;
		}

		public string Server
		{
			get;
			set;
		}

		public MLLPConnectionSetting()
		{
			this.LoadApplicationAccept = true;
			this.LoadApplicationError = true;
			this.LoadApplicationReject = true;
		}
	}
}