using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class TextUpdatedEventArgs
	{
		public int OldLength
		{
			get;
			set;
		}

		public TextUpdatedEventArgs()
		{
		}
	}
}