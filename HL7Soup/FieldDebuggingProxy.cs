using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class FieldDebuggingProxy
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Field BaseType
		{
			get;
			set;
		}

		public ICollection<IHL7Component> DebugHelperComponents
		{
			get
			{
				return this.BaseType.GetComponents();
			}
		}

		public string DebugHelperDescription
		{
			get
			{
				if (this.BaseType.XPart == null)
				{
					return "Unavailable";
				}
				return this.BaseType.XPart.Description;
			}
		}

		public bool IsFieldRepeated
		{
			get
			{
				return this.BaseType.IsFieldRepeated;
			}
		}

		public string LocationCode
		{
			get
			{
				return this.BaseType.LocationCode;
			}
		}

		public string Text
		{
			get
			{
				return this.BaseType.Text;
			}
		}

		public FieldDebuggingProxy(Field basetype)
		{
			this.BaseType = basetype;
		}
	}
}