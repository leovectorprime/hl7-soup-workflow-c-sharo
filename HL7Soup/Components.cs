using HL7Soup.Integrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HL7Soup
{
	public class Components : Collection<IHL7Component>, IHL7Components, ICollection<IHL7Component>, IEnumerable<IHL7Component>, IEnumerable
	{
		public Components()
		{
		}

		public Components(IList<IHL7Component> components) : base(components)
		{
		}

		public Components(IEnumerable<IHL7Component> components) : base(components.ToList<IHL7Component>())
		{
		}

		public override string ToString()
		{
			return string.Join<IHL7Component>("&", base.Items);
		}
	}
}