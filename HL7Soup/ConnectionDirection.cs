using System;

namespace HL7Soup
{
	public enum ConnectionDirection
	{
		Send,
		Receive
	}
}