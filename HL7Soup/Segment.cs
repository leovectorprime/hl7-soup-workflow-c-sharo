using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using HL7Soup.Workflow.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;

namespace HL7Soup
{
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(SegmentDebuggingProxy))]
	public class Segment : BasePart, IHL7Segment, IHL7Part
	{
		private string header;

		private ObservableCollection<BasePart> allChildParts = new ObservableCollection<BasePart>();

		public ObservableCollection<BasePart> AllChildParts
		{
			get
			{
				bool showFieldsWithEmptyValues = Settings.Default.ShowFieldsWithEmptyValues;
				this.allChildParts.Clear();
				if (base.ChildParts != null)
				{
					foreach (BasePart childPart in base.ChildParts)
					{
						if (childPart == null || childPart.Index <= -1)
						{
							continue;
						}
						if (showFieldsWithEmptyValues || !string.IsNullOrEmpty(childPart.Text))
						{
							this.allChildParts.Add(childPart);
						}
						childPart.Load();
						if (childPart.ChildParts == null)
						{
							continue;
						}
						foreach (BasePart basePart in childPart.ChildParts)
						{
							if (showFieldsWithEmptyValues || !string.IsNullOrEmpty(basePart.Text))
							{
								this.allChildParts.Add(basePart);
							}
							basePart.Load();
							if (basePart.ChildParts == null)
							{
								continue;
							}
							foreach (BasePart childPart1 in basePart.ChildParts)
							{
								if (!showFieldsWithEmptyValues && string.IsNullOrEmpty(childPart1.Text))
								{
									continue;
								}
								this.allChildParts.Add(childPart1);
							}
						}
					}
				}
				return this.allChildParts;
			}
		}

		public ObservableCollection<BasePart> CurrentAllChildParts
		{
			get
			{
				return this.allChildParts;
			}
		}

		public Field CurrentField
		{
			get
			{
				return (Field)base.CurrentChildPart;
			}
		}

		private string DebuggerDisplay
		{
			get
			{
				string path = this.GetPath();
				string str = "";
				string str1 = "                                                            ";
				if (base.XPart != null)
				{
					str = Helpers.TruncateString(base.XPart.Description, Math.Max(27 - this.GetPath().Length, 12));
				}
				return string.Concat(new string[] { path, "(", string.Concat(str, ") ", str1).Substring(0, 60 - (int)((double)(path.Length + str.Length) * 0.9)), "  ", base.Text });
			}
		}

		public ObservableCollection<Field> Fields
		{
			get;
			private set;
		}

		public int FirstNonSpaceCharacter
		{
			get;
			set;
		}

		public string Header
		{
			get
			{
				if (this.header == null)
				{
					this.GetHeader();
				}
				return this.header;
			}
		}

		public bool IsHeader
		{
			get
			{
				return this.Header == "MSH";
			}
		}

		public Segment(string text, BasePart parent, string locationCode, int documentLocation, int index, int firstNonSpaceCharacter, XBasePart xPart) : base(text, parent, locationCode, index, documentLocation, xPart)
		{
			this.FirstNonSpaceCharacter = firstNonSpaceCharacter;
		}

		internal string GetCurrentLocationCode()
		{
			return string.Concat(this.Header, (this.CurrentField != null ? string.Concat(".", this.CurrentField.LocationCode) : ""));
		}

		public override string GetCurrentPath()
		{
			if (base.CurrentChildPart == null)
			{
				return base.LocationCode;
			}
			string currentPath = base.CurrentChildPart.GetCurrentPath();
			return string.Concat(base.LocationCode, (string.IsNullOrWhiteSpace(currentPath) ? "" : string.Concat("-", currentPath)));
		}

		public override string GetDescriptionName()
		{
			return this.Header;
		}

		public BasePart GetField(PathSplitter path)
		{
			string str = (path.FieldRepeatIndex == "[0]" || path.FieldRepeatIndex == "[1]" ? path.Field : string.Concat(path.Field, path.FieldRepeatIndex));
			return (
				from p in this.Fields
				where p.LocationCode == str
				select p).FirstOrDefault<Field>();
		}

		public IHL7Field GetField(int fieldLocation)
		{
			string debuggerDisplay = this.DebuggerDisplay;
			base.Load();
			return (
				from p in this.Fields
				where p.LocationCode == fieldLocation.ToString()
				select p).FirstOrDefault<Field>();
		}

		public IHL7Field GetField(int fieldLocation, int repeatIndex)
		{
			base.Load();
			string str = fieldLocation.ToString();
			if (repeatIndex > 1)
			{
				str = string.Concat(new object[] { str, "[", repeatIndex, "]" });
			}
			return this.GetField(str);
		}

		public IHL7Field GetField(string locationCode)
		{
			base.Load();
			return (
				from p in this.Fields
				where p.LocationCode == locationCode
				select p).FirstOrDefault<Field>();
		}

		public ObservableCollection<Field> GetFields(Segment segment)
		{
			return this.GetFields(segment.Text);
		}

		public ObservableCollection<Field> GetFields(string text)
		{
			string[] array = this.SplitSegmentIntoFields(text);
			ObservableCollection<Field> observableCollection = new ObservableCollection<Field>();
			int num = 0;
			if (this.Header == "MSH")
			{
				List<string> list = array.ToList<string>();
				list.Insert(1, base.Message.FieldSeperator);
				array = list.ToArray();
			}
			int length = 0;
			int num1 = 0;
			string[] strArrays = array;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				int num2 = 0;
				num1++;
				string[] strArrays1 = this.SplitFieldsIntoRepeatFields(str);
				if (num == 2 && this.Header == "MSH")
				{
					strArrays1 = new string[] { str };
				}
				string[] strArrays2 = strArrays1;
				for (int j = 0; j < (int)strArrays2.Length; j++)
				{
					string str1 = strArrays2[j];
					num2++;
					string str2 = "";
					if (num2 > 1)
					{
						str2 = string.Concat("[", num2, "]");
						num--;
					}
					string str3 = string.Concat((num == 0 ? "" : num.ToString()), str2);
					if (num == 1 && this.Header == "MSH")
					{
						length -= 2;
					}
					Field field = new Field(str1, this, str3, num - 1, length, base.GetDefinitionByIndex(num - 1))
					{
						RepeatGroup = num1.ToString()
					};
					if ((int)strArrays1.Length > 1)
					{
						field.IsFieldRepeated = true;
					}
					observableCollection.Add(field);
					num++;
					length = length + str1.Length + 1;
				}
				if (num2 == 0)
				{
					length = length + str.Length + 1;
				}
			}
			return observableCollection;
		}

		public IHL7Fields GetFields()
		{
			base.Load();
			return new HL7Soup.Fields(this.Fields);
		}

		private string GetHeader()
		{
			this.header = Segment.GetHeader(base.Text);
			return this.header;
		}

		public static string GetHeader(string text)
		{
			text = text.Trim();
			if (string.IsNullOrWhiteSpace(text) || text.Length < 3)
			{
				return "";
			}
			return text.Substring(0, 3).ToUpper();
		}

		public BasePart GetPart(int field)
		{
			return this.GetPart(new PathSplitter(base.LocationCode, field));
		}

		public BasePart GetPart(int field, int component)
		{
			PathSplitter pathSplitter = new PathSplitter(base.LocationCode, field, component);
			return this.GetPart(pathSplitter);
		}

		public override BasePart GetPart(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Field))
			{
				BasePart field = this.GetField(path);
				if (path.PathTerminationPoint == PathTypes.Field || path.PathTerminationPoint == PathTypes.FieldRepeatIndex)
				{
					return field;
				}
				if (field != null)
				{
					BasePart part = field.GetPart(path);
					if (part != null)
					{
						return part;
					}
					if (path.Component == "1")
					{
						return field;
					}
				}
			}
			return null;
		}

		public string GetPartNiceText(int field)
		{
			PathSplitter pathSplitter = new PathSplitter(base.LocationCode, field);
			return SecurityElement.Escape(this.GetPartText(pathSplitter).Trim());
		}

		public string GetPartNiceText(int field, int component)
		{
			PathSplitter pathSplitter = new PathSplitter(base.LocationCode, field, component);
			return SecurityElement.Escape(this.GetPartText(pathSplitter).Trim());
		}

		public string GetPartNiceText(PathSplitter path)
		{
			return SecurityElement.Escape(this.GetPartText(path).Trim());
		}

		public string GetPartText(PathSplitter path)
		{
			BasePart part = this.GetPart(path);
			if (part != null)
			{
				part.Load();
				if (part.ChildParts != null && part.ChildParts.Count > 0)
				{
					part = part.ChildParts[0];
					if (part.ChildParts != null && part.ChildParts.Count > 0)
					{
						part = part.ChildParts[0];
					}
				}
				if (part != null)
				{
					return part.Text;
				}
			}
			return "";
		}

		void HL7Soup.Integrations.IHL7Part.SetText(string text)
		{
			this.SetTextQuietly(text);
			this.RebuildParentTextQuietly();
			this.Reload();
		}

		protected override void LoadChildParts()
		{
			this.Fields = this.GetFields(this);
			base.ChildParts = this.Fields.Cast<BasePart>().ToList<BasePart>();
		}

		protected override void OnChildPartsChanged()
		{
		}

		public override void Reload()
		{
			this.GetHeader();
			base.Reload();
		}

		public override HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Field))
			{
				Field field = (Field)this.GetField(path);
				if (field != null)
				{
					base.CurrentChildPart = field;
					HL7Soup.PositionInTheDocument positionInTheDocument = base.CurrentChildPart.SetCurrentPath(path);
					if (positionInTheDocument == null)
					{
						return new HL7Soup.PositionInTheDocument(this.CurrentField.LocationOfFirstCharacter, this.CurrentField.Text.Length);
					}
					HL7Soup.PositionInTheDocument start = positionInTheDocument;
					start.Start = start.Start + this.CurrentField.LocationOfFirstCharacter;
					return positionInTheDocument;
				}
			}
			return null;
		}

		public string[] SplitFieldsIntoRepeatFields(string text)
		{
			return base.Split(text, base.Message.FieldRepeatSeperator);
		}

		public string[] SplitSegmentIntoFields(string text)
		{
			return base.Split(text, base.Message.FieldSeperator);
		}

		public override string ToString()
		{
			string text = "";
			if (this.Fields == null)
			{
				text = base.Text;
			}
			else
			{
				foreach (Field field in this.Fields)
				{
					if (field.Text == base.Message.FieldSeperator)
					{
						continue;
					}
					if (!field.IsRepeatField)
					{
						text = (!field.HasChildren ? string.Concat(text, field.Text, base.Message.FieldSeperator) : string.Concat(text, field.ToString(), base.Message.FieldSeperator));
					}
					else
					{
						text = (!field.HasChildren ? string.Concat(text.Substring(0, text.Length - 1), base.Message.FieldRepeatSeperator, field.Text, base.Message.FieldSeperator) : string.Concat(text.Substring(0, text.Length - 1), base.Message.FieldRepeatSeperator, field.ToString(), base.Message.FieldSeperator));
					}
				}
			}
			if (text.EndsWith(base.Message.FieldSeperator) && text.Length > 4)
			{
				text = text.Substring(0, text.Length - 1);
			}
			return text;
		}
	}
}