using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup.Workflow.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
	public sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance;

		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		[UserScopedSetting]
		public int AlignmentGap
		{
			get
			{
				return (int)this["AlignmentGap"];
			}
			set
			{
				this["AlignmentGap"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("0")]
		[UserScopedSetting]
		public int AlignmentGap2
		{
			get
			{
				return (int)this["AlignmentGap2"];
			}
			set
			{
				this["AlignmentGap2"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("\\.br\\ ")]
		[UserScopedSetting]
		public string CarriageReturnEscapeCharacter
		{
			get
			{
				return (string)this["CarriageReturnEscapeCharacter"];
			}
			set
			{
				this["CarriageReturnEscapeCharacter"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("DefaultMessageHighlighters")]
		[UserScopedSetting]
		public string CurrentMessageHighlighters
		{
			get
			{
				return (string)this["CurrentMessageHighlighters"];
			}
			set
			{
				this["CurrentMessageHighlighters"] = value;
			}
		}

		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("8000")]
		[UserScopedSetting]
		public string DefaultEditorTCPSendTimeout
		{
			get
			{
				return (string)this["DefaultEditorTCPSendTimeout"];
			}
			set
			{
				this["DefaultEditorTCPSendTimeout"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("22222")]
		[UserScopedSetting]
		public int DefaultReceivePort
		{
			get
			{
				return (int)this["DefaultReceivePort"];
			}
			set
			{
				this["DefaultReceivePort"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("22222")]
		[UserScopedSetting]
		public int DefaultSendPort
		{
			get
			{
				return (int)this["DefaultSendPort"];
			}
			set
			{
				this["DefaultSendPort"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("20000")]
		public string DefaultTCPSendTimeout
		{
			get
			{
				return (string)this["DefaultTCPSendTimeout"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Default")]
		public string Encoding
		{
			get
			{
				return (string)this["Encoding"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool ForceXPath
		{
			get
			{
				return (bool)this["ForceXPath"];
			}
			set
			{
				this["ForceXPath"] = value;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		public bool HL7SenderAllowMultiMessageResponse
		{
			get
			{
				return (bool)this["HL7SenderAllowMultiMessageResponse"];
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("McDonald,MacDonald,MacDonnel,McDonnel,MacGregor,McGregor,LeBlanc,McIntosh,MacMillan,McMillan,MacCormac,MacCormac,MacCormack,McCormack,MacGille,McGille,MacLachlan,McLachlan,D'Urville,McLuhan,McLuthier,McMurry,MacMurray,McMahon,McLaren,MacFarland,McFarland,MacFarlane,McFarlane,MacGillivray,McGillivray,MacGyver,MacIsaac,McIsaac,MacKay,McKay,MacKenzie,McKenzie,McKessy,MacKinnon,McKinnon,Mcklin,MacLellan,McLellan,MacMurphy,MacMurphy,MacPherson,McPherson,MacDermott,McDermott,MacDermut,McDermut,McDonough,MacDougall,McDougall,MacDowall,McDowall,MacDowell,McDowell,MacDuff,McDuff,MacElhaney,McElhaneyMcEnroe,MacEntire,McEntire,MacEwan,McEwan,MacFadden,McFadden,MacFaul,McFaul,MacFee,McFee,MacGann,McGann,MacGavin,McGavin,MacGee,McGee,MacGhee,McGhee,MacGill,McGill,MacGillicuddy,McGillicuddy,MacGillivray,McGillivray,MacGinley,McGinley,MacGinnis,McGinnis,MacGowan,McGowan,MacGrady,McGrady,MacGrath,McGrath,MacGuffin,McGuffin,MacGuinness,McGuinness,MacGuire,McGuire,MacHenry,McHenry,MacHugh,McHugh,McIlwain,MacInerney,McInerney,MacInnis,McInnis,McIntire,MacIntyre,McIntyre,MacIver,McIver,MacIvor,McIvor,MacKeehan,McKeehan,MacKeever,McKeever,MacKelvey,McKelvey,MacKendrick,McKendrick,MacKenna,McKenna,MacKenney,McKenney,MacKernan,McKernan,MacKey,McKey,MacKiernan,McKiernan,MacKinley,McKinley,MacKinney,McKinney,MacKinnis,McKinnis,MacKnight,McKnight,MacLane,McLane,MacLaughlin,McLaughlin,MacLean,McLean,MacLeister,McLeister,MacLemore,McLemore,MacLendon,McLendon,MacLennan,McLennan,MacManus,McManus,MacMurphy,McMurphy,MacMurray,McMurray,aMcMurtray,MacNabb,McNabb,MacNair,McNair,MacNall,McNall,MacNally,McNally,MacNamara,McNamara,MacNeal,McNeal,MacNeil,McNeil,MacNeill,McNeill,MacNeish,McNeish,MacNevin,McNevin,MacNiel,McNiel,MacNulty,McNulty,MacNutt,McNutt,MacPheeters,McPheeters,MacPike,McPike,MacQueen,McQueen,MacRay,McRay,MacShea,McShea,aMcVey,MacWan,McWan,MacWhirter,McWhirter,MacWhorter,McWhorter,MacWilliams,McWilliams")]
		[UserScopedSetting]
		public string McNamesStartPatterns
		{
			get
			{
				return (string)this["McNamesStartPatterns"];
			}
			set
			{
				this["McNamesStartPatterns"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("13")]
		[UserScopedSetting]
		public double MessageFontSize
		{
			get
			{
				return (double)this["MessageFontSize"];
			}
			set
			{
				this["MessageFontSize"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("MessageTypeDescription")]
		[UserScopedSetting]
		public string MessageListDescriptionField
		{
			get
			{
				return (string)this["MessageListDescriptionField"];
			}
			set
			{
				this["MessageListDescriptionField"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("6")]
		[UserScopedSetting]
		public double MessageStoryFontSize
		{
			get
			{
				return (double)this["MessageStoryFontSize"];
			}
			set
			{
				this["MessageStoryFontSize"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool ShowFieldsWithEmptyValues
		{
			get
			{
				return (bool)this["ShowFieldsWithEmptyValues"];
			}
			set
			{
				this["ShowFieldsWithEmptyValues"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool TrimMessageBeforeSending
		{
			get
			{
				return (bool)this["TrimMessageBeforeSending"];
			}
			set
			{
				this["TrimMessageBeforeSending"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("True")]
		[UserScopedSetting]
		public bool ValidateMessages
		{
			get
			{
				return (bool)this["ValidateMessages"];
			}
			set
			{
				this["ValidateMessages"] = value;
			}
		}

		static Settings()
		{
			Settings.defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		}

		public Settings()
		{
		}
	}
}