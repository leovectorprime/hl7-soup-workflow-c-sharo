using System;

namespace HL7Soup
{
	public delegate void DialogValidatedEventHandler(object sender, Guid id);
}