using System;

namespace HL7Soup
{
	public delegate void RaiseErrorEventHandler(object sender, string text);
}