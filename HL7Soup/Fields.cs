using HL7Soup.Integrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HL7Soup
{
	public class Fields : Collection<IHL7Field>, IHL7Fields, ICollection<IHL7Field>, IEnumerable<IHL7Field>, IEnumerable
	{
		public Fields()
		{
		}

		public Fields(IList<IHL7Field> fields) : base(fields)
		{
		}

		public Fields(IEnumerable<IHL7Field> fields) : base(fields.ToList<IHL7Field>())
		{
		}

		public override string ToString()
		{
			return string.Join<IHL7Field>("I", base.Items);
		}
	}
}