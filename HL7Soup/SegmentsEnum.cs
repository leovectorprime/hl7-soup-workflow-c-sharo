using System;

namespace HL7Soup
{
	public enum SegmentsEnum
	{
		MSH,
		PID,
		MSA
	}
}