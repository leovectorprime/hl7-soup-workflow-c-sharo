using System;

namespace HL7Soup.Integrations
{
	public enum FunctionProviderStatuses
	{
		Constructed,
		Ready,
		Processing,
		Finalizing,
		RollingBack,
		Canceled,
		Closed,
		Errored
	}
}