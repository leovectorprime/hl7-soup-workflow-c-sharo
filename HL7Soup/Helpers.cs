using HL7Soup.Functions.Settings;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace HL7Soup
{
	public static class Helpers
	{
		private readonly static string[] allDateTimeFormats;

		private static Encoding encoding;

		private static bool encodingFound;

		private readonly static string[] allDateTimeNoSecondsFormats;

		private readonly static string[] allDateOnlyFormats;

		private static bool IsBusy;

		private static string[] mcNames;

		private static TextInfo textInfo;

		private static string[] McNames
		{
			get
			{
				if (Helpers.mcNames == null && Settings.Default.McNamesStartPatterns != null)
				{
					Helpers.mcNames = Settings.Default.McNamesStartPatterns.Split(new char[] { ',' });
				}
				return Helpers.mcNames;
			}
		}

		static Helpers()
		{
			Helpers.allDateTimeFormats = new string[] { "yyyyMMddHHmmss.ffffzzzz", "yyyyMMddHHmmss.fffzzzz", "yyyyMMddHHmmss.ffzzzz", "yyyyMMddHHmmss.fzzzz", "yyyyMMddHHmmss.ffff", "yyyyMMddHHmmss.fff", "yyyyMMddHHmmss.ff", "yyyyMMddHHmmss.f", "yyyyMMddTHHmmssZZzz", "yyyyMMddTHHmmsszzz", "yyyyMMddTHHmmsszz", "yyyyMMddTHHmmssZ", "yyyyMMddHHmmssZZzz", "yyyyMMddHHmmsszzz", "yyyyMMddHHmmsszz", "yyyyMMddHHmmssZ", "yyyy-MM-ddTHH:mm:sszzz", "yyyy-MM-ddTHH:mm:sszz", "yyyy-MM-ddTHH:mm:ssZ", "yyyyMMddHHmmZZzz", "yyyyMMddHHmmzzz", "yyyyMMddHHmmzz", "yyyyMMddHHmmZ", "yyyyMMddTHHmmZZzz", "yyyyMMddTHHmmzzz", "yyyyMMddTHHmmzz", "yyyyMMddTHHmmZ", "yyyy-MM-ddTHH:mmzzz", "yyyy-MM-ddTHH:mmzz", "yyyy-MM-ddTHH:mmZ", "yyyyMMddTHHzzz", "yyyyMMddTHHzz", "yyyyMMddTHHZ", "yyyy-MM-ddTHHzzz", "yyyy-MM-ddTHHzz", "yyyy-MM-ddTHHZ", "yyyyMMdd", "yyyyMM", "yyyy", "yyyy-MM-dd", "yyyy-MM", "yyyy", "yyyyMMddTHHmmss", "yyyyMMddTHHmm", "yyyyMMddTHH", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMMddHH" };
			Helpers.encoding = Encoding.Default;
			Helpers.encodingFound = false;
			Helpers.allDateTimeNoSecondsFormats = new string[] { "yyyyMMddHHmm", "yyyyMMddHH", "yyyyMMddTHHmm", "yyyyMMddTHH" };
			Helpers.allDateOnlyFormats = new string[] { "yyyyMMdd", "yyyyMM", "yyyy", "yyyy-MM-dd", "yyyy-MM", "yyyy" };
			Helpers.mcNames = null;
			Helpers.textInfo = (new CultureInfo("en-US", false)).TextInfo;
		}

		public static string Base64Decode(string base64EncodedData)
		{
			byte[] numArray = Convert.FromBase64String(base64EncodedData);
			return Encoding.UTF8.GetString(numArray);
		}

		public static string Base64Encode(string plainText)
		{
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
		}

		public static string CamelCaseToText(string camelCase)
		{
			List<char> chrs = new List<char>()
			{
				camelCase[0]
			};
			foreach (char chr in camelCase.Skip<char>(1))
			{
				if (!char.IsUpper(chr))
				{
					chrs.Add(chr);
				}
				else
				{
					chrs.Add(' ');
					chrs.Add(char.ToLower(chr));
				}
			}
			return new string(chrs.ToArray());
		}

		public static string CSVEncode(string str)
		{
			if ((str.Contains(",") || str.Contains("\"") || str.Contains("\r") ? false : !str.Contains("\n")))
			{
				return str;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("\"");
			string str1 = str;
			for (int i = 0; i < str1.Length; i++)
			{
				char chr = str1[i];
				stringBuilder.Append(chr);
				if (chr == '\"')
				{
					stringBuilder.Append("\"");
				}
			}
			stringBuilder.Append("\"");
			return stringBuilder.ToString();
		}

		public static object Deserialize(string filePath)
		{
			object obj;
			FileStream fileStream = new FileStream(filePath, FileMode.Open);
			try
			{
				try
				{
					obj = Helpers.Deserialize(fileStream);
				}
				catch (SerializationException serializationException)
				{
					Console.WriteLine(string.Concat("Failed to deserialize. Reason: ", serializationException.Message));
					throw;
				}
			}
			finally
			{
				fileStream.Close();
			}
			return obj;
		}

		public static object Deserialize(Stream stream)
		{
			return (new BinaryFormatter()).Deserialize(stream);
		}

		private static void dispatcherTimer_Tick(object sender, EventArgs e)
		{
			DispatcherTimer dispatcherTimer = sender as DispatcherTimer;
			if (dispatcherTimer != null)
			{
				Helpers.SetBusyState(false);
				dispatcherTimer.Stop();
			}
		}

		public static string ExecuteProcessAndGetTextString(string filePath, string arguments)
		{
			return Helpers.ExecuteProcessAndGetTextString(filePath, arguments, false);
		}

		public static string ExecuteProcessAndGetTextString(string filePath, string arguments, bool forceElevation)
		{
			int num = 0;
			return Helpers.ExecuteProcessAndGetTextString(filePath, arguments, forceElevation, out num);
		}

		public static string ExecuteProcessAndGetTextString(string filePath, string arguments, out int processExitCode)
		{
			return Helpers.ExecuteProcessAndGetTextString(filePath, arguments, false, out processExitCode);
		}

		public static string ExecuteProcessAndGetTextString(string filePath, string arguments, bool forceElevation, out int processExitCode)
		{
			Process process = new Process();
			if (!forceElevation)
			{
				process.StartInfo = new ProcessStartInfo()
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					RedirectStandardError = true,
					CreateNoWindow = true,
					FileName = filePath,
					Arguments = arguments
				};
				HL7Soup.Log.Instance.Debug(string.Concat("processes loaded from ", filePath));
				bool flag = false;
				(new Timer((object state) => {
					if (!flag)
					{
						return;
					}
					flag = false;
				})).Change(1000, 1000);
				process.Start();
				StringBuilder stringBuilder = new StringBuilder();
				while (!process.StandardOutput.EndOfStream)
				{
					string str = process.StandardOutput.ReadLine();
					flag = true;
					stringBuilder.AppendLine(str);
				}
				processExitCode = process.ExitCode;
				if (processExitCode == 0 || !stringBuilder.ToString().Contains("Run as Administrator") && !stringBuilder.ToString().Contains("requires elevation"))
				{
					return stringBuilder.ToString();
				}
			}
			process = new Process()
			{
				StartInfo = new ProcessStartInfo()
				{
					WindowStyle = ProcessWindowStyle.Hidden,
					UseShellExecute = true,
					Verb = "runas",
					FileName = filePath,
					Arguments = arguments
				}
			};
			HL7Soup.Log.Instance.Debug(string.Concat("processes loading as admin loaded from ", filePath));
			process.Start();
			process.WaitForExit();
			processExitCode = process.ExitCode;
			if (processExitCode > 0)
			{
				HL7Soup.Log.Instance.Error(string.Concat(new string[] { "Failed to execute the process ", filePath, " ", arguments, "\r\nPlease run it in an elevated command prompt to see the actual error." }));
			}
			return "";
		}

		public static T FindDescendant<T>(DependencyObject obj)
		where T : DependencyObject
		{
			T t;
			if (obj is T)
			{
				return (T)(obj as T);
			}
			if (obj == null)
			{
				t = default(T);
				return t;
			}
			int childrenCount = VisualTreeHelper.GetChildrenCount(obj);
			if (childrenCount < 1)
			{
				t = default(T);
				return t;
			}
			for (int i = 0; i < childrenCount; i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				if (child is T)
				{
					return (T)(child as T);
				}
			}
			for (int j = 0; j < childrenCount; j++)
			{
				DependencyObject dependencyObject = Helpers.FindDescendant<T>(VisualTreeHelper.GetChild(obj, j));
				if (dependencyObject != null && dependencyObject is T)
				{
					return (T)(dependencyObject as T);
				}
			}
			t = default(T);
			return t;
		}

		public static T FindParent<T>(DependencyObject child)
		where T : DependencyObject
		{
			DependencyObject parent = VisualTreeHelper.GetParent(child);
			if (parent == null)
			{
				return default(T);
			}
			T t = (T)(parent as T);
			if (t != null)
			{
				return t;
			}
			return Helpers.FindParent<T>(parent);
		}

		public static string FormatString(string value, string format)
		{
			double num;
			DateTime minValue = DateTime.MinValue;
			if (!DateTime.TryParse(value, out minValue))
			{
				minValue = Helpers.ParseISO8601String(value);
			}
			if (minValue > DateTime.MinValue)
			{
				return string.Format(string.Concat("{0:", format, "}"), minValue);
			}
			if (value == null || !double.TryParse(value.Trim(), out num))
			{
				return value;
			}
			return string.Format(string.Concat("{0:", format, "}"), num);
		}

		internal static string GetAge(string ISO8601)
		{
			DateTime dateTime = Helpers.ParseISO8601String(ISO8601);
			if (dateTime <= DateTime.MinValue)
			{
				return "";
			}
			DateTime today = DateTime.Today;
			int year = today.Year - dateTime.Year;
			if (dateTime > today.AddYears(-year))
			{
				year--;
			}
			return year.ToString();
		}

		public static string GetDateFormat(string date)
		{
			int i;
			if (Helpers.ParseISO8601StringToString(date, "") != "")
			{
				DateTime minValue = DateTime.MinValue;
				string[] strArrays = Helpers.allDateOnlyFormats;
				for (i = 0; i < (int)strArrays.Length; i++)
				{
					string str = strArrays[i];
					if (DateTime.TryParseExact(date, str, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
					{
						return str;
					}
				}
				strArrays = Helpers.allDateTimeNoSecondsFormats;
				for (i = 0; i < (int)strArrays.Length; i++)
				{
					string str1 = strArrays[i];
					if (DateTime.TryParseExact(date, str1, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
					{
						return str1;
					}
				}
				strArrays = Helpers.allDateTimeFormats;
				for (i = 0; i < (int)strArrays.Length; i++)
				{
					string str2 = strArrays[i];
					if (DateTime.TryParseExact(date, str2, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
					{
						return str2;
					}
				}
			}
			return "yyyyMMddTHHmmss";
		}

		internal static Encoding GetEncoding(bool isSending)
		{
			string upper = Settings.Default.Encoding.ToUpper();
			if (Helpers.encodingFound && upper != "DEFAULTUTF-8")
			{
				return Helpers.encoding;
			}
			if (upper == "DEFAULT")
			{
				if (Encoding.Default.BodyName.ToUpper() != "ISO-8859-1")
				{
					Helpers.encoding = Encoding.UTF8;
				}
				else
				{
					Helpers.encoding = Encoding.Default;
				}
			}
			else if (upper == "UTF-8" || upper == "UTF8")
			{
				Helpers.encoding = Encoding.UTF8;
			}
			else if (upper == "ASCII")
			{
				Helpers.encoding = Encoding.ASCII;
			}
			else if (upper == "DEFAULTUTF-8")
			{
				if (!isSending)
				{
					Helpers.encoding = Encoding.Default;
				}
				else
				{
					Helpers.encoding = Encoding.UTF8;
				}
			}
			else if (!string.IsNullOrWhiteSpace(upper))
			{
				Helpers.encoding = Encoding.GetEncoding(upper);
			}
			if (!Helpers.encodingFound)
			{
				Helpers.encodingFound = true;
				HL7Soup.Log.Instance.Debug(string.Concat("Encoding is ", Helpers.encoding.BodyName));
			}
			return Helpers.encoding;
		}

		public static string GetEnumDescription(Enum enumObj)
		{
			object[] customAttributes = enumObj.GetType().GetField(enumObj.ToString()).GetCustomAttributes(false);
			if (customAttributes.Length == 0)
			{
				return enumObj.ToString();
			}
			return (customAttributes[0] as DescriptionAttribute).Description;
		}

		public static string GetIso8601String(string datestring)
		{
			DateTime minValue = DateTime.MinValue;
			DateTime.TryParse(datestring, out minValue);
			if (minValue == DateTime.MinValue)
			{
				return "";
			}
			return Helpers.GetIso8601String(minValue);
		}

		public static string GetIso8601String(DateTime date, string dateFormat)
		{
			return string.Format(string.Concat("{0:", dateFormat, "}"), date);
		}

		public static string GetIso8601String(DateTime date)
		{
			if (date.Hour == 0 && date.Minute == 0 && date.Second == 0)
			{
				return string.Format("{0:yyyyMMdd}", date);
			}
			if (date.Second == 0)
			{
				return string.Format("{0:yyyyMMddHHmm}", date);
			}
			return string.Format("{0:yyyyMMddHHmmss}", date);
		}

		public static ISenderSetting GetSendConnectionSettingByName(string connectionName)
		{
			ISenderSetting senderSetting;
			if (SystemSettings.Instance.FunctionSettings != null)
			{
				using (IEnumerator<ISenderSetting> enumerator = SystemSettings.Instance.FunctionSettings.SenderSettings.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ISenderSetting current = enumerator.Current;
						if (current.Name != connectionName)
						{
							continue;
						}
						senderSetting = current;
						return senderSetting;
					}
					return null;
				}
				return senderSetting;
			}
			return null;
		}

		public static string GetTimeAgo(string ISO8601)
		{
			DateTime dateTime = Helpers.ParseISO8601String(ISO8601);
			if (dateTime <= DateTime.MinValue)
			{
				return "";
			}
			return Helpers.GetTimeAgo(dateTime);
		}

		public static string GetTimeAgo(DateTime input)
		{
			TimeSpan timeSpan = DateTime.Now.Subtract(input);
			double totalMinutes = timeSpan.TotalMinutes;
			string str = " ago";
			if (totalMinutes < 0)
			{
				totalMinutes = Math.Abs(totalMinutes);
				str = " from now";
			}
			KeyValuePair<double, Func<string>> keyValuePair = (new SortedList<double, Func<string>>()
			{
				{ 0.75, new Func<string>(() => "less than a minute") },
				{ 1.5, new Func<string>(() => "about a minute") },
				{ 45, new Func<string>(() => string.Format("{0} minutes", Math.Round(totalMinutes))) },
				{ 90, new Func<string>(() => "about an hour") },
				{ 1440, new Func<string>(() => string.Format("about {0} hours", Math.Round(Math.Abs(timeSpan.TotalHours)))) },
				{ 2880, new Func<string>(() => "a day") },
				{ 43200, new Func<string>(() => string.Format("{0} days", Math.Floor(Math.Abs(timeSpan.TotalDays)))) },
				{ 86400, new Func<string>(() => "about a month") },
				{ 525600, new Func<string>(() => string.Format("{0} months", Math.Floor(Math.Abs(timeSpan.TotalDays / 30)))) },
				{ 1051200, new Func<string>(() => "about a year") },
				{ double.MaxValue, new Func<string>(() => string.Format("{0} years", Math.Floor(Math.Abs(timeSpan.TotalDays / 365)))) }
			}).First<KeyValuePair<double, Func<string>>>((KeyValuePair<double, Func<string>> n) => totalMinutes < n.Key);
			return string.Concat(keyValuePair.Value(), str);
		}

		public static string GetTimeSpanWords(TimeSpan aTimeSpan)
		{
			int days;
			string str;
			string str1;
			string str2;
			string str3;
			string empty = string.Empty;
			if (aTimeSpan.Days > 0)
			{
				string str4 = empty;
				if (aTimeSpan.Days > 1 || !string.IsNullOrWhiteSpace(empty))
				{
					days = aTimeSpan.Days;
					str3 = days.ToString();
				}
				else
				{
					str3 = "";
				}
				empty = string.Concat(str4, str3, " day", (aTimeSpan.Days > 1 ? "s" : ""));
			}
			if (aTimeSpan.Hours > 0)
			{
				if (!string.IsNullOrEmpty(empty))
				{
					empty = string.Concat(empty, " ");
				}
				string str5 = empty;
				if (aTimeSpan.Hours > 1 || !string.IsNullOrWhiteSpace(empty))
				{
					days = aTimeSpan.Hours;
					str2 = days.ToString();
				}
				else
				{
					str2 = "";
				}
				empty = string.Concat(str5, str2, " hour", (aTimeSpan.Hours > 1 ? "s" : ""));
			}
			if (aTimeSpan.Minutes > 0)
			{
				if (!string.IsNullOrEmpty(empty))
				{
					empty = string.Concat(empty, " ");
				}
				string str6 = empty;
				if (aTimeSpan.Minutes > 1 || !string.IsNullOrWhiteSpace(empty))
				{
					days = aTimeSpan.Minutes;
					str1 = days.ToString();
				}
				else
				{
					str1 = "";
				}
				empty = string.Concat(str6, str1, " minute", (aTimeSpan.Minutes > 1 ? "s" : ""));
			}
			if (aTimeSpan.Seconds > 0)
			{
				if (!string.IsNullOrEmpty(empty))
				{
					empty = string.Concat(empty, " ");
				}
				string str7 = empty;
				if (aTimeSpan.Seconds > 1 || !string.IsNullOrWhiteSpace(empty))
				{
					days = aTimeSpan.Seconds;
					str = days.ToString();
				}
				else
				{
					str = "";
				}
				empty = string.Concat(str7, str, " second", (aTimeSpan.Seconds > 1 ? "s" : ""));
			}
			if (string.IsNullOrEmpty(empty))
			{
				empty = "0 seconds.";
			}
			return empty.Trim();
		}

		public static string HL7Decode(string value)
		{
			return value.Replace("\\F\\", "|").Replace("\\S\\", "^").Replace("\\R\\", "~").Replace("\\T\\", "&").Replace("\\E\\", "\\");
		}

		public static string HL7Encode(string value)
		{
			return value.Replace("\\", "\\E\\").Replace("&", "\\T\\").Replace("~", "\\R\\").Replace("^", "\\S\\").Replace("|", "\\F\\").Replace("\r\n", Settings.Default.CarriageReturnEscapeCharacter);
		}

		public static bool Invert(this bool val, bool isNot)
		{
			if (!isNot)
			{
				return val;
			}
			return !val;
		}

		internal static bool IsLowerCase(string value)
		{
			return value.ToLowerInvariant() == value;
		}

		internal static bool IsTitleCase(string value)
		{
			return Helpers.textInfo.ToTitleCase(value.ToLower()) == value;
		}

		internal static bool IsUpperCase(string value)
		{
			return value.ToUpperInvariant() == value;
		}

		public static string McNameCase(string value)
		{
			string[] strArrays = Helpers.TitleCase(value).Split(new char[] { ' ' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string[] mcNames = Helpers.McNames;
				for (int j = 0; j < (int)mcNames.Length; j++)
				{
					string str = mcNames[j];
					if (strArrays[i].StartsWith(str, StringComparison.InvariantCultureIgnoreCase))
					{
						strArrays[i] = string.Concat(str, strArrays[i].Substring(str.Length));
					}
				}
			}
			return string.Join(" ", strArrays);
		}

		public static DateTime ParseISO8601String(string str)
		{
			DateTime minValue = DateTime.MinValue;
			if (DateTime.TryParseExact(str, Helpers.allDateTimeFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
			{
				return minValue;
			}
			return DateTime.MinValue;
		}

		public static string ParseISO8601StringToString(string str)
		{
			return Helpers.ParseISO8601StringToString(str, "Invalid Date");
		}

		public static string ParseISO8601StringToString(string str, string invalidText)
		{
			DateTime minValue = DateTime.MinValue;
			if (DateTime.TryParseExact(str, Helpers.allDateOnlyFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
			{
				return string.Format("{0:d}", minValue);
			}
			if (DateTime.TryParseExact(str, Helpers.allDateTimeNoSecondsFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
			{
				return string.Format("{0:g}", minValue);
			}
			if (!DateTime.TryParseExact(str, Helpers.allDateTimeFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out minValue))
			{
				return invalidText;
			}
			return string.Format("{0:G}", minValue);
		}

		public static string ReplaceInvalidFileNameChars(this string s, string replacement = "")
		{
			return Regex.Replace(s, string.Concat("[", Regex.Escape(new string(Path.GetInvalidFileNameChars())), "]"), replacement, RegexOptions.IgnoreCase);
		}

		public static string ReplaceInvalidFilePathChars(this string s, string replacement = "")
		{
			return Regex.Replace(s, string.Concat("[", Regex.Escape(new string(Path.GetInvalidPathChars())), "]"), replacement, RegexOptions.IgnoreCase);
		}

		public static void Serialize(string filePath, object obj)
		{
			FileStream fileStream = new FileStream(filePath, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			try
			{
				try
				{
					binaryFormatter.Serialize(fileStream, obj);
				}
				catch (SerializationException serializationException)
				{
					Console.WriteLine(string.Concat("Failed to serialize. Reason: ", serializationException.Message));
					throw;
				}
			}
			finally
			{
				fileStream.Close();
			}
		}

		public static void SetBusyState()
		{
			Helpers.SetBusyState(true);
		}

		private static void SetBusyState(bool busy)
		{
			if (busy != Helpers.IsBusy)
			{
				Helpers.IsBusy = busy;
				if (Application.Current != null)
				{
					Application.Current.Dispatcher.Invoke(() => Mouse.OverrideCursor = (busy ? Cursors.Wait : null));
					if (Helpers.IsBusy)
					{
						DispatcherTimer dispatcherTimer = new DispatcherTimer(TimeSpan.FromSeconds(0), DispatcherPriority.ApplicationIdle, new EventHandler(Helpers.dispatcherTimer_Tick), Application.Current.Dispatcher);
					}
				}
			}
		}

		public static string TitleCase(string strParam)
		{
			if (string.IsNullOrEmpty(strParam))
			{
				return strParam;
			}
			string upper = strParam.Substring(0, 1).ToUpper();
			strParam = strParam.Substring(1).ToLower();
			string str = "";
			for (int i = 0; i < strParam.Length; i++)
			{
				if (i > 1)
				{
					str = strParam.Substring(i - 1, 1);
				}
				upper = (str.Equals(" ") || str.Equals("\t") || str.Equals("\n") || str.Equals(".") ? string.Concat(upper, strParam.Substring(i, 1).ToUpper()) : string.Concat(upper, strParam.Substring(i, 1)));
			}
			return upper;
		}

		public static System.Windows.Media.Color ToMediaColor(this System.Drawing.Color color)
		{
			return System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
		}

		public static string TruncateString(string text, int maxLength)
		{
			return string.Concat(text.Substring(0, Math.Min(maxLength, text.Length)), (text.Length > maxLength ? "..." : ""));
		}

		public class CustomBinder : SerializationBinder
		{
			public static string typeNamex;

			static CustomBinder()
			{
				Helpers.CustomBinder.typeNamex = "";
			}

			public CustomBinder()
			{
			}

			public override Type BindToType(string assemblyName, string typeName)
			{
				Helpers.CustomBinder.typeNamex = string.Concat(new string[] { Helpers.CustomBinder.typeNamex, typeName, " assembly = ", assemblyName, Environment.NewLine });
				return Type.GetType(string.Concat(typeName, ", ", assemblyName));
			}
		}
	}
}