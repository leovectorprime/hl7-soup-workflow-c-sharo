using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class WorkflowInstance : IDisposable, IWorkflowInstance
	{
		private DateTime receivedDate = DateTime.Now;

		public List<IActivityInstance> Activities
		{
			get;
			set;
		}

		public IActivityInstance CurrentActivityInstance
		{
			get
			{
				if (this.ForceCurrentActivityToTheReceiver)
				{
					return this.Activities[0];
				}
				return this.Activities[this.Activities.Count - 1];
			}
		}

		public string ErrorMessage
		{
			get;
			private set;
		}

		public bool Filtered
		{
			get;
			set;
		}

		public bool ForceCurrentActivityToTheReceiver
		{
			get;
			set;
		}

		public bool HasErrored
		{
			get;
			private set;
		}

		public Guid Id
		{
			get
			{
				return JustDecompileGenerated_get_Id();
			}
			set
			{
				JustDecompileGenerated_set_Id(value);
			}
		}

		private Guid JustDecompileGenerated_Id_k__BackingField;

		public Guid JustDecompileGenerated_get_Id()
		{
			return this.JustDecompileGenerated_Id_k__BackingField;
		}

		internal void JustDecompileGenerated_set_Id(Guid value)
		{
			this.JustDecompileGenerated_Id_k__BackingField = value;
		}

		public int InstanceId
		{
			get;
			set;
		}

		public IMessage Message
		{
			get;
			private set;
		}

		public IActivityInstance ReceivingActivityInstance
		{
			get
			{
				return this.Activities[0];
			}
		}

		public IMessage ResponseMessage
		{
			get;
			private set;
		}

		private Dictionary<string, string> Variables
		{
			get;
			set;
		}

		public WorkflowInstance(Guid rootFunctionId, int instanceID)
		{
			this.Id = rootFunctionId;
			this.InstanceId = instanceID;
			this.Activities = new List<IActivityInstance>();
			this.Variables = new Dictionary<string, string>();
		}

		public void AddActivity(IMessage message, Guid functionId)
		{
			ActivityInstance activityInstance = new ActivityInstance(message, functionId);
			this.Activities.Add(activityInstance);
			this.Message = message;
		}

		public void AddActivity(Guid sourceFunction, Guid functionId)
		{
			IActivityInstance activityInstance = null;
			foreach (IActivityInstance activity in this.Activities)
			{
				if (activity.Id != sourceFunction)
				{
					continue;
				}
				activityInstance = activity;
			}
			if (activityInstance == null)
			{
				throw new Exception(string.Format("Cannot create a new activity that gets it's source message from the activity {0} as this activity hasn't executed", sourceFunction));
			}
			activityInstance = new ActivityInstance(activityInstance.Message, functionId);
			this.Activities.Add(activityInstance);
			this.Message = activityInstance.Message;
		}

		public bool CheckActivitiesAfterReceiverFiltered()
		{
			if (this.Activities.Count < 2)
			{
				return false;
			}
			for (int i = 1; i < this.Activities.Count; i++)
			{
				if (!this.Activities[i].Filtered)
				{
					return false;
				}
			}
			this.Filtered = true;
			return this.Filtered;
		}

		public void Dispose()
		{
			if (this.Message != null)
			{
				this.Message.Dispose();
			}
			if (this.ResponseMessage != null)
			{
				this.ResponseMessage.Dispose();
			}
			foreach (IActivityInstance activity in this.Activities)
			{
				if (activity.Message != null)
				{
					activity.Message.Dispose();
				}
				if (activity.ResponseMessage == null)
				{
					continue;
				}
				activity.ResponseMessage.Dispose();
			}
		}

		public void Errored(string errorMessage)
		{
			this.HasErrored = true;
			this.ErrorMessage = errorMessage;
		}

		public IActivityInstance GetActivityInstance(Guid settingId)
		{
			return this.Activities.FirstOrDefault<IActivityInstance>((IActivityInstance p) => p.Id == settingId);
		}

		internal List<HL7Soup.MessageLog.Variable> GetLoggerVariables()
		{
			List<HL7Soup.MessageLog.Variable> variables = new List<HL7Soup.MessageLog.Variable>();
			if (this.Variables != null)
			{
				foreach (KeyValuePair<string, string> variable in this.Variables)
				{
					variables.Add(new HL7Soup.MessageLog.Variable()
					{
						Name = variable.Key,
						Value = variable.Value
					});
				}
			}
			return variables;
		}

		internal IMessage GetMessage(Guid settingId, MessageSourceDirection direction)
		{
			IActivityInstance activityInstance = this.GetActivityInstance(settingId);
			if (activityInstance != null)
			{
				ISetting settingById = FunctionHelpers.GetSettingById(settingId);
				if (settingById is IReceiverSetting)
				{
					if (direction == MessageSourceDirection.inbound)
					{
						return activityInstance.Message;
					}
					return activityInstance.ResponseMessage;
				}
				if (settingById is ISenderSetting)
				{
					if (direction == MessageSourceDirection.inbound)
					{
						return activityInstance.ResponseMessage;
					}
					return activityInstance.Message;
				}
			}
			return null;
		}

		public string GetVariable(string variableName)
		{
			HL7Soup.Functions.Variable variable = HL7Soup.Functions.Variable.GetVariable(variableName);
			variable.Name = variable.Name.ToUpper();
			if (!variable.IsActivityBinding)
			{
				if (variable.Name == "CURRENTDATETIME")
				{
					return variable.ProcessValue(DateTime.Now, "yyyyMMddHHmmss");
				}
				if (variable.Name == "RECEIVEDDATE")
				{
					return variable.ProcessValue(this.receivedDate, "yyyyMMddHHmmss");
				}
				if (variable.Name == "WORKFLOWERROR")
				{
					return variable.ProcessValue(this.HasErrored.ToString());
				}
				if (variable.Name == "WORKFLOWINSTANCEID")
				{
					return variable.ProcessValue(this.InstanceId);
				}
				if (variable.Name == "MLLPSTART")
				{
					return new string(new char[] { '\v' });
				}
				if (variable.Name == "MLLPEND")
				{
					return new string(new char[] { '\u001C', '\r' });
				}
				if (variable.Name == "CR")
				{
					return new string(new char[] { '\r' });
				}
				if (this.Variables.ContainsKey(variable.Name))
				{
					return variable.ProcessValue(this.Variables[variable.Name]);
				}
			}
			else
			{
				Guid empty = Guid.Empty;
				if (Guid.TryParse(variableName.Substring(0, Guid.Empty.ToString().Length), out empty))
				{
					Guid guid = Guid.Empty;
					MessageSourceDirection messageSourceDirection = MessageSourceDirection.variable;
					Enum.TryParse<MessageSourceDirection>(variableName.Substring(guid.ToString().Length).Trim(), true, out messageSourceDirection);
					IMessage message = this.GetMessage(empty, messageSourceDirection);
					if (message != null)
					{
						return HL7Soup.Functions.Variable.EncodeValue(message.Text, variable);
					}
				}
			}
			return null;
		}

		public string ProcessVariables(string value)
		{
			foreach (string listOfVariablesInText in FunctionHelpers.GetListOfVariablesInText(value))
			{
				string variable = this.GetVariable(listOfVariablesInText);
				if (variable == null)
				{
					continue;
				}
				value = value.Replace(string.Concat("${", listOfVariablesInText, "}"), variable);
			}
			return value;
		}

		public void SetReponseMessage(IMessage responseMessage)
		{
			((ActivityInstance)this.CurrentActivityInstance).ResponseMessage = responseMessage;
			this.ResponseMessage = responseMessage;
		}

		public void SetVariable(string variableName, string value)
		{
			string upper = variableName.ToUpper();
			if (upper != "WORKFLOWERROR")
			{
				this.Variables[upper] = value;
				return;
			}
			bool flag = bool.Parse(value);
			if (flag)
			{
				MessageLogger.Instance.AddMessage(new MessageEvent("WorkflowError variable set to true by workflow logic.", this, this.CurrentActivityInstance.Id, LogItemStatus.Errored, LogItemDataType.Error));
			}
			this.HasErrored = flag;
		}
	}
}