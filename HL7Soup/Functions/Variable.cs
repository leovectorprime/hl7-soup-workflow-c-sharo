using HL7Soup;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security;

namespace HL7Soup.Functions
{
	public class Variable : IValueCanBeFormatted
	{
		public Encodings Encoding
		{
			get;
			set;
		}

		public int End
		{
			get;
			set;
		}

		public string Format
		{
			get;
			set;
		}

		public bool hasFormat
		{
			get
			{
				return !string.IsNullOrEmpty(this.Format);
			}
		}

		public bool IsActivityBinding
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int Start
		{
			get;
			set;
		}

		public TextFormats TextFormat
		{
			get;
			set;
		}

		public HL7Soup.Functions.Truncation Truncation
		{
			get;
			set;
		}

		public int TruncationLength { get; set; } = 50;

		public Variable()
		{
		}

		public static string EncodeValue(string value, IValueCanBeFormatted valueCanBeFormatted)
		{
			if (valueCanBeFormatted.Encoding != Encodings.None)
			{
				switch (valueCanBeFormatted.Encoding)
				{
					case Encodings.HL7Encode:
					{
						return Helpers.HL7Encode(value);
					}
					case Encodings.XMLEncode:
					{
						return SecurityElement.Escape(value);
					}
					case Encodings.CSVEncode:
					{
						return Helpers.CSVEncode(value);
					}
					case Encodings.JSONEncode:
					{
						return JsonConvert.ToString(value);
					}
					case Encodings.Base64Encode:
					{
						return Helpers.Base64Encode(value);
					}
					case Encodings.HL7Decode:
					{
						return Helpers.HL7Decode(value);
					}
					case Encodings.XMLDecode:
					{
						return WebUtility.HtmlDecode(value);
					}
					case Encodings.Base64Decode:
					{
						return Helpers.Base64Decode(value);
					}
					case Encodings.URLEncode:
					{
						return Uri.EscapeUriString(value);
					}
					case Encodings.URLDecode:
					{
						return Uri.UnescapeDataString(value);
					}
				}
			}
			return value;
		}

		public static string FormatTextValue(string value, IValueCanBeFormatted valueCanBeFormatted)
		{
			if (valueCanBeFormatted.TextFormat != TextFormats.None)
			{
				switch (valueCanBeFormatted.TextFormat)
				{
					case TextFormats.LowerCase:
					{
						value = value.ToLower();
						break;
					}
					case TextFormats.UpperCase:
					{
						value = value.ToUpper();
						break;
					}
					case TextFormats.TitleCase:
					{
						value = Helpers.TitleCase(value);
						break;
					}
					case TextFormats.McNameCase:
					{
						value = Helpers.McNameCase(value);
						break;
					}
				}
			}
			if (valueCanBeFormatted.Truncation != HL7Soup.Functions.Truncation.None)
			{
				switch (valueCanBeFormatted.Truncation)
				{
					case HL7Soup.Functions.Truncation.Truncate:
					{
						value = (value.Length > valueCanBeFormatted.TruncationLength ? value.Substring(0, valueCanBeFormatted.TruncationLength) : value);
						break;
					}
					case HL7Soup.Functions.Truncation.TruncateWithDotDotDot:
					{
						value = Helpers.TruncateString(value, Math.Max(0, valueCanBeFormatted.TruncationLength - 3));
						break;
					}
					case HL7Soup.Functions.Truncation.Trim:
					{
						value = value.Trim();
						break;
					}
				}
			}
			return value;
		}

		public static Variable GetVariable(string text)
		{
			return Variable.GetVariable(new Variable(), text);
		}

		public static Variable GetVariable(Variable variable, string text)
		{
			string[] strArrays = text.Split(new char[] { ':' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if (i == 0)
				{
					variable.Name = str;
					if (str.Length >= 36)
					{
						Guid empty = Guid.Empty;
						Guid.TryParse(str.Substring(0, 36), out empty);
						if (empty != Guid.Empty)
						{
							variable.IsActivityBinding = true;
						}
					}
				}
				else if (!str.StartsWith("Truncate", StringComparison.InvariantCultureIgnoreCase) || !str.EndsWith(")"))
				{
					if (str != null)
					{
						switch (str)
						{
							case "":
							{
								goto Label0;
							}
							case "HL7Encode":
							{
								variable.Encoding = Encodings.HL7Encode;
								goto Label0;
							}
							case "XMLEncode":
							{
								variable.Encoding = Encodings.XMLEncode;
								goto Label0;
							}
							case "CSVEncode":
							{
								variable.Encoding = Encodings.CSVEncode;
								goto Label0;
							}
							case "JSONEncode":
							{
								variable.Encoding = Encodings.JSONEncode;
								goto Label0;
							}
							case "HL7Decode":
							{
								variable.Encoding = Encodings.HL7Decode;
								goto Label0;
							}
							case "XMLDecode":
							{
								variable.Encoding = Encodings.XMLDecode;
								goto Label0;
							}
							case "Base64Encode":
							{
								variable.Encoding = Encodings.Base64Encode;
								goto Label0;
							}
							case "Base64Decode":
							{
								variable.Encoding = Encodings.Base64Decode;
								goto Label0;
							}
							case "URLEncode":
							{
								variable.Encoding = Encodings.URLEncode;
								goto Label0;
							}
							case "URLDecode":
							{
								variable.Encoding = Encodings.URLDecode;
								goto Label0;
							}
							case "LowerCase":
							{
								variable.TextFormat = TextFormats.LowerCase;
								goto Label0;
							}
							case "UpperCase":
							{
								variable.TextFormat = TextFormats.UpperCase;
								goto Label0;
							}
							case "TitleCase":
							{
								variable.TextFormat = TextFormats.TitleCase;
								goto Label0;
							}
							case "McNameCase":
							{
								variable.TextFormat = TextFormats.McNameCase;
								goto Label0;
							}
							default:
							{
								if (str != "Trim")
								{
									goto Label1;
								}
								variable.Truncation = HL7Soup.Functions.Truncation.Trim;
								goto Label0;
							}
						}
					}
				Label1:
					if (!string.IsNullOrEmpty(variable.Format))
					{
						Variable variable1 = variable;
						variable1.Format = string.Concat(variable1.Format, ":", str);
					}
					else
					{
						variable.Format = str;
					}
				}
				else if (str.StartsWith("TruncateWithDotDotDot(", StringComparison.InvariantCultureIgnoreCase))
				{
					int num = 0;
					int.TryParse(str.Substring(22, str.Length - 23), out num);
					if (num > 0)
					{
						variable.TruncationLength = num;
						variable.Truncation = HL7Soup.Functions.Truncation.TruncateWithDotDotDot;
					}
				}
				else if (str.StartsWith("Truncate(", StringComparison.InvariantCultureIgnoreCase))
				{
					int num1 = 0;
					int.TryParse(str.Substring(9, str.Length - 10), out num1);
					if (num1 > 0)
					{
						variable.TruncationLength = num1;
						variable.Truncation = HL7Soup.Functions.Truncation.Truncate;
					}
				}
			Label0:
			}
			return variable;
		}

		public static string ProcessFormat(bool value, IValueCanBeFormatted valueCanBeFormatted)
		{
			string str = value.ToString();
			if (valueCanBeFormatted.hasFormat)
			{
				str = string.Format(string.Concat("{0:", valueCanBeFormatted.Format, "}"), value);
			}
			str = Variable.FormatTextValue(str, valueCanBeFormatted);
			str = Variable.EncodeValue(str, valueCanBeFormatted);
			return str;
		}

		public static string ProcessFormat(DateTime value, string defaultFormat, IValueCanBeFormatted valueCanBeFormatted)
		{
			if (!valueCanBeFormatted.hasFormat)
			{
				valueCanBeFormatted.Format = defaultFormat;
			}
			return Variable.EncodeValue(string.Format(string.Concat("{0:", valueCanBeFormatted.Format, "}"), value), valueCanBeFormatted);
		}

		public static string ProcessFormat(int value, IValueCanBeFormatted valueCanBeFormatted)
		{
			string str = value.ToString();
			if (valueCanBeFormatted.hasFormat)
			{
				str = string.Format(string.Concat("{0:", valueCanBeFormatted.Format, "}"), value);
			}
			str = Variable.EncodeValue(str, valueCanBeFormatted);
			return str;
		}

		public static string ProcessFormat(decimal value, IValueCanBeFormatted valueCanBeFormatted)
		{
			string str = value.ToString();
			if (valueCanBeFormatted.hasFormat)
			{
				str = string.Format(string.Concat("{0:", valueCanBeFormatted.Format, "}"), value);
			}
			str = Variable.EncodeValue(str, valueCanBeFormatted);
			return str;
		}

		public static string ProcessFormat(string value, IValueCanBeFormatted valueCanBeFormatted)
		{
			string str = value;
			if (valueCanBeFormatted.hasFormat)
			{
				str = Helpers.FormatString(value, valueCanBeFormatted.Format);
			}
			str = Variable.FormatTextValue(str, valueCanBeFormatted);
			str = Variable.EncodeValue(str, valueCanBeFormatted);
			return str;
		}

		internal string ProcessValue(bool value)
		{
			return Variable.ProcessFormat(value, this);
		}

		internal string ProcessValue(DateTime value, string defaultFormat)
		{
			return Variable.ProcessFormat(value, defaultFormat, this);
		}

		internal string ProcessValue(int value)
		{
			return Variable.ProcessFormat(value, this);
		}

		internal string ProcessValue(decimal value)
		{
			return Variable.ProcessFormat(value, this);
		}

		internal string ProcessValue(string value)
		{
			return Variable.ProcessFormat(value, this);
		}

		public override string ToString()
		{
			string[] name = new string[] { "${", this.Name, null, null, null, null, null };
			name[2] = (this.Encoding != Encodings.None ? string.Concat(":", Helpers.GetEnumDescription(this.Encoding)) : "");
			name[3] = (this.TextFormat != TextFormats.None ? string.Concat(":", Helpers.GetEnumDescription(this.TextFormat)) : "");
			name[4] = (this.Truncation != HL7Soup.Functions.Truncation.None ? string.Concat(":", Helpers.GetEnumDescription(this.Truncation), (this.Truncation == HL7Soup.Functions.Truncation.Trim ? "" : string.Concat("(", this.TruncationLength, ")"))) : "");
			name[5] = (!string.IsNullOrEmpty(this.Format) ? string.Concat(":", this.Format) : "");
			name[6] = "}";
			return string.Concat(name);
		}
	}
}