using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;

namespace HL7Soup.Functions
{
	[Serializable]
	public class DynamicMessageType : FunctionMessage
	{
		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.Unknown;
			}
		}

		public override string Text
		{
			get
			{
				return this.ToString();
			}
		}

		public DynamicMessageType() : base("")
		{
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return this;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return this;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return this;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return this;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return this;
		}

		public override string GetValueAtPath(string path)
		{
			throw new NotImplementedException();
		}

		public override void SetStructureAtPath(string toPath, string fromValue)
		{
			throw new NotImplementedException();
		}

		protected override void SetText(string text)
		{
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			throw new NotImplementedException();
		}
	}
}