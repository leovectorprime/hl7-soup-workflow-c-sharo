using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions
{
	public abstract class FunctionMessage : IMessage, IDisposable
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		protected string _text;

		public abstract bool IsWellFormed
		{
			get;
		}

		public abstract MessageTypes MessageType
		{
			get;
		}

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public virtual string Text
		{
			get
			{
				return this._text;
			}
		}

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public HL7Soup.WorkflowInstance WorkflowInstance
		{
			get;
			set;
		}

		public FunctionMessage(string text)
		{
			this._text = text;
		}

		public virtual void Dispose()
		{
		}

		public abstract IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting);

		public abstract IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting);

		public abstract IMessage GenerateErrorMessage(string errorMessage);

		public abstract IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting);

		public abstract IMessage GenerateRejectMessage(string rejectMessage);

		public abstract string GetValueAtPath(string path);

		public void ProcessVariables(IWorkflowInstance workflowInstance)
		{
			this.SetText(((HL7Soup.WorkflowInstance)workflowInstance).ProcessVariables(this.Text));
		}

		public abstract void SetStructureAtPath(string toPath, string fromValue);

		protected abstract void SetText(string text);

		public abstract void SetValueAtPath(string toPath, string fromValue);
	}
}