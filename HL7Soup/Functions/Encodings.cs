using System;

namespace HL7Soup.Functions
{
	public enum Encodings
	{
		None,
		HL7Encode,
		XMLEncode,
		CSVEncode,
		JSONEncode,
		Base64Encode,
		HL7Decode,
		XMLDecode,
		Base64Decode,
		URLEncode,
		URLDecode
	}
}