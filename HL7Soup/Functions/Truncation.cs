using System;

namespace HL7Soup.Functions
{
	public enum Truncation
	{
		None,
		Truncate,
		TruncateWithDotDotDot,
		Trim
	}
}