using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using NLog;
using System;

namespace HL7Soup.Functions
{
	public class TextMessage : FunctionMessage, IDisposable
	{
		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.TextWithVariables;
			}
		}

		public override string Text
		{
			get
			{
				return this._text;
			}
		}

		public TextMessage(string text) : base(text)
		{
			this.SetText(text);
		}

		internal void AppendLine(string fromValue)
		{
			if (string.IsNullOrEmpty(fromValue))
			{
				return;
			}
			if (fromValue == Environment.NewLine)
			{
				this._text = string.Concat(this._text, fromValue);
				return;
			}
			if (string.IsNullOrEmpty(this.Text))
			{
				this._text = fromValue;
				return;
			}
			this._text = string.Concat(this._text, Environment.NewLine, fromValue);
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override string GetValueAtPath(string path)
		{
			return this._text;
		}

		public override void SetStructureAtPath(string toPath, string fromValue)
		{
			this.SetValueAtPath(toPath, fromValue);
		}

		protected override void SetText(string text)
		{
			this._text = text;
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			try
			{
				this._text = fromValue;
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
		}
	}
}