using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions
{
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(HL7MessageDebuggingProxy))]
	public class HL7V2MessageType : FunctionMessage, IDisposable, IHL7Message, IHL7Part, IMessage
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private HL7Soup.Message _hl7Message;

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Segment _msh;

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Segment _ackmsh;

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string _messageId;

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Segment ACKMSH
		{
			get
			{
				if (this._ackmsh == null)
				{
					HL7Soup.Message message = new HL7Soup.Message(this.Message.ToString(), false);
					this._ackmsh = message.GetPart(new PathSplitter(SegmentsEnum.MSH)) as Segment;
				}
				return this._ackmsh;
			}
		}

		private string DebuggerDisplay
		{
			get
			{
				this.Message.Load();
				return string.Concat(new string[] { this.MessageId, ", ", this.GetPart("MSH-9").Text, " - ", this.Message.MessageTypeDescription });
			}
		}

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public override bool IsWellFormed
		{
			get
			{
				return this.MSH != null;
			}
		}

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private HL7Soup.Message Message
		{
			get
			{
				if (this._hl7Message == null)
				{
					this._hl7Message = new HL7Soup.Message(this._text, false);
				}
				return this._hl7Message;
			}
		}

		public string MessageId
		{
			get
			{
				if (this._messageId == null)
				{
					this._messageId = this.Message.GetPartText(new PathSplitter("MSH", 10));
				}
				return this._messageId;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.HL7V2;
			}
		}

		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Segment MSH
		{
			get
			{
				if (this._msh == null)
				{
					this._msh = this.Message.GetPart(new PathSplitter(SegmentsEnum.MSH)) as Segment;
				}
				return this._msh;
			}
		}

		public override string Text
		{
			get
			{
				return this.Message.Text;
			}
		}

		public HL7V2MessageType(string text) : base(text)
		{
		}

		public void AddSegment(IHL7Segment segment)
		{
			this.AddSegment(segment.Text);
		}

		public void AddSegment(string segment)
		{
			if (string.IsNullOrWhiteSpace(segment))
			{
				return;
			}
			StringBuilder stringBuilder = new StringBuilder(this.Message.Text.Trim().TrimEnd(new char[] { '\r', '\n' }));
			stringBuilder.Append("\r");
			stringBuilder.Append(segment);
			this.Message.SetTextQuietly(stringBuilder.ToString());
			this.Reload();
		}

		public void BeginUpdate()
		{
			this.Message.BeginUpdate();
		}

		public override void Dispose()
		{
			base.Dispose();
			if (this._hl7Message != null)
			{
				this._hl7Message.Dispose();
			}
		}

		public void EndUpdate()
		{
			this.Message.EndUpdate();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return new HL7V2MessageType(string.Concat(this.ProcessMSHForACK(this.ACKMSH).ToString(), "\rMSA|AA|", this.MessageId));
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			Segment segment = this.ProcessMSHForACK(this.ACKMSH);
			return new HL7V2MessageType(string.Concat(new string[] { segment.ToString(), "\rMSA|AE|", this.MessageId, "|", errorMessage }));
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			if (this.IsWellFormed)
			{
				return this.GenerateErrorMessage(setting.ErrorMessage);
			}
			return new HL7V2MessageType(string.Concat("MSH|^~\\&|||||", string.Format("{0:yyyyMMddHHmmss}", DateTime.Now), "||ACK^000|||2.1\rMSA|AE|MSGID12349876\rERR| Could not find MSH segment in message"));
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return this.GenerateRejectMessage(setting.RejectMessage);
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			Segment segment = this.ProcessMSHForACK(this.ACKMSH);
			return new HL7V2MessageType(string.Concat(new string[] { segment.ToString(), "\rMSA|AR|", this.MessageId, "|", rejectMessage }));
		}

		public IHL7Part GetPart(string path)
		{
			return this.Message.GetPart(new PathSplitter(path)) as IHL7Part;
		}

		public IHL7Segment GetSegment(string locationCode)
		{
			return this.Message.GetPart(new PathSplitter(locationCode)) as IHL7Segment;
		}

		public IHL7Segments GetSegments()
		{
			this.Message.Load();
			return new Segments(this.Message.Segments.Values);
		}

		public IHL7Segments GetSegments(string header)
		{
			if (header == null)
			{
				return this.GetSegments();
			}
			if (header.Length != 3)
			{
				throw new ArgumentException("The header must be three letters long. e.g. OBX.");
			}
			this.Message.Load();
			return new Segments(
				from segment in this.Message.Segments.Values
				where segment.Header == header
				select segment);
		}

		public override string GetValueAtPath(string path)
		{
			PathSplitter pathSplitter = new PathSplitter(path, true);
			return this.Message.GetPartText(pathSplitter);
		}

		void HL7Soup.Integrations.IHL7Part.SetText(string text)
		{
			this.Message.SetTextQuietly(text);
			this.Reload();
		}

		private Segment ProcessMSHForACK(Segment msh)
		{
			string partText = msh.GetPartText(new PathSplitter("MSH", 3));
			string str = msh.GetPartText(new PathSplitter("MSH", 4));
			BasePart part = msh.GetPart(new PathSplitter("MSH", 3));
			if (part != null)
			{
				part.Text = msh.GetPartText(new PathSplitter("MSH", 5));
			}
			part = msh.GetPart(new PathSplitter("MSH", 4));
			if (part != null)
			{
				part.Text = msh.GetPartText(new PathSplitter("MSH", 6));
			}
			part = msh.GetPart(new PathSplitter("MSH", 5));
			if (part != null)
			{
				part.Text = partText;
			}
			part = msh.GetPart(new PathSplitter("MSH", 6));
			if (part != null)
			{
				part.Text = str;
			}
			part = msh.GetPart(new PathSplitter("MSH", 9, 1));
			if (part != null)
			{
				part.Text = "ACK";
			}
			return msh;
		}

		internal void Reload()
		{
			if (this.Message.BeginUpdateCount < 1)
			{
				this.Message.Reload();
			}
		}

		internal void ReloadFromInternalMessageParts()
		{
			this.Message.SetTextQuietly(this.Message.ToString());
			if (this.Message.BeginUpdateCount < 1)
			{
				this.Message.Reload();
			}
		}

		public void RemoveSegment(IHL7Segment segment)
		{
			this.Message.Segments.Remove(segment.GetPath());
			this.ReloadFromInternalMessageParts();
		}

		public override void SetStructureAtPath(string toPath, string fromValue)
		{
			PathSplitter pathSplitter = new PathSplitter(toPath, true);
			BasePart part = this.Message.GetPart(pathSplitter);
			if (part == null)
			{
				part = this.Message.CreatePart(pathSplitter, fromValue);
				if (pathSplitter.PathTerminationPoint == PathTypes.SegmentIndex || pathSplitter.PathTerminationPoint == PathTypes.Segment)
				{
					part = null;
				}
			}
			else if (pathSplitter.PathTerminationPoint == PathTypes.Field)
			{
				Field field = part as Field;
				if (field != null && field.IsFieldRepeated)
				{
					foreach (Field relatedRepeatField in field.GetRelatedRepeatFields(true))
					{
						((Segment)field.Parent).Fields.Remove(relatedRepeatField);
					}
				}
			}
			if (part != null)
			{
				part.SetTextQuietly(fromValue);
				part.Reload();
				part.RebuildParentTextQuietly();
			}
		}

		protected override void SetText(string text)
		{
			this.Message.SetTextQuietly(text);
			this.Message.Reload();
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			fromValue = Helpers.HL7Encode(fromValue);
			this.SetStructureAtPath(toPath, fromValue);
		}
	}
}