using System;
using System.ComponentModel;

namespace HL7Soup.Functions
{
	public enum MessageTypes
	{
		Unknown,
		[Description("HL7")]
		HL7V2,
		HL7V3,
		FHIR,
		XML,
		CSV,
		SQL,
		TextWithVariables,
		HL7V2Path,
		XPath,
		CSVPath,
		JSON,
		JSONPath,
		Text,
		Binary
	}
}