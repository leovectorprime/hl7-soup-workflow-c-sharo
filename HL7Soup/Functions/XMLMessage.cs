using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml;

namespace HL7Soup.Functions
{
	public class XMLMessage : FunctionMessage, IDisposable, IXmlMessage, IMessage
	{
		private XmlDocument doc;

		private XmlNode root;

		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.XML;
			}
		}

		public XmlNamespaceManager NamespaceManager
		{
			get;
			set;
		}

		public override string Text
		{
			get
			{
				return this.doc.OuterXml;
			}
		}

		public XMLMessage(string text) : base(text)
		{
			this.SetText(text);
		}

		internal void CreateNamespaceManager(Dictionary<string, string> namespaces)
		{
			if (namespaces != null && namespaces.Count > 0)
			{
				XmlNamespaceManager xmlNamespaceManagers = new XmlNamespaceManager(this.doc.NameTable);
				foreach (KeyValuePair<string, string> @namespace in namespaces)
				{
					xmlNamespaceManagers.AddNamespace(@namespace.Key, @namespace.Value);
				}
				this.NamespaceManager = xmlNamespaceManagers;
			}
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		private static XmlNode GetChildNodeByPathSegment(string pathSegment, XmlNode parentNode)
		{
			XmlNode itemOf;
			try
			{
				if (!pathSegment.StartsWith("@"))
				{
					int num = 0;
					string str = null;
					int num1 = pathSegment.IndexOf("[");
					if (num1 < 0)
					{
						str = pathSegment;
					}
					else
					{
						num1++;
						int num2 = pathSegment.IndexOf("]", num1);
						num = int.Parse(pathSegment.Substring(num1, num2 - num1));
						str = pathSegment.Substring(0, num1 - 1);
					}
					int num3 = 1;
					foreach (XmlNode childNode in parentNode.ChildNodes)
					{
						string str1 = null;
						str1 = (!str.Contains(":") ? childNode.LocalName : childNode.Name);
						if (!string.Equals(str1, str, StringComparison.InvariantCultureIgnoreCase) && !(str == "*"))
						{
							continue;
						}
						if (num3 < num)
						{
							num3++;
						}
						else
						{
							itemOf = childNode;
							return itemOf;
						}
					}
					return null;
				}
				else
				{
					if (pathSegment == "@")
					{
						throw new Exception("Could not find attribute name in the path.  The @ symbol must be followed by the name of the attribute.");
					}
					pathSegment = pathSegment.Substring(1);
					itemOf = parentNode.Attributes[pathSegment];
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new Exception(string.Concat(new string[] { "Could not get value from path segment '", pathSegment, "' due to ", exception.Message, ".  Path segments should look like 'ElementName[index]'." }), exception);
			}
			return itemOf;
		}

		private XmlNode GetNodeByPath(string path)
		{
			return XMLMessage.GetNodeByPath(path, this.root, this.NamespaceManager);
		}

		public static XmlNode GetNodeByPath(string path, XmlNode rootNode, XmlNamespaceManager NamespaceManager)
		{
			if (path.StartsWith("xpath:"))
			{
				return rootNode.SelectSingleNode(path.Substring(6), NamespaceManager);
			}
			if (HL7Soup.Workflow.Properties.Settings.Default.ForceXPath)
			{
				return rootNode.SelectSingleNode(path, NamespaceManager);
			}
			XmlNode childNodeByPathSegment = null;
			string[] strArrays = path.Split(new char[] { '/' });
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				if (i != 0 || !(strArrays[i] == ""))
				{
					if (childNodeByPathSegment != null)
					{
						childNodeByPathSegment = XMLMessage.GetChildNodeByPathSegment(strArrays[i], childNodeByPathSegment);
					}
					else
					{
						string str = null;
						str = (!path.Contains(":") ? rootNode.LocalName : rootNode.Name);
						if (string.Equals(str, strArrays[i], StringComparison.InvariantCultureIgnoreCase))
						{
							childNodeByPathSegment = rootNode;
						}
					}
					if (childNodeByPathSegment == null)
					{
						return null;
					}
				}
			}
			return childNodeByPathSegment;
		}

		public string GetStructureAtPath(string path)
		{
			try
			{
				XmlNode nodeByPath = this.GetNodeByPath(path);
				if (nodeByPath != null)
				{
					return nodeByPath.OuterXml;
				}
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
			return "";
		}

		public override string GetValueAtPath(string path)
		{
			try
			{
				XmlNode nodeByPath = this.GetNodeByPath(path);
				if (nodeByPath != null)
				{
					return nodeByPath.InnerText;
				}
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
			return "";
		}

		public override void SetStructureAtPath(string path, string value)
		{
			XmlNode nodeByPath = this.GetNodeByPath(path);
			if (nodeByPath != null)
			{
				if (nodeByPath.ParentNode is XmlDocument)
				{
					((XmlDocument)nodeByPath.ParentNode).LoadXml(value);
					return;
				}
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(string.Concat("<Wrapper>", value, "</Wrapper>"));
				foreach (XmlNode childNode in xmlDocument.FirstChild.ChildNodes)
				{
					XmlNode xmlNodes = nodeByPath.OwnerDocument.ImportNode(childNode, true);
					nodeByPath.ParentNode.AppendChild(xmlNodes);
				}
				nodeByPath.ParentNode.RemoveChild(nodeByPath);
			}
		}

		protected override void SetText(string text)
		{
			this.doc = new XmlDocument()
			{
				XmlResolver = null
			};
			if (!string.IsNullOrEmpty(text))
			{
				this.doc.LoadXml(text);
			}
			this.root = this.doc.DocumentElement;
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			XmlNode nodeByPath = this.GetNodeByPath(toPath);
			if (nodeByPath != null)
			{
				nodeByPath.InnerText = fromValue;
			}
		}
	}
}