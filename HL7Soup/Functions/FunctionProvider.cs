using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using NLog;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions
{
	public abstract class FunctionProvider : IFunctionProvider
	{
		protected ISetting baseSetting;

		public string ErrorMessage
		{
			get;
			set;
		}

		public ISendReceiveManager Manager
		{
			get;
			set;
		}

		public HL7Soup.Functions.FunctionProviderStatuses Status
		{
			get;
			set;
		}

		public FunctionProvider()
		{
			this.Status = HL7Soup.Functions.FunctionProviderStatuses.Constructed;
		}

		public void AllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
			try
			{
				this.Status = HL7Soup.Functions.FunctionProviderStatuses.Finalizing;
				this.FunctionAllFunctionsSucceded(workflowInstance);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Errored(workflowInstance, string.Concat("Error finalizing. ", exception.Message), exception);
			}
		}

		public void Cancel()
		{
			try
			{
				this.FunctionCanceled();
				this.Status = HL7Soup.Functions.FunctionProviderStatuses.Canceled;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Errored(string.Concat("Error canceling.", exception.Message), exception);
			}
		}

		public void Close()
		{
			try
			{
				this.FunctionClosed();
				this.Status = HL7Soup.Functions.FunctionProviderStatuses.Closed;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Errored(string.Concat("Error closing.", exception.Message), exception);
			}
		}

		protected void Errored(WorkflowInstance workflowInstance, string message)
		{
			this.Errored(workflowInstance, message, null);
		}

		protected void Errored(string message)
		{
			this.Errored(null, message, null);
		}

		protected void Errored(WorkflowInstance workflowInstance, Exception ex)
		{
			this.Errored(workflowInstance, ex.Message, ex);
		}

		protected void Errored(string message, Exception ex)
		{
			this.Errored(null, message, ex);
		}

		protected void Errored(WorkflowInstance workflowInstance, string message, Exception ex)
		{
			string str;
			this.Manager.Errored(this, this.baseSetting);
			if (workflowInstance != null)
			{
				Guid id = this.baseSetting.Id;
				if (this.baseSetting is TransformerSetting)
				{
					id = workflowInstance.CurrentActivityInstance.Id;
				}
				if (this.baseSetting is FilterSetting)
				{
					id = workflowInstance.CurrentActivityInstance.Id;
				}
				MessageLogger.Instance.AddMessage(new MessageEvent(message, workflowInstance, id, LogItemStatus.Errored, LogItemDataType.Error));
				WorkflowInstance workflowInstance1 = workflowInstance;
				if (!string.IsNullOrEmpty(message))
				{
					str = message;
				}
				else if (ex != null)
				{
					str = ex.Message;
				}
				else
				{
					str = null;
				}
				workflowInstance1.Errored(str);
			}
			if (ex == null)
			{
				Log.Instance.Error(string.Concat(this.baseSetting.Name, ": ", message));
				this.ErrorMessage = message;
			}
			else if (message != null)
			{
				Log.Instance.Error(ex, string.Concat(this.baseSetting.Name, ": ", message));
				this.ErrorMessage = message;
			}
			else
			{
				Log.Instance.Error(ex, string.Concat(this.baseSetting.Name, ": "));
				this.ErrorMessage = ex.Message;
			}
			this.Status = HL7Soup.Functions.FunctionProviderStatuses.Errored;
		}

		protected virtual void FunctionAllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
		}

		protected virtual void FunctionCanceled()
		{
		}

		protected virtual void FunctionClosed()
		{
		}

		protected abstract void FunctionPrepare(WorkflowInstance workflowInstance);

		protected virtual void FunctionRollBack(WorkflowInstance workflowInstance)
		{
		}

		public void Prepare(ISetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			try
			{
				this.baseSetting = setting;
				this.Manager = manager;
				this.FunctionPrepare(workflowInstance);
				this.Status = HL7Soup.Functions.FunctionProviderStatuses.Ready;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Errored(string.Concat("Error preparing to start Workflow:\r\n\r\n", exception.Message), exception);
			}
		}

		public void RollBack(WorkflowInstance workflowInstance)
		{
			try
			{
				this.Status = HL7Soup.Functions.FunctionProviderStatuses.RollingBack;
				this.FunctionRollBack(workflowInstance);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Errored(workflowInstance, string.Concat("Error finalizing. ", exception.Message), exception);
			}
		}
	}
}