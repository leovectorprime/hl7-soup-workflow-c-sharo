using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.SourceProviders;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace HL7Soup.Functions.EditorSendingBehaviours
{
	public class HttpEditor : HttpSender, IEditorSendingBehaviour, IFunctionProvider, ISender
	{
		private IAutomaticSendSetting autoSettings;

		private ISourceProviderSetting sourceProviderSetting;

		private ISourceProvider sourceProvider;

		public new IHttpEditorSetting Setting
		{
			get
			{
				return (IHttpEditorSetting)this.baseSetting;
			}
		}

		public HttpEditor()
		{
		}

		public ISourceProvider AutomaticSend(WorkflowInstance workflowInstance)
		{
			ISourceProvider sourceProvider;
			try
			{
				ISourceProvider sourceProvider1 = this.FunctionAutomaticSend(workflowInstance);
				base.Status = HL7Soup.Functions.FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
				sourceProvider = sourceProvider1;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Error sending.", exception.Message), exception);
				return null;
			}
			return sourceProvider;
		}

		protected ISourceProvider FunctionAutomaticSend(WorkflowInstance workflowInstance)
		{
			if (this.sourceProvider == null)
			{
				this.sourceProvider = FunctionHelpers.GetSourceProviderInstance(this.sourceProviderSetting, base.Manager, workflowInstance);
			}
			if (this.autoSettings.MoveNext)
			{
				this.sourceProvider.GetNext(workflowInstance);
			}
			if (this.autoSettings.MoveStatic)
			{
				this.sourceProvider.GetFirst(workflowInstance);
			}
			if (workflowInstance.Message != null && this.sourceProvider.Status != HL7Soup.Functions.FunctionProviderStatuses.Errored && !this.sourceProvider.Pending)
			{
				this.FunctionSend(workflowInstance);
			}
			return this.sourceProvider;
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			this.autoSettings = SystemSettings.Instance.GetAutomaticSendConnectionBySendConnection(this.Setting);
			this.sourceProviderSetting = SystemSettings.Instance.GetSourceProviderBySendConnection(this.Setting);
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			base.SendAndReturnResponse(workflowInstance);
			if (workflowInstance.ResponseMessage != null)
			{
				Application.Current.Dispatcher.Invoke(() => {
					AckMessage aCKResponseForHL7V2Editor = this.GetACKResponseForHL7V2Editor(workflowInstance.ResponseMessage.Text);
					if (aCKResponseForHL7V2Editor != null)
					{
						this.Manager.RaiseMessageReceived(aCKResponseForHL7V2Editor);
					}
				});
			}
		}

		private AckMessage GetACKResponseForHL7V2Editor(string responseData)
		{
			if (responseData != null)
			{
				Message message = new Message(responseData, false);
				Segment part = message.GetPart(new PathSplitter(SegmentsEnum.MSH)) as Segment;
				if (part != null)
				{
					part.GetPartText(new PathSplitter("MSH", 10));
					message.GetPartText(new PathSplitter("MSA", 1));
					if (this.Setting.LoadResult)
					{
						return new AckMessage(message.Text.ToString(), false);
					}
				}
			}
			return null;
		}
	}
}