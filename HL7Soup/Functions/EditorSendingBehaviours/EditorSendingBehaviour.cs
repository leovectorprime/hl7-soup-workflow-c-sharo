using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.SourceProviders;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HL7Soup.Functions.EditorSendingBehaviours
{
	public abstract class EditorSendingBehaviour : FunctionProvider, IEditorSendingBehaviour, IFunctionProvider, ISender
	{
		protected EditorSendingBehaviour()
		{
		}

		public ISourceProvider AutomaticSend(WorkflowInstance workflowInstance)
		{
			ISourceProvider sourceProvider;
			try
			{
				ISourceProvider sourceProvider1 = this.FunctionAutomaticSend(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
				sourceProvider = sourceProvider1;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Error sending.", exception.Message), exception);
				return null;
			}
			return sourceProvider;
		}

		protected abstract ISourceProvider FunctionAutomaticSend(WorkflowInstance workflowInstance);

		protected abstract void FunctionSend(WorkflowInstance workflowInstance);

		public void Send(WorkflowInstance workflowInstance)
		{
			try
			{
				this.FunctionSend(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Error sending.", exception.Message), exception);
			}
		}
	}
}