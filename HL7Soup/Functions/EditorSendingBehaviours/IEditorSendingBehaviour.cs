using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.SourceProviders;

namespace HL7Soup.Functions.EditorSendingBehaviours
{
	public interface IEditorSendingBehaviour : IFunctionProvider, ISender
	{
		ISourceProvider AutomaticSend(WorkflowInstance workflowInstance);
	}
}