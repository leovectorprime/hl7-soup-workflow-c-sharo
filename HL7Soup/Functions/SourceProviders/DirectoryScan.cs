using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.SourceProviders;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.SourceProviders
{
	public class DirectoryScan : SourceProvider
	{
		private FileSystemWatcher watcher = new FileSystemWatcher();

		private int currentLocation;

		public HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		public List<FileInfo> Files
		{
			get;
			set;
		}

		public IDirectoryScanSourceProviderSetting Setting
		{
			get
			{
				return (IDirectoryScanSourceProviderSetting)this.baseSetting;
			}
		}

		public DirectoryScan()
		{
		}

		protected override void FunctionAllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
			base.FunctionAllFunctionsSucceded(workflowInstance);
			try
			{
				if (this.DocumentManager != null)
				{
					this.DocumentManager.Delete(this.DocumentManager.Messages[this.currentLocation]);
				}
			}
			catch (Exception exception)
			{
				throw;
			}
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			this.watcher.Created -= new FileSystemEventHandler(this.OnCreated);
			this.watcher.Deleted -= new FileSystemEventHandler(this.OnDeleted);
			this.watcher = null;
			if (this.DocumentManager != null)
			{
				this.DocumentManager.Dispose();
				this.DocumentManager = null;
			}
		}

		protected override void FunctionGetFirst(WorkflowInstance workflowInstance)
		{
			this.currentLocation = 0;
			base.GetNext(workflowInstance);
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			base.Pending = false;
			if (this.DocumentManager == null)
			{
				if (this.Files.Count > 0)
				{
					if (!File.Exists(this.Files[0].FullName))
					{
						this.Files.RemoveAt(0);
						base.Pending = true;
						return;
					}
					this.DocumentManager = new HL7Soup.DocumentManager()
					{
						MessageType = this.Setting.MessageType,
						AutomaticValidation = false
					};
					this.DocumentManager.Load(this.Files[0].FullName);
					this.Files.RemoveAt(0);
				}
				else if (!this.Setting.EndAfterProcessing)
				{
					base.Pending = true;
				}
				else
				{
					base.Finished = true;
				}
			}
			if (this.DocumentManager.Messages.Count > this.currentLocation)
			{
				workflowInstance.AddActivity(FunctionHelpers.GetFunctionMessage(this.DocumentManager.Messages[this.currentLocation].ToString(), this.Setting.MessageType), this.Setting.Id);
			}
			if (this.DocumentManager.Messages.Count <= this.currentLocation)
			{
				this.currentLocation = 0;
				if (this.Files.Count == 0 && this.Setting.EndAfterProcessing)
				{
					base.Finished = true;
				}
				this.DocumentManager.Dispose();
				this.DocumentManager = null;
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			this.currentLocation = 0;
			this.Files = new List<FileInfo>();
			FileInfo[] files = (new DirectoryInfo(this.Setting.DirectoryPath)).GetFiles(this.Setting.DirectoryFilter, SearchOption.TopDirectoryOnly);
			for (int i = 0; i < (int)files.Length; i++)
			{
				FileInfo fileInfo = files[i];
				this.Files.Add(fileInfo);
			}
			if (!this.Setting.EndAfterProcessing)
			{
				this.Watch();
			}
		}

		protected override void FunctionRollBack(WorkflowInstance workflowInstance)
		{
			base.FunctionRollBack(workflowInstance);
		}

		private void OnCreated(object source, FileSystemEventArgs e)
		{
			this.Files.Add(new FileInfo(e.FullPath));
		}

		private void OnDeleted(object source, FileSystemEventArgs e)
		{
			this.Files.Remove(new FileInfo(e.FullPath));
		}

		private void Watch()
		{
			this.watcher.Path = this.Setting.DirectoryPath;
			this.watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.LastAccess;
			this.watcher.Filter = this.Setting.DirectoryFilter;
			this.watcher.Created += new FileSystemEventHandler(this.OnCreated);
			this.watcher.Deleted += new FileSystemEventHandler(this.OnDeleted);
			this.watcher.EnableRaisingEvents = true;
		}
	}
}