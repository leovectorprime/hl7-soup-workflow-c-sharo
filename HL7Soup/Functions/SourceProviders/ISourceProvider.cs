using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.SourceProviders
{
	public interface ISourceProvider : IFunctionProvider
	{
		bool Finished
		{
			get;
			set;
		}

		bool Pending
		{
			get;
			set;
		}

		void GetFirst(WorkflowInstance workflowInstance);

		void GetNext(WorkflowInstance workflowInstance);
	}
}