using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.SourceProviders;
using HL7Soup.Integrations;
using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace HL7Soup.Functions.SourceProviders
{
	public class CurrentTab : SourceProvider
	{
		private int currentLocation = -2147483648;

		private IMessage staticMessage;

		public ObservableCollection<Message> CurrentMessages
		{
			get
			{
				if (this.DocumentManager.FilteredMessages != null && this.DocumentManager.FilteredMessages.Count > 0)
				{
					return this.DocumentManager.FilteredMessages;
				}
				return this.DocumentManager.Messages;
			}
		}

		public HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		public ICurrentTabSourceProviderSetting Setting
		{
			get
			{
				return (ICurrentTabSourceProviderSetting)this.baseSetting;
			}
		}

		public CurrentTab()
		{
		}

		protected override void FunctionGetFirst(WorkflowInstance workflowInstance)
		{
			if (this.staticMessage != null)
			{
				workflowInstance.AddActivity(this.staticMessage, this.Setting.Id);
				return;
			}
			if (this.currentLocation == -2147483648)
			{
				this.currentLocation = this.CurrentMessages.Count - 1;
			}
			this.staticMessage = FunctionHelpers.GetFunctionMessageDeterminedFromText(this.CurrentMessages[this.currentLocation].ToString());
			workflowInstance.AddActivity(this.staticMessage, this.Setting.Id);
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			if (this.Setting.ReverseOrder)
			{
				this.FunctionGetPrevious(workflowInstance);
				return;
			}
			if (this.currentLocation == -2147483648 || this.currentLocation == -1)
			{
				this.currentLocation = this.CurrentMessages.Count - 1;
			}
			workflowInstance.AddActivity(new HL7V2MessageType(this.CurrentMessages[this.currentLocation].ToString()), this.Setting.Id);
			this.currentLocation--;
			if (this.CurrentMessages.Count <= this.currentLocation || this.currentLocation < 0)
			{
				base.Finished = true;
				this.currentLocation = -2147483648;
			}
		}

		protected void FunctionGetPrevious(WorkflowInstance workflowInstance)
		{
			if (this.currentLocation == -2147483648 || this.currentLocation == -1)
			{
				this.currentLocation = 0;
			}
			workflowInstance.AddActivity(new HL7V2MessageType(this.CurrentMessages[this.currentLocation].ToString()), this.Setting.Id);
			this.currentLocation++;
			if (this.CurrentMessages.Count <= this.currentLocation)
			{
				base.Finished = true;
				this.currentLocation = -2147483648;
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			this.DocumentManager = base.Manager.DocumentManager;
			this.currentLocation = this.GetStartLocation();
		}

		private int GetStartLocation()
		{
			MessageBoxResult messageBoxResult = MessageBoxResult.None;
			int currentDocumentIndex = -2147483648;
			if (this.Setting.StartLocation == StartLocation.CurrentItem)
			{
				Application.Current.Dispatcher.Invoke(() => {
					currentDocumentIndex = this.DocumentManager.CurrentDocumentIndex;
					if (currentDocumentIndex == 0 && !this.Setting.ReverseOrder && this.Setting.StartLocation == StartLocation.CurrentItem)
					{
						messageBoxResult = MessageBox.Show("The currently selected item is the last message.  Auto sending this message isn't going to get you very far.  Would you like to start at the first message instead?", "Should I start at the begining?", MessageBoxButton.YesNo, MessageBoxImage.Question);
					}
				});
			}
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				currentDocumentIndex = -2147483648;
			}
			return currentDocumentIndex;
		}
	}
}