using HL7Soup;
using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.SourceProviders
{
	public abstract class SourceProvider : FunctionProvider, ISourceProvider, IFunctionProvider
	{
		public bool Finished
		{
			get;
			set;
		}

		public bool Pending
		{
			get;
			set;
		}

		protected SourceProvider()
		{
		}

		protected abstract void FunctionGetFirst(WorkflowInstance workflowInstance);

		protected abstract void FunctionGetNext(WorkflowInstance workflowInstance);

		public void GetFirst(WorkflowInstance workflowInstance)
		{
			try
			{
				this.FunctionGetFirst(workflowInstance);
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
			}
			catch (Exception exception)
			{
				base.Errored(workflowInstance, "Error getting first.", exception);
			}
		}

		public void GetNext(WorkflowInstance workflowInstance)
		{
			try
			{
				this.FunctionGetNext(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Error getting next.", exception.Message), exception);
			}
		}
	}
}