using System;

namespace HL7Soup.Functions
{
	public interface IValueCanBeFormatted
	{
		Encodings Encoding
		{
			get;
			set;
		}

		int End
		{
			get;
			set;
		}

		string Format
		{
			get;
			set;
		}

		bool hasFormat
		{
			get;
		}

		bool IsActivityBinding
		{
			get;
			set;
		}

		int Start
		{
			get;
			set;
		}

		TextFormats TextFormat
		{
			get;
			set;
		}

		HL7Soup.Functions.Truncation Truncation
		{
			get;
			set;
		}

		int TruncationLength
		{
			get;
			set;
		}

		string ToString();
	}
}