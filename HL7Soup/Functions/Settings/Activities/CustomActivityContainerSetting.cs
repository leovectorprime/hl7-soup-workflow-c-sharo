using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Activities
{
	[Serializable]
	public class CustomActivityContainerSetting : SenderWithResponseSetting, ISettingWithVariables
	{
		private InMessageAttribute tempInMessage;

		private OutMessageAttribute tempOutMessage;

		private Dictionary<string, IVariableCreator> variables;

		private Dictionary<string, CustomActivityParameter> parameters;

		public string CustomActivityName
		{
			get;
			set;
		}

		public string CustomActivityTypeName
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				return this.CustomActivityName;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Activities.CustomActivityContainer";
			}
		}

		public InMessageAttribute InMessage
		{
			get;
			set;
		}

		public OutMessageAttribute OutMessage
		{
			get;
			set;
		}

		public Dictionary<string, CustomActivityParameter> Parameters
		{
			get
			{
				return this.parameters;
			}
			set
			{
				this.parameters = value;
			}
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditCustomActivityContainerSetting";
			}
		}

		public CustomActivityContainerSetting()
		{
		}

		public void CreateParameters(Type type)
		{
			this.parameters = new Dictionary<string, CustomActivityParameter>();
			foreach (Attribute attribute in new List<Attribute>(Attribute.GetCustomAttributes(type, typeof(ParameterAttribute))))
			{
				ParameterAttribute customActivityParameter = attribute as ParameterAttribute;
				if (customActivityParameter == null)
				{
					continue;
				}
				this.parameters[customActivityParameter.Name] = new CustomActivityParameter(customActivityParameter);
			}
		}

		public Type GetCustomActivityType()
		{
			Type type = Type.GetType(this.CustomActivityTypeName);
			if (type == null)
			{
				throw new Exception(string.Concat("Cannot create the type ", this.CustomActivityTypeName, ".  Is the containing assembly in the 'Custom Libraries' directory?"));
			}
			return type;
		}

		public InMessageAttribute GetInMessage()
		{
			if (this.tempInMessage != null)
			{
				return this.tempInMessage;
			}
			this.InMessage = (InMessageAttribute)Attribute.GetCustomAttribute(this.GetCustomActivityType(), typeof(InMessageAttribute));
			this.tempInMessage = this.InMessage;
			return this.InMessage;
		}

		public OutMessageAttribute GetOutMessage()
		{
			if (this.tempOutMessage != null)
			{
				return this.tempOutMessage;
			}
			this.OutMessage = (OutMessageAttribute)Attribute.GetCustomAttribute(this.GetCustomActivityType(), typeof(OutMessageAttribute));
			this.tempOutMessage = this.OutMessage;
			return this.OutMessage;
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.variables != null)
			{
				return this.variables;
			}
			this.variables = new Dictionary<string, IVariableCreator>();
			foreach (Attribute attribute in new List<Attribute>(Attribute.GetCustomAttributes(this.GetCustomActivityType(), typeof(VariableAttribute))))
			{
				VariableAttribute variableCreator = attribute as VariableAttribute;
				if (variableCreator == null)
				{
					continue;
				}
				this.variables[variableCreator.Name] = new VariableCreator(variableCreator.Name, variableCreator.SampleValue, true);
			}
			return this.variables;
		}
	}
}