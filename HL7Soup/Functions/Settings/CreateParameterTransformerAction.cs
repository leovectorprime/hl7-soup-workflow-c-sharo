using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CreateParameterTransformerAction : TransformerAction
	{
		public bool IsRequired
		{
			get;
			set;
		}

		public string ParameterDescription
		{
			get;
			set;
		}

		public string ParameterName
		{
			get;
			set;
		}

		public override string TransformerName
		{
			get
			{
				return this.ToString();
			}
		}

		public CreateParameterTransformerAction(ParameterAttribute parameter)
		{
			this.ParameterName = parameter.Name;
			this.ParameterDescription = parameter.Description;
			this.IsRequired = parameter.IsRequired;
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			workflowInstance.SetVariable(this.ParameterName, fromValue);
		}

		public override string ToString()
		{
			string[] parameterName = new string[] { this.ParameterName, " <- ", base.FromPath, " (", Enum.GetName(typeof(HL7Soup.Functions.MessageTypes), base.FromType), ") ", null };
			parameterName[6] = base.FromSetting.ToString();
			return string.Concat(parameterName);
		}
	}
}