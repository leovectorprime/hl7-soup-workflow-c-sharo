using HL7Soup;
using HL7Soup.Functions;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class SenderSetting : ActivitySetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Sender";
			}
		}

		public virtual bool InboundMessageNotAvailable
		{
			get;
			set;
		}

		public string MessageTemplate
		{
			get;
			set;
		}

		public HL7Soup.Functions.MessageTypes MessageType
		{
			get;
			set;
		}

		public bool UserCanEditTemplate
		{
			get
			{
				return JustDecompileGenerated_get_UserCanEditTemplate();
			}
			set
			{
				JustDecompileGenerated_set_UserCanEditTemplate(value);
			}
		}

		private bool JustDecompileGenerated_UserCanEditTemplate_k__BackingField = true;

		public bool JustDecompileGenerated_get_UserCanEditTemplate()
		{
			return this.JustDecompileGenerated_UserCanEditTemplate_k__BackingField;
		}

		public void JustDecompileGenerated_set_UserCanEditTemplate(bool value)
		{
			this.JustDecompileGenerated_UserCanEditTemplate_k__BackingField = value;
		}

		protected SenderSetting()
		{
		}

		public string MessageTemplateRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.MessageTemplate);
		}
	}
}