using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class AppendLineTransformerAction : TransformerAction
	{
		private Guid toSetting;

		private MessageSourceDirection toDirection;

		public MessageSourceDirection ToDirection
		{
			get
			{
				return this.toDirection;
			}
			set
			{
				this.toDirection = value;
				base.NotifyPropertyChanged("ToDirection");
			}
		}

		public Guid ToSetting
		{
			get
			{
				return this.toSetting;
			}
			set
			{
				this.toSetting = value;
				base.NotifyPropertyChanged("ToSetting");
			}
		}

		public override string TransformerName
		{
			get
			{
				return "Append Line";
			}
		}

		public AppendLineTransformerAction()
		{
		}

		public override void Prepare(WorkflowInstance workflowInstance)
		{
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			IMessage message = workflowInstance.GetMessage(this.ToSetting, this.ToDirection);
			if (message != null)
			{
				if (message is IHL7Message)
				{
					((IHL7Message)message).AddSegment(fromValue);
					return;
				}
				if (message is TextMessage)
				{
					((TextMessage)message).AppendLine(fromValue);
				}
			}
		}

		public override string ToString()
		{
			return "Append Line";
		}
	}
}