using System;
using System.Text;

namespace HL7Soup.Functions.Settings
{
	public interface IMLLPConnectionSetting
	{
		char[] FrameEnd
		{
			get;
			set;
		}

		char[] FrameStart
		{
			get;
			set;
		}

		Encoding MessageEncoding
		{
			get;
			set;
		}

		int Port
		{
			get;
			set;
		}

		string Server
		{
			get;
			set;
		}
	}
}