using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class Setting : ISetting
	{
		private Guid id = Guid.NewGuid();

		private const int systemVersion = 1;

		public abstract string ConnectionTypeName
		{
			get;
		}

		public abstract string Details
		{
			get;
		}

		public abstract string FunctionProviderType
		{
			get;
		}

		public Guid Id
		{
			get
			{
				if (this.id == Guid.Empty)
				{
					this.id = Guid.NewGuid();
				}
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		public virtual string Name
		{
			get;
			set;
		}

		public abstract string SettingDialogType
		{
			get;
		}

		public int Version { get; set; } = 1;

		protected Setting()
		{
		}

		public List<Guid> GetAllActivitiesAtRootLevel()
		{
			return Setting.GetAllActivitiesAtRootLevel(this);
		}

		public List<Guid> GetAllActivitiesAtRootLevel(Guid lastSettingId)
		{
			return Setting.GetAllActivitiesAtRootLevel(this, lastSettingId);
		}

		public static List<Guid> GetAllActivitiesAtRootLevel(ISetting setting)
		{
			return Setting.GetAllActivitiesAtRootLevel(setting, Guid.Empty);
		}

		public static List<Guid> GetAllActivitiesAtRootLevel(ISetting setting, Guid lastSettingId)
		{
			List<Guid> guids = new List<Guid>()
			{
				setting.Id
			};
			if (setting.Id == lastSettingId)
			{
				IReceiverWithResponseSetting receiverWithResponseSetting = setting as IReceiverWithResponseSetting;
				if (receiverWithResponseSetting == null || !receiverWithResponseSetting.ReturnCustomResponse)
				{
					return guids;
				}
			}
			ISettingWithActivities settingWithActivity = setting as ISettingWithActivities;
			if (settingWithActivity != null)
			{
				if (lastSettingId == Guid.Empty)
				{
					guids.AddRange(settingWithActivity.Activities);
				}
				else
				{
					foreach (Guid activity in settingWithActivity.Activities)
					{
						guids.Add(activity);
						if (activity != lastSettingId)
						{
							continue;
						}
						return guids;
					}
				}
			}
			return guids;
		}

		public static List<ISetting> GetAllSettingInstances(ISetting rootSetting)
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Guid allActivitiesAtRootLevel in rootSetting.GetAllActivitiesAtRootLevel())
			{
				ISetting settingById = FunctionHelpers.GetSettingById(allActivitiesAtRootLevel);
				if (settingById == null)
				{
					continue;
				}
				settings.Add(settingById);
				ISettingWithFilters settingWithFilter = settingById as ISettingWithFilters;
				if (settingWithFilter != null)
				{
					ISetting setting = FunctionHelpers.GetSettingById(settingWithFilter.Filters);
					if (setting != null)
					{
						settings.Add(setting);
					}
				}
				ISettingWithTransformers settingWithTransformer = settingById as ISettingWithTransformers;
				if (settingWithTransformer == null)
				{
					continue;
				}
				ISetting settingById1 = FunctionHelpers.GetSettingById(settingWithTransformer.Transformers);
				if (settingById1 == null)
				{
					continue;
				}
				settings.Add(settingById1);
			}
			return settings;
		}

		public virtual List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			return new List<ISetting>();
		}

		public virtual void SwapActivityIds(Dictionary<Guid, Guid> oldAndNewGuidDictionary)
		{
		}

		public virtual string Upgrade()
		{
			if (this.Version > 1)
			{
				throw new Exception("Cannot import this workflow as it was created on a newer version.  Please upgrade to import.");
			}
			this.Version = 1;
			return null;
		}
	}
}