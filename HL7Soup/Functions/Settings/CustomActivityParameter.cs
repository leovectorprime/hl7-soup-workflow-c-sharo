using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CustomActivityParameter
	{
		public bool IsRequired
		{
			get;
			set;
		}

		public string ParameterDescription
		{
			get;
			set;
		}

		public string ParameterName
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

		public CustomActivityParameter(ParameterAttribute parameter)
		{
			this.ParameterName = parameter.Name;
			this.ParameterDescription = parameter.Description;
			this.IsRequired = parameter.IsRequired;
		}

		public static string GetTheValue(WorkflowInstance workflowInstance, string value)
		{
			string str = value;
			foreach (string listOfVariablesInText in FunctionHelpers.GetListOfVariablesInText(value))
			{
				str = str.Replace(string.Concat("${", listOfVariablesInText, "}"), workflowInstance.GetVariable(listOfVariablesInText));
			}
			return str;
		}

		public void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			workflowInstance.SetVariable(this.ParameterName, fromValue);
		}
	}
}