using HL7Soup.Dialogs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class TransformerSetting : Setting, ITransformerSetting, ISetting, ISettingWithVariables
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Transformer";
			}
		}

		public override string Details
		{
			get
			{
				return "Transformer Details";
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Transformers.Default";
			}
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditTransformerSetting";
			}
		}

		public List<ITransformerAction> Transformers
		{
			get;
			set;
		}

		public TransformerSetting()
		{
			this.Transformers = new List<ITransformerAction>();
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			return TransformerSetting.GetVariables(this.Transformers);
		}

		public static Dictionary<string, IVariableCreator> GetVariables(List<ITransformerAction> transformers)
		{
			Dictionary<string, IVariableCreator> strs = new Dictionary<string, IVariableCreator>();
			foreach (IVariableCreator variableCreator in transformers.OfType<IVariableCreator>())
			{
				if (variableCreator.VariableName == null)
				{
					continue;
				}
				strs[variableCreator.VariableName] = variableCreator;
			}
			foreach (CustomTransformerAction customTransformerAction in transformers.OfType<CustomTransformerAction>())
			{
				foreach (IVariableCreator value in customTransformerAction.GetVariables().Values)
				{
					strs[value.VariableName] = value;
				}
			}
			foreach (CodeTransformerAction codeTransformerAction in transformers.OfType<CodeTransformerAction>())
			{
				foreach (IVariableCreator value1 in codeTransformerAction.GetVariables().Values)
				{
					strs[value1.VariableName] = value1;
				}
			}
			using (IEnumerator<ForEachTransformerAction> enumerator = transformers.OfType<ForEachTransformerAction>().GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					ForEachTransformerAction current = enumerator.Current;
					strs["ForEachIterator"] = new VariableCreator("ForEachIterator", "1");
				}
			}
			return strs;
		}
	}
}