using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class DatabaseSenderSetting : SenderWithResponseSetting, IDatabaseSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		public string ConnectionString
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Database Sender";
			}
		}

		public DataProviders DataProvider
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				return "Database Query";
			}
		}

		public override bool DifferentResponseMessageType
		{
			get
			{
				return true;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.DatabaseSender";
			}
		}

		public override bool InboundMessageNotAvailable
		{
			get
			{
				return true;
			}
		}

		public List<DatabaseSettingParameter> Parameters
		{
			get
			{
				return JustDecompileGenerated_get_Parameters();
			}
			set
			{
				JustDecompileGenerated_set_Parameters(value);
			}
		}

		private List<DatabaseSettingParameter> JustDecompileGenerated_Parameters_k__BackingField;

		public List<DatabaseSettingParameter> JustDecompileGenerated_get_Parameters()
		{
			return this.JustDecompileGenerated_Parameters_k__BackingField;
		}

		public void JustDecompileGenerated_set_Parameters(List<DatabaseSettingParameter> value)
		{
			this.JustDecompileGenerated_Parameters_k__BackingField = value;
		}

		public ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditDatabaseSenderSetting";
			}
		}

		public DatabaseSenderSetting()
		{
			this.ConnectionString = "";
			base.MessageType = HL7Soup.Functions.MessageTypes.SQL;
			base.ResponseMessageType = HL7Soup.Functions.MessageTypes.CSV;
			this.ResponseNotAvailable = true;
			this.Parameters = new List<DatabaseSettingParameter>();
		}

		public override void SwapActivityIds(Dictionary<Guid, Guid> oldAndNewGuidDictionary)
		{
			if (this.Parameters != null)
			{
				foreach (DatabaseSettingParameter parameter in this.Parameters)
				{
					parameter.FromSetting = FunctionHelpers.ReplaceActivity(parameter.FromSetting, oldAndNewGuidDictionary);
				}
			}
			base.SwapActivityIds(oldAndNewGuidDictionary);
		}
	}
}