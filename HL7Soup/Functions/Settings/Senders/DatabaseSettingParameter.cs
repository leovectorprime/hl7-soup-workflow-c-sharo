using HL7Soup.Dialogs;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class DatabaseSettingParameter : INotifyPropertyChanged, IComparable<DatabaseSettingParameter>, IValueCanBeFormatted
	{
		private HL7Soup.Functions.MessageTypes fromType;

		private bool isValid = true;

		private MessageSourceDirection fromDirection = MessageSourceDirection.variable;

		private Guid fromSetting;

		[NonSerialized]
		public IActivityHost ActivityHost;

		public bool AllowBinding { get; set; } = true;

		public Encodings Encoding
		{
			get;
			set;
		}

		public int End
		{
			get;
			set;
		}

		public string Format
		{
			get;
			set;
		}

		public MessageSourceDirection FromDirection
		{
			get
			{
				return this.fromDirection;
			}
			set
			{
				this.fromDirection = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromDirection"));
				}
			}
		}

		public Dictionary<string, string> FromNamespaces
		{
			get;
			set;
		}

		public Guid FromSetting
		{
			get
			{
				return this.fromSetting;
			}
			set
			{
				this.fromSetting = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromSetting"));
				}
			}
		}

		public HL7Soup.Functions.MessageTypes FromType
		{
			get
			{
				return this.fromType;
			}
			set
			{
				this.fromType = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromType"));
				}
			}
		}

		public bool hasFormat
		{
			get
			{
				return !string.IsNullOrEmpty(this.Format);
			}
		}

		public bool IsActivityBinding
		{
			get;
			set;
		}

		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
				}
			}
		}

		public string Name
		{
			get;
			set;
		}

		public string NameWithoutAt
		{
			get
			{
				return this.Name.Substring(1);
			}
		}

		public int Start
		{
			get;
			set;
		}

		public TextFormats TextFormat
		{
			get;
			set;
		}

		public HL7Soup.Functions.Truncation Truncation
		{
			get;
			set;
		}

		public int TruncationLength { get; set; } = 50;

		public string Value
		{
			get;
			set;
		}

		public DatabaseSettingParameter()
		{
		}

		public DatabaseSettingParameter Clone()
		{
			return (DatabaseSettingParameter)this.MemberwiseClone();
		}

		public int CompareTo(DatabaseSettingParameter other)
		{
			if (other == null)
			{
				return 1;
			}
			return this.Name.CompareTo(other.Name);
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}