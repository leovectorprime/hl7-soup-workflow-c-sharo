using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.WCF;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class WebServiceSenderSetting : SenderWithResponseSetting, IWebServiceSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		private Encoding messageEncoding;

		public string Action
		{
			get;
			set;
		}

		public bool Authentication
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Web Service Sender";
			}
		}

		public override string Details
		{
			get
			{
				string text = "Call Web Service";
				if (!string.IsNullOrEmpty(this.ServiceName))
				{
					text = Helpers.CamelCaseToText(this.ServiceName);
					if (this.Operation != null)
					{
						text = Helpers.CamelCaseToText(this.Operation.Name);
					}
				}
				else if (this.Operation != null)
				{
					text = Helpers.CamelCaseToText(this.Operation.Name);
				}
				return string.Concat(text, " (WCF)");
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.WebServiceSender";
			}
		}

		public bool ManualConfiguration
		{
			get;
			set;
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public WebServiceOperation Operation
		{
			get;
			set;
		}

		public bool PassAuthenticationInSoapHeader
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public override bool ResponseNotAvailable
		{
			get
			{
				if (this.WaitForResponse)
				{
					return false;
				}
				return true;
			}
		}

		public string Server
		{
			get;
			set;
		}

		public string ServiceName
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditWebServiceSenderSetting";
			}
		}

		public string UserName
		{
			get;
			set;
		}

		public bool UseSoap12
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		public string Wsdl
		{
			get;
			set;
		}

		public WebServiceSenderSetting()
		{
		}
	}
}