using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class FileWriterSenderSetting : SenderSetting, IFileWriterSenderSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "TCP Sender";
			}
		}

		public override string Details
		{
			get
			{
				return string.Concat("Write to file ", this.GetFileName(this.FilePathToWrite));
			}
		}

		public string DirectoryToMoveInto
		{
			get;
			set;
		}

		public string FilePathToWrite
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.FileWriterSender";
			}
		}

		public int MaxRecordsPerFile
		{
			get;
			set;
		}

		public bool MoveIntoDirectoryOnComplete
		{
			get;
			set;
		}

		public ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditFileWriterSenderSetting";
			}
		}

		public FileWriterSenderSetting()
		{
			this.FilePathToWrite = "";
			this.MaxRecordsPerFile = 5000;
			this.DirectoryToMoveInto = "c:\\";
			this.MoveIntoDirectoryOnComplete = false;
		}

		public string DirectoryToMoveIntoRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.DirectoryToMoveInto);
		}

		public string FilePathToWriteRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.FilePathToWrite);
		}

		public string GetFileName(string filePath)
		{
			if (filePath == null || filePath.Length <= 40)
			{
				return filePath;
			}
			return string.Concat("...", filePath.Substring(filePath.Length - 40));
		}
	}
}