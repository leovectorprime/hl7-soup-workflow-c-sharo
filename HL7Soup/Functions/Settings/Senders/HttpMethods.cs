using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public enum HttpMethods
	{
		POST,
		GET,
		PUT,
		DELETE
	}
}