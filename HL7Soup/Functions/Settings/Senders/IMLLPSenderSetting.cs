using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IMLLPSenderSetting : ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IMLLPConnectionSetting
	{
		bool UseResponse
		{
			get;
			set;
		}

		bool WaitForResponse
		{
			get;
			set;
		}
	}
}