using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface ICodeSenderSetting : ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, ISettingWithVariables
	{
		string Code
		{
			get;
			set;
		}

		bool UseResponse
		{
			get;
			set;
		}

		List<string> VariableNames
		{
			get;
			set;
		}
	}
}