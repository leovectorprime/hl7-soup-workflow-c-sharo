using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.WCF;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class WebServiceEditorSetting : EditorSendingBehaviourSetting, IWebServiceEditorSetting, IEditorSendingBehaviourSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IWebServiceSenderSetting, ISenderWithResponseSetting
	{
		private Encoding messageEncoding;

		public string Action
		{
			get;
			set;
		}

		public bool Authentication
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Web Service Sender";
			}
		}

		public override string Details
		{
			get
			{
				string text = "Call Web Service";
				if (!string.IsNullOrEmpty(this.ServiceName))
				{
					text = Helpers.CamelCaseToText(this.ServiceName);
					if (this.Operation != null)
					{
						text = Helpers.CamelCaseToText(this.Operation.Name);
					}
				}
				else if (this.Operation != null)
				{
					text = Helpers.CamelCaseToText(this.Operation.Name);
				}
				return string.Concat(text, " (WCF)");
			}
		}

		public virtual bool DifferentResponseMessageType
		{
			get;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.EditorSendingBehaviors.WebServiceEditor";
			}
		}

		public bool LoadResult
		{
			get;
			set;
		}

		public bool ManualConfiguration
		{
			get;
			set;
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public WebServiceOperation Operation
		{
			get;
			set;
		}

		public bool PassAuthenticationInSoapHeader
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public string ResponseMessageTemplate
		{
			get;
			set;
		}

		public HL7Soup.Functions.MessageTypes ResponseMessageType
		{
			get;
		}

		public bool ResponseNotAvailable
		{
			get
			{
				if (this.WaitForResponse)
				{
					return false;
				}
				return true;
			}
		}

		public string Server
		{
			get;
			set;
		}

		public string ServiceName
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditorSendingBehaviors.EditWebServiceEditorSetting";
			}
		}

		public string UserName
		{
			get;
			set;
		}

		public bool UseSoap12
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		public string Wsdl
		{
			get;
			set;
		}

		public WebServiceEditorSetting()
		{
			this.LoadResult = false;
			base.MessageType = HL7Soup.Functions.MessageTypes.XML;
		}

		public List<ISetting> GetAutomaticSenderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.AutomaticSendSettings))
			{
				ISetting filterSettingById = FunctionHelpers.GetFilterSettingById(base.AutomaticSendSettings);
				if (filterSettingById != null)
				{
					settings.Add(filterSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.AutomaticSendSettings]);
			}
			return settings;
		}

		public List<ISetting> GetSourceProviderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.SourceProviderSettings))
			{
				ISetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(base.SourceProviderSettings);
				if (transformerSettingById != null)
				{
					settings.Add(transformerSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.SourceProviderSettings]);
			}
			return settings;
		}
	}
}