using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public enum DataProviders
	{
		SqlClient,
		OracleClient,
		OleDb,
		Odbc
	}
}