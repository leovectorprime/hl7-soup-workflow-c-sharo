using HL7Soup;
using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IHttpSenderSetting : ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		bool Authentication
		{
			get;
			set;
		}

		string ContentType
		{
			get;
			set;
		}

		HttpMethods Method
		{
			get;
			set;
		}

		string Password
		{
			get;
			set;
		}

		string Server
		{
			get;
			set;
		}

		string UserName
		{
			get;
			set;
		}

		bool WaitForResponse
		{
			get;
			set;
		}

		string ServerRuntimeValue(WorkflowInstance workflowInstance);
	}
}