using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IWebServiceEditorSetting : IEditorSendingBehaviourSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IWebServiceSenderSetting, ISenderWithResponseSetting
	{
		bool LoadResult
		{
			get;
			set;
		}
	}
}