using HL7Soup.Functions.Settings;
using HL7Soup.Functions.WCF;
using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IWebServiceSenderSetting : ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		string Action
		{
			get;
			set;
		}

		bool Authentication
		{
			get;
			set;
		}

		bool ManualConfiguration
		{
			get;
			set;
		}

		WebServiceOperation Operation
		{
			get;
			set;
		}

		bool PassAuthenticationInSoapHeader
		{
			get;
			set;
		}

		string Password
		{
			get;
			set;
		}

		string Server
		{
			get;
			set;
		}

		string ServiceName
		{
			get;
			set;
		}

		string UserName
		{
			get;
			set;
		}

		bool UseSoap12
		{
			get;
			set;
		}

		bool WaitForResponse
		{
			get;
			set;
		}

		string Wsdl
		{
			get;
			set;
		}
	}
}