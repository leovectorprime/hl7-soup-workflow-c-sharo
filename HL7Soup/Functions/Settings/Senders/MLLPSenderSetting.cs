using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class MLLPSenderSetting : SenderWithResponseSetting, IMLLPSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IMLLPConnectionSetting
	{
		private char[] frameStart = new char[] { '\v' };

		private char[] frameEnd = new char[] { '\u001C', '\r' };

		private Encoding messageEncoding;

		public override string ConnectionTypeName
		{
			get
			{
				return "TCP Sender";
			}
		}

		public override string Details
		{
			get
			{
				return string.Concat("Send TCP ", (this.Server != "127.0.0.1" ? this.Server : string.Concat("Localhost : ", this.Port)));
			}
		}

		public char[] FrameEnd
		{
			get
			{
				return this.frameEnd;
			}
			set
			{
				this.frameEnd = value;
			}
		}

		public char[] FrameStart
		{
			get
			{
				return this.frameStart;
			}
			set
			{
				this.frameStart = value;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.MLLPSender";
			}
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public int Port
		{
			get;
			set;
		}

		public ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		public override bool ResponseNotAvailable
		{
			get
			{
				if (!this.UseResponse && !this.WaitForResponse)
				{
					return true;
				}
				if (!this.UseResponse)
				{
					return false;
				}
				bool waitForResponse = this.WaitForResponse;
				return false;
			}
		}

		public string Server
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditMLLPSenderSetting";
			}
		}

		public bool UseResponse
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		public MLLPSenderSetting()
		{
			this.Server = "127.0.0.1";
			this.Port = 22222;
		}
	}
}