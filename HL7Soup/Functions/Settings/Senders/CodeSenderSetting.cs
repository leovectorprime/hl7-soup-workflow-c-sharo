using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class CodeSenderSetting : SenderWithResponseSetting, ICodeSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, ISettingWithVariables
	{
		private Dictionary<string, IVariableCreator> variables;

		public string Code { get; set; } = "//The following code is just a indication for getting started for an Activity. Edit or delete as you wish. Use the variable workflowInstance and activityInstance to access the running states of the workflow.\r\n\r\n//Get the message template\r\nIHL7Message message = (IHL7Message)activityInstance.Message; //Use IHL7Message for HL7 Messages and IMessage for other message types\r\n\r\n//Get the response message (only available if the Response Message checkbox is ticked)\r\nIHL7Message responseMessage = (IHL7Message)activityInstance.ResponseMessage;\r\n\r\n//Copy a value from the inbound message (MSH-3) \r\nstring value = message.GetValueAtPath(\"MSH-3\");\r\n\r\n//Write the value to the response message (MSH-3)\r\nif (responseMessage!=null)\r\n{\r\n    responseMessage.SetValueAtPath(\"MSH-3\", value);\r\n}\r\n\r\n//Get a variable value\r\nstring receivedDate = workflowInstance.GetVariable(\"ReceivedDate\");\r\n\r\n//Convert an HL7 Date to a DateTime\r\nDateTime date = HL7Helpers.GetDateFromHL7Date(receivedDate);\r\n\r\n//Set a variable value\r\nworkflowInstance.SetVariable(\"MyVariable\",\"ValueOfMyVariable\");\r\n\r\n//Get all segments and loop over them\r\nIHL7Segments allSegments = message.GetSegments();\r\nforeach (IHL7Segment segment in allSegments)\r\n{\r\n    //Do Something\r\n}";

		public override string ConnectionTypeName
		{
			get
			{
				return "Run Code";
			}
		}

		public override string Details
		{
			get
			{
				return "Run Code";
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.CodeSender";
			}
		}

		public override bool ResponseNotAvailable
		{
			get
			{
				if (!this.UseResponse)
				{
					return true;
				}
				bool useResponse = this.UseResponse;
				return false;
			}
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditCodeSenderSetting";
			}
		}

		public bool UseResponse
		{
			get;
			set;
		}

		public List<string> VariableNames { get; set; } = new List<string>();

		public CodeSenderSetting()
		{
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.variables != null)
			{
				return this.variables;
			}
			this.variables = new Dictionary<string, IVariableCreator>();
			if (this.VariableNames != null)
			{
				foreach (string variableName in this.VariableNames)
				{
					this.variables[variableName] = new VariableCreator(variableName, "", true);
				}
			}
			return this.variables;
		}
	}
}