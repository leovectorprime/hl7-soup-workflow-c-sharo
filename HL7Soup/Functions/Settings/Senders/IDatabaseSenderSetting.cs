using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IDatabaseSenderSetting : ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		string ConnectionString
		{
			get;
			set;
		}

		DataProviders DataProvider
		{
			get;
			set;
		}

		List<DatabaseSettingParameter> Parameters
		{
			get;
		}
	}
}