using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.Senders
{
	[Serializable]
	public class HttpSenderSetting : SenderWithResponseSetting, IHttpSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		private Encoding messageEncoding;

		public bool Authentication
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Http Sender";
			}
		}

		public string ContentType
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				string str = "Call Http Service";
				if (!string.IsNullOrEmpty(this.Server))
				{
					str = string.Concat("Call ", this.Server);
				}
				return str;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Senders.HttpSender";
			}
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public HttpMethods Method
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public override bool ResponseNotAvailable
		{
			get
			{
				if (this.WaitForResponse)
				{
					return false;
				}
				return true;
			}
		}

		public string Server
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditHttpSenderSetting";
			}
		}

		public string UserName
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		public HttpSenderSetting()
		{
			this.ContentType = "text/plain";
			this.Method = HttpMethods.POST;
		}

		public string ServerRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.Server);
		}
	}
}