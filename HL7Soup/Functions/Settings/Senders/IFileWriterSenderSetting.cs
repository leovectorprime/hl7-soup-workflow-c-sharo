using HL7Soup;
using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.Senders
{
	public interface IFileWriterSenderSetting : ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		string DirectoryToMoveInto
		{
			get;
			set;
		}

		string FilePathToWrite
		{
			get;
			set;
		}

		int MaxRecordsPerFile
		{
			get;
			set;
		}

		bool MoveIntoDirectoryOnComplete
		{
			get;
			set;
		}

		string DirectoryToMoveIntoRuntimeValue(WorkflowInstance workflowInstance);

		string FilePathToWriteRuntimeValue(WorkflowInstance workflowInstance);
	}
}