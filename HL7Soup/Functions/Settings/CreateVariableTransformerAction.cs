using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CreateVariableTransformerAction : TransformerActionWithFormatting, IVariableCreator
	{
		private string variableName;

		private string sampleVariableValue;

		private bool sampleValueIsDefaultValue;

		public bool SampleValueIsDefaultValue
		{
			get
			{
				return this.sampleValueIsDefaultValue;
			}
			set
			{
				this.sampleValueIsDefaultValue = value;
				base.NotifyPropertyChanged("SampleValueIsDefaultValue");
			}
		}

		public string SampleVariableValue
		{
			get
			{
				return this.sampleVariableValue;
			}
			set
			{
				this.sampleVariableValue = value;
				base.NotifyPropertyChanged("SampleVariableValue");
			}
		}

		public override string TransformerName
		{
			get
			{
				return "Set variable value";
			}
		}

		public string VariableName
		{
			get
			{
				return this.variableName;
			}
			set
			{
				this.variableName = value;
				base.NotifyPropertyChanged("VariableName");
			}
		}

		public CreateVariableTransformerAction()
		{
		}

		public override void Highlight(string text, MessageSourceDirection direction, Guid setting)
		{
			if (string.IsNullOrEmpty(text))
			{
				base.IsHighlighted = false;
				return;
			}
			base.Highlight(text, direction, setting);
			if (base.IsHighlighted)
			{
				return;
			}
			if (this.VariableName == null || direction != MessageSourceDirection.variable || string.Concat("${", this.VariableName, "}").IndexOf(text, StringComparison.InvariantCultureIgnoreCase) <= -1)
			{
				return;
			}
			base.IsHighlighted = true;
		}

		IVariableCreator HL7Soup.Functions.Settings.IVariableCreator.Clone()
		{
			return (IVariableCreator)base.MemberwiseClone();
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			fromValue = Variable.ProcessFormat(fromValue, this);
			workflowInstance.SetVariable(this.VariableName, fromValue);
		}

		public override string ToString()
		{
			string[] variableName = new string[] { this.VariableName, " <- ", base.FromPath, " (", Enum.GetName(typeof(HL7Soup.Functions.MessageTypes), base.FromType), ") ", null };
			variableName[6] = base.FromSetting.ToString();
			return string.Concat(variableName);
		}
	}
}