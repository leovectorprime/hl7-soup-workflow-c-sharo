using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CustomTransformerAction : TransformerAction
	{
		private Dictionary<string, CreateParameterTransformerAction> parameters;

		private Dictionary<string, IVariableCreator> variables;

		public string CustomTransformerName
		{
			get;
			set;
		}

		public string CustomTransformerTypeName
		{
			get;
			set;
		}

		public Dictionary<string, CreateParameterTransformerAction> Parameters
		{
			get
			{
				return this.parameters;
			}
		}

		public override string TransformerName
		{
			get
			{
				return this.ToString();
			}
		}

		public CustomTransformerAction()
		{
			this.parameters = new Dictionary<string, CreateParameterTransformerAction>();
		}

		public override ITransformerAction Clone()
		{
			CustomTransformerAction strs = (CustomTransformerAction)base.Clone();
			strs.parameters = new Dictionary<string, CreateParameterTransformerAction>();
			foreach (CreateParameterTransformerAction value in this.parameters.Values)
			{
				strs.parameters[value.ParameterName] = (CreateParameterTransformerAction)value.Clone();
			}
			if (this.variables != null)
			{
				strs.variables = new Dictionary<string, IVariableCreator>();
				foreach (IVariableCreator variableCreator in this.variables.Values)
				{
					strs.variables[variableCreator.VariableName] = variableCreator.Clone();
				}
			}
			return strs;
		}

		public void CreateParameters(Type type)
		{
			foreach (Attribute attribute in new List<Attribute>(Attribute.GetCustomAttributes(type, typeof(ParameterAttribute))))
			{
				ParameterAttribute createParameterTransformerAction = attribute as ParameterAttribute;
				if (createParameterTransformerAction == null)
				{
					continue;
				}
				this.parameters[createParameterTransformerAction.Name] = new CreateParameterTransformerAction(createParameterTransformerAction);
			}
		}

		public Type GetCustomTransformerType()
		{
			Type type = Type.GetType(this.CustomTransformerTypeName);
			if (type == null)
			{
				throw new Exception(string.Concat("Cannot create the type ", this.CustomTransformerTypeName, ".  Is the containing assembly in the 'Custom Libraries' directory?"));
			}
			return type;
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.variables != null)
			{
				return this.variables;
			}
			this.variables = new Dictionary<string, IVariableCreator>();
			foreach (Attribute attribute in new List<Attribute>(Attribute.GetCustomAttributes(this.GetCustomTransformerType(), typeof(VariableAttribute))))
			{
				VariableAttribute variableCreator = attribute as VariableAttribute;
				if (variableCreator == null)
				{
					continue;
				}
				this.variables[variableCreator.Name] = new VariableCreator(variableCreator.Name, variableCreator.SampleValue, true);
			}
			return this.variables;
		}

		public override void Highlight(string text, MessageSourceDirection direction, Guid setting)
		{
			if (string.IsNullOrEmpty(text))
			{
				base.IsHighlighted = false;
				return;
			}
			base.Highlight(text, direction, setting);
			if (base.IsHighlighted)
			{
				return;
			}
			foreach (CreateParameterTransformerAction value in this.parameters.Values)
			{
				base.IsHighlighted = this.HighlightTextValue(value.FromPath, value.FromSetting, value.FromDirection, text, direction, setting);
				if (!base.IsHighlighted)
				{
					continue;
				}
				return;
			}
		}

		public override void Process(WorkflowInstance workflowInstance)
		{
			Type customTransformerType = this.GetCustomTransformerType();
			ICustomTransformer customTransformer = Activator.CreateInstance(customTransformerType) as ICustomTransformer;
			if (customTransformer == null)
			{
				throw new Exception(string.Concat("Creating instance of IIntegrationFunctionProvider returned null. ", customTransformerType.FullName));
			}
			Dictionary<string, string> strs = new Dictionary<string, string>();
			foreach (CreateParameterTransformerAction value in this.Parameters.Values)
			{
				string theValue = TransformerAction.GetTheValue(workflowInstance, value.FromPath, value.FromSetting, value.FromDirection, value.FromNamespaces);
				strs[value.ParameterName] = theValue;
			}
			customTransformer.Transform(workflowInstance, workflowInstance.CurrentActivityInstance.Message, strs);
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public override string ToString()
		{
			return this.CustomTransformerName;
		}

		public void Transform()
		{
		}
	}
}