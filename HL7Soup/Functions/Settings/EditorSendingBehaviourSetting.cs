using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class EditorSendingBehaviourSetting : SenderSetting, IEditorSendingBehaviourSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		public Guid AutomaticSendSettings
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Sending Behavior";
			}
		}

		public override bool DisableNotAvailable
		{
			get
			{
				return true;
			}
		}

		public override bool FiltersNotAvailable
		{
			get
			{
				return true;
			}
		}

		public override bool InboundMessageNotAvailable
		{
			get
			{
				return false;
			}
		}

		public Guid SourceProviderSettings
		{
			get;
			set;
		}

		public override bool TransformersNotAvailable
		{
			get
			{
				return true;
			}
		}

		protected EditorSendingBehaviourSetting()
		{
		}
	}
}