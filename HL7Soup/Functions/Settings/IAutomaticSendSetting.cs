using System;

namespace HL7Soup.Functions.Settings
{
	public interface IAutomaticSendSetting : ISetting
	{
		TimeSpan Delay
		{
			get;
			set;
		}

		bool InfinateLoop
		{
			get;
			set;
		}

		bool MoveNext
		{
			get;
			set;
		}

		bool MoveStatic
		{
			get;
			set;
		}
	}
}