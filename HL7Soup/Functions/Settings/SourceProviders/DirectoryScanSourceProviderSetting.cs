using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.SourceProviders
{
	[Serializable]
	public class DirectoryScanSourceProviderSetting : SourceProviderSetting, IDirectoryScanSourceProviderSetting, ISourceProviderSetting, ISetting, IDirectoryScanSetting, IFunctionWithMessageSetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Directory Scan";
			}
		}

		public bool DeleteFileOnComplete
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public override string Details
		{
			get
			{
				return string.Concat("Directory Scan ", this.DirectoryPath);
			}
		}

		public string DirectoryFilter
		{
			get;
			set;
		}

		public string DirectoryPath
		{
			get;
			set;
		}

		public string DirectoryToMoveInto
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public bool EndAfterProcessing
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.SourceProviders.DirectoryScan";
			}
		}

		public HL7Soup.Functions.MessageTypes MessageType
		{
			get;
			set;
		}

		public bool MoveIntoDirectoryOnComplete
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public bool SearchForNewFiles
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditDirectoryScanSourceProviderSetting";
			}
		}

		public DirectoryScanSourceProviderSetting()
		{
			this.DirectoryPath = "c:\\";
			this.DirectoryFilter = "*.hl7";
			this.SearchForNewFiles = true;
			this.MessageType = HL7Soup.Functions.MessageTypes.HL7V2;
		}
	}
}