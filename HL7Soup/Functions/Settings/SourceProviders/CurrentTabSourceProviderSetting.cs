using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.SourceProviders
{
	[Serializable]
	public class CurrentTabSourceProviderSetting : SourceProviderSetting, ICurrentTabSourceProviderSetting, ISourceProviderSetting, ISetting
	{
		public override string Details
		{
			get
			{
				string str;
				if (this.StartLocation == HL7Soup.Functions.Settings.SourceProviders.StartLocation.CurrentItem)
				{
					str = string.Concat("from selected", (this.ReverseOrder ? " down" : " up"));
				}
				else
				{
					str = (this.ReverseOrder ? "from top down" : "from bottom up");
				}
				return string.Concat("Current Tab ", str);
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.SourceProviders.CurrentTab";
			}
		}

		public bool ReverseOrder
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditCurrentTabSourceProviderSetting";
			}
		}

		public HL7Soup.Functions.Settings.SourceProviders.StartLocation StartLocation
		{
			get;
			set;
		}

		public CurrentTabSourceProviderSetting()
		{
		}
	}
}