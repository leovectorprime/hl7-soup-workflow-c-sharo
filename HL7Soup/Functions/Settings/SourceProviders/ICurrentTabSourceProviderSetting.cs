using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.SourceProviders
{
	public interface ICurrentTabSourceProviderSetting : ISourceProviderSetting, ISetting
	{
		bool ReverseOrder
		{
			get;
			set;
		}

		HL7Soup.Functions.Settings.SourceProviders.StartLocation StartLocation
		{
			get;
			set;
		}
	}
}