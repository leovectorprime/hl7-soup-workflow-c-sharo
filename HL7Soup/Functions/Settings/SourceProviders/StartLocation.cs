using System;

namespace HL7Soup.Functions.Settings.SourceProviders
{
	public enum StartLocation
	{
		CurrentItem,
		FirstItem
	}
}