using HL7Soup.Functions.Settings;

namespace HL7Soup.Functions.Settings.SourceProviders
{
	public interface IDirectoryScanSourceProviderSetting : ISourceProviderSetting, ISetting, IDirectoryScanSetting, IFunctionWithMessageSetting
	{

	}
}