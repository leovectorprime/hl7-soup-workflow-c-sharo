using HL7Soup;
using HL7Soup.MessageFilters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class BeginConditionalTransformerAction : TransformerAction, ITransformerActionWithPair
	{
		private bool isPairSelected;

		public bool ConditionResult
		{
			get;
			set;
		}

		public ObservableCollection<MessageFilter> Filters { get; set; } = new ObservableCollection<MessageFilter>();

		public bool IsPairSelected
		{
			get
			{
				return this.isPairSelected;
			}
			set
			{
				this.isPairSelected = value;
				base.NotifyPropertyChanged("IsPairSelected");
			}
		}

		public List<MessageFilter> NewArrangedFilters
		{
			get;
			set;
		}

		public int PairId
		{
			get;
			set;
		}

		public override string TransformerName
		{
			get
			{
				return "Begin Conditional";
			}
		}

		public BeginConditionalTransformerAction()
		{
		}

		public override ITransformerAction Clone()
		{
			BeginConditionalTransformerAction observableCollection = (BeginConditionalTransformerAction)base.Clone();
			if (this.NewArrangedFilters != null)
			{
				observableCollection.Filters = new ObservableCollection<MessageFilter>(this.NewArrangedFilters);
			}
			return observableCollection;
		}

		public override void Highlight(string text, MessageSourceDirection direction, Guid setting)
		{
			if (string.IsNullOrEmpty(text))
			{
				base.IsHighlighted = false;
				return;
			}
			base.Highlight(text, direction, setting);
			if (base.IsHighlighted)
			{
				return;
			}
			foreach (MessageFilter filter in this.Filters)
			{
				if (filter.ComparerUsesValue())
				{
					base.IsHighlighted = this.HighlightTextValue(filter.ToPath, filter.ToSetting, filter.ToDirection, text, direction, setting);
					if (base.IsHighlighted)
					{
						return;
					}
				}
				base.IsHighlighted = this.HighlightTextValue(filter.Path, filter.FromSetting, filter.FromDirection, text, direction, setting);
				if (!base.IsHighlighted)
				{
					continue;
				}
				return;
			}
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			this.ConditionResult = MessageFilter.EvaluateConditions(workflowInstance, this.Filters.ToList<MessageFilter>());
		}

		public override string ToString()
		{
			return "Begin Conditional";
		}
	}
}