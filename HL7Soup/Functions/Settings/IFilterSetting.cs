using HL7Soup.MessageFilters;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public interface IFilterSetting : ISetting, ITransformerSetting, ISettingWithVariables
	{
		List<MessageFilter> Filters
		{
			get;
		}
	}
}