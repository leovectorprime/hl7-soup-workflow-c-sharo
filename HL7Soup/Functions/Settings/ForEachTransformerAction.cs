using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using HL7Soup.MessageFilters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class ForEachTransformerAction : TransformerAction, ITransformerActionWithPair
	{
		private string tempPath = "";

		private bool isPairSelected;

		private List<string> SiblingValues = new List<string>();

		public bool IsPairSelected
		{
			get
			{
				return this.isPairSelected;
			}
			set
			{
				this.isPairSelected = value;
				base.NotifyPropertyChanged("IsPairSelected");
			}
		}

		public int Iterator
		{
			get;
			set;
		}

		public List<MessageFilter> NewArrangedFilters
		{
			get;
			internal set;
		}

		public int NextLocation
		{
			get;
			set;
		}

		public int PairId
		{
			get;
			set;
		}

		public override string TransformerName
		{
			get
			{
				return "For Each";
			}
		}

		public ForEachTransformerAction()
		{
		}

		public override void PrepareItteration(WorkflowInstance workflowInstance)
		{
			this.Iterator = 0;
			workflowInstance.SetVariable("ForEachIterator", "0");
			this.SiblingValues = new List<string>();
			this.NextLocation = 0;
			this.tempPath = "";
		}

		public override void Process(WorkflowInstance workflowInstance)
		{
			int iterator;
			if (this.Iterator == 0)
			{
				IMessage message = workflowInstance.GetMessage(base.FromSetting, base.FromDirection);
				if (message != null)
				{
					XMLMessage xMLMessage = message as XMLMessage;
					if (xMLMessage != null)
					{
						xMLMessage.CreateNamespaceManager(base.FromNamespaces);
					}
					this.tempPath = base.FromPath;
					HL7Soup.Functions.MessageTypes fromType = base.FromType;
					switch (fromType)
					{
						case HL7Soup.Functions.MessageTypes.HL7V2:
						{
							IHL7Message hL7Message = message as IHL7Message;
							if (hL7Message == null)
							{
								break;
							}
							IHL7Part part = hL7Message.GetPart(base.FromPath);
							if (part == null)
							{
								break;
							}
							IHL7Field hL7Field = part as IHL7Field;
							if (hL7Field == null)
							{
								IHL7Segment hL7Segment = part as IHL7Segment;
								if (hL7Segment == null)
								{
									throw new Exception(string.Concat("The 'For each' transformer is bound to the path '", base.FromPath, "' which is not a repeatable value.  You must bind either to a Field or a Segment Value.  E.g. PID-13 is all the patients home phone numbers.  OBX is all the OBX segments."));
								}
								using (IEnumerator<IHL7Segment> enumerator = hL7Message.GetSegments(hL7Segment.Header).GetEnumerator())
								{
									while (enumerator.MoveNext())
									{
										IHL7Segment current = enumerator.Current;
										this.SiblingValues.Add(current.Text);
									}
									break;
								}
							}
							else
							{
								using (IEnumerator<IHL7Field> enumerator1 = hL7Field.GetRelatedRepeatFields().GetEnumerator())
								{
									while (enumerator1.MoveNext())
									{
										IHL7Field current1 = enumerator1.Current;
										this.SiblingValues.Add(current1.Text);
									}
									break;
								}
							}
							break;
						}
						case HL7Soup.Functions.MessageTypes.HL7V3:
						case HL7Soup.Functions.MessageTypes.XML:
						{
							if (base.FromPath.EndsWith("]"))
							{
								throw new Exception(string.Concat("The path in a for each transformer should not end with an explicit instance.  For instance the path Customer/phonenumber[1] implies that you want to loop over every instance of the first phone number rather than ever phone number.  The path should have been Customer/phonenumber. You need to remove the instance [x] from end of this path: ", base.FromPath));
							}
							this.tempPath = string.Concat(base.FromPath, "[1]");
							for (int i = 1; i < 100000; i++)
							{
								string structureAtPath = ((IXmlMessage)message).GetStructureAtPath(string.Concat(base.FromPath, string.Format("[{0}]", i)));
								if (string.IsNullOrEmpty(structureAtPath))
								{
									if (this.Iterator != -1)
									{
										this.Iterator = this.Iterator + 1;
									}
									else
									{
										this.Iterator = 1;
									}
									iterator = this.Iterator;
									workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
									if (this.SiblingValues.Count >= this.Iterator)
									{
										this.SetTheFromValue(workflowInstance, this.SiblingValues[this.Iterator - 1], this.tempPath);
										return;
									}
									if (this.Iterator > 0 && this.SiblingValues.Count > 0)
									{
										this.SetTheFromValue(workflowInstance, this.SiblingValues[0], this.tempPath);
									}
									this.SiblingValues = new List<string>();
									iterator = this.Iterator - 1;
									workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
									this.Iterator = -1;
									return;
								}
								this.SiblingValues.Add(structureAtPath);
							}
							break;
						}
						case HL7Soup.Functions.MessageTypes.FHIR:
						{
							if (base.FromPath.EndsWith("]"))
							{
								throw new Exception(string.Concat("The path in a for each transformer should not end with an explicit instance.  For instance the path Customer/phonenumber[1] implies that you want to loop over every instance of the first phone number rather than ever phone number.  The path should have been Customer/phonenumber. You need to remove the instance [x] from end of this path: ", base.FromPath));
							}
							this.tempPath = string.Concat(base.FromPath, "[1]");
							for (int j = 1; j < 100000; j++)
							{
								string str = ((IJsonMessage)message).GetStructureAtPath(string.Concat(base.FromPath, string.Format("[{0}]", j)));
								if (string.IsNullOrEmpty(str))
								{
									if (this.Iterator != -1)
									{
										this.Iterator = this.Iterator + 1;
									}
									else
									{
										this.Iterator = 1;
									}
									iterator = this.Iterator;
									workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
									if (this.SiblingValues.Count >= this.Iterator)
									{
										this.SetTheFromValue(workflowInstance, this.SiblingValues[this.Iterator - 1], this.tempPath);
										return;
									}
									if (this.Iterator > 0 && this.SiblingValues.Count > 0)
									{
										this.SetTheFromValue(workflowInstance, this.SiblingValues[0], this.tempPath);
									}
									this.SiblingValues = new List<string>();
									iterator = this.Iterator - 1;
									workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
									this.Iterator = -1;
									return;
								}
								this.SiblingValues.Add(str);
							}
							break;
						}
						default:
						{
							if (fromType == HL7Soup.Functions.MessageTypes.JSON)
							{
								goto case HL7Soup.Functions.MessageTypes.FHIR;
							}
							throw new Exception(string.Concat("The message type ", Helpers.GetEnumDescription(base.FromType), " does not support iteration, so cannot be the reference of a ForEach transformer."));
						}
					}
				}
			}
			if (this.Iterator != -1)
			{
				this.Iterator = this.Iterator + 1;
			}
			else
			{
				this.Iterator = 1;
			}
			iterator = this.Iterator;
			workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
			if (this.SiblingValues.Count >= this.Iterator)
			{
				this.SetTheFromValue(workflowInstance, this.SiblingValues[this.Iterator - 1], this.tempPath);
				return;
			}
			if (this.Iterator > 0 && this.SiblingValues.Count > 0)
			{
				this.SetTheFromValue(workflowInstance, this.SiblingValues[0], this.tempPath);
			}
			this.SiblingValues = new List<string>();
			iterator = this.Iterator - 1;
			workflowInstance.SetVariable("ForEachIterator", iterator.ToString());
			this.Iterator = -1;
		}

		public void SetTheFromValue(WorkflowInstance workflowInstance, string fromValue, string path)
		{
			IMessage message = workflowInstance.GetMessage(base.FromSetting, base.FromDirection);
			if (message == null)
			{
				return;
			}
			HL7Soup.Functions.MessageTypes fromType = base.FromType;
			if (fromType == HL7Soup.Functions.MessageTypes.HL7V2)
			{
				message.SetStructureAtPath(path, fromValue);
				return;
			}
			if ((int)fromType - (int)HL7Soup.Functions.MessageTypes.HL7V3 > (int)HL7Soup.Functions.MessageTypes.HL7V3 && fromType != HL7Soup.Functions.MessageTypes.JSON)
			{
				throw new Exception(string.Concat("The message type ", Helpers.GetEnumDescription(base.FromType), " does not support iteration, so cannot be the reference of a ForEach transformer."));
			}
			message.SetStructureAtPath(path, fromValue);
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public override string ToString()
		{
			return "For Each";
		}
	}
}