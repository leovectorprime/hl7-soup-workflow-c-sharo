using System;

namespace HL7Soup.Functions.Settings
{
	public interface ISettingWithFilters
	{
		Guid Filters
		{
			get;
			set;
		}

		bool FiltersNotAvailable
		{
			get;
			set;
		}
	}
}