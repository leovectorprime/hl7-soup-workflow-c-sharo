using HL7Soup;
using System;

namespace HL7Soup.Functions.Settings
{
	public interface ISenderSetting : IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		bool InboundMessageNotAvailable
		{
			get;
		}

		string MessageTemplate
		{
			get;
			set;
		}

		bool UserCanEditTemplate
		{
			get;
		}

		string MessageTemplateRuntimeValue(WorkflowInstance workflowInstance);
	}
}