using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public interface ISettingWithActivities
	{
		List<Guid> Activities
		{
			get;
			set;
		}
	}
}