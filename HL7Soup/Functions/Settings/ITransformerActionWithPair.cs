using System;

namespace HL7Soup.Functions.Settings
{
	public interface ITransformerActionWithPair
	{
		bool IsPairSelected
		{
			get;
			set;
		}

		int PairId
		{
			get;
			set;
		}
	}
}