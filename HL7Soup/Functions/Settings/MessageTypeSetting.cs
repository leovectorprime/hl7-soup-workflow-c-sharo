using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class MessageTypeSetting : Setting, IMessageTypeSetting, ISetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Message Type";
			}
		}

		protected MessageTypeSetting()
		{
		}
	}
}