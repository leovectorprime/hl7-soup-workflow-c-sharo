using HL7Soup.Functions;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class TransformerActionWithFormatting : TransformerAction, IValueCanBeFormatted
	{
		public Encodings Encoding
		{
			get;
			set;
		}

		public int End
		{
			get;
			set;
		}

		public string Format
		{
			get;
			set;
		}

		public bool hasFormat
		{
			get
			{
				return !string.IsNullOrEmpty(this.Format);
			}
		}

		public bool IsActivityBinding
		{
			get;
			set;
		}

		public int Start
		{
			get;
			set;
		}

		public TextFormats TextFormat
		{
			get;
			set;
		}

		public HL7Soup.Functions.Truncation Truncation
		{
			get;
			set;
		}

		public int TruncationLength { get; set; } = 50;

		protected TransformerActionWithFormatting()
		{
		}
	}
}