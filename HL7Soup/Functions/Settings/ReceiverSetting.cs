using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class ReceiverSetting : Setting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern
	{
		private Guid filters = Guid.Empty;

		private List<Guid> activities = new List<Guid>();

		private bool addIncomingMessageToCurrentTab;

		private string workflowPatternName = "";

		public List<Guid> Activities
		{
			get
			{
				return this.activities;
			}
			set
			{
				this.activities = value;
			}
		}

		public bool AddIncomingMessageToCurrentTab
		{
			get
			{
				return this.addIncomingMessageToCurrentTab;
			}
			set
			{
				this.addIncomingMessageToCurrentTab = value;
			}
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Receiver";
			}
		}

		public bool Disabled
		{
			get;
			set;
		}

		public virtual bool DisableNotAvailable
		{
			get;
			set;
		}

		public Guid Filters
		{
			get
			{
				return this.filters;
			}
			set
			{
				this.filters = value;
			}
		}

		public virtual bool FiltersNotAvailable
		{
			get;
			set;
		}

		public DateTime LastModified { get; set; } = DateTime.MinValue;

		public HL7Soup.Functions.MessageTypes MessageType
		{
			get;
			set;
		}

		public string ReceivedMessageTemplate
		{
			get;
			set;
		}

		public string WorkflowPatternName
		{
			get
			{
				if (string.IsNullOrEmpty(this.workflowPatternName))
				{
					return this.Name;
				}
				return this.workflowPatternName;
			}
			set
			{
				this.workflowPatternName = value;
			}
		}

		protected ReceiverSetting()
		{
		}

		public List<ISetting> GetActivityInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Guid activity in this.Activities)
			{
				if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(activity))
				{
					ISetting activitySettingById = FunctionHelpers.GetActivitySettingById(activity);
					if (activitySettingById == null)
					{
						continue;
					}
					settings.Add(activitySettingById);
				}
				else
				{
					settings.Add(settingsBeingEdited[activity]);
				}
			}
			return settings;
		}

		public override List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> childSettingInstances = base.GetChildSettingInstances(settingsBeingEdited);
			childSettingInstances.AddRange(this.GetFilterInstances(settingsBeingEdited));
			childSettingInstances.AddRange(this.GetActivityInstances(settingsBeingEdited));
			return childSettingInstances;
		}

		public List<ISetting> GetFilterInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(this.Filters))
			{
				ISetting filterSettingById = FunctionHelpers.GetFilterSettingById(this.Filters);
				if (filterSettingById != null)
				{
					settings.Add(filterSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[this.Filters]);
			}
			return settings;
		}
	}
}