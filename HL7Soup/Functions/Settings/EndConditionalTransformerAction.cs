using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class EndConditionalTransformerAction : TransformerAction, ITransformerActionWithPair
	{
		private bool isPairSelected;

		public bool IsPairSelected
		{
			get
			{
				return this.isPairSelected;
			}
			set
			{
				this.isPairSelected = value;
				base.NotifyPropertyChanged("IsPairSelected");
			}
		}

		public int PairId
		{
			get;
			set;
		}

		public override string TransformerName
		{
			get
			{
				return "End Conditional";
			}
		}

		public EndConditionalTransformerAction()
		{
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public override string ToString()
		{
			return "End Conditional";
		}
	}
}