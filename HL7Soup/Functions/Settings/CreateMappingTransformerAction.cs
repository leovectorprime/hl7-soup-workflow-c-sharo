using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CreateMappingTransformerAction : TransformerActionWithFormatting
	{
		private HL7Soup.Functions.MessageTypes toType;

		private string toPath;

		private bool allowMessageStructureToChange;

		private Guid toSetting;

		private MessageSourceDirection toDirection;

		[DefaultValue(false)]
		public bool AllowMessageStructureToChange
		{
			get
			{
				return this.allowMessageStructureToChange;
			}
			set
			{
				this.allowMessageStructureToChange = value;
				base.NotifyPropertyChanged("AllowMessageStructureToChange");
			}
		}

		public MessageSourceDirection ToDirection
		{
			get
			{
				return this.toDirection;
			}
			set
			{
				this.toDirection = value;
				base.NotifyPropertyChanged("ToDirection");
			}
		}

		public Dictionary<string, string> ToNamespaces
		{
			get;
			set;
		}

		public string ToPath
		{
			get
			{
				return this.toPath;
			}
			set
			{
				this.toPath = value;
				base.NotifyPropertyChanged("ToPath");
			}
		}

		public Guid ToSetting
		{
			get
			{
				return this.toSetting;
			}
			set
			{
				this.toSetting = value;
				base.NotifyPropertyChanged("ToSetting");
			}
		}

		public HL7Soup.Functions.MessageTypes ToType
		{
			get
			{
				return this.toType;
			}
			set
			{
				this.toType = value;
				base.NotifyPropertyChanged("ToType");
			}
		}

		public override string TransformerName
		{
			get
			{
				return "Create mapping between messages";
			}
		}

		public CreateMappingTransformerAction()
		{
		}

		public override void Highlight(string text, MessageSourceDirection direction, Guid setting)
		{
			if (string.IsNullOrEmpty(text))
			{
				base.IsHighlighted = false;
				return;
			}
			base.Highlight(text, direction, setting);
			if (base.IsHighlighted)
			{
				return;
			}
			base.IsHighlighted = this.HighlightTextValue(this.ToPath, this.toSetting, this.toDirection, text, direction, setting);
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
			fromValue = Variable.ProcessFormat(fromValue, this);
			IMessage message = workflowInstance.GetMessage(this.ToSetting, this.ToDirection);
			if (message != null)
			{
				XMLMessage xMLMessage = message as XMLMessage;
				if (xMLMessage != null)
				{
					xMLMessage.CreateNamespaceManager(this.ToNamespaces);
				}
				if (this.AllowMessageStructureToChange)
				{
					message.SetStructureAtPath(this.ToPath, fromValue);
					return;
				}
				message.SetValueAtPath(this.ToPath, fromValue);
			}
		}

		public override string ToString()
		{
			string[] toPath = new string[] { this.ToPath, " (", Enum.GetName(typeof(HL7Soup.Functions.MessageTypes), this.ToType), ") <- ", base.FromPath, " (", Enum.GetName(typeof(HL7Soup.Functions.MessageTypes), base.FromType), ") ", null };
			toPath[8] = base.FromSetting.ToString();
			return string.Concat(toPath);
		}
	}
}