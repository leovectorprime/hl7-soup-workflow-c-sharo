using HL7Soup;
using System;

namespace HL7Soup.Functions.Settings
{
	public interface IReceiverWithResponseSetting : IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers
	{
		string ErrorMessage
		{
			get;
			set;
		}

		string RejectMessage
		{
			get;
			set;
		}

		ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		string ResponseMessageTemplate
		{
			get;
			set;
		}

		bool ReturnApplicationAccept
		{
			get;
			set;
		}

		bool ReturnApplicationError
		{
			get;
			set;
		}

		bool ReturnApplicationReject
		{
			get;
			set;
		}

		bool ReturnCustomResponse
		{
			get;
			set;
		}

		bool ReturnNoResponse
		{
			get;
			set;
		}

		bool ReturnResponseFromActivity
		{
			get;
			set;
		}

		Guid ReturnResponseFromActivityId
		{
			get;
			set;
		}

		string ReturnResponseFromActivityName
		{
			get;
			set;
		}

		string ResponseMessageTemplateRuntimeValue(WorkflowInstance workflowInstance);
	}
}