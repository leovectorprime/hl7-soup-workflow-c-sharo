using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class TransformerAction : ITransformerAction, INotifyPropertyChanged
	{
		private string fromPath;

		private Guid fromSetting;

		private HL7Soup.Functions.MessageTypes fromType;

		private MessageSourceDirection fromDirection = MessageSourceDirection.variable;

		[NonSerialized]
		private bool isHighlighted;

		private int indentDepth;

		public string Description
		{
			get
			{
				return this.ToString();
			}
		}

		public MessageSourceDirection FromDirection
		{
			get
			{
				return this.fromDirection;
			}
			set
			{
				this.fromDirection = value;
				this.NotifyPropertyChanged("FromDirection");
			}
		}

		public Dictionary<string, string> FromNamespaces
		{
			get;
			set;
		}

		public string FromPath
		{
			get
			{
				return this.fromPath;
			}
			set
			{
				this.fromPath = value;
				this.NotifyPropertyChanged("FromPath");
			}
		}

		public Guid FromSetting
		{
			get
			{
				return this.fromSetting;
			}
			set
			{
				this.fromSetting = value;
				this.NotifyPropertyChanged("FromSetting");
			}
		}

		public HL7Soup.Functions.MessageTypes FromType
		{
			get
			{
				return this.fromType;
			}
			set
			{
				this.fromType = value;
				this.NotifyPropertyChanged("FromType");
			}
		}

		public int IndentDepth
		{
			get
			{
				return this.indentDepth;
			}
			set
			{
				this.indentDepth = value;
				this.NotifyPropertyChanged("IndentDepth");
			}
		}

		public bool IsHighlighted
		{
			get
			{
				return this.isHighlighted;
			}
			set
			{
				this.isHighlighted = value;
				this.NotifyPropertyChanged("IsHighlighted");
			}
		}

		public abstract string TransformerName
		{
			get;
		}

		protected TransformerAction()
		{
		}

		public virtual ITransformerAction Clone()
		{
			return (TransformerAction)this.MemberwiseClone();
		}

		public virtual string GetTheFromValue(WorkflowInstance workflowInstance)
		{
			return TransformerAction.GetTheValue(workflowInstance, this.FromPath, this.FromSetting, this.FromDirection, this.FromNamespaces);
		}

		public static string GetTheValue(WorkflowInstance workflowInstance, string path, Guid setting, MessageSourceDirection direction, Dictionary<string, string> namespaces)
		{
			string valueAtPath = "";
			if (direction != MessageSourceDirection.variable)
			{
				IMessage message = workflowInstance.GetMessage(setting, direction);
				if (message != null)
				{
					XMLMessage xMLMessage = message as XMLMessage;
					if (xMLMessage != null)
					{
						xMLMessage.CreateNamespaceManager(namespaces);
					}
					valueAtPath = message.GetValueAtPath(path);
				}
			}
			else
			{
				foreach (string listOfVariablesInText in FunctionHelpers.GetListOfVariablesInText(path))
				{
					path = path.Replace(string.Concat("${", listOfVariablesInText, "}"), workflowInstance.GetVariable(listOfVariablesInText));
				}
				valueAtPath = path;
			}
			return valueAtPath;
		}

		public virtual void Highlight(string text, MessageSourceDirection direction, Guid setting)
		{
			if (string.IsNullOrEmpty(text))
			{
				this.IsHighlighted = false;
				return;
			}
			this.IsHighlighted = this.HighlightTextValue(this.FromPath, this.fromSetting, this.FromDirection, text, direction, setting);
		}

		protected virtual bool HighlightTextValue(string path, Guid settingPath, MessageSourceDirection settingDirection, string text, MessageSourceDirection direction, Guid setting)
		{
			if (path != null)
			{
				if (direction == MessageSourceDirection.variable)
				{
					if (path.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) > -1)
					{
						return true;
					}
				}
				else if (direction == settingDirection && setting == settingPath && (path.Equals(text, StringComparison.InvariantCultureIgnoreCase) || Regex.Match(path, string.Concat("\\b", text, "\\b"), RegexOptions.IgnoreCase).Success))
				{
					return true;
				}
			}
			return false;
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public virtual void Prepare(WorkflowInstance workflowInstance)
		{
		}

		public virtual void PrepareItteration(WorkflowInstance workflowInstance)
		{
		}

		public virtual void Process(WorkflowInstance workflowInstance)
		{
			this.SetTheToValue(workflowInstance, this.GetTheFromValue(workflowInstance));
		}

		public static void SetIndentDepth(IList<ITransformerAction> transformers)
		{
			int num = 5;
			int num1 = 0;
			int num2 = 0;
			Stack<int> nums = new Stack<int>();
			Stack<int> nums1 = new Stack<int>();
			foreach (ITransformerAction transformer in transformers)
			{
				num2++;
				if (transformer is EndConditionalTransformerAction)
				{
					num1 -= num;
					if (num1 < 0)
					{
						num1 = 0;
					}
					if (nums1.Count <= 0)
					{
						((EndConditionalTransformerAction)transformer).PairId = 0;
					}
					else
					{
						((EndConditionalTransformerAction)transformer).PairId = nums1.Pop();
					}
				}
				else if (transformer is NextTransformerAction)
				{
					num1 -= num;
					if (num1 < 0)
					{
						num1 = 0;
					}
					if (nums.Count <= 0)
					{
						((NextTransformerAction)transformer).PairId = 0;
					}
					else
					{
						((NextTransformerAction)transformer).PairId = nums.Pop();
					}
				}
				transformer.IndentDepth = num1;
				if (!(transformer is BeginConditionalTransformerAction))
				{
					if (!(transformer is ForEachTransformerAction))
					{
						continue;
					}
					num1 += num;
					((ForEachTransformerAction)transformer).PairId = num2;
					nums.Push(num2);
				}
				else
				{
					num1 += num;
					((BeginConditionalTransformerAction)transformer).PairId = num2;
					nums1.Push(num2);
				}
			}
		}

		public abstract void SetTheToValue(WorkflowInstance workflowInstance, string fromValue);

		public event PropertyChangedEventHandler PropertyChanged;
	}
}