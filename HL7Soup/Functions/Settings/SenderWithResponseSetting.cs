using HL7Soup;
using HL7Soup.Functions;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class SenderWithResponseSetting : SenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Sender";
			}
		}

		public virtual bool DifferentResponseMessageType
		{
			get;
			set;
		}

		public string ResponseMessageTemplate
		{
			get;
			set;
		}

		public HL7Soup.Functions.MessageTypes ResponseMessageType
		{
			get
			{
				return JustDecompileGenerated_get_ResponseMessageType();
			}
			set
			{
				JustDecompileGenerated_set_ResponseMessageType(value);
			}
		}

		private HL7Soup.Functions.MessageTypes JustDecompileGenerated_ResponseMessageType_k__BackingField;

		public HL7Soup.Functions.MessageTypes JustDecompileGenerated_get_ResponseMessageType()
		{
			return this.JustDecompileGenerated_ResponseMessageType_k__BackingField;
		}

		public void JustDecompileGenerated_set_ResponseMessageType(HL7Soup.Functions.MessageTypes value)
		{
			this.JustDecompileGenerated_ResponseMessageType_k__BackingField = value;
		}

		public virtual bool ResponseNotAvailable
		{
			get;
			set;
		}

		protected SenderWithResponseSetting()
		{
		}

		public string ResponseMessageTemplateRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.ResponseMessageTemplate);
		}
	}
}