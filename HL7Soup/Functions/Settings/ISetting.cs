using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public interface ISetting
	{
		string ConnectionTypeName
		{
			get;
		}

		string Details
		{
			get;
		}

		string FunctionProviderType
		{
			get;
		}

		Guid Id
		{
			get;
			set;
		}

		string Name
		{
			get;
			set;
		}

		string SettingDialogType
		{
			get;
		}

		int Version
		{
			get;
			set;
		}

		List<Guid> GetAllActivitiesAtRootLevel();

		List<Guid> GetAllActivitiesAtRootLevel(Guid lastSettingId);

		List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited);

		void SwapActivityIds(Dictionary<Guid, Guid> oldAndNewGuidDictionary);

		string Upgrade();
	}
}