using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Settings
{
	public interface ISenderWithResponseSetting : ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		bool DifferentResponseMessageType
		{
			get;
		}

		string ResponseMessageTemplate
		{
			get;
			set;
		}

		HL7Soup.Functions.MessageTypes ResponseMessageType
		{
			get;
		}

		bool ResponseNotAvailable
		{
			get;
		}
	}
}