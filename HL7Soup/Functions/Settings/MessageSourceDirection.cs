using System;

namespace HL7Soup.Functions.Settings
{
	public enum MessageSourceDirection
	{
		inbound,
		outbound,
		variable
	}
}