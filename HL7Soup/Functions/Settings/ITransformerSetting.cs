using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public interface ITransformerSetting : ISetting, ISettingWithVariables
	{
		List<ITransformerAction> Transformers
		{
			get;
			set;
		}
	}
}