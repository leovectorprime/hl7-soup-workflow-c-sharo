using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public interface ISettingWithVariables
	{
		Dictionary<string, IVariableCreator> GetVariables();
	}
}