namespace HL7Soup.Functions.Settings
{
	public interface IActivitySetting : ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled
	{

	}
}