using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Settings
{
	public interface ITransformerAction
	{
		string Description
		{
			get;
		}

		MessageSourceDirection FromDirection
		{
			get;
			set;
		}

		string FromPath
		{
			get;
			set;
		}

		Guid FromSetting
		{
			get;
			set;
		}

		HL7Soup.Functions.MessageTypes FromType
		{
			get;
			set;
		}

		int IndentDepth
		{
			get;
			set;
		}

		ITransformerAction Clone();

		void Highlight(string text, MessageSourceDirection direction, Guid setting);

		void Prepare(WorkflowInstance workflowInstance);

		void PrepareItteration(WorkflowInstance workflowInstance);

		void Process(WorkflowInstance workflowInstance);
	}
}