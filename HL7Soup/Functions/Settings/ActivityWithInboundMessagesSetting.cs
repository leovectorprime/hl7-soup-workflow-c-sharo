using HL7Soup.Functions;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class ActivityWithInboundMessagesSetting : ActivitySetting
	{
		public HL7Soup.Functions.MessageTypes MessageType
		{
			get;
			set;
		}

		public string ReceivedMessageTemplate
		{
			get;
			set;
		}

		public ActivityWithInboundMessagesSetting()
		{
		}
	}
}