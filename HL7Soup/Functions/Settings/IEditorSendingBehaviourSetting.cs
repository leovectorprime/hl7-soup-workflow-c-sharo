using System;

namespace HL7Soup.Functions.Settings
{
	public interface IEditorSendingBehaviourSetting : ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting
	{
		Guid AutomaticSendSettings
		{
			get;
			set;
		}

		Guid SourceProviderSettings
		{
			get;
			set;
		}
	}
}