using HL7Soup.Functions.Settings;

namespace HL7Soup.Functions.Settings.Filters
{
	public interface IFilterHostSetting : IFilterSetting, ISetting, ITransformerSetting, ISettingWithVariables
	{

	}
}