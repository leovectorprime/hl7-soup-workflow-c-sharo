using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Filters
{
	[Serializable]
	public class FilterHostSetting : FilterSetting, IFilterHostSetting, IFilterSetting, ISetting, ITransformerSetting, ISettingWithVariables
	{
		public override string Details
		{
			get
			{
				return "Filter X";
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Filters.Default";
			}
		}

		public override string Name
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditFilterHostSetting";
			}
		}

		public FilterHostSetting()
		{
		}
	}
}