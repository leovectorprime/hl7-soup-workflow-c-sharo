using System;

namespace HL7Soup.Functions.Settings
{
	public interface IVariableCreator
	{
		bool SampleValueIsDefaultValue
		{
			get;
			set;
		}

		string SampleVariableValue
		{
			get;
			set;
		}

		string VariableName
		{
			get;
			set;
		}

		IVariableCreator Clone();
	}
}