using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class ReceiverWithResponseSetting : ReceiverSetting, IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers
	{
		private Guid transformers = Guid.Empty;

		public string ErrorMessage
		{
			get;
			set;
		}

		public string RejectMessage
		{
			get;
			set;
		}

		public ReturnResponsePriority ReponsePriority { get; set; } = ReturnResponsePriority.AfterAllProcessing;

		public string ResponseMessageTemplate
		{
			get;
			set;
		}

		public bool ReturnApplicationAccept
		{
			get;
			set;
		}

		public bool ReturnApplicationError
		{
			get;
			set;
		}

		public bool ReturnApplicationReject
		{
			get;
			set;
		}

		public bool ReturnCustomResponse
		{
			get;
			set;
		}

		public bool ReturnNoResponse
		{
			get;
			set;
		}

		public bool ReturnResponseFromActivity
		{
			get;
			set;
		}

		public Guid ReturnResponseFromActivityId
		{
			get;
			set;
		}

		public string ReturnResponseFromActivityName
		{
			get;
			set;
		}

		public Guid Transformers
		{
			get
			{
				return this.transformers;
			}
			set
			{
				this.transformers = value;
			}
		}

		public bool TransformersNotAvailable
		{
			get;
			set;
		}

		protected ReceiverWithResponseSetting()
		{
		}

		public string ResponseMessageTemplateRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.ResponseMessageTemplate);
		}
	}
}