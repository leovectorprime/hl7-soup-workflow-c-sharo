using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Settings
{
	public interface IFunctionWithMessageSetting : ISetting
	{
		HL7Soup.Functions.MessageTypes MessageType
		{
			get;
			set;
		}
	}
}