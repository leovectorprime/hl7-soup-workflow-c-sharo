using HL7Soup;
using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CommentTransformerAction : TransformerAction
	{
		private string comment;

		public string Comment
		{
			get
			{
				return this.comment;
			}
			set
			{
				this.comment = value;
				base.NotifyPropertyChanged("Comment");
			}
		}

		public override string TransformerName
		{
			get
			{
				return "Comment";
			}
		}

		public CommentTransformerAction()
		{
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public override string ToString()
		{
			return "Comment";
		}
	}
}