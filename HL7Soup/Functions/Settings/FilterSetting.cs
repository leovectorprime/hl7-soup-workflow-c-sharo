using HL7Soup.MessageFilters;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class FilterSetting : Setting, IFilterSetting, ISetting, ITransformerSetting, ISettingWithVariables
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Filter";
			}
		}

		public List<MessageFilter> Filters
		{
			get
			{
				return JustDecompileGenerated_get_Filters();
			}
			set
			{
				JustDecompileGenerated_set_Filters(value);
			}
		}

		private List<MessageFilter> JustDecompileGenerated_Filters_k__BackingField;

		public List<MessageFilter> JustDecompileGenerated_get_Filters()
		{
			return this.JustDecompileGenerated_Filters_k__BackingField;
		}

		public void JustDecompileGenerated_set_Filters(List<MessageFilter> value)
		{
			this.JustDecompileGenerated_Filters_k__BackingField = value;
		}

		public List<ITransformerAction> Transformers
		{
			get;
			set;
		}

		public FilterSetting()
		{
			this.Transformers = new List<ITransformerAction>();
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.Transformers == null)
			{
				this.Transformers = new List<ITransformerAction>();
			}
			return TransformerSetting.GetVariables(this.Transformers);
		}
	}
}