using System;

namespace HL7Soup.Functions.Settings
{
	public interface ISettingCanBeDisabled
	{
		bool Disabled
		{
			get;
			set;
		}

		bool DisableNotAvailable
		{
			get;
			set;
		}
	}
}