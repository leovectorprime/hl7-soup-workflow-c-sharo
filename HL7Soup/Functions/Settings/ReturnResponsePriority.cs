using System;

namespace HL7Soup.Functions.Settings
{
	public enum ReturnResponsePriority
	{
		UponArrival,
		AfterValidation,
		AfterAllProcessing
	}
}