using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.Receivers
{
	public interface IMLLPReceiverSetting : IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers, IMLLPConnectionSetting
	{
		bool KeepConnectionOpen
		{
			get;
			set;
		}

		bool RelayMessage
		{
			get;
			set;
		}

		string RelayName
		{
			get;
			set;
		}
	}
}