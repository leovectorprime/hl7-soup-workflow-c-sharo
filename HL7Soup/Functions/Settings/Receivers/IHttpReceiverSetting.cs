using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace HL7Soup.Functions.Settings.Receivers
{
	public interface IHttpReceiverSetting : IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers
	{
		bool Authentication
		{
			get;
			set;
		}

		string AuthenticationPassword
		{
			get;
			set;
		}

		string AuthenticationUserName
		{
			get;
			set;
		}

		bool CertificateFromFile
		{
			get;
			set;
		}

		string CertificatePassword
		{
			get;
			set;
		}

		string CertificatePath
		{
			get;
			set;
		}

		StoreLocation CertificateStoreLocation
		{
			get;
			set;
		}

		StoreName CertificateStoreName
		{
			get;
			set;
		}

		string CertificateThumbPrint
		{
			get;
			set;
		}

		X509FindType CertificateX509FindType
		{
			get;
			set;
		}

		bool ExtractParameters
		{
			get;
			set;
		}

		bool ExtractUrlSections
		{
			get;
			set;
		}

		int Port
		{
			get;
			set;
		}

		Dictionary<string, IVariableCreator> QueryStringParameters
		{
			get;
			set;
		}

		string ServiceName
		{
			get;
			set;
		}

		List<IVariableCreator> UrlSections
		{
			get;
			set;
		}

		bool UseDefaultSSLCertificate
		{
			get;
			set;
		}

		bool UseSsl
		{
			get;
			set;
		}

		string GetURL();

		string GetURL(bool usePlusInsteadOfServerName);
	}
}