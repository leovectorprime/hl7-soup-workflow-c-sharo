using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Receivers
{
	[Serializable]
	public class DatabaseReceiverSetting : ReceiverSetting, IDatabaseReceiverSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers, IReceiverExecutesPostProcessSetting
	{
		private Guid transformers = Guid.Empty;

		public string ConnectionString
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Database Reader";
			}
		}

		public DataProviders DataProvider
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				return "Database Reader";
			}
		}

		public bool EndAfterProcessing
		{
			get;
			set;
		}

		public bool ExecutePostProcess
		{
			get
			{
				return this.ExecutePostProcessQuery;
			}
			set
			{
				this.ExecutePostProcessQuery = value;
			}
		}

		public bool ExecutePostProcessQuery
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Receivers.DatabaseReceiver";
			}
		}

		public List<DatabaseSettingParameter> Parameters
		{
			get
			{
				return JustDecompileGenerated_get_Parameters();
			}
			set
			{
				JustDecompileGenerated_set_Parameters(value);
			}
		}

		private List<DatabaseSettingParameter> JustDecompileGenerated_Parameters_k__BackingField;

		public List<DatabaseSettingParameter> JustDecompileGenerated_get_Parameters()
		{
			return this.JustDecompileGenerated_Parameters_k__BackingField;
		}

		public void JustDecompileGenerated_set_Parameters(List<DatabaseSettingParameter> value)
		{
			this.JustDecompileGenerated_Parameters_k__BackingField = value;
		}

		public TimeSpan PollingInterval { get; set; } = new TimeSpan(0, 0, 10);

		public List<DatabaseSettingParameter> PostExecutionParameters
		{
			get
			{
				return JustDecompileGenerated_get_PostExecutionParameters();
			}
			set
			{
				JustDecompileGenerated_set_PostExecutionParameters(value);
			}
		}

		private List<DatabaseSettingParameter> JustDecompileGenerated_PostExecutionParameters_k__BackingField;

		public List<DatabaseSettingParameter> JustDecompileGenerated_get_PostExecutionParameters()
		{
			return this.JustDecompileGenerated_PostExecutionParameters_k__BackingField;
		}

		public void JustDecompileGenerated_set_PostExecutionParameters(List<DatabaseSettingParameter> value)
		{
			this.JustDecompileGenerated_PostExecutionParameters_k__BackingField = value;
		}

		public string PostExecutionSqlQuery
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditDatabaseReceiverSetting";
			}
		}

		public string SqlQuery
		{
			get;
			set;
		}

		public Guid Transformers
		{
			get
			{
				return this.transformers;
			}
			set
			{
				this.transformers = value;
			}
		}

		public bool TransformersNotAvailable
		{
			get;
			set;
		}

		public DatabaseReceiverSetting()
		{
			this.ConnectionString = "";
			base.MessageType = HL7Soup.Functions.MessageTypes.CSV;
			this.Parameters = new List<DatabaseSettingParameter>();
		}

		public override void SwapActivityIds(Dictionary<Guid, Guid> oldAndNewGuidDictionary)
		{
			if (this.Parameters != null)
			{
				foreach (DatabaseSettingParameter parameter in this.Parameters)
				{
					parameter.FromSetting = FunctionHelpers.ReplaceActivity(parameter.FromSetting, oldAndNewGuidDictionary);
				}
			}
			if (this.PostExecutionParameters != null)
			{
				foreach (DatabaseSettingParameter postExecutionParameter in this.PostExecutionParameters)
				{
					postExecutionParameter.FromSetting = FunctionHelpers.ReplaceActivity(postExecutionParameter.FromSetting, oldAndNewGuidDictionary);
				}
			}
			base.SwapActivityIds(oldAndNewGuidDictionary);
		}
	}
}