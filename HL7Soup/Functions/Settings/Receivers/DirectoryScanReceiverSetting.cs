using HL7Soup.Dialogs;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.Receivers
{
	[Serializable]
	public class DirectoryScanReceiverSetting : ReceiverSetting, IDirectoryScanReceiverSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, IDirectoryScanSetting, ISettingWithVariables, ISettingWithTransformers
	{
		private Dictionary<string, IVariableCreator> variables;

		private Guid transformers = Guid.Empty;

		public override string ConnectionTypeName
		{
			get
			{
				return "Directory Scan";
			}
		}

		public bool DeleteFileOnComplete
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				return string.Concat("Directory Scan ", this.DirectoryPath);
			}
		}

		public string DirectoryFilter
		{
			get;
			set;
		}

		public string DirectoryPath
		{
			get;
			set;
		}

		public string DirectoryToMoveInto
		{
			get;
			set;
		}

		public bool EndAfterProcessing
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Receivers.DirectoryScan";
			}
		}

		public bool MoveIntoDirectoryOnComplete
		{
			get;
			set;
		}

		public bool SearchForNewFiles
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditDirectoryScanReceiverSetting";
			}
		}

		public Guid Transformers
		{
			get
			{
				return this.transformers;
			}
			set
			{
				this.transformers = value;
			}
		}

		public bool TransformersNotAvailable
		{
			get;
			set;
		}

		public DirectoryScanReceiverSetting()
		{
			this.DirectoryPath = "c:\\";
			this.DirectoryFilter = "*.hl7";
			this.SearchForNewFiles = true;
			base.MessageType = HL7Soup.Functions.MessageTypes.HL7V2;
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.variables != null)
			{
				return this.variables;
			}
			this.variables = new Dictionary<string, IVariableCreator>();
			this.variables["DirectoryScannerFileName"] = new VariableCreator("DirectoryScannerFileName", "SampleValue.txt", true);
			return this.variables;
		}
	}
}