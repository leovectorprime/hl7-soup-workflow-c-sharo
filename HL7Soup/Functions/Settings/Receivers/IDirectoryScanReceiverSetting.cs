using HL7Soup.Functions.Settings;

namespace HL7Soup.Functions.Settings.Receivers
{
	public interface IDirectoryScanReceiverSetting : IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, IDirectoryScanSetting, ISettingWithVariables, ISettingWithTransformers
	{

	}
}