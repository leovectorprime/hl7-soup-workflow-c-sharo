using HL7Soup.Functions.Settings;
using System;
using System.Security.Cryptography.X509Certificates;

namespace HL7Soup.Functions.Settings.Receivers
{
	public interface IWebServiceReceiverSetting : IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers
	{
		bool Authentication
		{
			get;
			set;
		}

		string AuthenticationPassword
		{
			get;
			set;
		}

		string AuthenticationUserName
		{
			get;
			set;
		}

		bool CertificateFromFile
		{
			get;
			set;
		}

		string CertificatePassword
		{
			get;
			set;
		}

		string CertificatePath
		{
			get;
			set;
		}

		StoreLocation CertificateStoreLocation
		{
			get;
			set;
		}

		StoreName CertificateStoreName
		{
			get;
			set;
		}

		string CertificateThumbPrint
		{
			get;
			set;
		}

		X509FindType CertificateX509FindType
		{
			get;
			set;
		}

		bool IncludeHttpLocalHostEndpoint
		{
			get;
			set;
		}

		int LocalHostEndpointPort
		{
			get;
			set;
		}

		int Port
		{
			get;
			set;
		}

		string ServiceName
		{
			get;
			set;
		}

		bool UseDefaultSSLCertificate
		{
			get;
			set;
		}

		bool UseSoap12
		{
			get;
			set;
		}

		bool UseSsl
		{
			get;
			set;
		}

		string GetURL();

		string GetURL(bool usePlusInsteadOfServerName);
	}
}