using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.Receivers
{
	[Serializable]
	public class MLLPReceiverSetting : ReceiverWithResponseSetting, IMLLPReceiverSetting, IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers, IMLLPConnectionSetting
	{
		private char[] frameStart = new char[] { '\v' };

		private char[] frameEnd = new char[] { '\u001C', '\r' };

		private Encoding messageEncoding;

		public override string ConnectionTypeName
		{
			get
			{
				return "MLLP";
			}
		}

		public override string Details
		{
			get
			{
				if (this.Server == "127.0.0.1")
				{
					return string.Concat("Receive on TCP Localhost:", this.Port);
				}
				if (this.Server == "0.0.0.0")
				{
					return string.Concat("Receive on TCP port ", this.Port);
				}
				return string.Concat(new object[] { "Receive on TCP ", this.Server, ":", this.Port });
			}
		}

		public char[] FrameEnd
		{
			get
			{
				return this.frameEnd;
			}
			set
			{
				this.frameEnd = value;
			}
		}

		public char[] FrameStart
		{
			get
			{
				return this.frameStart;
			}
			set
			{
				this.frameStart = value;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Receivers.MLLPReceiver";
			}
		}

		public bool KeepConnectionOpen
		{
			get;
			set;
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public override string Name
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public bool RelayMessage
		{
			get;
			set;
		}

		public string RelayName
		{
			get;
			set;
		}

		public string Server
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditMLLPReceiverConnectionSetting";
			}
		}

		public MLLPReceiverSetting()
		{
			this.Server = "0.0.0.0";
			this.Port = 22222;
			base.AddIncomingMessageToCurrentTab = true;
			base.TransformersNotAvailable = true;
		}
	}
}