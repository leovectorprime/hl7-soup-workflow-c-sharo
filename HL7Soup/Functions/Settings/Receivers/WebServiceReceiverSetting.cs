using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace HL7Soup.Functions.Settings.Receivers
{
	[Serializable]
	public class WebServiceReceiverSetting : ReceiverWithResponseSetting, IWebServiceReceiverSetting, IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers
	{
		public bool Authentication
		{
			get;
			set;
		}

		public string AuthenticationPassword
		{
			get;
			set;
		}

		public string AuthenticationUserName
		{
			get;
			set;
		}

		public bool CertificateFromFile
		{
			get;
			set;
		}

		public string CertificatePassword
		{
			get;
			set;
		}

		public string CertificatePath
		{
			get;
			set;
		}

		public StoreLocation CertificateStoreLocation
		{
			get;
			set;
		}

		public StoreName CertificateStoreName
		{
			get;
			set;
		}

		public string CertificateThumbPrint
		{
			get;
			set;
		}

		public X509FindType CertificateX509FindType
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "WebService";
			}
		}

		public override string Details
		{
			get
			{
				string serviceName = "Web Service";
				if (!string.IsNullOrWhiteSpace(this.ServiceName))
				{
					serviceName = this.ServiceName;
				}
				return string.Concat(new object[] { "Receive ", Helpers.GetEnumDescription(base.MessageType), " with ", Helpers.TruncateString(serviceName, 30), " WCF:", this.Port });
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Receivers.WebServiceReceiver";
			}
		}

		public bool IncludeHttpLocalHostEndpoint
		{
			get;
			set;
		}

		public int LocalHostEndpointPort
		{
			get;
			set;
		}

		public override string Name
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public string ServiceName
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditWebServiceReceiverConnectionSetting";
			}
		}

		public bool UseDefaultSSLCertificate { get; set; } = true;

		public bool UseSoap12
		{
			get;
			set;
		}

		public bool UseSsl
		{
			get;
			set;
		}

		public WebServiceReceiverSetting()
		{
			this.UseSsl = false;
			this.IncludeHttpLocalHostEndpoint = false;
			this.CertificateFromFile = true;
			this.CertificatePath = "";
			this.CertificatePassword = "";
			this.CertificateStoreLocation = StoreLocation.LocalMachine;
			this.CertificateStoreName = StoreName.My;
			this.CertificateX509FindType = X509FindType.FindByThumbprint;
			this.CertificateThumbPrint = "";
			this.Port = 8080;
			this.LocalHostEndpointPort = 8081;
			this.ServiceName = "HL7Soup";
			this.Authentication = false;
			base.AddIncomingMessageToCurrentTab = true;
			base.TransformersNotAvailable = false;
			base.ReturnCustomResponse = true;
		}

		public string GetURL()
		{
			return WebServiceReceiverSetting.GetURL(this.UseSsl, this.Port, this.ServiceName);
		}

		public string GetURL(bool usePlusInsteadOfServerName)
		{
			return WebServiceReceiverSetting.GetURL(this.UseSsl, this.Port, this.ServiceName, usePlusInsteadOfServerName);
		}

		public static string GetURL(bool useSsl, int port, string serviceName)
		{
			return WebServiceReceiverSetting.GetURL(useSsl, port, serviceName, false);
		}

		public static string GetURL(bool useSsl, int port, string serviceName, bool usePlusInsteadOfServerName)
		{
			string machineName = Environment.MachineName;
			string str = "http";
			if (useSsl)
			{
				str = "https";
			}
			object[] objArray = new object[] { str, null, null, null };
			objArray[1] = (usePlusInsteadOfServerName ? "+" : machineName);
			objArray[2] = port;
			objArray[3] = serviceName;
			return string.Format("{0}://{1}:{2}/{3}", objArray);
		}
	}
}