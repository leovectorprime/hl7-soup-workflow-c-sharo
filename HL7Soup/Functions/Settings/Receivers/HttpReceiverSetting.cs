using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace HL7Soup.Functions.Settings.Receivers
{
	[Serializable]
	public class HttpReceiverSetting : ReceiverWithResponseSetting, IHttpReceiverSetting, IReceiverWithResponseSetting, IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers, ISettingWithVariables
	{
		public bool Authentication
		{
			get;
			set;
		}

		public string AuthenticationPassword
		{
			get;
			set;
		}

		public string AuthenticationUserName
		{
			get;
			set;
		}

		public bool CertificateFromFile
		{
			get;
			set;
		}

		public string CertificatePassword
		{
			get;
			set;
		}

		public string CertificatePath
		{
			get;
			set;
		}

		public StoreLocation CertificateStoreLocation
		{
			get;
			set;
		}

		public StoreName CertificateStoreName
		{
			get;
			set;
		}

		public string CertificateThumbPrint
		{
			get;
			set;
		}

		public X509FindType CertificateX509FindType
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Http";
			}
		}

		public override string Details
		{
			get
			{
				string serviceName = "HTTP";
				if (!string.IsNullOrWhiteSpace(this.ServiceName))
				{
					serviceName = this.ServiceName;
				}
				object[] enumDescription = new object[] { "Receive ", Helpers.GetEnumDescription(base.MessageType), " with ", Helpers.TruncateString(serviceName, 30), null, null };
				enumDescription[4] = (this.UseSsl ? " HTTPS:" : " HTTP:");
				enumDescription[5] = this.Port;
				return string.Concat(enumDescription);
			}
		}

		public bool ExtractParameters
		{
			get;
			set;
		}

		public bool ExtractUrlSections
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Receivers.HttpReceiver";
			}
		}

		public override string Name
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public Dictionary<string, IVariableCreator> QueryStringParameters
		{
			get;
			set;
		}

		public string ServiceName
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditHttpReceiverConnectionSetting";
			}
		}

		public List<IVariableCreator> UrlSections
		{
			get;
			set;
		}

		public bool UseDefaultSSLCertificate { get; set; } = true;

		public bool UseSsl
		{
			get;
			set;
		}

		public HttpReceiverSetting()
		{
			this.QueryStringParameters = new Dictionary<string, IVariableCreator>();
			this.UrlSections = new List<IVariableCreator>();
			this.UseSsl = false;
			this.CertificateFromFile = true;
			this.CertificatePath = "";
			this.CertificatePassword = "";
			this.CertificateStoreLocation = StoreLocation.LocalMachine;
			this.CertificateStoreName = StoreName.My;
			this.CertificateX509FindType = X509FindType.FindByThumbprint;
			this.CertificateThumbPrint = "";
			this.Port = 8080;
			this.ServiceName = "HL7Soup";
			this.Authentication = false;
			base.AddIncomingMessageToCurrentTab = true;
			base.TransformersNotAvailable = false;
			base.ReturnCustomResponse = true;
		}

		public string GetURL()
		{
			return HttpReceiverSetting.GetURL(this.UseSsl, this.Port, this.ServiceName);
		}

		public string GetURL(bool usePlusInsteadOfServerName)
		{
			return HttpReceiverSetting.GetURL(this.UseSsl, this.Port, this.ServiceName, usePlusInsteadOfServerName);
		}

		public static string GetURL(bool useSsl, int port, string serviceName)
		{
			return HttpReceiverSetting.GetURL(useSsl, port, serviceName, false);
		}

		public static string GetURL(bool useSsl, int port, string serviceName, bool usePlusInsteadOfServerName)
		{
			string lower = Environment.MachineName.ToLower();
			string str = "http";
			if (useSsl)
			{
				str = "https";
			}
			object[] objArray = new object[] { str, null, null, null };
			objArray[1] = (usePlusInsteadOfServerName ? "+" : lower);
			objArray[2] = port;
			objArray[3] = serviceName.Replace(" ", "%20");
			string str1 = string.Format("{0}://{1}:{2}/{3}", objArray);
			if (!str1.EndsWith("/"))
			{
				str1 = string.Concat(str1, "/");
			}
			return str1;
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			Dictionary<string, IVariableCreator> strs = new Dictionary<string, IVariableCreator>();
			strs["HttpMethod"] = new VariableCreator("HttpMethod", "POST", true);
			if (this.ExtractParameters)
			{
				foreach (KeyValuePair<string, IVariableCreator> queryStringParameter in this.QueryStringParameters)
				{
					strs[queryStringParameter.Key] = queryStringParameter.Value;
				}
			}
			if (this.ExtractUrlSections)
			{
				foreach (IVariableCreator urlSection in this.UrlSections)
				{
					strs[urlSection.VariableName] = urlSection;
				}
			}
			return strs;
		}
	}
}