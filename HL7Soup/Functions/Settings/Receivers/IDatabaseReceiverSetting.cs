using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings.Receivers
{
	public interface IDatabaseReceiverSetting : IReceiverSetting, ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern, ISettingWithTransformers, IReceiverExecutesPostProcessSetting
	{
		string ConnectionString
		{
			get;
			set;
		}

		DataProviders DataProvider
		{
			get;
			set;
		}

		bool EndAfterProcessing
		{
			get;
			set;
		}

		bool ExecutePostProcessQuery
		{
			get;
			set;
		}

		List<DatabaseSettingParameter> Parameters
		{
			get;
		}

		TimeSpan PollingInterval
		{
			get;
			set;
		}

		List<DatabaseSettingParameter> PostExecutionParameters
		{
			get;
		}

		string PostExecutionSqlQuery
		{
			get;
			set;
		}

		string SqlQuery
		{
			get;
			set;
		}
	}
}