using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class NextTransformerAction : TransformerAction, ITransformerActionWithPair
	{
		[NonSerialized]
		private bool isPairSelected;

		public int ForEachLocation
		{
			get;
			internal set;
		}

		public bool IsPairSelected
		{
			get
			{
				return this.isPairSelected;
			}
			set
			{
				this.isPairSelected = value;
				base.NotifyPropertyChanged("IsPairSelected");
			}
		}

		public int PairId
		{
			get;
			set;
		}

		public override string TransformerName
		{
			get
			{
				return "Next";
			}
		}

		public NextTransformerAction()
		{
		}

		public override void PrepareItteration(WorkflowInstance workflowInstance)
		{
			this.ForEachLocation = 0;
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public override string ToString()
		{
			return "Next";
		}
	}
}