using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Settings.Transformers
{
	public class TransformerType : INotifyPropertyChanged
	{
		private bool isVisibleForMessageType;

		public bool IsVisibleForMessageType
		{
			get
			{
				return this.isVisibleForMessageType;
			}
			set
			{
				this.isVisibleForMessageType = value;
				this.NotifyPropertyChanged("IsVisibleForMessageType");
			}
		}

		public string Name
		{
			get;
			set;
		}

		public List<HL7Soup.Functions.MessageTypes> SupportedMessageTypes { get; set; } = new List<HL7Soup.Functions.MessageTypes>();

		public string TypeName
		{
			get;
			set;
		}

		public TransformerType()
		{
		}

		public TransformerType(Type type, string name)
		{
			this.Name = name;
			this.TypeName = type.AssemblyQualifiedName;
		}

		public TransformerType(Type type, string name, List<HL7Soup.Functions.MessageTypes> supportedMessageTypes)
		{
			this.Name = name;
			this.TypeName = type.AssemblyQualifiedName;
			this.SupportedMessageTypes = supportedMessageTypes;
		}

		public TransformerType(string assemblyQualifiedName, string name)
		{
			this.Name = name;
			this.TypeName = assemblyQualifiedName;
		}

		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public void SetIsVisibleForMessageType(HL7Soup.Functions.MessageTypes messageType)
		{
			if (this.SupportedMessageTypes.Count == 0)
			{
				this.IsVisibleForMessageType = true;
				return;
			}
			foreach (HL7Soup.Functions.MessageTypes supportedMessageType in this.SupportedMessageTypes)
			{
				this.IsVisibleForMessageType = false;
				if (supportedMessageType != messageType)
				{
					continue;
				}
				this.IsVisibleForMessageType = true;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}