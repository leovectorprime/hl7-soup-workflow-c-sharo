using System;

namespace HL7Soup.Functions.Settings
{
	public interface ISettingWithTransformers
	{
		Guid Transformers
		{
			get;
			set;
		}

		bool TransformersNotAvailable
		{
			get;
			set;
		}
	}
}