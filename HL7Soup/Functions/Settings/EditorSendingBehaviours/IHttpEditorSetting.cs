using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;

namespace HL7Soup.Functions.Settings.EditorSendingBehaviours
{
	public interface IHttpEditorSetting : IEditorSendingBehaviourSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IHttpSenderSetting, ISenderWithResponseSetting
	{
		bool LoadResult
		{
			get;
			set;
		}
	}
}