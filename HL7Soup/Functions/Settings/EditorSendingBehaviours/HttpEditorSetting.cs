using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.EditorSendingBehaviours
{
	[Serializable]
	public class HttpEditorSetting : EditorSendingBehaviourSetting, IHttpEditorSetting, IEditorSendingBehaviourSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IHttpSenderSetting, ISenderWithResponseSetting
	{
		private Encoding messageEncoding;

		public bool Authentication
		{
			get;
			set;
		}

		public override string ConnectionTypeName
		{
			get
			{
				return "Http Sender";
			}
		}

		public string ContentType
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				string str = "Call Http Service";
				if (!string.IsNullOrEmpty(this.Server))
				{
					str = string.Concat("Call ", this.Server);
				}
				return str;
			}
		}

		public virtual bool DifferentResponseMessageType
		{
			get;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.EditorSendingBehaviours.HttpEditor";
			}
		}

		public bool LoadResult
		{
			get;
			set;
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public HttpMethods Method
		{
			get;
			set;
		}

		public override string Name
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		public string ResponseMessageTemplate
		{
			get;
			set;
		}

		public HL7Soup.Functions.MessageTypes ResponseMessageType
		{
			get;
		}

		public virtual bool ResponseNotAvailable
		{
			get;
		}

		public string Server
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditorSendingBehaviors.EditHttpEditorSetting";
			}
		}

		public string UserName
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		public HttpEditorSetting()
		{
			this.LoadResult = true;
			this.ContentType = "text/plain";
			this.Method = HttpMethods.POST;
			base.MessageType = HL7Soup.Functions.MessageTypes.HL7V2;
		}

		public List<ISetting> GetAutomaticSenderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.AutomaticSendSettings))
			{
				ISetting filterSettingById = FunctionHelpers.GetFilterSettingById(base.AutomaticSendSettings);
				if (filterSettingById != null)
				{
					settings.Add(filterSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.AutomaticSendSettings]);
			}
			return settings;
		}

		public override List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> childSettingInstances = base.GetChildSettingInstances(settingsBeingEdited);
			childSettingInstances.AddRange(this.GetAutomaticSenderInstances(settingsBeingEdited));
			childSettingInstances.AddRange(this.GetSourceProviderInstances(settingsBeingEdited));
			return childSettingInstances;
		}

		public List<ISetting> GetSourceProviderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.SourceProviderSettings))
			{
				ISetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(base.SourceProviderSettings);
				if (transformerSettingById != null)
				{
					settings.Add(transformerSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.SourceProviderSettings]);
			}
			return settings;
		}

		public string ServerRuntimeValue(WorkflowInstance workflowInstance)
		{
			return workflowInstance.ProcessVariables(this.Server);
		}
	}
}