using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;

namespace HL7Soup.Functions.Settings.EditorSendingBehaviours
{
	public interface IHL7V2EditorSendingBehaviourSetting : IMLLPSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IMLLPConnectionSetting, IEditorSendingBehaviourSetting
	{
		bool LoadApplicationAccept
		{
			get;
			set;
		}

		bool LoadApplicationError
		{
			get;
			set;
		}

		bool LoadApplicationReject
		{
			get;
			set;
		}
	}
}