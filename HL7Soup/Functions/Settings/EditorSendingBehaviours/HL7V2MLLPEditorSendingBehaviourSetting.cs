using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup.Functions.Settings.EditorSendingBehaviours
{
	[Serializable]
	public class HL7V2MLLPEditorSendingBehaviourSetting : EditorSendingBehaviourSetting, IHL7V2EditorSendingBehaviourSetting, IMLLPSenderSetting, ISenderWithResponseSetting, ISenderSetting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled, IFunctionWithMessageSetting, IMLLPConnectionSetting, IEditorSendingBehaviourSetting
	{
		private char[] frameStart = new char[] { '\v' };

		private char[] frameEnd = new char[] { '\u001C', '\r' };

		private Encoding messageEncoding;

		public override string ConnectionTypeName
		{
			get
			{
				return "MLLP";
			}
		}

		public override string Details
		{
			get
			{
				if (this.Server != "127.0.0.1")
				{
					return this.Server;
				}
				return string.Concat("Localhost : ", this.Port);
			}
		}

		public virtual bool DifferentResponseMessageType
		{
			get;
		}

		public char[] FrameEnd
		{
			get
			{
				return this.frameEnd;
			}
			set
			{
				this.frameEnd = value;
			}
		}

		public char[] FrameStart
		{
			get
			{
				return this.frameStart;
			}
			set
			{
				this.frameStart = value;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.EditorSendingBehaviours.HL7V2MLLPEditorSendingBehaviour";
			}
		}

		public bool LoadApplicationAccept
		{
			get;
			set;
		}

		public bool LoadApplicationError
		{
			get;
			set;
		}

		public bool LoadApplicationReject
		{
			get;
			set;
		}

		public Encoding MessageEncoding
		{
			get
			{
				return Helpers.GetEncoding(true);
			}
			set
			{
				this.messageEncoding = value;
			}
		}

		public override string Name
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public ReturnResponsePriority ReponsePriority
		{
			get;
			set;
		}

		public string ResponseMessageTemplate
		{
			get;
			set;
		}

		public HL7Soup.Functions.MessageTypes ResponseMessageType
		{
			get;
		}

		public virtual bool ResponseNotAvailable
		{
			get;
		}

		public string Server
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditHL7V2MLLPEditorSendingBehaviourSetting";
			}
		}

		public bool UseResponse
		{
			get;
			set;
		}

		public bool WaitForResponse { get; set; } = true;

		[DefaultValue(true)]
		public bool WorkflowResponseEqualsThisResponse { get; set; } = true;

		public HL7V2MLLPEditorSendingBehaviourSetting()
		{
			this.LoadApplicationAccept = true;
			this.LoadApplicationError = true;
			this.LoadApplicationReject = true;
			this.Server = "127.0.0.1";
			this.Port = 22222;
			base.MessageType = HL7Soup.Functions.MessageTypes.HL7V2;
		}

		public List<ISetting> GetAutomaticSenderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.AutomaticSendSettings))
			{
				ISetting filterSettingById = FunctionHelpers.GetFilterSettingById(base.AutomaticSendSettings);
				if (filterSettingById != null)
				{
					settings.Add(filterSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.AutomaticSendSettings]);
			}
			return settings;
		}

		public override List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> childSettingInstances = base.GetChildSettingInstances(settingsBeingEdited);
			childSettingInstances.AddRange(this.GetAutomaticSenderInstances(settingsBeingEdited));
			childSettingInstances.AddRange(this.GetSourceProviderInstances(settingsBeingEdited));
			return childSettingInstances;
		}

		public List<ISetting> GetSourceProviderInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(base.SourceProviderSettings))
			{
				ISetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(base.SourceProviderSettings);
				if (transformerSettingById != null)
				{
					settings.Add(transformerSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[base.SourceProviderSettings]);
			}
			return settings;
		}
	}
}