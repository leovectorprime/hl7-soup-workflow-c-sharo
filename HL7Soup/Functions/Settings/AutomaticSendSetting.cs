using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class AutomaticSendSetting : Setting, IAutomaticSendSetting, ISetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Auto Sender";
			}
		}

		public TimeSpan Delay
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				TimeSpan timeSpan;
				string str = string.Concat("every ", Helpers.GetTimeSpanWords(this.Delay));
				if (this.MoveStatic)
				{
					TimeSpan delay = this.Delay;
					TimeSpan delay1 = this.Delay;
					timeSpan = new TimeSpan();
					str = (delay1 != timeSpan ? string.Concat("Resend every ", Helpers.GetTimeSpanWords(this.Delay)) : "Resend rapidly");
				}
				if (this.MoveNext)
				{
					TimeSpan timeSpan1 = this.Delay;
					TimeSpan delay2 = this.Delay;
					timeSpan = new TimeSpan();
					str = (delay2 != timeSpan ? string.Concat("Move next ", str) : "Move next rapidly");
				}
				return str;
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.AutomaticSenders.AutomaticSend";
			}
		}

		public bool InfinateLoop
		{
			get;
			set;
		}

		public bool MoveNext
		{
			get;
			set;
		}

		public bool MoveStatic
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditAutomaticSendSetting";
			}
		}

		public AutomaticSendSetting()
		{
			this.MoveNext = true;
		}
	}
}