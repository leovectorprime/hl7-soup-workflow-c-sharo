using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Functions.Senders;
using HL7Soup.Integrations;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class CodeTransformerAction : TransformerAction
	{
		[NonSerialized]
		private ScriptRunner<string> runner;

		private Dictionary<string, IVariableCreator> variables;

		public string Code { get; set; } = "//The following code is just a indication for getting started for transformers. Edit or delete as you wish. Use the variable workflowInstance and activityInstance to access the running states of the workflow.\r\n\r\n//Get the destination message\r\nIHL7Message destinationMessage = (IHL7Message)activityInstance.Message; //Use IHL7Message for HL7 Messages and IMessage for other message types\r\n\r\n//Get the source message. In this case, the message sent to this workflow\r\nIHL7Message sourceMessage = (IHL7Message)workflowInstance.ReceivingActivityInstance.Message;\r\n\r\n//Copy a value from the source (MSH-3) \r\nstring value = sourceMessage.GetValueAtPath(\"MSH-3\");\r\n\r\n//Write the value to the destination (MSH-3)\r\ndestinationMessage.SetValueAtPath(\"MSH-3\", value);\r\n\r\n//Get a variable value\r\nstring receivedDate = workflowInstance.GetVariable(\"ReceivedDate\");\r\n\r\n//Convert an HL7 Date to a DateTime\r\nDateTime date = HL7Helpers.GetDateFromHL7Date(receivedDate);\r\n\r\n//Set a variable value\r\nworkflowInstance.SetVariable(\"MyVariable\",\"ValueOfMyVariable\");";

		public override string TransformerName
		{
			get
			{
				return this.ToString();
			}
		}

		public List<string> VariableNames { get; set; } = new List<string>();

		public CodeTransformerAction()
		{
		}

		public Dictionary<string, IVariableCreator> GetVariables()
		{
			if (this.variables != null)
			{
				return this.variables;
			}
			this.variables = new Dictionary<string, IVariableCreator>();
			if (this.VariableNames != null)
			{
				foreach (string variableName in this.VariableNames)
				{
					this.variables[variableName] = new VariableCreator(variableName, "", true);
				}
			}
			return this.variables;
		}

		public override void Prepare(WorkflowInstance workflowInstance)
		{
			try
			{
				Script<string> script = CSharpScript.Create<string>(this.Code, ScriptOptions.Default.WithReferences(new Assembly[] { typeof(IHL7Message).Assembly }).WithImports(new string[] { "HL7Soup.Integrations", "System", "System.Collections.Generic", "System.Linq", "System.Text" }), typeof(MyContext), null);
				this.runner = script.CreateDelegate(new CancellationToken());
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new Exception(string.Concat("Couldn't compile transformer code:\r\n", exception.Message), exception);
			}
		}

		public override void Process(WorkflowInstance workflowInstance)
		{
			MyContext myContext = new MyContext()
			{
				workflowInstance = workflowInstance,
				activityInstance = workflowInstance.CurrentActivityInstance
			};
			Task<string> task = this.runner(myContext, new CancellationToken());
			if (task.Status == TaskStatus.Faulted)
			{
				if (task.Exception.InnerException == null)
				{
					throw new Exception(string.Concat("Executing transformers custom script code errored:\r\n", task.Exception.Message), task.Exception);
				}
				throw new Exception(string.Concat("Error executing transformers custom script code:\r\n", task.Exception.InnerException.Message), task.Exception.InnerException);
			}
		}

		public override void SetTheToValue(WorkflowInstance workflowInstance, string fromValue)
		{
		}

		public void SetVariableNames(List<string> variableNames)
		{
			this.VariableNames = variableNames;
			this.variables = null;
		}

		public override string ToString()
		{
			return "Code";
		}

		public void Transform()
		{
		}
	}
}