using System;

namespace HL7Soup.Functions.Settings
{
	public interface IWorkflowPattern
	{
		DateTime LastModified
		{
			get;
			set;
		}

		string WorkflowPatternName
		{
			get;
			set;
		}
	}
}