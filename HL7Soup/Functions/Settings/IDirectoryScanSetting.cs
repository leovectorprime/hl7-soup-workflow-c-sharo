using System;

namespace HL7Soup.Functions.Settings
{
	public interface IDirectoryScanSetting : IFunctionWithMessageSetting, ISetting
	{
		bool DeleteFileOnComplete
		{
			get;
			set;
		}

		string DirectoryFilter
		{
			get;
			set;
		}

		string DirectoryPath
		{
			get;
			set;
		}

		string DirectoryToMoveInto
		{
			get;
			set;
		}

		bool EndAfterProcessing
		{
			get;
			set;
		}

		bool MoveIntoDirectoryOnComplete
		{
			get;
			set;
		}

		bool SearchForNewFiles
		{
			get;
			set;
		}
	}
}