using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class ClientResponseDestinationSetting : Setting, IClientResponseDestinationSetting, ISetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "Response Destination";
			}
		}

		protected ClientResponseDestinationSetting()
		{
		}
	}
}