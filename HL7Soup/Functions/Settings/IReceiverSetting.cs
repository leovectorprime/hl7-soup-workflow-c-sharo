using System;

namespace HL7Soup.Functions.Settings
{
	public interface IReceiverSetting : ISetting, IFunctionWithMessageSetting, ISettingWithActivities, ISettingWithFilters, ISettingCanBeDisabled, IWorkflowPattern
	{
		bool AddIncomingMessageToCurrentTab
		{
			get;
			set;
		}

		string ReceivedMessageTemplate
		{
			get;
			set;
		}
	}
}