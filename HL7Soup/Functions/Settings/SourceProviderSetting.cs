using System;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public abstract class SourceProviderSetting : Setting, ISourceProviderSetting, ISetting
	{
		public override string ConnectionTypeName
		{
			get
			{
				return "SourceProvider";
			}
		}

		protected SourceProviderSetting()
		{
		}
	}
}