using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	[Serializable]
	public class DynamicProperty : IDynamicProperty, INotifyPropertyChanged
	{
		private string name;

		public bool isRepeatable
		{
			get;
			set;
		}

		public string LeftText
		{
			get;
			set;
		}

		public List<int> Locations
		{
			get;
			set;
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("Name"));
				}
			}
		}

		public string RightText
		{
			get;
			set;
		}

		public object Value
		{
			get;
			set;
		}

		public DynamicProperty()
		{
			this.Locations = new List<int>();
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}