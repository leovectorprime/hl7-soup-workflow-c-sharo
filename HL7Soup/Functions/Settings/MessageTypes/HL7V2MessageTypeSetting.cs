using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	[Serializable]
	public class HL7V2MessageTypeSetting : MessageTypeSetting, IHL7V2MessageTypeSetting, IMessageTypeSetting, ISetting
	{
		public override string Details
		{
			get
			{
				return "HL7 v2 messsage";
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.MessageTypes.HL7V2MessageType";
			}
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditHL7V2MessageTypeSetting";
			}
		}

		public HL7V2MessageTypeSetting()
		{
		}
	}
}