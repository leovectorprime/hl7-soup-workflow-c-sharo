using HL7Soup.Functions.Settings;
using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	[Serializable]
	public class DynamicMessageTypeSetting : MessageTypeSetting, IDynamicMessageTypeSetting, IMessageTypeSetting, ISetting
	{
		public string BaseMessage
		{
			get;
			set;
		}

		public override string Details
		{
			get
			{
				return "Dynamic Type";
			}
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.MessageTypes.DynamicMessageType";
			}
		}

		public Collection<IDynamicProperty> Properties
		{
			get;
			set;
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditDynamicMessageTypeSetting";
			}
		}

		public DynamicMessageTypeSetting()
		{
		}
	}
}