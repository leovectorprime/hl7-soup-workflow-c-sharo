using HL7Soup.Functions.Settings;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	public interface IHL7V2MessageTypeSetting : IMessageTypeSetting, ISetting
	{

	}
}