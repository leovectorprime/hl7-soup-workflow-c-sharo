using HL7Soup.Functions.Settings;
using System;
using System.Collections.ObjectModel;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	public interface IDynamicMessageTypeSetting : IMessageTypeSetting, ISetting
	{
		string BaseMessage
		{
			get;
			set;
		}

		Collection<IDynamicProperty> Properties
		{
			get;
			set;
		}
	}
}