using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings.MessageTypes
{
	public interface IDynamicProperty
	{
		bool isRepeatable
		{
			get;
			set;
		}

		string LeftText
		{
			get;
			set;
		}

		List<int> Locations
		{
			get;
			set;
		}

		string Name
		{
			get;
			set;
		}

		string RightText
		{
			get;
			set;
		}

		object Value
		{
			get;
			set;
		}
	}
}