using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Settings
{
	public class NotSetSetting : ISetting
	{
		public string ConnectionTypeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public string Details
		{
			get
			{
				return "Setting not set";
			}
		}

		public bool Disabled
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		public string FunctionProviderType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public Guid Id
		{
			get
			{
				return Guid.Empty;
			}
			set
			{
			}
		}

		public string Name
		{
			get
			{
				return "";
			}
			set
			{
			}
		}

		public string SettingDialogType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public int Version
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public NotSetSetting()
		{
		}

		public List<Guid> GetAllActivitiesAtRootLevel()
		{
			return Setting.GetAllActivitiesAtRootLevel(this);
		}

		public List<Guid> GetAllActivitiesAtRootLevel(Guid lastSettingId)
		{
			return Setting.GetAllActivitiesAtRootLevel(this, lastSettingId);
		}

		public List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			throw new NotImplementedException();
		}

		public void SwapActivityIds(Dictionary<Guid, Guid> oldAndNewGuidDictionary)
		{
		}

		public string Upgrade()
		{
			return "";
		}
	}
}