using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Settings
{
	[Serializable]
	public class ActivitySetting : Setting, IActivitySetting, ISetting, ISettingWithFilters, ISettingWithTransformers, ISettingCanBeDisabled
	{
		private Guid filters = Guid.Empty;

		private Guid transformers = Guid.Empty;

		private Guid sender = Guid.Empty;

		public override string ConnectionTypeName
		{
			get
			{
				return "Activity";
			}
		}

		public override string Details
		{
			get
			{
				if (this.Sender == Guid.Empty)
				{
					return "Send to ...";
				}
				if (FunctionHelpers.GetSenderSettingById(this.sender) == null)
				{
					return "Send to ...";
				}
				return string.Concat("Send to ", FunctionHelpers.GetSenderSettingById(this.sender).Name);
			}
		}

		public bool Disabled
		{
			get;
			set;
		}

		public virtual bool DisableNotAvailable
		{
			get;
			set;
		}

		public Guid Filters
		{
			get
			{
				return this.filters;
			}
			set
			{
				this.filters = value;
			}
		}

		public virtual bool FiltersNotAvailable
		{
			get;
			set;
		}

		public override string FunctionProviderType
		{
			get
			{
				return "HL7Soup.Functions.Activities.Activity";
			}
		}

		public Guid Sender
		{
			get
			{
				return this.sender;
			}
			set
			{
				this.sender = value;
			}
		}

		public override string SettingDialogType
		{
			get
			{
				return "HL7Soup.Dialogs.EditActivitySetting";
			}
		}

		public Guid Transformers
		{
			get
			{
				return this.transformers;
			}
			set
			{
				this.transformers = value;
			}
		}

		public virtual bool TransformersNotAvailable
		{
			get;
			set;
		}

		public ActivitySetting()
		{
		}

		public override List<ISetting> GetChildSettingInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> childSettingInstances = base.GetChildSettingInstances(settingsBeingEdited);
			childSettingInstances.AddRange(this.GetFilterInstances(settingsBeingEdited));
			childSettingInstances.AddRange(this.GetTransformerInstances(settingsBeingEdited));
			return childSettingInstances;
		}

		public List<ISetting> GetFilterInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(this.Filters))
			{
				ISetting filterSettingById = FunctionHelpers.GetFilterSettingById(this.filters);
				if (filterSettingById != null)
				{
					settings.Add(filterSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[this.Filters]);
			}
			return settings;
		}

		public List<ISetting> GetTransformerInstances(Dictionary<Guid, ISetting> settingsBeingEdited)
		{
			List<ISetting> settings = new List<ISetting>();
			if (settingsBeingEdited == null || !settingsBeingEdited.ContainsKey(this.Transformers))
			{
				ISetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(this.Transformers);
				if (transformerSettingById != null)
				{
					settings.Add(transformerSettingById);
				}
			}
			else
			{
				settings.Add(settingsBeingEdited[this.Transformers]);
			}
			return settings;
		}
	}
}