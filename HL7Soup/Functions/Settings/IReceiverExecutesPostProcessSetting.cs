using System;

namespace HL7Soup.Functions.Settings
{
	public interface IReceiverExecutesPostProcessSetting
	{
		bool ExecutePostProcess
		{
			get;
			set;
		}
	}
}