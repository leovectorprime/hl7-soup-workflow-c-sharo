using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceOperation : IComparable
	{
		public string Action
		{
			get;
			set;
		}

		public WebServiceObject Input
		{
			get;
			internal set;
		}

		public bool IsOneWay
		{
			get;
			internal set;
		}

		public string Name
		{
			get;
			set;
		}

		public WebServiceObject Output
		{
			get;
			internal set;
		}

		public bool PassAuthenticationInSoapHeader
		{
			get;
			set;
		}

		public string RequestSoap
		{
			get;
			set;
		}

		public string ResponseSoap
		{
			get;
			set;
		}

		public bool UseSoap12
		{
			get;
			set;
		}

		public WebServiceOperation()
		{
		}

		public int CompareTo(object obj)
		{
			return this.Name.CompareTo(((WebServiceOperation)obj).Name);
		}

		public override string ToString()
		{
			return this.Name;
		}
	}
}