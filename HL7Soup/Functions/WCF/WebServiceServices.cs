using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceServices
	{
		public List<WebServiceEndPoint> EndPoint { get; set; } = new List<WebServiceEndPoint>();

		public string Name
		{
			get;
			set;
		}

		public WebServiceServices()
		{
		}
	}
}