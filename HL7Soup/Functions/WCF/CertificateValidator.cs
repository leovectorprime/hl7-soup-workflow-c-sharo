using HL7Soup;
using NLog;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Windows;

namespace HL7Soup.Functions.WCF
{
	public static class CertificateValidator
	{
		private static bool logs;

		public static bool WarnAboutInvalidCertificates
		{
			get;
			set;
		}

		static CertificateValidator()
		{
		}

		public static void StartValidating()
		{
			ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Combine(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback((object send, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => {
				string thumbprint;
				if (send is SmtpClient)
				{
					return true;
				}
				if (((HttpWebRequest)send).Address.AbsoluteUri == "https://hl7soup.com/SoupLicenseService/LicenseService.svc" && !certificate.Subject.ToLowerInvariant().Contains("cn=hl7soup.com") && DateTime.Now < DateTime.Parse("2020-12-12"))
				{
					Log.Instance.Error(string.Concat("The certificate of the license server had an incorrect public key.  This is not the certificate the license server is pinned to.\r\n", (certificate != null ? certificate.ToString() : null), " now=", DateTime.Now.ToString()));
					return false;
				}
				if (sslPolicyErrors == SslPolicyErrors.None)
				{
					return true;
				}
				X509Certificate2 x509Certificate2 = (X509Certificate2)certificate;
				if (x509Certificate2 != null)
				{
					thumbprint = x509Certificate2.Thumbprint;
				}
				else
				{
					thumbprint = null;
				}
				if (thumbprint == SharedSettings.DefaultSSLCertificateThumbprint)
				{
					return true;
				}
				if (CertificateValidator.WarnAboutInvalidCertificates)
				{
					if (!CertificateValidator.logs)
					{
						MessageBox.Show(string.Concat("The servers SSL certificate did not pass validation but you can still use it if you choose to.\r\n\r\nInvalid due to '", sslPolicyErrors.ToString(), "'.\r\n\r\nIf you do not trust the source of this service you should not use it."), "Invalid Certificate", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					}
					else
					{
						Log.Instance.Warn(string.Concat("The SSL certificate did not pass validation due to '", sslPolicyErrors.ToString(), "'.\r\nIf you do not trust the source of this service you should not use it."));
					}
				}
				return true;
			}));
		}

		public static void StartValidatingToLogs()
		{
			CertificateValidator.logs = true;
			CertificateValidator.StartValidating();
		}
	}
}