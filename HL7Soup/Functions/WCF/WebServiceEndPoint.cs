using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceEndPoint
	{
		public string Address
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public List<WebServiceOperation> Operations { get; set; } = new List<WebServiceOperation>();

		public WebServiceEndPoint()
		{
		}
	}
}