using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;

namespace HL7Soup.Functions.WCF
{
	public class WsdlParser
	{
		private Dictionary<string, XmlNode> MessageDictionary = new Dictionary<string, XmlNode>();

		private int stack;

		private Dictionary<string, List<XmlNode>> complexTypes = new Dictionary<string, List<XmlNode>>();

		private Dictionary<string, WsdlParser.Namespaces> complexTypesNamespaces = new Dictionary<string, WsdlParser.Namespaces>();

		private Dictionary<string, IEnumerable<XmlNode>> extensionElements = new Dictionary<string, IEnumerable<XmlNode>>();

		private Dictionary<string, XmlNode> Elements = new Dictionary<string, XmlNode>();

		private Dictionary<string, XmlNode> Imports = new Dictionary<string, XmlNode>();

		private Dictionary<string, XmlNode> Schemas = new Dictionary<string, XmlNode>();

		private Dictionary<string, List<XmlNode>> SchemasImports = new Dictionary<string, List<XmlNode>>();

		public bool Authentication
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public bool SingleBinding
		{
			get;
			set;
		}

		public string UserName
		{
			get;
			set;
		}

		public string Wsdl { get; set; } = "";

		public WsdlParser()
		{
		}

		private int AddChildProperties(StringBuilder soapMessage, int indent, string schemaNamespace, XmlNode operationElement, List<XmlNode> types, string currentNamespace, string parentType, HashSet<string> parentNamespacesAlreadyDefined, WsdlParser.Namespaces namespaces, ref bool formQualified)
		{
			this.stack++;
			int num = this.stack;
			int num1 = 0;
			foreach (XmlNode childNode in this.GetChildNodes(operationElement, "complexType"))
			{
				if (parentType != this.GetAttributByName(childNode, "name", false) && parentType != "")
				{
					continue;
				}
				namespaces.GetNamespaces(childNode);
				int num2 = 0;
				foreach (XmlNode allPropertyElement in this.GetAllPropertyElements(childNode, namespaces, operationElement))
				{
					string attributByName = this.GetAttributByName(allPropertyElement, "name");
					string str = schemaNamespace;
					string attributByName1 = this.GetAttributByName(allPropertyElement, "type", false);
					if (attributByName1 != "")
					{
						str = attributByName1.Substring(0, attributByName1.IndexOf(":"));
						attributByName1 = attributByName1.Substring(str.Length + 1);
					}
					num1++;
					soapMessage.Append("\r\n");
					soapMessage.Append(new string(' ', indent));
					soapMessage.Append("<");
					if (!string.IsNullOrEmpty(namespaces.GetNamespaceShortcut(currentNamespace)))
					{
						soapMessage.Append(namespaces.GetNamespaceShortcut(currentNamespace));
						soapMessage.Append(":");
					}
					soapMessage.Append(attributByName);
					WsdlParser.Namespaces @namespace = namespaces.Clone();
					XmlNode xmlNodes = null;
					xmlNodes = (attributByName1 == "" ? allPropertyElement : this.GetSchemaElement(attributByName1, @namespace.GetNamespace(str), types, @namespace, ref formQualified));
					if (xmlNodes == null)
					{
						if (!this.IsElementFormQualified(allPropertyElement, formQualified))
						{
							soapMessage.Append(" xmlns=\"\"");
						}
						soapMessage.Append(">");
					}
					else
					{
						string namespace1 = currentNamespace;
						if (attributByName1 != "" && @namespace.GetNamespace(str) != schemaNamespace)
						{
							if (!parentNamespacesAlreadyDefined.Contains(@namespace.GetNamespace(str)))
							{
								parentNamespacesAlreadyDefined.Add(@namespace.GetNamespace(str));
								soapMessage.Append("  xmlns:");
								soapMessage.Append(@namespace.GetNamespaceShortcut(@namespace.GetNamespace(str)));
								soapMessage.Append("=\"");
								soapMessage.Append(@namespace.GetNamespace(str));
								soapMessage.Append("\"");
							}
							namespace1 = @namespace.GetNamespace(str);
						}
						soapMessage.Append(">");
						num2 = this.AddChildProperties(soapMessage, indent + 3, namespaces.GetNamespaceShortcut(currentNamespace), xmlNodes, types, namespace1, attributByName1, new HashSet<string>(parentNamespacesAlreadyDefined.ToList<string>()), @namespace, ref formQualified);
					}
					if (num2 >= 1)
					{
						soapMessage.Append("\r\n");
						soapMessage.Append(new string(' ', indent));
						num2 = 0;
					}
					soapMessage.Append("</");
					if (!string.IsNullOrEmpty(namespaces.GetNamespaceShortcut(currentNamespace)))
					{
						soapMessage.Append(namespaces.GetNamespaceShortcut(currentNamespace));
						soapMessage.Append(":");
					}
					soapMessage.Append(attributByName);
					soapMessage.Append(">");
				}
			}
			return num1;
		}

		private int AddHeaderChildProperties(StringBuilder soapMessage, int indent, string schemaNamespace, XmlNode operationElement, List<XmlNode> types, string currentNamespace, string parentType, HashSet<string> parentNamespacesAlreadyDefined, WsdlParser.Namespaces namespaces, ref bool formQualified)
		{
			if (this.GetAttributByName(operationElement, "type") == "")
			{
				return this.AddChildProperties(soapMessage, indent, schemaNamespace, operationElement, types, currentNamespace, parentType, parentNamespacesAlreadyDefined, namespaces, ref formQualified);
			}
			string attributByName = this.GetAttributByName(operationElement, "name");
			soapMessage.Append(new string(' ', indent));
			soapMessage.Append("<");
			if (!string.IsNullOrEmpty(namespaces.GetNamespaceShortcut(currentNamespace)))
			{
				soapMessage.Append(namespaces.GetNamespaceShortcut(currentNamespace));
				soapMessage.Append(":");
			}
			soapMessage.Append(attributByName);
			if (!this.IsElementFormQualified(operationElement, formQualified))
			{
				soapMessage.Append(" xmlns=\"\"");
			}
			soapMessage.Append(">");
			soapMessage.Append("</");
			if (!string.IsNullOrEmpty(namespaces.GetNamespaceShortcut(currentNamespace)))
			{
				soapMessage.Append(namespaces.GetNamespaceShortcut(currentNamespace));
				soapMessage.Append(":");
			}
			soapMessage.Append(attributByName);
			soapMessage.Append(">");
			soapMessage.Append("\r\n");
			return 1;
		}

		private bool DoesSchemaHaveAnyComplexTypes(XmlNode schema)
		{
			bool flag;
			IEnumerator enumerator = schema.ChildNodes.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					if (((XmlNode)enumerator.Current).LocalName != "complexType")
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return flag;
		}

		private IEnumerable<XmlNode> GetAllPropertiesFromExtensions(XmlNode operationElement, string type, WsdlParser.Namespaces namespaces)
		{
			IEnumerable<XmlNode> item;
			if (this.extensionElements.ContainsKey(type))
			{
				return this.extensionElements[type];
			}
			XmlNode xmlNodes = null;
			IEnumerator enumerator = operationElement.ChildNodes.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					XmlNode current = (XmlNode)enumerator.Current;
					if (!string.Equals(current.LocalName, "complextype", StringComparison.OrdinalIgnoreCase))
					{
						continue;
					}
					xmlNodes = current;
					if (type != this.GetAttributByName(xmlNodes, "name", false) && type != "")
					{
						continue;
					}
					namespaces = namespaces.GetNamespaces(current);
					this.extensionElements[type] = this.GetAllPropertyElements(xmlNodes, namespaces, operationElement);
					item = this.extensionElements[type];
					return item;
				}
				this.extensionElements[type] = new List<XmlNode>();
				return new List<XmlNode>();
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return item;
		}

		private List<XmlNode> GetAllPropertyElements(XmlNode complexType, WsdlParser.Namespaces namespaces, XmlNode operationElement)
		{
			string attributByName = this.GetAttributByName(complexType, "name", false);
			if (attributByName != "" && this.complexTypes.ContainsKey(attributByName))
			{
				namespaces.UseThese(this.complexTypesNamespaces[attributByName]);
				return this.complexTypes[attributByName];
			}
			List<XmlNode> xmlNodes = new List<XmlNode>();
			foreach (XmlNode childNode in complexType.ChildNodes)
			{
				if (childNode.LocalName == "sequence")
				{
					foreach (XmlNode childNode1 in childNode.ChildNodes)
					{
						if (childNode1.LocalName != "element")
						{
							continue;
						}
						namespaces.GetNamespaces(childNode1);
						xmlNodes.Add(childNode1);
					}
				}
				if (!string.Equals(childNode.LocalName, "complexcontent", StringComparison.OrdinalIgnoreCase))
				{
					continue;
				}
				foreach (XmlNode xmlNodes1 in childNode.ChildNodes)
				{
					if (xmlNodes1.LocalName != "extension")
					{
						continue;
					}
					string str = "";
					string attributByName1 = this.GetAttributByName(xmlNodes1, "base", false);
					if (attributByName1 != "")
					{
						str = attributByName1.Substring(0, attributByName1.IndexOf(":"));
						attributByName1 = attributByName1.Substring(str.Length + 1);
					}
					xmlNodes.AddRange(this.GetAllPropertiesFromExtensions(operationElement, attributByName1, namespaces));
					xmlNodes.AddRange(this.GetAllPropertyElements(xmlNodes1, namespaces, operationElement));
				}
			}
			if (attributByName != "")
			{
				this.complexTypesNamespaces[attributByName] = namespaces;
				this.complexTypes[attributByName] = xmlNodes;
			}
			return xmlNodes;
		}

		private string GetAttributByName(XmlNode element, string name)
		{
			return this.GetAttributByName(element, name, true);
		}

		private string GetAttributByName(XmlNode element, string name, bool mustFind)
		{
			string value;
			name = this.RemoveNamespace(name);
			if (element != null)
			{
				IEnumerator enumerator = element.Attributes.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						XmlNode current = (XmlNode)enumerator.Current;
						if (!string.Equals(this.RemoveNamespace(current.Name), name, StringComparison.OrdinalIgnoreCase))
						{
							continue;
						}
						value = current.Value;
						return value;
					}
					if (mustFind)
					{
						throw new Exception(string.Concat("Couldn't find attribute '", name, "'"));
					}
					return "";
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				return value;
			}
			if (mustFind)
			{
				throw new Exception(string.Concat("Couldn't find attribute '", name, "'"));
			}
			return "";
		}

		private List<XmlNode> GetChildNodes(XmlNode parentNode, string localName)
		{
			List<XmlNode> xmlNodes = new List<XmlNode>();
			string attributByName = this.GetAttributByName(parentNode, "type", false);
			if (string.IsNullOrEmpty(attributByName))
			{
				foreach (XmlNode childNode in parentNode.ChildNodes)
				{
					if (!string.Equals(childNode.LocalName, localName, StringComparison.OrdinalIgnoreCase))
					{
						continue;
					}
					xmlNodes.Add(childNode);
				}
			}
			else
			{
				string str = attributByName;
				if (str.Contains(":"))
				{
					str = str.Substring(str.IndexOf(":") + 1);
				}
				foreach (XmlNode childNode1 in parentNode.ParentNode.ChildNodes)
				{
					if (!(childNode1.LocalName == localName) || !string.Equals(this.GetAttributByName(childNode1, "name", false), str, StringComparison.OrdinalIgnoreCase))
					{
						continue;
					}
					xmlNodes.Add(childNode1);
				}
			}
			return xmlNodes;
		}

		private XmlNode GetImportedSchema(string schemaNamespace, XmlNode schema, WsdlParser.Namespaces namespaces, ref bool formQualified)
		{
			string attributByName = this.GetAttributByName(schema, "targetNamespace", false);
			string str = string.Concat(schemaNamespace, attributByName);
			if (this.Schemas.ContainsKey(str))
			{
				formQualified = this.SetFormQualified(this.Schemas[str], formQualified);
				return this.Schemas[str];
			}
			if (!this.SchemasImports.ContainsKey(str))
			{
				List<XmlNode> xmlNodes = new List<XmlNode>();
				foreach (XmlNode childNode in schema.ChildNodes)
				{
					if (childNode.LocalName != "import")
					{
						continue;
					}
					string value = "";
					string value1 = "";
					foreach (XmlNode attribute in childNode.Attributes)
					{
						if (string.Equals(attribute.LocalName, "schemalocation", StringComparison.OrdinalIgnoreCase))
						{
							value1 = attribute.Value;
						}
						if (attribute.LocalName != "namespace")
						{
							continue;
						}
						value = attribute.Value;
					}
					if (!(value != "") || !(value1 != ""))
					{
						continue;
					}
					xmlNodes.Add(childNode);
				}
				this.SchemasImports[str] = xmlNodes;
			}
			foreach (XmlNode item in this.SchemasImports[str])
			{
				if (item.LocalName != "import")
				{
					continue;
				}
				string str1 = "";
				string value2 = "";
				foreach (XmlNode attribute1 in item.Attributes)
				{
					if (string.Equals(attribute1.LocalName, "schemalocation", StringComparison.OrdinalIgnoreCase))
					{
						value2 = attribute1.Value;
					}
					if (attribute1.LocalName != "namespace")
					{
						continue;
					}
					str1 = attribute1.Value;
				}
				if (!(str1 != "") || !(value2 != ""))
				{
					continue;
				}
				if (!this.Imports.ContainsKey(str1))
				{
					XmlDocument xmlDocument = new XmlDocument();
					this.LoadWsdl(xmlDocument, value2);
					this.Imports[str1] = xmlDocument;
				}
				if (str1 != schemaNamespace)
				{
					continue;
				}
				foreach (XmlNode childNode1 in this.Imports[str1].ChildNodes)
				{
					if (childNode1.LocalName != "schema")
					{
						continue;
					}
					schema = childNode1;
					namespaces = namespaces.GetNamespaces(schema);
				}
				if (schema != null)
				{
					continue;
				}
				throw new Exception("schema section missing in WSDL imported wsdl");
			}
			this.Schemas[str] = schema;
			formQualified = this.SetFormQualified(schema, formQualified);
			return schema;
		}

		private WebServiceMessage GetMessage(XmlNode definitions, XmlNode xmlnode2)
		{
			WebServiceMessage webServiceMessage = new WebServiceMessage()
			{
				Name = this.GetAttributByName(xmlnode2, "part"),
				Message = this.GetAttributByName(xmlnode2, "message")
			};
			if (webServiceMessage.Message.Contains(":"))
			{
				webServiceMessage.Message = webServiceMessage.Message.Substring(webServiceMessage.Message.IndexOf(":") + 1);
			}
			foreach (XmlNode childNode in this.GetMessageNode(webServiceMessage.Message, definitions).ChildNodes)
			{
				if (!(childNode.LocalName == "part") || !(this.GetAttributByName(childNode, "name") == webServiceMessage.Name))
				{
					continue;
				}
				webServiceMessage.Element = this.GetAttributByName(childNode, "element");
				return webServiceMessage;
			}
			return webServiceMessage;
		}

		private XmlNode GetMessageNode(string message, XmlNode definitions)
		{
			XmlNode xmlNodes;
			if (this.MessageDictionary.ContainsKey(message))
			{
				return this.MessageDictionary[message];
			}
			IEnumerator enumerator = definitions.ChildNodes.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					XmlNode current = (XmlNode)enumerator.Current;
					if (!(current.LocalName == "message") || !(this.GetAttributByName(current, "name", false) == message))
					{
						continue;
					}
					this.MessageDictionary[message] = current;
					xmlNodes = current;
					return xmlNodes;
				}
				throw new Exception(string.Concat("Could not find message definition called ", message));
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return xmlNodes;
		}

		private XmlNode GetOperationElement(string elementName, string schemaNamespace, List<XmlNode> types, WsdlParser.Namespaces namespaces, ref bool formQualified)
		{
			XmlNode xmlNodes;
			IDisposable disposable;
			string str = string.Concat(schemaNamespace, elementName);
			if (this.Elements.ContainsKey(str))
			{
				return this.Elements[str];
			}
			XmlNode importedSchema = null;
			List<XmlNode>.Enumerator enumerator = types.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					XmlNode current = enumerator.Current;
					if (current.LocalName != "schema")
					{
						continue;
					}
					importedSchema = current;
					formQualified = this.SetFormQualified(importedSchema, formQualified);
					string str1 = schemaNamespace;
					if (!str1.EndsWith("/"))
					{
						str1 = string.Concat(str1, "/");
					}
					importedSchema = this.GetImportedSchema(schemaNamespace, importedSchema, namespaces, ref formQualified);
					if (!this.IsCorrectNamespace(importedSchema, schemaNamespace))
					{
						continue;
					}
					namespaces = namespaces.GetNamespaces(current);
					XmlNode xmlNodes1 = null;
					IEnumerator enumerator1 = importedSchema.ChildNodes.GetEnumerator();
					try
					{
						while (enumerator1.MoveNext())
						{
							XmlNode current1 = (XmlNode)enumerator1.Current;
							if (current1.LocalName != "element")
							{
								continue;
							}
							xmlNodes1 = current1;
							IEnumerator enumerator2 = xmlNodes1.Attributes.GetEnumerator();
							try
							{
								while (enumerator2.MoveNext())
								{
									XmlNode current2 = (XmlNode)enumerator2.Current;
									if (!(current2.LocalName == "name") || !(current2.Value == elementName))
									{
										continue;
									}
									this.Elements[str] = xmlNodes1;
									namespaces = namespaces.GetNamespaces(current1);
									xmlNodes = xmlNodes1;
									return xmlNodes;
								}
							}
							finally
							{
								disposable = enumerator2 as IDisposable;
								if (disposable != null)
								{
									disposable.Dispose();
								}
							}
						}
					}
					finally
					{
						disposable = enumerator1 as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
					if (xmlNodes1 != null)
					{
						continue;
					}
					throw new Exception("element section missing in WSDL");
				}
				if (importedSchema == null)
				{
					throw new Exception("schema section missing in WSDL");
				}
				return null;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return xmlNodes;
		}

		private string GetOperationName(WebServiceOperation webServiceOperation, bool isInbound)
		{
			WebServiceObject output = webServiceOperation.Output;
			if (isInbound)
			{
				output = webServiceOperation.Input;
			}
			if (output.Name != "")
			{
				return output.Name;
			}
			if (isInbound)
			{
				return webServiceOperation.Name;
			}
			return string.Concat(webServiceOperation.Name, "Response");
		}

		private XmlNode GetSchemaElement(string elementName, string schemaNamespace, List<XmlNode> types, WsdlParser.Namespaces namespaces, ref bool formQualified)
		{
			XmlNode xmlNodes;
			string str = string.Concat(schemaNamespace, elementName);
			if (this.Elements.ContainsKey(str))
			{
				namespaces = namespaces.GetNamespaces(this.Elements[str]);
				return this.Elements[str];
			}
			XmlNode importedSchema = null;
			List<XmlNode>.Enumerator enumerator = types.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					XmlNode current = enumerator.Current;
					if (current.LocalName != "schema")
					{
						continue;
					}
					importedSchema = current;
					importedSchema = this.GetImportedSchema(schemaNamespace, importedSchema, namespaces, ref formQualified);
					if (!this.IsCorrectNamespace(importedSchema, schemaNamespace))
					{
						continue;
					}
					if (this.DoesSchemaHaveAnyComplexTypes(importedSchema))
					{
						this.Elements[str] = importedSchema;
						namespaces = namespaces.GetNamespaces(current);
						xmlNodes = importedSchema;
						return xmlNodes;
					}
					else
					{
						xmlNodes = null;
						return xmlNodes;
					}
				}
				if (importedSchema == null)
				{
					throw new Exception("schema section missing in WSDL");
				}
				return null;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return xmlNodes;
		}

		private WebServiceObject GetWebServiceObject(XmlNode definitions, XmlNode xmlnode1)
		{
			WebServiceObject webServiceObject = new WebServiceObject()
			{
				Name = this.GetAttributByName(xmlnode1, "name", false)
			};
			foreach (XmlNode childNode in xmlnode1.ChildNodes)
			{
				if (childNode.LocalName == "header" && this.GetAttributByName(childNode, "message", false) != "")
				{
					WebServiceMessage message = this.GetMessage(definitions, childNode);
					webServiceObject.Header.Add(message);
				}
				if (!(childNode.LocalName == "body") || !(this.GetAttributByName(childNode, "message", false) != ""))
				{
					continue;
				}
				WebServiceMessage webServiceMessage = this.GetMessage(definitions, childNode);
				webServiceObject.Body.Add(webServiceMessage);
			}
			return webServiceObject;
		}

		public WebServiceProperties GetWebServiceProperties(string wsdl)
		{
			this.Wsdl = wsdl;
			XmlDocument xmlDocument = new XmlDocument();
			this.LoadWsdl(xmlDocument, this.Wsdl);
			XmlNode xmlNodes = null;
			foreach (XmlNode childNode in xmlDocument.ChildNodes)
			{
				if (childNode.LocalName != "definitions")
				{
					continue;
				}
				xmlNodes = childNode;
			}
			if (xmlNodes == null)
			{
				throw new Exception("definitions section missing in WSDL");
			}
			string value = null;
			foreach (XmlNode attribute in xmlNodes.Attributes)
			{
				if (!string.Equals(attribute.LocalName, "targetnamespace", StringComparison.OrdinalIgnoreCase))
				{
					continue;
				}
				value = attribute.Value;
			}
			if (value == null)
			{
				throw new Exception("targetNamespace attribute missing in definitions section");
			}
			foreach (XmlNode childNode1 in xmlNodes.ChildNodes)
			{
				if (childNode1.LocalName != "import")
				{
					continue;
				}
				string attributByName = this.GetAttributByName(childNode1, "location");
				XmlDocument xmlDocument1 = new XmlDocument();
				this.LoadWsdl(xmlDocument1, attributByName);
				XmlNode xmlNodes1 = xmlDocument.ImportNode(xmlDocument1.DocumentElement, true);
				if (xmlNodes1.LocalName != "definitions")
				{
					throw new Exception("Import doesn't contain definitions.  Please contact HL7 Soup support for help.");
				}
				string str = this.GetAttributByName(xmlNodes1, "targetNamespace", false);
				if (!string.IsNullOrEmpty(str))
				{
					value = str;
				}
				List<XmlNode> xmlNodes2 = new List<XmlNode>();
				foreach (XmlNode childNode2 in xmlNodes1.ChildNodes)
				{
					xmlNodes2.Add(childNode2);
				}
				foreach (XmlNode xmlNode in xmlNodes2)
				{
					xmlNodes.AppendChild(xmlNode);
				}
			}
			List<XmlNode> xmlNodes3 = new List<XmlNode>();
			List<XmlNode> xmlNodes4 = new List<XmlNode>();
			List<XmlNode> xmlNodes5 = new List<XmlNode>();
			List<XmlNode> xmlNodes6 = new List<XmlNode>();
			XmlNode xmlNodes7 = null;
			foreach (XmlNode childNode3 in xmlNodes.ChildNodes)
			{
				if (childNode3.LocalName == "service")
				{
					xmlNodes3.Add(childNode3);
				}
				if (childNode3.LocalName == "binding")
				{
					xmlNodes4.Add(childNode3);
				}
				if (childNode3.LocalName == "portType")
				{
					xmlNodes5.Add(childNode3);
				}
				if (childNode3.LocalName == "types")
				{
					foreach (XmlNode childNode4 in childNode3.ChildNodes)
					{
						xmlNodes6.Add(childNode4);
					}
				}
				if (!string.Equals(childNode3.LocalName, "policy", StringComparison.InvariantCultureIgnoreCase))
				{
					continue;
				}
				xmlNodes7 = childNode3;
			}
			if (xmlNodes3.Count == 0)
			{
				throw new Exception("service section missing in WSDL");
			}
			if (xmlNodes4.Count == 0)
			{
				throw new Exception("binding section missing in WSDL");
			}
			if (xmlNodes6.Count == 0)
			{
				throw new Exception("types section missing in WSDL");
			}
			bool flag = this.PassAuthenticationInSoapHeader(xmlNodes7);
			WebServiceProperties webServiceProperty = new WebServiceProperties();
			foreach (XmlNode xmlNode1 in xmlNodes3)
			{
				WebServiceServices webServiceService = new WebServiceServices();
				webServiceProperty.WebServiceServices.Add(webServiceService);
				foreach (XmlNode attribute1 in xmlNode1.Attributes)
				{
					if (attribute1.LocalName != "name")
					{
						continue;
					}
					webServiceService.Name = attribute1.Value;
				}
				int num = 0;
				XmlNode xmlNodes8 = null;
				foreach (XmlNode childNode5 in xmlNode1.ChildNodes)
				{
					num++;
					if (this.SingleBinding && num > 1)
					{
						continue;
					}
					if (childNode5.LocalName == "port")
					{
						xmlNodes8 = childNode5;
					}
					string attributByName1 = this.GetAttributByName(xmlNodes8, "name");
					string str1 = this.GetAttributByName(xmlNodes8, "binding");
					string str2 = str1;
					if (str1.Contains(":"))
					{
						str2 = str1.Substring(str1.IndexOf(":") + 1);
					}
					bool flag1 = false;
					XmlNode xmlNodes9 = null;
					foreach (XmlNode childNode6 in xmlNodes8.ChildNodes)
					{
						if (childNode6.LocalName != "address")
						{
							continue;
						}
						if (childNode6.NamespaceURI.StartsWith("http://schemas.xmlsoap.org/wsdl/soap12"))
						{
							flag1 = true;
						}
						xmlNodes9 = childNode6;
						goto Label0;
					}
				Label0:
					if (xmlNodes9 == null)
					{
						throw new Exception("address section missing in WSDL");
					}
					foreach (XmlNode xmlNode2 in xmlNodes4)
					{
						if (this.GetAttributByName(xmlNode2, "name") != str2)
						{
							continue;
						}
						WebServiceEndPoint webServiceEndPoint = new WebServiceEndPoint()
						{
							Name = attributByName1
						};
						string str3 = this.RemoveNamespace(this.GetAttributByName(xmlNode2, "type"));
						webServiceService.EndPoint.Add(webServiceEndPoint);
						foreach (XmlNode attribute2 in xmlNodes9.Attributes)
						{
							if (attribute2.LocalName != "location")
							{
								continue;
							}
							webServiceEndPoint.Address = attribute2.Value;
						}
						XmlNode xmlNodes10 = null;
						foreach (XmlNode childNode7 in xmlNode2.ChildNodes)
						{
							if (childNode7.LocalName != "operation")
							{
								continue;
							}
							xmlNodes10 = childNode7;
							WebServiceOperation webServiceOperation = new WebServiceOperation()
							{
								UseSoap12 = flag1,
								PassAuthenticationInSoapHeader = flag
							};
							foreach (XmlNode attribute3 in xmlNodes10.Attributes)
							{
								if (attribute3.LocalName != "name")
								{
									continue;
								}
								webServiceOperation.Name = attribute3.Value;
								goto Label1;
							}
						Label1:
							this.Elements = new Dictionary<string, XmlNode>();
							foreach (XmlNode xmlNode3 in xmlNodes5)
							{
								if (this.GetAttributByName(xmlNode3, "name") != str3)
								{
									continue;
								}
								foreach (XmlNode childNode8 in xmlNode3.ChildNodes)
								{
									if (childNode8.LocalName != "operation")
									{
										continue;
									}
									string attributByName2 = this.GetAttributByName(childNode8, "name");
									if (attributByName2 != webServiceOperation.Name)
									{
										continue;
									}
									foreach (XmlNode childNode9 in childNode8.ChildNodes)
									{
										if (childNode9.LocalName == "input")
										{
											string attributByName3 = this.GetAttributByName(childNode9, "action", false);
											if (attributByName3 == "")
											{
												attributByName3 = attributByName2;
											}
											webServiceOperation.Action = attributByName3;
										}
									}
								}
							}
							foreach (XmlNode childNode10 in xmlNodes10.ChildNodes)
							{
								if (childNode10.LocalName == "input")
								{
									webServiceOperation.Input = this.GetWebServiceObject(xmlNodes, childNode10);
								}
								if (childNode10.LocalName != "output")
								{
									continue;
								}
								webServiceOperation.Output = this.GetWebServiceObject(xmlNodes, childNode10);
							}
							HashSet<string> strs = new HashSet<string>();
							WsdlParser.Namespaces @namespace = new WsdlParser.Namespaces(value);
							this.extensionElements = new Dictionary<string, IEnumerable<XmlNode>>();
							this.complexTypes = new Dictionary<string, List<XmlNode>>();
							this.complexTypesNamespaces = new Dictionary<string, WsdlParser.Namespaces>();
							this.Schemas = new Dictionary<string, XmlNode>();
							bool flag2 = false;
							string operationName = this.GetOperationName(webServiceOperation, true);
							XmlNode operationElement = this.GetOperationElement(operationName, value, xmlNodes6, @namespace, ref flag2);
							int num1 = 6;
							StringBuilder stringBuilder = new StringBuilder();
							if (flag1)
							{
								stringBuilder.Append("<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n  <s:Header");
							}
							else
							{
								stringBuilder.Append("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <s:Header");
							}
							if (webServiceOperation.Input.Header.Count > 0)
							{
								stringBuilder.Append("  xmlns:");
								stringBuilder.Append(@namespace.GetNamespaceShortcut(value));
								stringBuilder.Append("=\"");
								stringBuilder.Append(value);
								stringBuilder.Append("\"");
							}
							stringBuilder.Append(">");
							if (webServiceOperation.Input.Header.Count > 0)
							{
								stringBuilder.Append("\r\n");
							}
							foreach (WebServiceMessage header in webServiceOperation.Input.Header)
							{
								num1 = 6;
								bool flag3 = false;
								XmlNode operationElement1 = this.GetOperationElement(header.Name, value, xmlNodes6, @namespace, ref flag3);
								this.AddHeaderChildProperties(stringBuilder, num1, value, operationElement1, xmlNodes6, value, "", strs, @namespace, ref flag3);
							}
							if (webServiceOperation.Input.Header.Count > 0)
							{
								stringBuilder.Append("  ");
							}
							stringBuilder.Append("</s:Header>\r\n  <s:Body>\r\n    <");
							stringBuilder.Append(webServiceOperation.Name);
							stringBuilder.Append(" xmlns=\"");
							stringBuilder.Append(value);
							stringBuilder.Append("\">");
							if (this.AddChildProperties(stringBuilder, 6, value, operationElement, xmlNodes6, value, "", strs, @namespace, ref flag2) > 0)
							{
								stringBuilder.Append("\r\n");
								stringBuilder.Append("    ");
							}
							stringBuilder.Append("</");
							stringBuilder.Append(webServiceOperation.Name);
							stringBuilder.Append(">\r\n  </s:Body>\r\n");
							stringBuilder.Append("</s:Envelope>");
							webServiceOperation.RequestSoap = stringBuilder.ToString();
							@namespace = new WsdlParser.Namespaces(value);
							this.extensionElements = new Dictionary<string, IEnumerable<XmlNode>>();
							this.complexTypes = new Dictionary<string, List<XmlNode>>();
							this.complexTypesNamespaces = new Dictionary<string, WsdlParser.Namespaces>();
							this.Schemas = new Dictionary<string, XmlNode>();
							flag2 = false;
							if (webServiceOperation.Output == null)
							{
								webServiceOperation.IsOneWay = true;
							}
							else
							{
								operationName = this.GetOperationName(webServiceOperation, false);
								operationElement = this.GetOperationElement(operationName, value, xmlNodes6, @namespace, ref flag2);
								stringBuilder = new StringBuilder();
								if (flag1)
								{
									stringBuilder.Append("<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n  <s:Header>\r\n  </s:Header>\r\n  <s:Body>\r\n    <");
								}
								else
								{
									stringBuilder.Append("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <s:Header>\r\n  </s:Header>\r\n  <s:Body>\r\n    <");
								}
								stringBuilder.Append(string.Concat(webServiceOperation.Name, "Response"));
								stringBuilder.Append(" xmlns=\"");
								stringBuilder.Append(value);
								stringBuilder.Append("\">");
								if (this.AddChildProperties(stringBuilder, 6, value, operationElement, xmlNodes6, value, "", strs, @namespace, ref flag2) > 0)
								{
									stringBuilder.Append("\r\n");
									stringBuilder.Append("    ");
								}
								stringBuilder.Append("</");
								stringBuilder.Append(string.Concat(webServiceOperation.Name, "Response"));
								stringBuilder.Append(">\r\n  </s:Body>\r\n");
								stringBuilder.Append("</s:Envelope>");
								webServiceOperation.ResponseSoap = stringBuilder.ToString();
							}
							webServiceEndPoint.Operations.Add(webServiceOperation);
						}
						if (xmlNodes10 == null)
						{
							throw new Exception("operation section missing in WSDL");
						}
						webServiceEndPoint.Operations.Sort();
					}
					if (xmlNodes8 != null)
					{
						continue;
					}
					throw new Exception("port section missing in WSDL");
				}
			}
			return webServiceProperty;
		}

		public string GetWsdlAddress(string location)
		{
			if (location.EndsWith(".xml") || location.Contains("?"))
			{
				return location;
			}
			XmlDocument xmlDocument = new XmlDocument();
			string str = string.Concat(location, "?wsdl");
			this.LoadWsdl(xmlDocument, str);
			if (!xmlDocument.HasChildNodes)
			{
				throw new Exception(string.Concat("Couldn't find WSDL address at ", location, "\r\nPlease change the server address to point to a valid wsdl address, or enter the server address and select manual configuration."));
			}
			return str;
		}

		private bool IsCorrectNamespace(XmlNode schema, string schemaNamespace)
		{
			bool flag;
			IEnumerator enumerator = schema.Attributes.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					XmlNode current = (XmlNode)enumerator.Current;
					if (!string.Equals(current.LocalName, "targetnamespace", StringComparison.OrdinalIgnoreCase) || !(current.Value == schemaNamespace))
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return flag;
		}

		private bool IsElementFormQualified(XmlNode element, bool currentformQualified)
		{
			if (element != null)
			{
				string attributByName = this.GetAttributByName(element, "form", false);
				if (attributByName == "qualified")
				{
					currentformQualified = true;
				}
				else if (attributByName == "unqualified ")
				{
					currentformQualified = false;
				}
			}
			return currentformQualified;
		}

		private void LoadWsdl(XmlDocument doc, string path)
		{
			string str;
			try
			{
				try
				{
					if (!this.PathIsUrl(path))
					{
						doc.Load(path);
						string innerXml = doc.InnerXml;
					}
					else
					{
						using (WsdlParser.WebClient webClient = new WsdlParser.WebClient())
						{
							if (this.Authentication)
							{
								webClient.Credentials = new NetworkCredential(this.UserName, this.Password);
							}
							webClient.Timeout = 30000;
							CertificateValidator.WarnAboutInvalidCertificates = true;
							str = webClient.DownloadString(path);
						}
						doc.LoadXml(str);
					}
				}
				catch (WebException webException1)
				{
					WebException webException = webException1;
					if (webException.InnerException == null || !webException.InnerException.Message.Contains("An existing connection was forcibly closed by the remote host") || !path.ToLower().StartsWith("https"))
					{
						if (webException.InnerException == null || !webException.InnerException.Message.Contains("The remote certificate is invalid according to the validation procedure.") || !path.ToLower().StartsWith("https"))
						{
							if (webException.InnerException == null || !webException.InnerException.Message.Contains("No connection could be made because the target machine actively refused it"))
							{
								throw new Exception(string.Concat("Could not open the wsdl file at '", path, "'"), webException);
							}
							throw new WebException(string.Concat(new string[] { "Could not open the wsdl file at '", path, "'.\r\r", webException.InnerException.Message, "\r\n\r\nIs the receiver running?" }), webException);
						}
						throw new WebException(string.Concat("Could not open the wsdl file at '", path, "'.  Has the Certificate imported into the certificate store on the remote computer. Double click the certificate file to import. Make sure it is imported to Local Machine under the Personal store. Also try pasting the URL into a browser for a further description - maybe the certificate has expired?\r\nThe remote certificate is invalid according to the validation procedure"), webException);
					}
					throw new WebException(string.Concat("Could not open the wsdl file at '", path, "'.  Has the ssl certificate been bound to the specified port on the remote computer?"), webException);
				}
			}
			finally
			{
				CertificateValidator.WarnAboutInvalidCertificates = false;
			}
		}

		private bool PassAuthenticationInSoapHeader(XmlNode policy)
		{
			bool flag;
			if (policy != null && policy.HasChildNodes && policy.FirstChild.HasChildNodes && policy.FirstChild.FirstChild.HasChildNodes)
			{
				IEnumerator enumerator = policy.FirstChild.FirstChild.ChildNodes.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						if (!string.Equals(((XmlNode)enumerator.Current).LocalName, "signedsupportingtokens", StringComparison.InvariantCultureIgnoreCase))
						{
							continue;
						}
						flag = true;
						return flag;
					}
					return false;
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				return flag;
			}
			return false;
		}

		private bool PathIsUrl(string path)
		{
			bool flag;
			if (File.Exists(path))
			{
				return false;
			}
			try
			{
				Uri uri = new Uri(path);
				flag = true;
			}
			catch (Exception exception)
			{
				throw;
			}
			return flag;
		}

		private string RemoveNamespace(string name)
		{
			if (name.Contains(":"))
			{
				name = name.Substring(name.IndexOf(":") + 1);
			}
			return name;
		}

		private bool SetFormQualified(XmlNode schema, bool currentformQualified)
		{
			if (schema != null)
			{
				string attributByName = this.GetAttributByName(schema, "elementFormDefault", false);
				if (attributByName == "qualified")
				{
					currentformQualified = true;
				}
				else if (attributByName == "unqualified ")
				{
					currentformQualified = false;
				}
			}
			return currentformQualified;
		}

		private class Namespaces
		{
			private Dictionary<string, string> ReverseNamespaces;

			private Dictionary<string, string> NamespacesShortcutKey;

			private static int nameSpaceCounter;

			public Namespaces()
			{
			}

			public Namespaces(string targetNamespace)
			{
				this.ReverseNamespaces[targetNamespace] = "";
				WsdlParser.Namespaces.nameSpaceCounter = 0;
			}

			public WsdlParser.Namespaces Clone()
			{
				return new WsdlParser.Namespaces()
				{
					ReverseNamespaces = this.ReverseNamespaces,
					NamespacesShortcutKey = new Dictionary<string, string>(this.NamespacesShortcutKey)
				};
			}

			public string GetNamespace(string shortcut)
			{
				if (this.NamespacesShortcutKey.ContainsKey(shortcut))
				{
					return this.NamespacesShortcutKey[shortcut];
				}
				if (this.NamespacesShortcutKey.Count != 0)
				{
					throw new Exception(string.Concat("could find namespace for ", shortcut));
				}
				return "";
			}

			public WsdlParser.Namespaces GetNamespaces(XmlNode element)
			{
				WsdlParser.Namespaces value = this;
				foreach (XmlNode attribute in element.Attributes)
				{
					if (!attribute.Name.StartsWith("xmlns:"))
					{
						continue;
					}
					int num = attribute.Name.IndexOf(":");
					string str = attribute.Name.Substring(num + 1);
					value.NamespacesShortcutKey[str] = attribute.Value;
					if (value.ReverseNamespaces.ContainsKey(attribute.Value))
					{
						continue;
					}
					value.ReverseNamespaces[attribute.Value] = "NamespaceShortcutNotYetDefined";
				}
				return value;
			}

			public string GetNamespaceShortcut(string ns)
			{
				if (!this.ReverseNamespaces.ContainsKey(ns))
				{
					throw new Exception(string.Concat("could find namespace shortcut for ", ns));
				}
				if (this.ReverseNamespaces[ns] == "NamespaceShortcutNotYetDefined")
				{
					WsdlParser.Namespaces.nameSpaceCounter++;
					char chr = (char)(WsdlParser.Namespaces.nameSpaceCounter + 96);
					this.ReverseNamespaces[ns] = chr.ToString();
				}
				return this.ReverseNamespaces[ns];
			}

			internal void UseThese(WsdlParser.Namespaces namespaces)
			{
				this.ReverseNamespaces = namespaces.ReverseNamespaces;
				this.NamespacesShortcutKey = namespaces.NamespacesShortcutKey;
			}
		}

		private class WebClient : System.Net.WebClient
		{
			public int Timeout
			{
				get;
				set;
			}

			public WebClient()
			{
			}

			protected override WebRequest GetWebRequest(Uri uri)
			{
				WebRequest webRequest = base.GetWebRequest(uri);
				webRequest.Timeout = this.Timeout;
				((HttpWebRequest)webRequest).ReadWriteTimeout = this.Timeout;
				return webRequest;
			}
		}
	}
}