using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceObject
	{
		public List<WebServiceMessage> Body { get; set; } = new List<WebServiceMessage>();

		public List<WebServiceMessage> Header { get; set; } = new List<WebServiceMessage>();

		public string Name
		{
			get;
			set;
		}

		public WebServiceObject()
		{
		}
	}
}