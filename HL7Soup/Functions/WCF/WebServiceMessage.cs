using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceMessage
	{
		public string Element
		{
			get;
			set;
		}

		public string Message
		{
			get;
			internal set;
		}

		public string Name
		{
			get;
			set;
		}

		public WebServiceMessage()
		{
		}
	}
}