using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.WCF
{
	[Serializable]
	public class WebServiceProperties
	{
		public List<HL7Soup.Functions.WCF.WebServiceServices> WebServiceServices { get; set; } = new List<HL7Soup.Functions.WCF.WebServiceServices>();

		public WebServiceProperties()
		{
		}
	}
}