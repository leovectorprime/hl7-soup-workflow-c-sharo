using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Filters
{
	public interface IFilter : IFunctionProvider
	{
		bool FilterMessage(WorkflowInstance workflowInstance);
	}
}