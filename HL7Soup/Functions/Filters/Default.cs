using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Transformers;
using HL7Soup.MessageFilters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HL7Soup.Functions.Filters
{
	public class Default : FilterHost
	{
		private Stack<bool> ConditionResults = new Stack<bool>();

		private int i;

		public Default()
		{
		}

		protected override bool FunctionFilter(WorkflowInstance workflowInstance)
		{
			if (base.Setting.Filters == null || base.Setting.Filters.Count == 0)
			{
				return false;
			}
			return !MessageFilter.EvaluateConditions(workflowInstance, base.Setting.Filters.ToList<MessageFilter>());
		}

		protected override void FunctionTransform(WorkflowInstance workflowInstance)
		{
			HL7Soup.Functions.Transformers.Default.FunctionTransform(workflowInstance, base.Setting, this.i, this.ConditionResults);
		}

		public string GetTransformerThatErrored()
		{
			ITransformerAction item = base.Setting.Transformers[this.i];
			if (item == null)
			{
				return string.Concat("Unknown Filter Transformer at position ", this.i + 1);
			}
			return string.Concat(new object[] { "'", item.Description, "' at position ", this.i + 1 });
		}
	}
}