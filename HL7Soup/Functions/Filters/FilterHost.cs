using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Transformers;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HL7Soup.Functions.Filters
{
	public abstract class FilterHost : FunctionProvider, IFilter, IFunctionProvider, ITransformer
	{
		public IFilterSetting Setting
		{
			get
			{
				return (IFilterSetting)this.baseSetting;
			}
		}

		public FilterHost()
		{
		}

		public bool FilterMessage(WorkflowInstance workflowInstance)
		{
			bool flag;
			try
			{
				bool flag1 = this.FunctionFilter(workflowInstance);
				((ActivityInstance)workflowInstance.CurrentActivityInstance).Filtered = flag1;
				if (flag1 && workflowInstance.Activities.Count <= 1)
				{
					workflowInstance.Filtered = true;
				}
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
				return true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Filter Error:  ", exception.Message), exception);
				flag = false;
			}
			return flag;
		}

		protected abstract bool FunctionFilter(WorkflowInstance workflowInstance);

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			HL7Soup.Functions.Transformers.Default.FunctionPrepareShared(workflowInstance, this.Setting);
		}

		protected abstract void FunctionTransform(WorkflowInstance workflowInstance);

		public bool Transform(WorkflowInstance workflowInstance)
		{
			bool flag;
			try
			{
				this.FunctionTransform(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
				flag = true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				string str = "";
				if (this is HL7Soup.Functions.Filters.Default)
				{
					str = string.Concat("(", ((HL7Soup.Functions.Filters.Default)this).GetTransformerThatErrored(), ")");
				}
				base.Errored(workflowInstance, string.Concat("Filter Transformer Error ", str, ":  ", exception.Message), exception);
				flag = false;
			}
			return flag;
		}
	}
}