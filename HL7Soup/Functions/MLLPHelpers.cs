using HL7Soup.Functions.Settings;
using System;
using System.Text;

namespace HL7Soup.Functions
{
	public static class MLLPHelpers
	{
		public static int GetFrameIndex(string data, int startPoint, char[] frame)
		{
			int num = -1;
			bool flag = true;
			int num1 = startPoint;
			while (flag)
			{
				flag = false;
				num = data.IndexOf(frame[0], num1);
				if (num <= 0)
				{
					continue;
				}
				for (int i = 1; i < (int)frame.Length; i++)
				{
					if (data.IndexOf(frame[i], num) != num + i)
					{
						flag = true;
						num1 = num + 1;
					}
				}
			}
			return num;
		}

		public static byte[] WrapInFrame(byte[] data, IMLLPConnectionSetting connection)
		{
			int length = (int)data.Length + (int)connection.FrameStart.Length + (int)connection.FrameEnd.Length;
			byte[] numArray = new byte[length];
			connection.MessageEncoding.GetBytes(connection.FrameStart).CopyTo(numArray, 0);
			data.CopyTo(numArray, (int)connection.FrameStart.Length);
			connection.MessageEncoding.GetBytes(connection.FrameEnd).CopyTo(numArray, length - (int)connection.FrameEnd.Length);
			return numArray;
		}
	}
}