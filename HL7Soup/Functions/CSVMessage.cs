using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace HL7Soup.Functions
{
	public class CSVMessage : FunctionMessage, IDisposable
	{
		public bool ContainsQuotes
		{
			get;
			set;
		}

		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.CSV;
			}
		}

		public override string Text
		{
			get
			{
				return string.Join(",", this.Values);
			}
		}

		private List<string> Values
		{
			get;
			set;
		}

		public CSVMessage(string text) : base(text)
		{
			this.SetText(text);
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override string GetValueAtPath(string path)
		{
			int num = -1;
			int.TryParse(path.Replace("[", "").Replace("]", ""), out num);
			if (num <= -1 || num > this.Values.Count - 1)
			{
				return "";
			}
			if (!this.ContainsQuotes)
			{
				return this.Values[num];
			}
			return this.Values[num].TrimEnd(new char[] { '\"' }).TrimStart(new char[] { '\"' });
		}

		public override void SetStructureAtPath(string toPath, string fromValue)
		{
			int num = -1;
			int.TryParse(toPath.Replace("[", "").Replace("]", ""), out num);
			if (num > -1 && this.Values.Count - 1 >= num)
			{
				this.Values[num] = fromValue;
			}
		}

		protected override void SetText(string text)
		{
			this.Values = new List<string>();
			this.ContainsQuotes = text.Contains("\"");
			foreach (object obj in (new Regex("((?<=\")[^\"]*(?=\"(,|$)+)|(?<=,|^)[^,\"]*(?=,|$))")).Matches(text))
			{
				string str = obj.ToString();
				if (this.ContainsQuotes)
				{
					str = string.Concat("\"", str, "\"");
				}
				this.Values.Add(str);
			}
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			int num = -1;
			int.TryParse(toPath.Replace("[", "").Replace("]", ""), out num);
			if (num > -1 && this.Values.Count - 1 >= num)
			{
				if (this.ContainsQuotes || fromValue.Contains(",") || fromValue.Contains("\""))
				{
					this.Values[num] = string.Concat("\"", fromValue.Replace("\"", "\"\""), "\"");
					this.ContainsQuotes = true;
					return;
				}
				this.Values[num] = fromValue;
			}
		}
	}
}