using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using NLog;
using System;

namespace HL7Soup.Functions
{
	public class BinaryMessage : FunctionMessage, IDisposable
	{
		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.TextWithVariables;
			}
		}

		public override string Text
		{
			get
			{
				return this._text;
			}
		}

		public BinaryMessage(string text) : base(text)
		{
			this.SetText(text);
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override string GetValueAtPath(string path)
		{
			return this._text;
		}

		public override void SetStructureAtPath(string toPath, string fromValue)
		{
			this.SetValueAtPath(toPath, fromValue);
		}

		protected override void SetText(string text)
		{
			this._text = text;
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			try
			{
				this._text = fromValue;
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
		}
	}
}