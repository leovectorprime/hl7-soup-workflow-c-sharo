using HL7Soup;
using HL7Soup.Functions;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Receivers
{
	public interface IReceiver : IFunctionProvider
	{
		bool Finished
		{
			get;
			set;
		}

		bool Pending
		{
			get;
			set;
		}

		void GetNext(WorkflowInstance workflowInstance);

		event MessageReceivedEventHandler MessageReceived;
	}
}