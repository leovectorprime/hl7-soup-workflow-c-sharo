using HL7Soup;
using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public abstract class Receiver : FunctionProvider, IReceiver, IFunctionProvider
	{
		public bool Finished
		{
			get;
			set;
		}

		public bool Pending
		{
			get;
			set;
		}

		protected Receiver()
		{
		}

		protected abstract void FunctionGetNext(WorkflowInstance workflowInstance);

		public void GetNext(WorkflowInstance workflowInstance)
		{
			try
			{
				this.FunctionGetNext(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
			}
			catch (Exception exception)
			{
				base.Errored(workflowInstance, exception);
			}
		}

		public abstract event MessageReceivedEventHandler MessageReceived;
	}
}