using HL7Soup;
using System;

namespace HL7Soup.Functions.Receivers
{
	public delegate void MessageReceivedEventHandler(object sender, ref WorkflowInstance workflowInstance);
}