using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;

namespace HL7Soup.Functions.Receivers
{
	public class MLLPReceiver : Receiver
	{
		private Socket listener;

		private Socket handler;

		public IMLLPReceiverSetting Setting
		{
			get
			{
				return (IMLLPReceiverSetting)this.baseSetting;
			}
		}

		public MLLPReceiver()
		{
		}

		private string CharArrayToAsciiString(char[] chars)
		{
			string str = "";
			char[] chrArray = chars;
			for (int i = 0; i < (int)chrArray.Length; i++)
			{
				char chr = chrArray[i];
				str = string.Concat(str, (int)chr, ",");
			}
			return str.TrimEnd(",".ToCharArray());
		}

		protected override void FunctionCanceled()
		{
			base.FunctionCanceled();
			if (this.Setting.KeepConnectionOpen && this.handler != null && this.handler.IsBound && this.handler.Connected)
			{
				this.handler.Shutdown(SocketShutdown.Both);
				this.handler.Close();
				this.handler = null;
			}
			if (this.listener != null && this.listener.IsBound)
			{
				this.listener.Close();
			}
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			try
			{
				if (this.listener != null && this.listener.IsBound)
				{
					this.listener.Close();
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				MessageBox.Show(string.Concat("FIX ME! This shouldn't happened, and was caused by adding the close here.  is was only in the cancel before, but was required here to close the connection after an error.", exception.ToString()));
			}
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			base.Pending = false;
			try
			{
				byte[] numArray = new byte[1000];
				string removeFrames = null;
				Console.WriteLine("Waiting for a connection...");
				if (!this.Setting.KeepConnectionOpen || this.handler == null || !this.handler.IsBound)
				{
					this.handler = this.listener.Accept();
				}
				removeFrames = null;
				bool flag = true;
				do
				{
					numArray = new byte[1024];
					int num = this.handler.Receive(numArray);
					removeFrames = string.Concat(removeFrames, this.Setting.MessageEncoding.GetString(numArray, 0, num));
					if (removeFrames.IndexOf(new string(this.Setting.FrameEnd)) > -1)
					{
						removeFrames = this.TryToRemoveFrames(removeFrames);
						MessageLogger.Instance.AddMessage(new MessageEvent(removeFrames, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Received));
						Console.WriteLine("Text received : {0}", removeFrames);
						using (IMessage functionMessage = FunctionHelpers.GetFunctionMessage(removeFrames, this.Setting.MessageType))
						{
							workflowInstance.AddActivity(functionMessage, this.Setting.Id);
							if (this.MessageReceived != null)
							{
								this.MessageReceived(this, ref workflowInstance);
							}
						}
						if (!this.Setting.ReturnNoResponse)
						{
							string str = (workflowInstance.ResponseMessage == null ? "" : workflowInstance.ResponseMessage.Text);
							byte[] bytes = this.Setting.MessageEncoding.GetBytes(str);
							bytes = MLLPHelpers.WrapInFrame(bytes, this.Setting);
							MessageLogger.Instance.AddMessage(new MessageEvent(str, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
							this.handler.Send(bytes);
						}
						if (!this.Setting.KeepConnectionOpen)
						{
							this.handler.Shutdown(SocketShutdown.Both);
							this.handler.Close();
						}
						return;
					}
					else
					{
						if (!this.Setting.KeepConnectionOpen)
						{
							continue;
						}
						Thread.Sleep(1);
						try
						{
							flag = (!this.handler.Poll(1, SelectMode.SelectRead) ? true : this.handler.Available != 0);
						}
						catch (SocketException socketException)
						{
							flag = false;
						}
					}
				}
				while (flag);
				this.handler.Shutdown(SocketShutdown.Both);
				this.handler.Close();
				this.handler = null;
				base.Pending = true;
			}
			catch (InvalidOperationException invalidOperationException1)
			{
				InvalidOperationException invalidOperationException = invalidOperationException1;
				base.Errored(workflowInstance, string.Concat("Cannot start receiving on ", this.Setting.Details, " because another process is already listening on that port."), invalidOperationException);
			}
			catch (SocketException socketException2)
			{
				SocketException socketException1 = socketException2;
				if (socketException1.ErrorCode != 10004)
				{
					Log.Instance.Error(socketException1.Message);
					base.Errored(workflowInstance, socketException1);
				}
				else
				{
					base.Finished = true;
					Log.Instance.Info("Receive canceled by user");
				}
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			IPAddress addressList = null;
			if (!IPAddress.TryParse(this.Setting.Server, out addressList))
			{
				addressList = Dns.Resolve(this.Setting.Server).AddressList[0];
			}
			IPEndPoint pEndPoint = new IPEndPoint(addressList, this.Setting.Port);
			try
			{
				this.listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				this.listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1);
				this.listener.Bind(pEndPoint);
				this.listener.Listen(10);
			}
			catch (SocketException socketException1)
			{
				SocketException socketException = socketException1;
				if (socketException.Message.Contains("Only one usage of each socket address"))
				{
					throw new Exception(string.Concat("The TCP address/port for this workflow (", pEndPoint.ToString(), ") is already in use.  You probably have another workflow already running on this address.  Either stop the other workflow or change the address of this one before starting."), socketException);
				}
				throw;
			}
		}

		private string TryToRemoveFrames(string partialMessage)
		{
			partialMessage = partialMessage.TrimStart(this.Setting.FrameStart);
			partialMessage = partialMessage.TrimEnd(this.Setting.FrameEnd);
			return partialMessage;
		}

		private string TryToRemoveFrames(string partialMessage, ref string reportText)
		{
			List<char> chrs = new List<char>();
			if (partialMessage.Length <= this.Setting.FrameEnd.GetUpperBound(0))
			{
				return partialMessage;
			}
			if (partialMessage.EndsWith(this.Setting.FrameEnd.ToString()))
			{
				reportText = "Message Succeded";
				return partialMessage.TrimEnd(this.Setting.FrameEnd);
			}
			reportText = string.Format("Couldn't find the end of the message.{2}It should have ended with {0}, but instead was {1}.{2}The final chars on this message have not been removed.  Please fix the source or{2}match the encoding to provide a complete message.", this.Setting.FrameEnd, this.CharArrayToAsciiString(partialMessage.Substring(partialMessage.Length - 2).ToCharArray()), Environment.NewLine);
			return partialMessage;
		}

		public override event MessageReceivedEventHandler MessageReceived;
	}
}