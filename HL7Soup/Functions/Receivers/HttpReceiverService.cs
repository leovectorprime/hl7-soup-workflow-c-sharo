using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HL7Soup.Functions.Receivers
{
	public class HttpReceiverService
	{
		private const int HandlerThread = 2;

		private readonly HttpListener listener;

		private IHttpReceiverSetting Setting;

		public string ErrorMessage
		{
			get;
			set;
		}

		public string HttpMethod
		{
			get;
			set;
		}

		public string InboundMessage
		{
			get;
			private set;
		}

		public Dictionary<string, string> InboundParameters
		{
			get;
			private set;
		}

		public string OutboundMessage
		{
			get;
			set;
		}

		public HttpReceiverService.HttpReceiverServiceStatus Status
		{
			get;
			set;
		}

		public string[] UrlSections
		{
			get;
			private set;
		}

		public HttpReceiverService(HttpListener listener, IHttpReceiverSetting setting)
		{
			this.Setting = setting;
			this.listener = listener;
			string uRL = this.Setting.GetURL(true);
			if (this.Setting.Authentication)
			{
				listener.AuthenticationSchemes = AuthenticationSchemes.Basic;
			}
			listener.Prefixes.Add(uRL);
		}

		private void ProcessRequestHandler(Task<HttpListenerContext> result)
		{
			if (!this.listener.IsListening)
			{
				return;
			}
			try
			{
				try
				{
					this.ErrorMessage = null;
					HttpListenerContext length = result.Result;
					if (this.Setting.Authentication)
					{
						HttpListenerBasicIdentity identity = (HttpListenerBasicIdentity)length.User.Identity;
						if (!(identity.Name == this.Setting.AuthenticationUserName) || !(identity.Password == this.Setting.AuthenticationPassword))
						{
							length.Response.StatusCode = 401;
							HttpReceiverService.ReturnEmptyContent(length);
							Log.Instance.Error(string.Concat("The Http Reciever '", this.Setting.Name, "' was called with an Incorrect Username or Password."));
							return;
						}
					}
					string end = (new StreamReader(length.Request.InputStream)).ReadToEnd();
					this.HttpMethod = length.Request.HttpMethod;
					if (this.HttpMethod != "HEAD")
					{
						this.InboundParameters = new Dictionary<string, string>();
						if (this.Setting.ExtractParameters)
						{
							foreach (string queryString in length.Request.QueryString)
							{
								if (queryString == null)
								{
									continue;
								}
								this.InboundParameters.Add(queryString, length.Request.QueryString[queryString]);
							}
						}
						if (this.Setting.ExtractUrlSections)
						{
							string rawUrl = length.Request.RawUrl;
							string str = string.Concat("/", this.Setting.ServiceName);
							if (rawUrl.StartsWith(str, StringComparison.InvariantCultureIgnoreCase))
							{
								rawUrl = rawUrl.Substring(Math.Min(str.Length + 1, rawUrl.Length));
							}
							else if (rawUrl.StartsWith("/"))
							{
								rawUrl = rawUrl.Substring(1);
							}
							int num = rawUrl.IndexOf("?");
							if (num > 0)
							{
								rawUrl = rawUrl.Substring(0, num);
							}
							this.UrlSections = rawUrl.Split(new char[] { '/' });
						}
						this.InboundMessage = end;
						this.Status = HttpReceiverService.HttpReceiverServiceStatus.receivedMessage;
						SpinWait.SpinUntil(() => this.Status == HttpReceiverService.HttpReceiverServiceStatus.responded, 120000);
						this.Status = HttpReceiverService.HttpReceiverServiceStatus.waiting;
						byte[] numArray = null;
						numArray = (this.Setting.MessageType != MessageTypes.Binary ? Encoding.UTF8.GetBytes(this.OutboundMessage ?? "") : Convert.FromBase64String(this.OutboundMessage));
						length.Response.ContentLength64 = (long)((int)numArray.Length);
						Stream outputStream = length.Response.OutputStream;
						outputStream.WriteAsync(numArray, 0, (int)numArray.Length);
						outputStream.Close();
					}
					else
					{
						Log.Instance.Debug("Received a ping.");
						length.Response.StatusCode = 200;
						HttpReceiverService.ReturnEmptyContent(length);
					}
				}
				catch (Exception exception2)
				{
					Exception exception = exception2;
					Log.Instance.Error(exception, "HTTP Receiver service error");
					this.ErrorMessage = exception.Message;
					try
					{
						HttpReceiverService.ReturnEmptyContent(result.Result, exception.Message);
					}
					catch (Exception exception1)
					{
					}
				}
			}
			finally
			{
				this.listener.GetContextAsync().ContinueWith(new Action<Task<HttpListenerContext>>(this.ProcessRequestHandler));
			}
		}

		private static void ReturnEmptyContent(HttpListenerContext context)
		{
			HttpReceiverService.ReturnEmptyContent(context, "");
		}

		private static void ReturnEmptyContent(HttpListenerContext context, string data)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(data);
			context.Response.ContentLength64 = (long)((int)bytes.Length);
			Stream outputStream = context.Response.OutputStream;
			outputStream.WriteAsync(bytes, 0, (int)bytes.Length);
			outputStream.Close();
		}

		public void Start()
		{
			if (this.listener.IsListening)
			{
				return;
			}
			try
			{
				this.listener.Start();
			}
			catch (HttpListenerException httpListenerException1)
			{
				HttpListenerException httpListenerException = httpListenerException1;
				if (httpListenerException.Message == "Access is denied")
				{
					throw new Exception(string.Concat("You need to either run as admininstrator or register this address by editing the workflow and clicking 'Register' at address registration?\r\n\r\n", httpListenerException.Message), httpListenerException);
				}
				throw;
			}
			for (int i = 0; i < 2; i++)
			{
				this.listener.GetContextAsync().ContinueWith(new Action<Task<HttpListenerContext>>(this.ProcessRequestHandler));
			}
		}

		public void Stop()
		{
			if (this.listener.IsListening)
			{
				this.listener.Stop();
			}
		}

		public enum HttpReceiverServiceStatus
		{
			waiting,
			receivedMessage,
			responded
		}
	}
}