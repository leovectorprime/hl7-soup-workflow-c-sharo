using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Http;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public class WebServiceReceiver : Receiver
	{
		private ServiceHost host;

		private WebServiceReceiverService webServiceReceiverService;

		public IWebServiceReceiverSetting Setting
		{
			get
			{
				return (IWebServiceReceiverSetting)this.baseSetting;
			}
		}

		public WebServiceReceiver()
		{
		}

		protected override void FunctionCanceled()
		{
			base.FunctionCanceled();
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			this.host.Close();
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			try
			{
				if (this.webServiceReceiverService.Status == WebServiceReceiverService.WebServiceReceiverServiceStatus.receivedMessage)
				{
					base.Pending = false;
					MessageLogger.Instance.AddMessage(new MessageEvent(this.webServiceReceiverService.InboundMessage, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Received));
					using (IMessage functionMessage = FunctionHelpers.GetFunctionMessage(this.webServiceReceiverService.InboundMessage, this.Setting.MessageType))
					{
						workflowInstance.AddActivity(functionMessage, this.Setting.Id);
						if (this.MessageReceived != null)
						{
							this.MessageReceived(this, ref workflowInstance);
						}
					}
					if (!this.Setting.ReturnNoResponse)
					{
						string str = (workflowInstance.ResponseMessage == null ? "" : workflowInstance.ResponseMessage.Text);
						MessageLogger.Instance.AddMessage(new MessageEvent(str, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
						this.webServiceReceiverService.OutboundMessage = str;
						this.webServiceReceiverService.Status = WebServiceReceiverService.WebServiceReceiverServiceStatus.responded;
					}
				}
				else
				{
					base.Pending = true;
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, exception.Message, exception);
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			if (!this.Setting.UseDefaultSSLCertificate)
			{
				HttpHelpers.EnableUrlToBeOpenedWithoutElevation(this.Setting.GetURL(true), HttpHelpers.AssemblyGuid, this.Setting.CertificateThumbPrint);
			}
			else
			{
				HttpHelpers.EnableUrlToBeOpenedWithoutElevation(this.Setting.GetURL(true), HttpHelpers.AssemblyGuid);
			}
			this.webServiceReceiverService = new WebServiceReceiverService();
			string machineName = Environment.MachineName;
			Uri uri = new Uri(this.Setting.GetURL());
			Uri uri1 = new Uri(string.Format("http://{0}:{1}/{2}", machineName, this.Setting.LocalHostEndpointPort, this.Setting.ServiceName));
			this.host = null;
			ServiceMetadataBehavior serviceMetadataBehavior = new ServiceMetadataBehavior();
			if (!this.Setting.IncludeHttpLocalHostEndpoint)
			{
				this.host = new ServiceHost(this.webServiceReceiverService, new Uri[] { uri });
			}
			else
			{
				this.host = new ServiceHost(this.webServiceReceiverService, new Uri[] { uri, uri1 });
				this.host.AddServiceEndpoint(typeof(IWebServiceReceiverService), new BasicHttpBinding(), uri1);
			}
			WSHttpBinding wSHttpBinding = new WSHttpBinding();
			BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
			if (!this.Setting.UseSsl)
			{
				serviceMetadataBehavior.HttpGetEnabled = true;
			}
			else
			{
				if (!this.Setting.UseSoap12)
				{
					if (!this.Setting.Authentication)
					{
						basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
						basicHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
					}
					else
					{
						basicHttpBinding.Security.Mode = BasicHttpSecurityMode.TransportWithMessageCredential;
						basicHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
						basicHttpBinding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
						ServiceCredentials serviceCredential = new ServiceCredentials();
						serviceCredential.UserNameAuthentication.CustomUserNamePasswordValidator = new CustomUserNameValidator(this.Setting.AuthenticationUserName, this.Setting.AuthenticationPassword);
						serviceCredential.UserNameAuthentication.UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom;
						this.host.Description.Behaviors.Add(serviceCredential);
					}
				}
				else if (!this.Setting.Authentication)
				{
					wSHttpBinding.Security.Mode = SecurityMode.Transport;
					wSHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
				}
				else
				{
					wSHttpBinding.Security.Mode = SecurityMode.TransportWithMessageCredential;
					wSHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
					wSHttpBinding.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
					ServiceCredentials customUserNameValidator = new ServiceCredentials();
					customUserNameValidator.UserNameAuthentication.CustomUserNamePasswordValidator = new CustomUserNameValidator(this.Setting.AuthenticationUserName, this.Setting.AuthenticationPassword);
					customUserNameValidator.UserNameAuthentication.UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom;
					this.host.Description.Behaviors.Add(customUserNameValidator);
				}
				serviceMetadataBehavior.HttpsGetEnabled = true;
			}
			this.host.Description.Behaviors.Add(serviceMetadataBehavior);
			if (this.Setting.UseSoap12)
			{
				this.host.AddServiceEndpoint(typeof(IWebServiceReceiverService), wSHttpBinding, uri);
				if (this.Setting.IncludeHttpLocalHostEndpoint)
				{
					this.host.AddServiceEndpoint(typeof(IWebServiceReceiverService), wSHttpBinding, uri1);
				}
			}
			else
			{
				this.host.AddServiceEndpoint(typeof(IWebServiceReceiverService), basicHttpBinding, uri);
				if (this.Setting.IncludeHttpLocalHostEndpoint)
				{
					this.host.AddServiceEndpoint(typeof(IWebServiceReceiverService), basicHttpBinding, uri1);
				}
			}
			this.host.Description.Behaviors.Find<ServiceBehaviorAttribute>().InstanceContextMode = InstanceContextMode.Single;
			try
			{
				this.host.Open();
			}
			catch (AddressAccessDeniedException addressAccessDeniedException1)
			{
				AddressAccessDeniedException addressAccessDeniedException = addressAccessDeniedException1;
				throw new Exception(string.Concat("You need to either run as admininstrator or register this address by editing the workflow and clicking 'Register' at address registration?\r\n\r\n", addressAccessDeniedException.Message), addressAccessDeniedException);
			}
			catch (AddressAlreadyInUseException addressAlreadyInUseException1)
			{
				AddressAlreadyInUseException addressAlreadyInUseException = addressAlreadyInUseException1;
				throw new Exception(string.Concat("To see what app currently bound to the port run 'netsh http show urlacl'\r\n\r\nTo remove a port binding run 'netsh http delete urlacl url=urlcurrentlyregisteredtoport'\r\n\r\n", addressAlreadyInUseException.Message), addressAlreadyInUseException);
			}
		}

		public override event MessageReceivedEventHandler MessageReceived;
	}
}