using System;
using System.ServiceModel;

namespace HL7Soup.Functions.Receivers
{
	[ServiceContract(Namespace="http://HL7Soup/WebServiceReceiver")]
	public interface IWebServiceReceiverService
	{
		[OperationContract]
		string SendMessage(string message);
	}
}