using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public class DatabaseReceiver : Receiver
	{
		private int currentLocation;

		private DateTime nextSendTime = DateTime.Now;

		public List<string> Records
		{
			get;
			set;
		}

		public IDatabaseReceiverSetting Setting
		{
			get
			{
				return (IDatabaseReceiverSetting)this.baseSetting;
			}
		}

		public DatabaseReceiver()
		{
		}

		protected override void FunctionAllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
			base.FunctionAllFunctionsSucceded(workflowInstance);
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			string text;
			base.Pending = false;
			if (this.Records.Count <= this.currentLocation)
			{
				if (this.Setting.EndAfterProcessing)
				{
					MessageLogger.Instance.AddMessage(new MessageEvent("The query executed, but there were not results.", workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Received));
					base.Finished = true;
					return;
				}
				if (this.nextSendTime > DateTime.Now)
				{
					Thread.Sleep(10);
					base.Pending = true;
					return;
				}
				this.GetRecords(workflowInstance);
				this.nextSendTime = DateTime.Now + this.Setting.PollingInterval;
				base.Pending = true;
				return;
			}
			workflowInstance.AddActivity(FunctionHelpers.GetFunctionMessage(this.Records[this.currentLocation].ToString(), this.Setting.MessageType), this.Setting.Id);
			MessageLogger instance = MessageLogger.Instance;
			IMessage message = workflowInstance.Message;
			if (message != null)
			{
				text = message.Text;
			}
			else
			{
				text = null;
			}
			instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Received));
			this.MessageReceived(this, ref workflowInstance);
			this.Records.RemoveAt(this.currentLocation);
			if (this.Setting.ExecutePostProcessQuery && !workflowInstance.HasErrored)
			{
				this.UpdateRecord(workflowInstance);
			}
			if (this.Records.Count > this.currentLocation)
			{
				return;
			}
			this.currentLocation = 0;
			if (this.Records.Count == 0 && this.Setting.EndAfterProcessing)
			{
				base.Finished = true;
			}
			this.nextSendTime = DateTime.Now + this.Setting.PollingInterval;
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			this.currentLocation = 0;
			this.Records = new List<string>();
			if (this.Setting.EndAfterProcessing)
			{
				this.GetRecords(workflowInstance);
			}
		}

		protected override void FunctionRollBack(WorkflowInstance workflowInstance)
		{
			base.FunctionRollBack(workflowInstance);
		}

		private void GetRecords(WorkflowInstance workflowInstance)
		{
			using (DbConnection dbConnection = DatabaseSender.GetDbConnection(this.Setting.DataProvider, this.Setting.ConnectionString))
			{
				using (DbCommand dbCommand = DatabaseSender.GetDbCommand(this.Setting.DataProvider))
				{
					string theValue = TransformerAction.GetTheValue(workflowInstance, this.Setting.SqlQuery, this.Setting.Id, MessageSourceDirection.variable, null);
					dbCommand.CommandText = theValue;
					dbCommand.CommandType = CommandType.Text;
					dbCommand.Connection = dbConnection;
					StringBuilder stringBuilder = new StringBuilder("Parameters:\r\n");
					if (this.Setting.Parameters != null)
					{
						foreach (DatabaseSettingParameter parameter in this.Setting.Parameters)
						{
							DbParameter dbParameter = DatabaseSender.GetDbParameter(this.Setting.DataProvider);
							dbParameter.ParameterName = parameter.Name;
							string str = TransformerAction.GetTheValue(workflowInstance, parameter.Value, parameter.FromSetting, parameter.FromDirection, null);
							str = HL7Soup.Functions.Variable.ProcessFormat(str, parameter);
							dbParameter.Value = str;
							dbCommand.Parameters.Add(dbParameter);
							stringBuilder.Append(string.Concat(new string[] { "\r\n", dbParameter.ParameterName, ": \"", Helpers.TruncateString(str, 200), "\"" }));
						}
					}
					if (stringBuilder.Length <= 13)
					{
						Log.Instance.Debug(string.Concat("Polled Database with ", theValue));
					}
					else
					{
						Log.Instance.Debug(string.Concat(new object[] { "Polled Database with ", theValue, "\r\n\r\n", stringBuilder }));
					}
					dbConnection.Open();
					DbDataReader dbDataReaders = dbCommand.ExecuteReader();
					DbDataReader dbDataReaders1 = dbDataReaders;
					using (dbDataReaders)
					{
						while (dbDataReaders1.Read())
						{
							StringBuilder stringBuilder1 = new StringBuilder();
							for (int i = 0; i < dbDataReaders1.FieldCount; i++)
							{
								Type fieldType = dbDataReaders1.GetFieldType(i);
								if (!typeof(byte[]).IsAssignableFrom(fieldType))
								{
									stringBuilder1.Append(string.Concat("\"", dbDataReaders1.GetValue(i).ToString().Replace("\"", "\"\""), "\"", (i == dbDataReaders1.FieldCount - 1 ? "" : ",")));
								}
								else
								{
									stringBuilder1.Append(string.Concat("\"", Convert.ToBase64String((byte[])dbDataReaders1.GetValue(i)), "\"", (i == dbDataReaders1.FieldCount - 1 ? "" : ",")));
								}
							}
							this.Records.Add(stringBuilder1.ToString());
						}
						dbDataReaders1.Close();
					}
					dbConnection.Close();
				}
			}
		}

		private void UpdateRecord(WorkflowInstance workflowInstance)
		{
			MessageLogger.Instance.AddMessage(new MessageEvent(this.Setting.PostExecutionSqlQuery, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
			using (DbConnection dbConnection = DatabaseSender.GetDbConnection(this.Setting.DataProvider, this.Setting.ConnectionString))
			{
				using (DbCommand dbCommand = DatabaseSender.GetDbCommand(this.Setting.DataProvider))
				{
					string theValue = TransformerAction.GetTheValue(workflowInstance, this.Setting.PostExecutionSqlQuery, this.Setting.Id, MessageSourceDirection.variable, null);
					dbCommand.CommandText = theValue;
					dbCommand.CommandType = CommandType.Text;
					dbCommand.Connection = dbConnection;
					StringBuilder stringBuilder = new StringBuilder("Parameters:\r\n");
					if (this.Setting.PostExecutionParameters != null)
					{
						foreach (DatabaseSettingParameter postExecutionParameter in this.Setting.PostExecutionParameters)
						{
							DbParameter dbParameter = DatabaseSender.GetDbParameter(this.Setting.DataProvider);
							dbParameter.ParameterName = postExecutionParameter.Name;
							string str = TransformerAction.GetTheValue(workflowInstance, postExecutionParameter.Value, postExecutionParameter.FromSetting, postExecutionParameter.FromDirection, null);
							str = HL7Soup.Functions.Variable.ProcessFormat(str, postExecutionParameter);
							dbParameter.Value = str;
							dbCommand.Parameters.Add(dbParameter);
							stringBuilder.Append(string.Concat(new string[] { "\r\n", dbParameter.ParameterName, ": \"", Helpers.TruncateString(str, 200), "\"" }));
						}
					}
					if (stringBuilder.Length <= 13)
					{
						Log.Instance.Debug(string.Concat("Polled Database with ", theValue));
					}
					else
					{
						MessageLogger.Instance.AddMessage(new MessageEvent(string.Concat(theValue, "\r\n\r\n", stringBuilder), workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
						Log.Instance.Debug(string.Concat(new object[] { "Polled Database with ", theValue, "\r\n\r\n", stringBuilder }));
					}
					dbConnection.Open();
					dbCommand.ExecuteNonQuery();
					dbConnection.Close();
					MessageLogger.Instance.AddMessage(new MessageEvent(string.Concat(theValue, "\r\n\r\n", stringBuilder), workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Response));
				}
			}
		}

		public override event MessageReceivedEventHandler MessageReceived;
	}
}