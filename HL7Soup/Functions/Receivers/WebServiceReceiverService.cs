using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public class WebServiceReceiverService : IWebServiceReceiverService
	{
		public string InboundMessage
		{
			get;
			set;
		}

		public string OutboundMessage
		{
			get;
			set;
		}

		public WebServiceReceiverService.WebServiceReceiverServiceStatus Status
		{
			get;
			set;
		}

		public WebServiceReceiverService()
		{
		}

		public string SendMessage(string message)
		{
			this.OutboundMessage = "";
			this.InboundMessage = message;
			this.Status = WebServiceReceiverService.WebServiceReceiverServiceStatus.receivedMessage;
			SpinWait.SpinUntil(() => this.Status == WebServiceReceiverService.WebServiceReceiverServiceStatus.responded, 120000);
			this.Status = WebServiceReceiverService.WebServiceReceiverServiceStatus.waiting;
			return this.OutboundMessage;
		}

		public enum WebServiceReceiverServiceStatus
		{
			waiting,
			receivedMessage,
			responded
		}
	}
}