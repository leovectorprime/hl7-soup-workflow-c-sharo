using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public class DirectoryScan : Receiver
	{
		private FileSystemWatcher watcher = new FileSystemWatcher();

		private int currentLocation;

		private string currentFileName = "";

		public HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		public List<FileInfo> Files
		{
			get;
			set;
		}

		public IDirectoryScanSetting Setting
		{
			get
			{
				return (IDirectoryScanSetting)this.baseSetting;
			}
		}

		public DirectoryScan()
		{
		}

		protected override void FunctionAllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
			base.FunctionAllFunctionsSucceded(workflowInstance);
			try
			{
				if (this.DocumentManager != null)
				{
					this.DocumentManager.Delete(this.DocumentManager.Messages[this.currentLocation]);
				}
			}
			catch (Exception exception)
			{
				throw;
			}
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			this.watcher.Created -= new FileSystemEventHandler(this.OnCreated);
			this.watcher.Deleted -= new FileSystemEventHandler(this.OnDeleted);
			this.watcher = null;
			if (this.DocumentManager != null)
			{
				this.DocumentManager.Dispose();
				this.DocumentManager = null;
			}
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			string text;
			base.Pending = false;
			if (this.DocumentManager == null)
			{
				if (this.Files.Count <= 0)
				{
					if (this.Setting.EndAfterProcessing)
					{
						base.Finished = true;
						return;
					}
					base.Pending = true;
					return;
				}
				if (!File.Exists(this.Files[0].FullName))
				{
					this.Files.RemoveAt(0);
					base.Pending = true;
					return;
				}
				this.currentFileName = this.Files[0].Name;
				this.WaitForFileToCopy();
				this.DocumentManager = new HL7Soup.DocumentManager()
				{
					MessageType = this.Setting.MessageType,
					AutomaticValidation = false
				};
				this.DocumentManager.Load(this.Files[0].FullName);
				this.Files.RemoveAt(0);
			}
			if (this.DocumentManager != null)
			{
				ObservableCollection<Message> messages = this.DocumentManager.Messages;
			}
			if (this.DocumentManager.Messages.Count > this.currentLocation)
			{
				workflowInstance.SetVariable("DirectoryScannerFileName", this.currentFileName);
				workflowInstance.AddActivity(FunctionHelpers.GetFunctionMessage(this.DocumentManager.Messages[this.currentLocation].ToString(), ((IReceiverSetting)this.Setting).MessageType), ((IReceiverSetting)this.Setting).Id);
				MessageLogger instance = MessageLogger.Instance;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				instance.AddMessage(new MessageEvent(text, workflowInstance, ((IReceiverSetting)this.Setting).Id, LogItemStatus.Processing, LogItemDataType.Received));
				this.MessageReceived(this, ref workflowInstance);
			}
			if (this.DocumentManager.Messages.Count <= this.currentLocation)
			{
				this.currentLocation = 0;
				if (this.Setting.DeleteFileOnComplete)
				{
					File.Delete(this.DocumentManager.FilePath);
				}
				if (this.Setting.MoveIntoDirectoryOnComplete)
				{
					string fileName = Path.GetFileName(this.DocumentManager.FilePath);
					string str = Path.Combine(this.Setting.DirectoryToMoveInto, fileName);
					if (File.Exists(str))
					{
						try
						{
							File.Delete(str);
						}
						catch (Exception exception1)
						{
							Exception exception = exception1;
							throw new Exception(string.Concat(new string[] { "Could not move file \"", this.DocumentManager.FilePath, "\" after processing because the destination already has a file of the same name \"", str, "\" that could not be removed." }), exception);
						}
					}
					File.Move(this.DocumentManager.FilePath, str);
				}
				if (this.Files.Count == 0 && this.Setting.EndAfterProcessing)
				{
					base.Finished = true;
				}
				base.Pending = true;
				this.DocumentManager.Dispose();
				this.DocumentManager = null;
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			this.currentLocation = 0;
			this.Files = new List<FileInfo>();
			foreach (FileInfo fileInfo in 
				from f in (IEnumerable<FileInfo>)(new DirectoryInfo(this.Setting.DirectoryPath)).GetFiles(this.Setting.DirectoryFilter, SearchOption.TopDirectoryOnly)
				orderby f.CreationTime
				select f)
			{
				this.Files.Add(fileInfo);
			}
			if (!this.Setting.EndAfterProcessing)
			{
				this.Watch();
			}
		}

		protected override void FunctionRollBack(WorkflowInstance workflowInstance)
		{
			base.FunctionRollBack(workflowInstance);
		}

		private bool IsFileLocked(FileInfo file)
		{
			bool flag;
			FileStream fileStream = null;
			try
			{
				try
				{
					fileStream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
					return false;
				}
				catch (IOException oException)
				{
					flag = true;
				}
			}
			finally
			{
				if (fileStream != null)
				{
					fileStream.Close();
				}
			}
			return flag;
		}

		private void OnCreated(object source, FileSystemEventArgs e)
		{
			this.Files.Add(new FileInfo(e.FullPath));
		}

		private void OnDeleted(object source, FileSystemEventArgs e)
		{
			this.Files.Remove(new FileInfo(e.FullPath));
		}

		private void WaitForFileToCopy()
		{
			FileInfo fileInfo = new FileInfo(this.Files[0].FullName);
			if (!this.IsFileLocked(fileInfo))
			{
				return;
			}
			Thread.Sleep(100);
			if (!this.IsFileLocked(fileInfo))
			{
				return;
			}
			Thread.Sleep(100);
			if (!this.IsFileLocked(fileInfo))
			{
				return;
			}
			Thread.Sleep(100);
			if (!this.IsFileLocked(fileInfo))
			{
				return;
			}
			Thread.Sleep(500);
			if (!this.IsFileLocked(fileInfo))
			{
				return;
			}
			Thread.Sleep(500);
			if (this.IsFileLocked(fileInfo))
			{
				Thread.Sleep(500);
			}
		}

		private void Watch()
		{
			this.watcher.Path = this.Setting.DirectoryPath;
			this.watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.LastAccess;
			this.watcher.Filter = this.Setting.DirectoryFilter;
			this.watcher.Created += new FileSystemEventHandler(this.OnCreated);
			this.watcher.Deleted += new FileSystemEventHandler(this.OnDeleted);
			this.watcher.EnableRaisingEvents = true;
		}

		public override event MessageReceivedEventHandler MessageReceived;
	}
}