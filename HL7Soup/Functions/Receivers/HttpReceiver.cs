using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Http;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Threading;

namespace HL7Soup.Functions.Receivers
{
	public class HttpReceiver : Receiver
	{
		private HttpReceiverService httpReceiverService;

		public IHttpReceiverSetting Setting
		{
			get
			{
				return (IHttpReceiverSetting)this.baseSetting;
			}
		}

		public HttpReceiver()
		{
		}

		protected override void FunctionCanceled()
		{
			base.FunctionCanceled();
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			this.httpReceiverService.Stop();
		}

		protected override void FunctionGetNext(WorkflowInstance workflowInstance)
		{
			try
			{
				if (this.httpReceiverService.Status == HttpReceiverService.HttpReceiverServiceStatus.receivedMessage)
				{
					base.Pending = false;
					if (!string.IsNullOrEmpty(this.httpReceiverService.ErrorMessage))
					{
						throw new Exception(this.httpReceiverService.ErrorMessage);
					}
					workflowInstance.SetVariable("HttpMethod", this.httpReceiverService.HttpMethod);
					if (this.Setting.ExtractParameters)
					{
						foreach (KeyValuePair<string, IVariableCreator> queryStringParameter in this.Setting.QueryStringParameters)
						{
							if (!queryStringParameter.Value.SampleValueIsDefaultValue)
							{
								continue;
							}
							workflowInstance.SetVariable(queryStringParameter.Key, queryStringParameter.Value.SampleVariableValue);
						}
						foreach (KeyValuePair<string, string> inboundParameter in this.httpReceiverService.InboundParameters)
						{
							if (!this.Setting.QueryStringParameters.ContainsKey(inboundParameter.Key) || !(inboundParameter.Value != ""))
							{
								continue;
							}
							workflowInstance.SetVariable(inboundParameter.Key, inboundParameter.Value);
						}
					}
					if (this.Setting.ExtractUrlSections)
					{
						foreach (IVariableCreator urlSection in this.Setting.UrlSections)
						{
							if (!urlSection.SampleValueIsDefaultValue)
							{
								continue;
							}
							workflowInstance.SetVariable(urlSection.VariableName, urlSection.SampleVariableValue);
						}
						for (int i = 0; i < Math.Min(this.Setting.UrlSections.Count, (int)this.httpReceiverService.UrlSections.Length); i++)
						{
							if (this.httpReceiverService.UrlSections[i] != "")
							{
								workflowInstance.SetVariable(this.Setting.UrlSections[i].VariableName, Uri.UnescapeDataString(this.httpReceiverService.UrlSections[i]));
							}
						}
					}
					MessageLogger.Instance.AddMessage(new MessageEvent(this.httpReceiverService.InboundMessage, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Received));
					using (IMessage functionMessage = FunctionHelpers.GetFunctionMessage(this.httpReceiverService.InboundMessage, this.Setting.MessageType))
					{
						workflowInstance.AddActivity(functionMessage, this.Setting.Id);
						if (this.MessageReceived != null)
						{
							this.MessageReceived(this, ref workflowInstance);
						}
					}
					if (!this.Setting.ReturnNoResponse)
					{
						string str = (workflowInstance.ResponseMessage == null ? "" : workflowInstance.ResponseMessage.Text);
						MessageLogger.Instance.AddMessage(new MessageEvent(str, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
						this.httpReceiverService.OutboundMessage = str;
						this.httpReceiverService.Status = HttpReceiverService.HttpReceiverServiceStatus.responded;
					}
				}
				else
				{
					base.Pending = true;
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, exception.Message, exception);
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			try
			{
				if (!this.Setting.UseDefaultSSLCertificate)
				{
					HttpHelpers.EnableUrlToBeOpenedWithoutElevation(this.Setting.GetURL(true), HttpHelpers.AssemblyGuid, this.Setting.CertificateThumbPrint);
				}
				else
				{
					HttpHelpers.EnableUrlToBeOpenedWithoutElevation(this.Setting.GetURL(true), HttpHelpers.AssemblyGuid);
				}
				this.httpReceiverService = new HttpReceiverService(new HttpListener(), this.Setting);
				this.httpReceiverService.Start();
			}
			catch (AddressAccessDeniedException addressAccessDeniedException1)
			{
				AddressAccessDeniedException addressAccessDeniedException = addressAccessDeniedException1;
				throw new Exception(string.Concat("Are you running as admininstrator?\r\n\r\n", addressAccessDeniedException.Message), addressAccessDeniedException);
			}
			catch (AddressAlreadyInUseException addressAlreadyInUseException1)
			{
				AddressAlreadyInUseException addressAlreadyInUseException = addressAlreadyInUseException1;
				throw new Exception(string.Concat("To see what app currently bound to the port run 'netsh http show urlacl'\r\n\r\nTo remove a port binding run 'netsh http delete urlacl url=urlcurrentlyregisteredtoport'\r\n\r\n", addressAlreadyInUseException.Message), addressAlreadyInUseException);
			}
		}

		public override event MessageReceivedEventHandler MessageReceived;
	}
}