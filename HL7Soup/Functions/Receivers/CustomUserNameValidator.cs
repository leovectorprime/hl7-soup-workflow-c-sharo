using HL7Soup;
using NLog;
using System;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace HL7Soup.Functions.Receivers
{
	public class CustomUserNameValidator : UserNamePasswordValidator
	{
		private string UserName;

		private string Password;

		public CustomUserNameValidator(string userName, string password)
		{
			this.UserName = userName;
			this.Password = password;
		}

		public override void Validate(string userName, string password)
		{
			if (userName == null || password == null)
			{
				throw new ArgumentNullException();
			}
			if (!(userName == this.UserName) || !(password == this.Password))
			{
				Log.Instance.Error("The Web Service Reciever was called with an Incorrect Username or Password.");
				throw new SecurityTokenException("The Web Service Reciever was called with an Incorrect Username or Password.");
			}
		}
	}
}