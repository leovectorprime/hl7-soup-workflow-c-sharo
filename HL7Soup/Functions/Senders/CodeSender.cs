using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace HL7Soup.Functions.Senders
{
	public class CodeSender : Sender
	{
		private ScriptRunner<string> runner;

		public new virtual ICodeSenderSetting Setting
		{
			get
			{
				return (ICodeSenderSetting)this.baseSetting;
			}
		}

		public CodeSender()
		{
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			try
			{
				Script<string> script = CSharpScript.Create<string>(this.Setting.Code, ScriptOptions.Default.WithReferences(new Assembly[] { typeof(IHL7Message).Assembly }).WithImports(new string[] { "HL7Soup.Integrations", "System", "System.Collections.Generic", "System.Linq", "System.Text" }), typeof(MyContext), null);
				this.runner = script.CreateDelegate(new CancellationToken());
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new Exception(string.Concat("Couldn't compile custom code in activity:\r\n", exception.Message), exception);
			}
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			this.SendAndReturnResponse(workflowInstance);
		}

		protected void SendAndReturnResponse(WorkflowInstance workflowInstance)
		{
			string text;
			if (this.Setting == null)
			{
				throw new NullReferenceException("Send connection setting must be provided,");
			}
			MessageLogger instance = MessageLogger.Instance;
			IMessage message = workflowInstance.Message;
			if (message != null)
			{
				text = message.Text;
			}
			else
			{
				text = null;
			}
			instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
			try
			{
				if (this.Setting.UseResponse)
				{
					if (this.Setting.ResponseMessageType != MessageTypes.Unknown)
					{
						((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(this.Setting.ResponseMessageTemplate, this.Setting.ResponseMessageType);
					}
					else
					{
						((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessageDeterminedFromText(this.Setting.ResponseMessageTemplate);
					}
				}
				MyContext myContext = new MyContext()
				{
					workflowInstance = workflowInstance,
					activityInstance = workflowInstance.CurrentActivityInstance
				};
				Task<string> task = this.runner(myContext, new CancellationToken());
				if (task.Status == TaskStatus.Faulted)
				{
					if (task.Exception.InnerException == null)
					{
						throw new Exception(string.Concat("Executing activities custom script code errored:\r\n", task.Exception.Message), task.Exception);
					}
					throw new Exception(string.Concat("Error executing activities custom script code:\r\n", task.Exception.InnerException.Message), task.Exception.InnerException);
				}
				HL7V2MessageType responseMessage = ((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage as HL7V2MessageType;
				if (responseMessage != null)
				{
					responseMessage.ReloadFromInternalMessageParts();
				}
				if (!this.Setting.UseResponse)
				{
					MessageLogger.Instance.AddMessage(new MessageEvent("Code Executed Successfully", workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Response));
				}
				else
				{
					MessageLogger.Instance.AddMessage(new MessageEvent((((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage == null ? "Code Executed Successfully" : ((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage.Text), workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Response));
				}
			}
			catch (Exception exception)
			{
				throw;
			}
			MessageLogger.Instance.AddMessage(new MessageEvent("", workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Sent));
		}
	}
}