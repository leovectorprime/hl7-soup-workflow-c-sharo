using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using NLog;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace HL7Soup.Functions.Senders
{
	public class HttpSender : Sender
	{
		public new virtual IHttpSenderSetting Setting
		{
			get
			{
				return (IHttpSenderSetting)this.baseSetting;
			}
		}

		public HttpSender()
		{
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			this.SendAndReturnResponse(workflowInstance);
		}

		protected void SendAndReturnResponse(WorkflowInstance workflowInstance)
		{
			string text;
			string str;
			string text1;
			if (this.Setting == null)
			{
				throw new NullReferenceException("Send connection setting must be provided,");
			}
			try
			{
				MessageLogger instance = MessageLogger.Instance;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
				string str1 = "";
				Logger logger = Log.Instance;
				string newLine = Environment.NewLine;
				IMessage message1 = workflowInstance.Message;
				if (message1 != null)
				{
					str = message1.Text;
				}
				else
				{
					str = null;
				}
				logger.Debug(string.Concat("Sent: ", newLine, str));
				Log.Instance.Debug(string.Concat("Received: ", Environment.NewLine, str1));
				MessageLogger.Instance.AddMessage(new MessageEvent(str1, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
				using (WebClient webClient = new WebClient())
				{
					webClient.Headers.Add("Content-Type", this.Setting.ContentType);
					string str2 = "";
					byte[] numArray = null;
					if (this.Setting.Method != HttpMethods.GET)
					{
						IMessage message2 = workflowInstance.Message;
						if (message2 != null)
						{
							text1 = message2.Text;
						}
						else
						{
							text1 = null;
						}
						str2 = text1;
						numArray = (str2 == null ? new byte[0] : Encoding.UTF8.GetBytes(str2));
					}
					if (this.Setting.Authentication)
					{
						webClient.Credentials = new NetworkCredential(this.Setting.UserName, this.Setting.Password);
					}
					string str3 = "";
					switch (this.Setting.Method)
					{
						case HttpMethods.POST:
						{
							str3 = "POST";
							break;
						}
						case HttpMethods.GET:
						{
							str3 = "GET";
							break;
						}
						case HttpMethods.PUT:
						{
							str3 = "PUT";
							break;
						}
						case HttpMethods.DELETE:
						{
							str3 = "DELETE";
							break;
						}
					}
					string end = "";
					if (this.Setting.Method != HttpMethods.GET)
					{
						byte[] numArray1 = webClient.UploadData(this.Setting.ServerRuntimeValue(workflowInstance), str3, numArray);
						end = (this.Setting.MessageType != MessageTypes.Binary ? Encoding.Default.GetString(numArray1) : Convert.ToBase64String(numArray1));
					}
					else
					{
						using (Stream stream = webClient.OpenRead(this.Setting.ServerRuntimeValue(workflowInstance)))
						{
							if (this.Setting.MessageType != MessageTypes.Binary)
							{
								using (StreamReader streamReader = new StreamReader(stream))
								{
									end = streamReader.ReadToEnd();
								}
							}
							else
							{
								using (MemoryStream memoryStream = new MemoryStream())
								{
									stream.CopyTo(memoryStream);
									end = Convert.ToBase64String(memoryStream.ToArray());
								}
							}
						}
					}
					Log.Instance.Debug(string.Concat("Sent: ", Environment.NewLine, str2));
					if (!this.Setting.ResponseNotAvailable)
					{
						str1 = end;
					}
				}
				Log.Instance.Debug(string.Concat("Received: ", Environment.NewLine, str1));
				MessageLogger.Instance.AddMessage(new MessageEvent(str1, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Completed));
				if (!(this.Setting is IEditorSendingBehaviourSetting))
				{
					((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(str1, this.Setting.MessageType);
				}
				else
				{
					workflowInstance.SetReponseMessage(FunctionHelpers.GetFunctionMessage(str1, this.Setting.MessageType));
				}
			}
			catch (WebException webException1)
			{
				WebException webException = webException1;
				string end1 = "";
				if (webException.Response != null)
				{
					end1 = (new StreamReader(webException.Response.GetResponseStream())).ReadToEnd();
				}
				if (end1 == "")
				{
					end1 = webException.Message;
					if (webException.InnerException != null)
					{
						end1 = string.Concat(end1, ":\r\n", webException.InnerException.Message);
					}
				}
				base.Errored(workflowInstance, end1, webException);
			}
			catch (Exception exception)
			{
				base.Errored(workflowInstance, exception);
			}
		}
	}
}