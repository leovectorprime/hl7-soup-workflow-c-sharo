using HL7Soup.Integrations;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Senders
{
	public class MyContext
	{
		public IActivityInstance activityInstance
		{
			get;
			set;
		}

		public IWorkflowInstance workflowInstance
		{
			get;
			set;
		}

		public MyContext()
		{
		}
	}
}