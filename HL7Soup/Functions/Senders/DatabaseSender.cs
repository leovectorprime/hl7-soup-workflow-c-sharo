using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Text;

namespace HL7Soup.Functions.Senders
{
	public class DatabaseSender : Sender
	{
		public new virtual IDatabaseSenderSetting Setting
		{
			get
			{
				return (IDatabaseSenderSetting)this.baseSetting;
			}
		}

		public DatabaseSender()
		{
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			this.SendAndReturnResponse(workflowInstance);
		}

		public static DbCommand GetDbCommand(DataProviders dataProvider)
		{
			switch (dataProvider)
			{
				case DataProviders.SqlClient:
				{
					return new SqlCommand();
				}
				case DataProviders.OracleClient:
				{
					return new OracleCommand();
				}
				case DataProviders.OleDb:
				{
					return new OleDbCommand();
				}
				case DataProviders.Odbc:
				{
					return new OdbcCommand();
				}
			}
			throw new NotImplementedException(string.Concat("Invalid Data Provider for command", dataProvider.ToString()));
		}

		public static DbConnection GetDbConnection(DataProviders dataProvider, string connectionString)
		{
			switch (dataProvider)
			{
				case DataProviders.SqlClient:
				{
					return new SqlConnection(connectionString);
				}
				case DataProviders.OracleClient:
				{
					return new OracleConnection(connectionString);
				}
				case DataProviders.OleDb:
				{
					return new OleDbConnection(connectionString);
				}
				case DataProviders.Odbc:
				{
					return new OdbcConnection(connectionString);
				}
			}
			throw new NotImplementedException(string.Concat("Invalid Data Provider ", dataProvider.ToString()));
		}

		public static DbParameter GetDbParameter(DataProviders dataProvider)
		{
			switch (dataProvider)
			{
				case DataProviders.SqlClient:
				{
					return new SqlParameter();
				}
				case DataProviders.OracleClient:
				{
					return new OracleParameter();
				}
				case DataProviders.OleDb:
				{
					return new OleDbParameter();
				}
				case DataProviders.Odbc:
				{
					return new OdbcParameter();
				}
			}
			throw new NotImplementedException(string.Concat("Invalid Data Provider for command", dataProvider.ToString()));
		}

		protected void SendAndReturnResponse(WorkflowInstance workflowInstance)
		{
			string text;
			string str;
			if (this.Setting == null)
			{
				throw new NullReferenceException("Send connection setting must be provided,");
			}
			try
			{
				MessageLogger instance = MessageLogger.Instance;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
				using (DbConnection dbConnection = DatabaseSender.GetDbConnection(this.Setting.DataProvider, this.Setting.ConnectionString))
				{
					using (DbCommand dbCommand = DatabaseSender.GetDbCommand(this.Setting.DataProvider))
					{
						DbCommand dbCommand1 = dbCommand;
						IMessage message1 = workflowInstance.Message;
						if (message1 != null)
						{
							str = message1.Text;
						}
						else
						{
							str = null;
						}
						dbCommand1.CommandText = str;
						dbCommand.CommandType = CommandType.Text;
						dbCommand.Connection = dbConnection;
						StringBuilder stringBuilder = new StringBuilder("Parameters:\r\n");
						if (this.Setting.Parameters != null)
						{
							foreach (DatabaseSettingParameter parameter in this.Setting.Parameters)
							{
								DbParameter dbParameter = DatabaseSender.GetDbParameter(this.Setting.DataProvider);
								dbParameter.ParameterName = parameter.Name;
								string theValue = TransformerAction.GetTheValue(workflowInstance, parameter.Value, parameter.FromSetting, parameter.FromDirection, null);
								theValue = HL7Soup.Functions.Variable.ProcessFormat(theValue, parameter);
								dbParameter.Value = theValue;
								dbCommand.Parameters.Add(dbParameter);
								stringBuilder.Append(string.Concat(new string[] { "\r\n", dbParameter.ParameterName, ": \"", Helpers.TruncateString(theValue, 200), "\"" }));
							}
						}
						if (stringBuilder.Length > 13)
						{
							MessageLogger.Instance.AddMessage(new MessageEvent((workflowInstance.Message == null ? stringBuilder.ToString() : string.Concat(workflowInstance.Message.Text, "\r\n\r\n", stringBuilder)), workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
						}
						dbConnection.Open();
						if (!this.Setting.ResponseNotAvailable)
						{
							StringBuilder stringBuilder1 = new StringBuilder();
							DbDataReader dbDataReaders = dbCommand.ExecuteReader();
							DbDataReader dbDataReaders1 = dbDataReaders;
							using (dbDataReaders)
							{
								if (dbDataReaders1.Read())
								{
									for (int i = 0; i < dbDataReaders1.FieldCount; i++)
									{
										Type fieldType = dbDataReaders1.GetFieldType(i);
										if (!typeof(byte[]).IsAssignableFrom(fieldType))
										{
											stringBuilder1.Append(string.Concat("\"", dbDataReaders1.GetValue(i).ToString().Replace("\"", "\"\""), "\"", (i == dbDataReaders1.FieldCount - 1 ? "" : ",")));
										}
										else
										{
											stringBuilder1.Append(string.Concat("\"", Convert.ToBase64String((byte[])dbDataReaders1.GetValue(i)), "\"", (i == dbDataReaders1.FieldCount - 1 ? "" : ",")));
										}
									}
								}
								dbDataReaders1.Close();
							}
							dbConnection.Close();
							MessageLogger.Instance.AddMessage(new MessageEvent(stringBuilder1.ToString(), workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
							((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(stringBuilder1.ToString(), MessageTypes.CSV);
						}
						else
						{
							dbCommand.ExecuteNonQuery();
							dbConnection.Close();
						}
					}
				}
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Completed));
			}
			catch (ArgumentNullException argumentNullException)
			{
				base.Errored(workflowInstance, argumentNullException);
			}
			catch (SocketException socketException)
			{
				base.Errored(workflowInstance, socketException);
			}
		}
	}
}