using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.IO;
using System.Text;

namespace HL7Soup.Functions.Senders
{
	public class FileWriterSender : Sender, ISender, IFunctionProvider
	{
		private int c;

		private Encoding encoding = Encoding.Default;

		private string lastFilePath = "";

		private string lastDirectory = "";

		public new IFileWriterSenderSetting Setting
		{
			get
			{
				return (IFileWriterSenderSetting)this.baseSetting;
			}
		}

		public FileWriterSender()
		{
		}

		protected override void FunctionClosed()
		{
			if (this.Setting.MoveIntoDirectoryOnComplete)
			{
				this.MoveFile();
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			Directory.CreateDirectory((new FileInfo(this.Setting.FilePathToWriteRuntimeValue(workflowInstance))).DirectoryName);
			if (this.Setting.MoveIntoDirectoryOnComplete)
			{
				Directory.CreateDirectory(this.Setting.DirectoryToMoveIntoRuntimeValue(workflowInstance));
			}
			this.encoding = Helpers.GetEncoding(true);
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			string text;
			string str;
			string text1;
			string str1;
			MessageLogger instance = MessageLogger.Instance;
			IMessage message = workflowInstance.Message;
			if (message != null)
			{
				text = message.Text;
			}
			else
			{
				text = null;
			}
			instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
			IMessage message1 = workflowInstance.Message;
			if (message1 != null)
			{
				str = message1.Text;
			}
			else
			{
				str = null;
			}
			if (!string.IsNullOrEmpty(str))
			{
				this.c++;
				if (this.Setting.MessageType != MessageTypes.Binary)
				{
					string writeRuntimeValue = this.Setting.FilePathToWriteRuntimeValue(workflowInstance);
					IMessage message2 = workflowInstance.Message;
					if (message2 != null)
					{
						text1 = message2.Text;
					}
					else
					{
						text1 = null;
					}
					File.AppendAllText(writeRuntimeValue, string.Concat(text1, Environment.NewLine), this.encoding);
				}
				else
				{
					string writeRuntimeValue1 = this.Setting.FilePathToWriteRuntimeValue(workflowInstance);
					IMessage message3 = workflowInstance.Message;
					if (message3 != null)
					{
						str1 = message3.Text;
					}
					else
					{
						str1 = null;
					}
					File.WriteAllBytes(writeRuntimeValue1, Convert.FromBase64String(str1));
				}
			}
			if (this.Setting.MaxRecordsPerFile <= this.c)
			{
				this.MoveFile(workflowInstance);
			}
			MessageLogger.Instance.AddMessage(new MessageEvent("", workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Sent));
			this.lastFilePath = this.Setting.FilePathToWriteRuntimeValue(workflowInstance);
			this.lastDirectory = this.Setting.DirectoryToMoveIntoRuntimeValue(workflowInstance);
		}

		private void MoveFile()
		{
			this.MoveFile(this.lastFilePath, this.lastDirectory);
		}

		private void MoveFile(WorkflowInstance workflowInstance)
		{
			this.MoveFile(this.Setting.FilePathToWriteRuntimeValue(workflowInstance), this.Setting.DirectoryToMoveIntoRuntimeValue(workflowInstance));
		}

		private void MoveFile(string file, string directory)
		{
			if (File.Exists(file) && this.Setting.MoveIntoDirectoryOnComplete)
			{
				this.c = 0;
				FileInfo fileInfo = new FileInfo(file);
				string str = string.Concat(fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length), string.Format("{0:yyyyMMddhhmmss}", DateTime.Now), fileInfo.Extension);
				int num = 0;
				while (File.Exists(Path.Combine(directory, str)))
				{
					num++;
					str = string.Concat(new object[] { fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length), string.Format("{0:yyyyMMddhhmmss}", DateTime.Now), "-", num, fileInfo.Extension });
				}
				File.Move(file, Path.Combine(directory, str));
			}
		}
	}
}