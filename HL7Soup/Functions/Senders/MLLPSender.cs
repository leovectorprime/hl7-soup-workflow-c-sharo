using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace HL7Soup.Functions.Senders
{
	public class MLLPSender : Sender
	{
		public new virtual IMLLPSenderSetting Setting
		{
			get
			{
				return (IMLLPSenderSetting)this.baseSetting;
			}
		}

		public MLLPSender()
		{
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			this.SendAndReturnResponse(workflowInstance);
		}

		protected void SendAndReturnResponse(WorkflowInstance workflowInstance)
		{
			byte[] numArray;
			int frameIndex;
			string text;
			string str;
			string text1;
			if (this.Setting == null)
			{
				throw new NullReferenceException("Send connection setting must be provided,");
			}
			try
			{
				MessageLogger instance = MessageLogger.Instance;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
				TcpClient tcpClient = new TcpClient(this.Setting.Server, this.Setting.Port);
				if (HL7Soup.Workflow.Properties.Settings.Default.TrimMessageBeforeSending)
				{
					IMessage message1 = workflowInstance.Message;
					if (message1 != null)
					{
						string str1 = message1.Text;
						if (str1 != null)
						{
							str = str1.Trim();
						}
						else
						{
							str = null;
						}
					}
					else
					{
						str = null;
					}
				}
				else
				{
					IMessage message2 = workflowInstance.Message;
					if (message2 != null)
					{
						str = message2.Text;
					}
					else
					{
						str = null;
					}
				}
				string str2 = str;
				numArray = (str2 == null ? new byte[0] : this.Setting.MessageEncoding.GetBytes(str2));
				byte[] numArray1 = MLLPHelpers.WrapInFrame(numArray, this.Setting);
				NetworkStream stream = tcpClient.GetStream();
				stream.Write(numArray1, 0, (int)numArray1.Length);
				int num = 5000;
				if (!(this.Setting is IEditorSendingBehaviourSetting))
				{
					int.TryParse(HL7Soup.Workflow.Properties.Settings.Default.DefaultTCPSendTimeout, out num);
					stream.ReadTimeout = num;
				}
				else
				{
					int.TryParse(HL7Soup.Workflow.Properties.Settings.Default.DefaultEditorTCPSendTimeout, out num);
					stream.ReadTimeout = num;
				}
				Logger logger = Log.Instance;
				string newLine = Environment.NewLine;
				IMessage message3 = workflowInstance.Message;
				if (message3 != null)
				{
					text1 = message3.Text;
				}
				else
				{
					text1 = null;
				}
				logger.Debug(string.Concat("Sent: ", newLine, text1));
				string empty = string.Empty;
				if (this.Setting.WaitForResponse)
				{
					numArray = new byte[25];
					int num1 = 0;
					if (HL7Soup.Workflow.Properties.Settings.Default.HL7SenderAllowMultiMessageResponse)
					{
						while (true)
						{
							int num2 = stream.Read(numArray, 0, (int)numArray.Length);
							num1 = num2;
							if (num2 == 0)
							{
								break;
							}
							empty = string.Concat(empty, this.Setting.MessageEncoding.GetString(numArray, 0, num1));
						}
						empty = empty.Replace(new string(this.Setting.FrameStart), "").Replace(new string(this.Setting.FrameEnd), "\r");
					}
					else
					{
						bool flag = false;
						do
						{
							int num3 = stream.Read(numArray, 0, (int)numArray.Length);
							num1 = num3;
							if (num3 != 0)
							{
								empty = string.Concat(empty, this.Setting.MessageEncoding.GetString(numArray, 0, num1));
								int frameIndex1 = MLLPHelpers.GetFrameIndex(empty, 0, this.Setting.FrameStart);
								if (frameIndex1 >= 0 && !flag)
								{
									empty = empty.Substring(frameIndex1 + (int)this.Setting.FrameStart.Length);
									flag = true;
								}
								frameIndex = MLLPHelpers.GetFrameIndex(empty, 0, this.Setting.FrameEnd);
							}
							else
							{
								goto Label0;
							}
						}
						while (frameIndex < 0);
						empty = empty.Substring(0, frameIndex);
					}
				Label0:
					Log.Instance.Debug(string.Concat("Received: ", Environment.NewLine, empty));
					MessageLogger.Instance.AddMessage(new MessageEvent(empty, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
					if (this.Setting.WaitForResponse && this.Setting.UseResponse)
					{
						base.Errored(workflowInstance, "The server respond with an empty message or closed the connection without responding. This activity is configured to require a valid response.");
					}
				}
				tcpClient.Close();
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Completed));
				if (!(this.Setting is IEditorSendingBehaviourSetting))
				{
					((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(empty, this.Setting.MessageType);
				}
				else
				{
					workflowInstance.SetReponseMessage(FunctionHelpers.GetFunctionMessage(empty, this.Setting.MessageType));
				}
			}
			catch (IOException oException)
			{
				base.Errored(workflowInstance, "The server did not respond with a complete message", oException);
			}
			catch (ArgumentNullException argumentNullException)
			{
				base.Errored(workflowInstance, argumentNullException);
			}
			catch (SocketException socketException)
			{
				base.Errored(workflowInstance, socketException);
			}
		}
	}
}