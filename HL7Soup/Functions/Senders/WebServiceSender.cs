using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Functions.WCF;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;

namespace HL7Soup.Functions.Senders
{
	public class WebServiceSender : Sender
	{
		public new virtual IWebServiceSenderSetting Setting
		{
			get
			{
				return (IWebServiceSenderSetting)this.baseSetting;
			}
		}

		public WebServiceSender()
		{
		}

		private static void AddSecurityCredentialsToSoapMessage(XmlDocument doc, string userName, string password)
		{
			if (doc == null)
			{
				throw new Exception("Cannot find soap message, so credentials could not be added.");
			}
			XmlNode firstChild = doc.FirstChild;
			if (firstChild == null)
			{
				throw new Exception("Cannot find soap envelope, so credentials could not be added.");
			}
			XmlNode xmlNodes = null;
			foreach (XmlNode childNode in firstChild.ChildNodes)
			{
				if (!string.Equals(childNode.LocalName, "header", StringComparison.InvariantCultureIgnoreCase))
				{
					continue;
				}
				xmlNodes = childNode;
				goto Label0;
			}
		Label0:
			if (firstChild.ChildNodes.Count == 0)
			{
				throw new Exception("No 'Body' found in soap message!");
			}
			if (xmlNodes != null)
			{
				foreach (object obj in xmlNodes.ChildNodes)
				{
					if (!string.Equals(((XmlNode)obj).LocalName, "security", StringComparison.InvariantCultureIgnoreCase))
					{
						continue;
					}
					return;
				}
			}
			else
			{
				XmlElement xmlElement = doc.CreateElement(firstChild.Prefix, "Header", firstChild.NamespaceURI);
				xmlNodes = firstChild.InsertBefore(xmlElement, firstChild.FirstChild);
			}
			XmlDocumentFragment str = doc.CreateDocumentFragment();
			StringBuilder stringBuilder = new StringBuilder("<o:Security xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" ");
			stringBuilder.Append("xmlns:");
			stringBuilder.Append(firstChild.Prefix);
			stringBuilder.Append("=\"");
			stringBuilder.Append(firstChild.NamespaceURI);
			stringBuilder.Append("\" ");
			stringBuilder.Append(firstChild.Prefix);
			stringBuilder.AppendLine(":mustUnderstand = \"1\" >");
			stringBuilder.AppendLine("<u:Timestamp u:Id=\"_0\">");
			stringBuilder.Append("<u:Created>");
			DateTime universalTime = DateTime.Now.AddMinutes(-1);
			universalTime = universalTime.ToUniversalTime();
			stringBuilder.Append(universalTime.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ"));
			stringBuilder.AppendLine("</u:Created>");
			stringBuilder.Append("<u:Expires>");
			universalTime = DateTime.Now.AddMinutes(4);
			universalTime = universalTime.ToUniversalTime();
			stringBuilder.Append(universalTime.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ"));
			stringBuilder.AppendLine("</u:Expires>");
			stringBuilder.AppendLine("</u:Timestamp>");
			stringBuilder.Append("<o:UsernameToken u:Id=\"uuid-");
			Guid guid = Guid.NewGuid();
			stringBuilder.Append(guid.ToString());
			stringBuilder.AppendLine("-1\">");
			stringBuilder.Append("<o:Username>");
			stringBuilder.Append(userName);
			stringBuilder.AppendLine("</o:Username>");
			stringBuilder.Append("<o:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">");
			stringBuilder.Append(password);
			stringBuilder.AppendLine("</o:Password>");
			stringBuilder.AppendLine("</o:UsernameToken>");
			stringBuilder.AppendLine("</o:Security>");
			stringBuilder.AppendLine("");
			stringBuilder.AppendLine("");
			stringBuilder.AppendLine("");
			str.InnerXml = stringBuilder.ToString();
			xmlNodes.AppendChild(str);
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
		}

		protected override void FunctionSend(WorkflowInstance workflowInstance)
		{
			this.SendAndReturnResponse(workflowInstance);
		}

		protected void SendAndReturnResponse(WorkflowInstance workflowInstance)
		{
			string text;
			string str;
			string text1;
			if (this.Setting == null)
			{
				throw new NullReferenceException("Send connection setting must be provided,");
			}
			try
			{
				MessageLogger instance = MessageLogger.Instance;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				instance.AddMessage(new MessageEvent(text, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
				WebServiceOperation operation = this.Setting.Operation;
				if (this.Setting.ManualConfiguration)
				{
					WebServiceOperation webServiceOperation = new WebServiceOperation()
					{
						Action = this.Setting.Action,
						UseSoap12 = this.Setting.UseSoap12,
						PassAuthenticationInSoapHeader = this.Setting.PassAuthenticationInSoapHeader
					};
				}
				string str1 = "";
				Logger logger = Log.Instance;
				string newLine = Environment.NewLine;
				IMessage message1 = workflowInstance.Message;
				if (message1 != null)
				{
					str = message1.Text;
				}
				else
				{
					str = null;
				}
				logger.Debug(string.Concat("Sent: ", newLine, str));
				IMessage message2 = workflowInstance.Message;
				if (message2 != null)
				{
					text1 = message2.Text;
				}
				else
				{
					text1 = null;
				}
				str1 = WebServiceSender.SendSOAPRequest(text1, this.Setting.Server, null, this.Setting.Operation, this.Setting.Authentication, this.Setting.UserName, this.Setting.Password, this.Setting.ResponseNotAvailable);
				Log.Instance.Debug(string.Concat("Received: ", Environment.NewLine, str1));
				MessageLogger.Instance.AddMessage(new MessageEvent(str1, workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Response));
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Completed));
				if (!(this.Setting is IEditorSendingBehaviourSetting))
				{
					((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(str1, this.Setting.MessageType);
				}
				else
				{
					workflowInstance.SetReponseMessage(FunctionHelpers.GetFunctionMessage(str1, this.Setting.MessageType));
				}
			}
			catch (WebException webException1)
			{
				WebException webException = webException1;
				string end = "";
				if (webException.Response != null)
				{
					end = (new StreamReader(webException.Response.GetResponseStream())).ReadToEnd();
				}
				if (end == "")
				{
					end = webException.Message;
					if (webException.InnerException != null)
					{
						end = string.Concat(end, ":\r\n", webException.InnerException.Message);
					}
				}
				base.Errored(workflowInstance, end, webException);
			}
			catch (Exception exception)
			{
				base.Errored(workflowInstance, exception);
			}
		}

		public static string SendSOAPRequest(string xmlStr, string url, Dictionary<string, string> parameters, WebServiceOperation operation, bool authentication, string userName, string password, bool responseNotAvailable)
		{
			XmlDocument xmlDocument = new XmlDocument();
			if (parameters != null)
			{
				string.Join(string.Empty, (
					from kv in parameters
					select string.Format("<{0}>{1}</{0}>", kv.Key, kv.Value)).ToArray<string>());
			}
			xmlDocument.LoadXml(xmlStr);
			HttpWebRequest networkCredential = (HttpWebRequest)WebRequest.Create(url);
			networkCredential.Headers.Add("SOAPAction", operation.Action ?? url);
			networkCredential.ContentType = (operation.UseSoap12 ? "application/soap+xml;charset=\"utf-8\"" : "text/xml;charset=\"utf-8\"");
			networkCredential.Accept = (operation.UseSoap12 ? "application/soap+xml" : "text/xml");
			networkCredential.Method = "POST";
			if (authentication)
			{
				if (!operation.PassAuthenticationInSoapHeader)
				{
					networkCredential.Credentials = new NetworkCredential(userName, password);
				}
				else
				{
					WebServiceSender.AddSecurityCredentialsToSoapMessage(xmlDocument, userName, password);
				}
			}
			using (Stream requestStream = networkCredential.GetRequestStream())
			{
				xmlDocument.Save(requestStream);
			}
			string end = null;
			using (WebResponse response = networkCredential.GetResponse())
			{
				if (!responseNotAvailable)
				{
					using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
					{
						end = streamReader.ReadToEnd();
					}
				}
			}
			return end;
		}
	}
}