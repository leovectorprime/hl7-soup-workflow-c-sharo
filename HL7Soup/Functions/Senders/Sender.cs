using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Activities;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HL7Soup.Functions.Senders
{
	public abstract class Sender : Activity, ISender, IFunctionProvider
	{
		protected Sender()
		{
		}

		protected abstract void FunctionSend(WorkflowInstance workflowInstance);

		public void Send(WorkflowInstance workflowInstance)
		{
			try
			{
				this.FunctionSend(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				base.Errored(workflowInstance, string.Concat("Error sending.", exception.Message), exception);
			}
		}
	}
}