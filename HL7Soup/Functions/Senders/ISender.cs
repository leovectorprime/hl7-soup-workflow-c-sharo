using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Senders
{
	public interface ISender : IFunctionProvider
	{
		void Send(WorkflowInstance workflowInstance);
	}
}