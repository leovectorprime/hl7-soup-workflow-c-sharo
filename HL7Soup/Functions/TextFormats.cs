using System;

namespace HL7Soup.Functions
{
	public enum TextFormats
	{
		None,
		LowerCase,
		UpperCase,
		TitleCase,
		McNameCase
	}
}