using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;

namespace HL7Soup.Functions.Transformers
{
	public class Default : Transformer
	{
		private Stack<bool> ConditionResults = new Stack<bool>();

		private int i;

		public Default()
		{
		}

		private static void FindLocationOfNext(int i, ForEachTransformerAction forEachTransformerAction, ITransformerSetting Setting)
		{
			int num = 0;
			if (i + 1 < Setting.Transformers.Count)
			{
				for (int i1 = i + 1; i1 < Setting.Transformers.Count; i1++)
				{
					if (Setting.Transformers[i1] is ForEachTransformerAction)
					{
						num++;
					}
					if (Setting.Transformers[i1] is NextTransformerAction)
					{
						if (num != 0)
						{
							num--;
						}
						else
						{
							forEachTransformerAction.NextLocation = i1;
							((NextTransformerAction)Setting.Transformers[i1]).ForEachLocation = i;
							break;
						}
					}
				}
			}
			if (forEachTransformerAction.NextLocation == 0)
			{
				throw new Exception("Could not find the corresponding 'Next' transformer to the ForEach transformer.");
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			Default.FunctionPrepareShared(workflowInstance, base.Setting);
		}

		public static void FunctionPrepareShared(WorkflowInstance workflowInstance, ITransformerSetting setting)
		{
			if (setting.Transformers != null)
			{
				foreach (ITransformerAction transformer in setting.Transformers)
				{
					transformer.Prepare(workflowInstance);
				}
			}
		}

		protected override void FunctionTransform(WorkflowInstance workflowInstance)
		{
			Default.FunctionTransform(workflowInstance, base.Setting, this.i, this.ConditionResults);
		}

		public static void FunctionTransform(WorkflowInstance workflowInstance, ITransformerSetting Setting, int i, Stack<bool> ConditionResults)
		{
			if (Setting.Transformers != null)
			{
				foreach (ITransformerAction transformer in Setting.Transformers)
				{
					transformer.PrepareItteration(workflowInstance);
				}
				i = 0;
				while (i < Setting.Transformers.Count)
				{
					ITransformerAction item = Setting.Transformers[i];
					if (item is EndConditionalTransformerAction)
					{
						if (ConditionResults.Count > 0)
						{
							ConditionResults.Pop();
						}
					}
					else if (ConditionResults.Count == 0 || ConditionResults.Peek())
					{
						item.Process(workflowInstance);
						if (item is BeginConditionalTransformerAction)
						{
							ConditionResults.Push(((BeginConditionalTransformerAction)item).ConditionResult);
						}
						else if (item is NextTransformerAction)
						{
							if (((NextTransformerAction)item).ForEachLocation == -1)
							{
								throw new Exception("Additional 'Next' transformer found without a corresponding 'For Each'");
							}
							i = ((NextTransformerAction)item).ForEachLocation - 1;
						}
						else if (item is ForEachTransformerAction)
						{
							ForEachTransformerAction forEachTransformerAction = item as ForEachTransformerAction;
							if (forEachTransformerAction.NextLocation == 0)
							{
								Default.FindLocationOfNext(i, forEachTransformerAction, Setting);
							}
							if (forEachTransformerAction.Iterator <= -1)
							{
								if (forEachTransformerAction.NextLocation + 1 >= Setting.Transformers.Count)
								{
									break;
								}
								i = forEachTransformerAction.NextLocation;
							}
						}
					}
					else if (item is BeginConditionalTransformerAction)
					{
						ConditionResults.Push(false);
					}
					i++;
				}
			}
		}

		public string GetTransformerThatErrored()
		{
			ITransformerAction item = base.Setting.Transformers[this.i];
			if (item == null)
			{
				return string.Concat("Unknown Transformer at position ", this.i + 1);
			}
			return string.Concat(new object[] { "'", item.Description, "' at position ", this.i + 1 });
		}
	}
}