using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Transformers
{
	public interface ITransformer : IFunctionProvider
	{
		bool Transform(WorkflowInstance workflowInstance);
	}
}