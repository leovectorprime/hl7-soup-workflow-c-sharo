using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HL7Soup.Functions.Transformers
{
	public abstract class Transformer : FunctionProvider, ITransformer, IFunctionProvider
	{
		public ITransformerSetting Setting
		{
			get
			{
				return (ITransformerSetting)this.baseSetting;
			}
		}

		public Transformer()
		{
		}

		protected abstract void FunctionTransform(WorkflowInstance workflowInstance);

		public bool Transform(WorkflowInstance workflowInstance)
		{
			bool flag;
			try
			{
				this.FunctionTransform(workflowInstance);
				base.Status = FunctionProviderStatuses.Processing;
				if (Thread.CurrentThread.Name != "QueueThreadWorker")
				{
					base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
				}
				flag = true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				string str = "";
				if (this is Default)
				{
					str = string.Concat("(", ((Default)this).GetTransformerThatErrored(), ")");
				}
				base.Errored(workflowInstance, string.Concat("Transformer Error ", str, ":  ", exception.Message), exception);
				flag = false;
			}
			return flag;
		}
	}
}