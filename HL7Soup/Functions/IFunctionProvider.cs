using HL7Soup;
using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Functions
{
	public interface IFunctionProvider
	{
		string ErrorMessage
		{
			get;
			set;
		}

		FunctionProviderStatuses Status
		{
			get;
			set;
		}

		void AllFunctionsSucceded(WorkflowInstance workflowInstance);

		void Cancel();

		void Close();

		void Prepare(ISetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance);

		void RollBack(WorkflowInstance workflowInstance);
	}
}