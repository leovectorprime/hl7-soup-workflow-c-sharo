using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Extensions;
using HL7Soup.Functions.Activities;
using HL7Soup.Functions.EditorSendingBehaviours;
using HL7Soup.Functions.Filters;
using HL7Soup.Functions.Receivers;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Activities;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.Settings.Filters;
using HL7Soup.Functions.Settings.MessageTypes;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Functions.Settings.SourceProviders;
using HL7Soup.Functions.SourceProviders;
using HL7Soup.Functions.Transformers;
using HL7Soup.Integrations;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace HL7Soup.Functions
{
	public static class FunctionHelpers
	{
		public static string activityTypeName
		{
			get;
			set;
		}

		public static List<ISetting> CreateNewSettingInstance(List<Type> settingType)
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Type type in settingType)
			{
				object obj = Activator.CreateInstance(type);
				ISetting setting = obj as ISetting;
				if (setting == null)
				{
					ICustomActivity customActivity = obj as CustomActivity;
					if (customActivity == null)
					{
						continue;
					}
					CustomActivityContainerSetting customActivityContainerSetting = new CustomActivityContainerSetting()
					{
						CustomActivityTypeName = type.AssemblyQualifiedName
					};
					string str = "";
					DisplayNameAttribute displayNameAttribute = customActivity.GetType().GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault<object>() as DisplayNameAttribute;
					str = (displayNameAttribute != null ? displayNameAttribute.DisplayName : customActivity.GetType().Name);
					customActivityContainerSetting.CustomActivityName = str;
					settings.Add(customActivityContainerSetting);
				}
				else
				{
					settings.Add(setting);
				}
			}
			return settings;
		}

		public static MessageTypes DeterminMessageType(string message)
		{
			if (message.TrimStart(Array.Empty<char>()).Length > 3 && message.TrimStart(Array.Empty<char>()).Substring(0, 3).ToUpper().StartsWith("MSH"))
			{
				return MessageTypes.HL7V2;
			}
			if (message.TrimStart(Array.Empty<char>()).Length > 2 && message.TrimStart(Array.Empty<char>()).Substring(0, 1).StartsWith("<") && message.TrimStart(Array.Empty<char>()).Substring(1, 1) != "%")
			{
				return MessageTypes.XML;
			}
			if (message.TrimStart(Array.Empty<char>()).StartsWith("{") && message.TrimEnd(Array.Empty<char>()).EndsWith("}"))
			{
				return MessageTypes.JSON;
			}
			if (message.TrimStart(Array.Empty<char>()).Length > 1 && message.Contains(","))
			{
				return MessageTypes.CSV;
			}
			return MessageTypes.Unknown;
		}

		private static IActivity GetActivityInstance(string activityTypeName)
		{
			if (activityTypeName == null)
			{
				throw new ArgumentNullException("activityTypeName", "The activityTypeName cannot be null");
			}
			return FunctionHelpers.GetActivityInstance(Type.GetType(activityTypeName));
		}

		private static IActivity GetActivityInstance(Type activityType)
		{
			IActivity activity = Activator.CreateInstance(activityType) as IActivity;
			if (activity == null)
			{
				throw new Exception(string.Concat("Creating instance of Activity returned null. ", activityType.FullName));
			}
			return activity;
		}

		public static IActivity GetActivityInstance(IActivitySetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (IActivity)manager.DictionaryOfFunctions[setting.Id];
			}
			IActivity activityInstance = FunctionHelpers.GetActivityInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = activityInstance;
			activityInstance.Prepare(setting, manager, workflowInstance);
			return activityInstance;
		}

		public static IActivitySetting GetActivitySettingById(Guid activityId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(activityId))
			{
				return (IActivitySetting)host.DictionaryOfSettings[activityId];
			}
			IActivitySetting activitySettingById = FunctionHelpers.GetActivitySettingById(activityId);
			host.DictionaryOfSettings[activityId] = activitySettingById;
			return activitySettingById;
		}

		public static IActivitySetting GetActivitySettingById(Guid id)
		{
			IActivitySetting activitySetting;
			using (IEnumerator<IActivitySetting> enumerator = SystemSettings.Instance.FunctionSettings.ActivitySettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IActivitySetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					activitySetting = current;
					return activitySetting;
				}
				return null;
			}
			return activitySetting;
		}

		public static IAutomaticSendSetting GetAutomaticSendSettingById(Guid id)
		{
			IAutomaticSendSetting automaticSendSetting;
			using (IEnumerator<IAutomaticSendSetting> enumerator = SystemSettings.Instance.FunctionSettings.AutomaticSenderSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IAutomaticSendSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					automaticSendSetting = current;
					return automaticSendSetting;
				}
				return null;
			}
			return automaticSendSetting;
		}

		public static IClientResponseDestinationSetting GetClientResponseDestinationSettingById(Guid id)
		{
			IClientResponseDestinationSetting clientResponseDestinationSetting;
			using (IEnumerator<IClientResponseDestinationSetting> enumerator = SystemSettings.Instance.FunctionSettings.ClientResponseDestinationSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IClientResponseDestinationSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					clientResponseDestinationSetting = current;
					return clientResponseDestinationSetting;
				}
				return null;
			}
			return clientResponseDestinationSetting;
		}

		public static List<ISetting> GetDefaultSettingsAvailableToThisSettingType(SettingTypes settingType)
		{
			List<ISetting> settings = new List<ISetting>();
			switch (settingType)
			{
				case SettingTypes.Activity:
				{
					return new List<ISetting>();
				}
				case SettingTypes.Sender:
				{
					return new List<ISetting>();
				}
				case SettingTypes.Receiver:
				{
					return new List<ISetting>();
				}
				case SettingTypes.Filter:
				{
					return new List<ISetting>();
				}
				case SettingTypes.Transformer:
				{
					return new List<ISetting>();
				}
				case SettingTypes.MessageType:
				{
					throw new Exception("Depricated");
				}
				case SettingTypes.EditorSendingBehaviour:
				{
					return new List<ISetting>();
				}
				case SettingTypes.AutomaticSender:
				{
					AutomaticSendSetting automaticSendSetting = new AutomaticSendSetting()
					{
						Id = Guid.NewGuid(),
						Delay = new TimeSpan(0, 0, 1),
						InfinateLoop = false,
						MoveNext = true,
						Name = automaticSendSetting.Details
					};
					settings.Add(automaticSendSetting);
					automaticSendSetting = new AutomaticSendSetting()
					{
						Id = Guid.NewGuid(),
						Delay = new TimeSpan(0, 0, 0),
						InfinateLoop = false,
						MoveNext = true,
						Name = automaticSendSetting.Details
					};
					settings.Add(automaticSendSetting);
					return settings;
				}
				case SettingTypes.SourceProvider:
				{
					ISetting currentTabSourceProviderSetting = new CurrentTabSourceProviderSetting()
					{
						Id = Guid.NewGuid(),
						Name = "Current Tab",
						StartLocation = StartLocation.CurrentItem,
						ReverseOrder = false
					};
					currentTabSourceProviderSetting.Name = currentTabSourceProviderSetting.Details;
					settings.Add(currentTabSourceProviderSetting);
					currentTabSourceProviderSetting = new DirectoryScanSourceProviderSetting()
					{
						Id = Guid.NewGuid(),
						Name = "Directory Scanner",
						DirectoryFilter = "*.hl7",
						DirectoryPath = "c:\\",
						EndAfterProcessing = true,
						SearchForNewFiles = false
					};
					currentTabSourceProviderSetting.Name = currentTabSourceProviderSetting.Details;
					settings.Add(currentTabSourceProviderSetting);
					return settings;
				}
				case SettingTypes.ClientResponseDestination:
				{
					return new List<ISetting>();
				}
			}
			throw new Exception("UI not ready to work with the new setting type.");
		}

		private static IEditorSendingBehaviour GetEditorSendingBehaviourInstance(string editorSendingBehaviourTypeName)
		{
			return FunctionHelpers.GetEditorSendingBehaviourInstance(Type.GetType(editorSendingBehaviourTypeName));
		}

		private static IEditorSendingBehaviour GetEditorSendingBehaviourInstance(Type editorSendingBehaviourType)
		{
			IEditorSendingBehaviour editorSendingBehaviour = Activator.CreateInstance(editorSendingBehaviourType) as IEditorSendingBehaviour;
			if (editorSendingBehaviour == null)
			{
				throw new Exception(string.Concat("Creating instance of Editor Sending Behaviour returned null. ", editorSendingBehaviourType.FullName));
			}
			return editorSendingBehaviour;
		}

		public static IEditorSendingBehaviour GetEditorSendingBehaviourInstance(IEditorSendingBehaviourSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (IEditorSendingBehaviour)manager.DictionaryOfFunctions[setting.Id];
			}
			IEditorSendingBehaviour editorSendingBehaviourInstance = FunctionHelpers.GetEditorSendingBehaviourInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = editorSendingBehaviourInstance;
			editorSendingBehaviourInstance.Prepare(setting, manager, workflowInstance);
			return editorSendingBehaviourInstance;
		}

		public static IEditorSendingBehaviourSetting GetEditorSendingBehaviourSettingById(Guid editorSendingBehaviourId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(editorSendingBehaviourId))
			{
				return (IEditorSendingBehaviourSetting)host.DictionaryOfSettings[editorSendingBehaviourId];
			}
			IEditorSendingBehaviourSetting editorSendingBehaviourSettingById = FunctionHelpers.GetEditorSendingBehaviourSettingById(editorSendingBehaviourId);
			host.DictionaryOfSettings[editorSendingBehaviourId] = editorSendingBehaviourSettingById;
			return editorSendingBehaviourSettingById;
		}

		public static IEditorSendingBehaviourSetting GetEditorSendingBehaviourSettingById(Guid id)
		{
			IEditorSendingBehaviourSetting editorSendingBehaviourSetting;
			using (IEnumerator<IEditorSendingBehaviourSetting> enumerator = SystemSettings.Instance.FunctionSettings.EditorSendingBehaviourSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IEditorSendingBehaviourSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					editorSendingBehaviourSetting = current;
					return editorSendingBehaviourSetting;
				}
				return null;
			}
			return editorSendingBehaviourSetting;
		}

		private static HL7Soup.Functions.Filters.IFilter GetFilterInstance(string filterTypeName)
		{
			return FunctionHelpers.GetFilterInstance(Type.GetType(filterTypeName));
		}

		private static HL7Soup.Functions.Filters.IFilter GetFilterInstance(Type filterType)
		{
			HL7Soup.Functions.Filters.IFilter filter = Activator.CreateInstance(filterType) as HL7Soup.Functions.Filters.IFilter;
			if (filter == null)
			{
				throw new Exception(string.Concat("Creating instance of Filter returned null. ", filterType.FullName));
			}
			return filter;
		}

		public static HL7Soup.Functions.Filters.IFilter GetFilterInstance(IFilterSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (HL7Soup.Functions.Filters.IFilter)manager.DictionaryOfFunctions[setting.Id];
			}
			HL7Soup.Functions.Filters.IFilter filterInstance = FunctionHelpers.GetFilterInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = filterInstance;
			filterInstance.Prepare(setting, manager, workflowInstance);
			return filterInstance;
		}

		public static IFilterSetting GetFilterSettingById(Guid filterId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(filterId))
			{
				return (IFilterSetting)host.DictionaryOfSettings[filterId];
			}
			IFilterSetting filterSettingById = FunctionHelpers.GetFilterSettingById(filterId);
			host.DictionaryOfSettings[filterId] = filterSettingById;
			return filterSettingById;
		}

		public static IFilterSetting GetFilterSettingById(Guid id)
		{
			IFilterSetting filterSetting;
			using (IEnumerator<IFilterSetting> enumerator = SystemSettings.Instance.FunctionSettings.FilterSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IFilterSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					filterSetting = current;
					return filterSetting;
				}
				return null;
			}
			return filterSetting;
		}

		public static IMessage GetFunctionMessage(string data, MessageTypes messageType)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.TextWithVariables:
				case MessageTypes.HL7V2Path:
				case MessageTypes.XPath:
				case MessageTypes.CSVPath:
				case MessageTypes.JSONPath:
				{
					throw new NotImplementedException();
				}
				case MessageTypes.HL7V2:
				{
					return new HL7V2MessageType(data);
				}
				case MessageTypes.XML:
				{
					return new XMLMessage(data);
				}
				case MessageTypes.CSV:
				{
					return new CSVMessage(data);
				}
				case MessageTypes.SQL:
				{
					return new TextMessage(data);
				}
				case MessageTypes.JSON:
				{
					return new JSONMessage(data);
				}
				case MessageTypes.Text:
				{
					return new TextMessage(data);
				}
				case MessageTypes.Binary:
				{
					return new BinaryMessage(data);
				}
				default:
				{
					throw new NotImplementedException();
				}
			}
		}

		public static IMessage GetFunctionMessageDeterminedFromText(string message)
		{
			switch (FunctionHelpers.DeterminMessageType(message))
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				case MessageTypes.TextWithVariables:
				case MessageTypes.HL7V2Path:
				case MessageTypes.XPath:
				case MessageTypes.CSVPath:
				case MessageTypes.JSONPath:
				{
					throw new NotImplementedException();
				}
				case MessageTypes.HL7V2:
				{
					return new HL7V2MessageType(message);
				}
				case MessageTypes.XML:
				{
					return new XMLMessage(message);
				}
				case MessageTypes.CSV:
				{
					return new CSVMessage(message);
				}
				case MessageTypes.JSON:
				{
					return new JSONMessage(message);
				}
				case MessageTypes.Text:
				{
					return new TextMessage(message);
				}
				case MessageTypes.Binary:
				{
					return new BinaryMessage(message);
				}
				default:
				{
					throw new NotImplementedException();
				}
			}
		}

		internal static List<string> GetListOfVariablesInText(string value)
		{
			List<string> strs = new List<string>();
			if (value == null)
			{
				return strs;
			}
			for (int i = value.IndexOf("${"); i > -1; i = value.IndexOf("${", i + 1))
			{
				int num = value.IndexOf("}", i);
				if (num > i)
				{
					strs.Add(value.Substring(i + 2, num - i - 2));
				}
			}
			return strs;
		}

		public static string GetMessage(ISetting setting, MessageSourceDirection direction)
		{
			string responseMessageTemplate = "";
			if (direction != MessageSourceDirection.outbound)
			{
				IReceiverSetting receiverSetting = setting as IReceiverSetting;
				if (receiverSetting == null)
				{
					ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
					if (senderWithResponseSetting != null)
					{
						responseMessageTemplate = senderWithResponseSetting.ResponseMessageTemplate;
					}
				}
				else
				{
					responseMessageTemplate = receiverSetting.ReceivedMessageTemplate;
				}
			}
			else
			{
				IReceiverWithResponseSetting receiverWithResponseSetting = setting as IReceiverWithResponseSetting;
				if (receiverWithResponseSetting == null)
				{
					ISenderSetting senderSetting = setting as ISenderSetting;
					if (senderSetting != null)
					{
						responseMessageTemplate = senderSetting.MessageTemplate;
					}
				}
				else
				{
					responseMessageTemplate = receiverWithResponseSetting.ResponseMessageTemplate;
				}
			}
			return responseMessageTemplate;
		}

		public static IMessageTypeSetting GetMessageTypeSettingById(Guid id)
		{
			IMessageTypeSetting messageTypeSetting;
			using (IEnumerator<IMessageTypeSetting> enumerator = SystemSettings.Instance.FunctionSettings.MessageTypeSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IMessageTypeSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					messageTypeSetting = current;
					return messageTypeSetting;
				}
				return null;
			}
			return messageTypeSetting;
		}

		private static IReceiver GetReceiverInstance(string receiverTypeName)
		{
			return FunctionHelpers.GetReceiverInstance(Type.GetType(receiverTypeName));
		}

		private static IReceiver GetReceiverInstance(Type receiverType)
		{
			IReceiver receiver = Activator.CreateInstance(receiverType) as IReceiver;
			if (receiver == null)
			{
				throw new Exception(string.Concat("Creating instance of Receiver returned null. ", receiverType.FullName));
			}
			return receiver;
		}

		public static IReceiver GetReceiverInstance(IReceiverSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			return FunctionHelpers.GetReceiverInstance(setting, manager, workflowInstance, false);
		}

		public static IReceiver GetReceiverInstance(IReceiverSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance, bool skipPreperation)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (IReceiver)manager.DictionaryOfFunctions[setting.Id];
			}
			IReceiver receiverInstance = FunctionHelpers.GetReceiverInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = receiverInstance;
			if (!skipPreperation)
			{
				receiverInstance.Prepare(setting, manager, workflowInstance);
			}
			return receiverInstance;
		}

		public static IReceiverSetting GetReceiverSettingById(Guid receiverId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(receiverId))
			{
				return (IReceiverSetting)host.DictionaryOfSettings[receiverId];
			}
			IReceiverSetting receiverSettingById = FunctionHelpers.GetReceiverSettingById(receiverId);
			host.DictionaryOfSettings[receiverId] = receiverSettingById;
			return receiverSettingById;
		}

		public static IReceiverSetting GetReceiverSettingById(Guid id)
		{
			IReceiverSetting receiverSetting;
			using (IEnumerator<IReceiverSetting> enumerator = SystemSettings.Instance.FunctionSettings.ReceiverSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IReceiverSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					receiverSetting = current;
					return receiverSetting;
				}
				return null;
			}
			return receiverSetting;
		}

		private static ISender GetSenderInstance(string senderTypeName)
		{
			return FunctionHelpers.GetSenderInstance(Type.GetType(senderTypeName));
		}

		private static ISender GetSenderInstance(Type senderType)
		{
			ISender sender = Activator.CreateInstance(senderType) as ISender;
			if (sender == null)
			{
				throw new Exception(string.Concat("Creating instance of Sender returned null. ", senderType.FullName));
			}
			return sender;
		}

		public static ISender GetSenderInstance(ISenderSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (ISender)manager.DictionaryOfFunctions[setting.Id];
			}
			ISender senderInstance = FunctionHelpers.GetSenderInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = senderInstance;
			senderInstance.Prepare(setting, manager, workflowInstance);
			return senderInstance;
		}

		public static ISenderSetting GetSenderSettingById(Guid senderId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(senderId))
			{
				return (ISenderSetting)host.DictionaryOfSettings[senderId];
			}
			ISenderSetting senderSettingById = FunctionHelpers.GetSenderSettingById(senderId);
			host.DictionaryOfSettings[senderId] = senderSettingById;
			return senderSettingById;
		}

		public static ISenderSetting GetSenderSettingById(Guid id)
		{
			ISenderSetting senderSetting;
			using (IEnumerator<ISenderSetting> enumerator = SystemSettings.Instance.FunctionSettings.SenderSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ISenderSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					senderSetting = current;
					return senderSetting;
				}
				return null;
			}
			return senderSetting;
		}

		public static ISetting GetSettingById(Guid id)
		{
			ISetting receiverSettingById = null;
			receiverSettingById = FunctionHelpers.GetReceiverSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetSenderSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetActivitySettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetFilterSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetTransformerSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetEditorSendingBehaviourSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetClientResponseDestinationSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetSourceProviderSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetMessageTypeSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			receiverSettingById = FunctionHelpers.GetAutomaticSendSettingById(id);
			if (receiverSettingById != null)
			{
				return receiverSettingById;
			}
			return null;
		}

		public static IList GetSettingsCollection(SettingTypes connectionSettingType)
		{
			switch (connectionSettingType)
			{
				case SettingTypes.Activity:
				{
					return SystemSettings.Instance.FunctionSettings.ActivitySettings;
				}
				case SettingTypes.Sender:
				{
					return SystemSettings.Instance.FunctionSettings.ActivitySettings;
				}
				case SettingTypes.Receiver:
				{
					return SystemSettings.Instance.FunctionSettings.ReceiverSettings;
				}
				case SettingTypes.Filter:
				{
					return SystemSettings.Instance.FunctionSettings.FilterSettings;
				}
				case SettingTypes.Transformer:
				{
					return SystemSettings.Instance.FunctionSettings.TransformerSettings;
				}
				case SettingTypes.MessageType:
				{
					return SystemSettings.Instance.FunctionSettings.MessageTypeSettings;
				}
				case SettingTypes.EditorSendingBehaviour:
				{
					return SystemSettings.Instance.FunctionSettings.EditorSendingBehaviourSettings;
				}
				case SettingTypes.AutomaticSender:
				{
					return SystemSettings.Instance.FunctionSettings.AutomaticSenderSettings;
				}
				case SettingTypes.SourceProvider:
				{
					return SystemSettings.Instance.FunctionSettings.SourceProviderSettings;
				}
				case SettingTypes.ClientResponseDestination:
				{
					return SystemSettings.Instance.FunctionSettings.ClientResponseDestinationSettings;
				}
			}
			throw new Exception("UI not ready to work with the new setting type.");
		}

		public static IList GetSettingsCollection(ISetting setting)
		{
			if (typeof(IEditorSendingBehaviourSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.EditorSendingBehaviourSettings;
			}
			if (typeof(IAutomaticSendSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.AutomaticSenderSettings;
			}
			if (typeof(ISourceProviderSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.SourceProviderSettings;
			}
			if (typeof(IActivitySetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.ActivitySettings;
			}
			if (typeof(IClientResponseDestinationSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.ClientResponseDestinationSettings;
			}
			if (typeof(IFilterSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.FilterSettings;
			}
			if (typeof(ITransformerSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.TransformerSettings;
			}
			if (typeof(IMessageTypeSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.MessageTypeSettings;
			}
			if (typeof(ISenderSetting).IsAssignableFrom(setting.GetType()))
			{
				return SystemSettings.Instance.FunctionSettings.SenderSettings;
			}
			if (!typeof(IReceiverSetting).IsAssignableFrom(setting.GetType()))
			{
				throw new Exception("UI not ready to work with the new setting type.");
			}
			return SystemSettings.Instance.FunctionSettings.ReceiverSettings;
		}

		public static List<Type> GetSettingType(SettingTypes connectionSettingType)
		{
			switch (connectionSettingType)
			{
				case SettingTypes.Activity:
				{
					List<Type> types = new List<Type>()
					{
						typeof(MLLPSenderSetting),
						typeof(DatabaseSenderSetting),
						typeof(FileWriterSenderSetting),
						typeof(HttpSenderSetting),
						typeof(WebServiceSenderSetting),
						typeof(CodeSenderSetting)
					};
					types.AddRange(SystemSettings.Instance.GetCustomActivityTypes());
					return types;
				}
				case SettingTypes.Sender:
				{
					return new List<Type>()
					{
						typeof(MLLPSenderSetting),
						typeof(DatabaseSenderSetting),
						typeof(FileWriterSenderSetting),
						typeof(HttpSenderSetting),
						typeof(WebServiceSenderSetting),
						typeof(CodeSenderSetting)
					};
				}
				case SettingTypes.Receiver:
				{
					return new List<Type>()
					{
						typeof(MLLPReceiverSetting),
						typeof(DatabaseReceiverSetting),
						typeof(DirectoryScanReceiverSetting),
						typeof(HttpReceiverSetting),
						typeof(WebServiceReceiverSetting)
					};
				}
				case SettingTypes.Filter:
				{
					return new List<Type>()
					{
						typeof(FilterHostSetting)
					};
				}
				case SettingTypes.Transformer:
				{
					return new List<Type>()
					{
						typeof(TransformerSetting)
					};
				}
				case SettingTypes.MessageType:
				{
					return new List<Type>()
					{
						typeof(HL7V2MessageTypeSetting),
						typeof(DynamicMessageTypeSetting)
					};
				}
				case SettingTypes.EditorSendingBehaviour:
				{
					return new List<Type>()
					{
						typeof(HL7V2MLLPEditorSendingBehaviourSetting),
						typeof(HttpEditorSetting),
						typeof(WebServiceEditorSetting)
					};
				}
				case SettingTypes.AutomaticSender:
				{
					return new List<Type>()
					{
						typeof(AutomaticSendSetting)
					};
				}
				case SettingTypes.SourceProvider:
				{
					return new List<Type>()
					{
						typeof(CurrentTabSourceProviderSetting),
						typeof(DirectoryScanSourceProviderSetting)
					};
				}
			}
			throw new Exception("UI not ready to work with the new setting type.");
		}

		public static SettingTypes GetSettingType(ISetting setting)
		{
			if (typeof(IEditorSendingBehaviourSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.EditorSendingBehaviour;
			}
			if (typeof(IAutomaticSendSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.AutomaticSender;
			}
			if (typeof(ISourceProviderSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.SourceProvider;
			}
			if (typeof(IClientResponseDestinationSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.ClientResponseDestination;
			}
			if (typeof(IFilterSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.Filter;
			}
			if (typeof(ITransformerSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.Transformer;
			}
			if (typeof(IMessageTypeSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.MessageType;
			}
			if (typeof(ISenderSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.Sender;
			}
			if (typeof(IReceiverSetting).IsAssignableFrom(setting.GetType()))
			{
				return SettingTypes.Receiver;
			}
			if (!typeof(IActivitySetting).IsAssignableFrom(setting.GetType()))
			{
				throw new Exception("UI not ready to work with the new setting type.");
			}
			return SettingTypes.Activity;
		}

		private static ISourceProvider GetSourceProviderInstance(string sourceProviderTypeName)
		{
			return FunctionHelpers.GetSourceProviderInstance(Type.GetType(sourceProviderTypeName));
		}

		private static ISourceProvider GetSourceProviderInstance(Type sourceProviderType)
		{
			ISourceProvider sourceProvider = Activator.CreateInstance(sourceProviderType) as ISourceProvider;
			if (sourceProvider == null)
			{
				throw new Exception(string.Concat("Creating instance of source provider returned null. ", sourceProviderType.FullName));
			}
			return sourceProvider;
		}

		public static ISourceProvider GetSourceProviderInstance(ISourceProviderSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (ISourceProvider)manager.DictionaryOfFunctions[setting.Id];
			}
			ISourceProvider sourceProviderInstance = FunctionHelpers.GetSourceProviderInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = sourceProviderInstance;
			sourceProviderInstance.Prepare(setting, manager, workflowInstance);
			return sourceProviderInstance;
		}

		public static ISourceProviderSetting GetSourceProviderSettingById(Guid sourceProviderId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(sourceProviderId))
			{
				return (ISourceProviderSetting)host.DictionaryOfSettings[sourceProviderId];
			}
			ISourceProviderSetting sourceProviderSettingById = FunctionHelpers.GetSourceProviderSettingById(sourceProviderId);
			host.DictionaryOfSettings[sourceProviderId] = sourceProviderSettingById;
			return sourceProviderSettingById;
		}

		public static ISourceProviderSetting GetSourceProviderSettingById(Guid id)
		{
			ISourceProviderSetting sourceProviderSetting;
			using (IEnumerator<ISourceProviderSetting> enumerator = SystemSettings.Instance.FunctionSettings.SourceProviderSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ISourceProviderSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					sourceProviderSetting = current;
					return sourceProviderSetting;
				}
				return null;
			}
			return sourceProviderSetting;
		}

		private static ITransformer GetTransformerInstance(string transformerTypeName)
		{
			return FunctionHelpers.GetTransformerInstance(Type.GetType(transformerTypeName));
		}

		private static ITransformer GetTransformerInstance(Type transformerType)
		{
			ITransformer transformer = Activator.CreateInstance(transformerType) as ITransformer;
			if (transformer == null)
			{
				throw new Exception(string.Concat("Creating instance of Transformer returned null. ", transformerType.FullName));
			}
			return transformer;
		}

		public static ITransformer GetTransformerInstance(ITransformerSetting setting, ISendReceiveManager manager, WorkflowInstance workflowInstance)
		{
			if (manager.DictionaryOfFunctions.ContainsKey(setting.Id))
			{
				return (ITransformer)manager.DictionaryOfFunctions[setting.Id];
			}
			ITransformer transformerInstance = FunctionHelpers.GetTransformerInstance(setting.FunctionProviderType);
			manager.DictionaryOfFunctions[setting.Id] = transformerInstance;
			transformerInstance.Prepare(setting, manager, workflowInstance);
			return transformerInstance;
		}

		public static ITransformerSetting GetTransformerSettingById(Guid transformerId, ISendReceiveManager host)
		{
			if (host.DictionaryOfSettings.ContainsKey(transformerId))
			{
				return (ITransformerSetting)host.DictionaryOfSettings[transformerId];
			}
			ITransformerSetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(transformerId);
			host.DictionaryOfSettings[transformerId] = transformerSettingById;
			return transformerSettingById;
		}

		public static ITransformerSetting GetTransformerSettingById(Guid id)
		{
			ITransformerSetting transformerSetting;
			foreach (ITransformerSetting transformerSetting1 in SystemSettings.Instance.FunctionSettings.TransformerSettings)
			{
				if (transformerSetting1.Id != id)
				{
					continue;
				}
				transformerSetting = transformerSetting1;
				return transformerSetting;
			}
			using (IEnumerator<IFilterSetting> enumerator = SystemSettings.Instance.FunctionSettings.FilterSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IFilterSetting current = enumerator.Current;
					if (current.Id != id)
					{
						continue;
					}
					transformerSetting = current;
					return transformerSetting;
				}
				return null;
			}
			return transformerSetting;
		}

		public static void PopulateDataProviderComboBox(ComboBox comboBox)
		{
			comboBox.Items.Clear();
			using (DataTable factoryClasses = DbProviderFactories.GetFactoryClasses())
			{
				foreach (DataRow row in factoryClasses.Rows)
				{
					if (row["Name"] == null)
					{
						continue;
					}
					if (row["Name"].ToString().Contains("Sql"))
					{
						comboBox.Items.Add("Sql Client");
					}
					else if (row["Name"].ToString().Contains("Oracle"))
					{
						comboBox.Items.Add("Oracle");
					}
					else if (!row["Name"].ToString().Contains("OleDb"))
					{
						if (!row["Name"].ToString().Contains("Odbc"))
						{
							continue;
						}
						comboBox.Items.Add("Odbc");
					}
					else
					{
						comboBox.Items.Add("OleDb");
					}
				}
			}
		}

		public static void PopulateMessageTypeComboBox(ComboBox comboBox)
		{
			FunctionHelpers.PopulateMessageTypeComboBox(comboBox, null);
		}

		public static void PopulateMessageTypeComboBox(ComboBox comboBox, MessageTypes messageType)
		{
			FunctionHelpers.PopulateMessageTypeComboBox(comboBox, new List<MessageTypes>()
			{
				messageType
			});
		}

		public static void PopulateMessageTypeComboBox(ComboBox comboBox, List<MessageTypes> messageTypes)
		{
			FunctionHelpers.PopulateMessageTypeComboBox(comboBox, messageTypes, MessageTypes.Unknown);
		}

		public static void PopulateMessageTypeComboBox(ComboBox comboBox, List<MessageTypes> messageTypes, MessageTypes selectedMessageType)
		{
			if (messageTypes == null)
			{
				messageTypes = new List<MessageTypes>()
				{
					MessageTypes.HL7V2,
					MessageTypes.XML,
					MessageTypes.CSV,
					MessageTypes.JSON,
					MessageTypes.Text,
					MessageTypes.Binary
				};
			}
			if (selectedMessageType == MessageTypes.Unknown)
			{
				selectedMessageType = messageTypes[0];
			}
			comboBox.Items.Clear();
			int num = 0;
			foreach (MessageTypes messageType in messageTypes)
			{
				switch (messageType)
				{
					case MessageTypes.HL7V2:
					{
						num = comboBox.Items.Add("HL7");
						if (selectedMessageType != MessageTypes.HL7V2)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.XML:
					{
						num = comboBox.Items.Add("XML");
						if (selectedMessageType != MessageTypes.XML)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.CSV:
					{
						num = comboBox.Items.Add("CSV");
						if (selectedMessageType != MessageTypes.CSV)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.SQL:
					{
						num = comboBox.Items.Add("SQL");
						if (selectedMessageType != MessageTypes.SQL)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.JSON:
					{
						num = comboBox.Items.Add("JSON");
						if (selectedMessageType != MessageTypes.JSON)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.Text:
					{
						num = comboBox.Items.Add("Text");
						if (selectedMessageType != MessageTypes.Text)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					case MessageTypes.Binary:
					{
						num = comboBox.Items.Add("Binary");
						if (selectedMessageType != MessageTypes.Binary)
						{
							continue;
						}
						comboBox.SelectedIndex = num;
						continue;
					}
					default:
					{
						continue;
					}
				}
			}
		}

		public static string RemoveVariablesFromMessage(string text, Dictionary<string, IVariableCreator> Variables, IActivityHost activityHost)
		{
			return FunctionHelpers.RemoveVariablesFromMessage(text, Variables, activityHost, new Dictionary<string, string>());
		}

		internal static string RemoveVariablesFromMessage(string text, Dictionary<string, IVariableCreator> Variables, IActivityHost activityHost, Dictionary<string, string> alreadyFoundVariables)
		{
			if (text == null)
			{
				return "";
			}
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			int num1 = text.IndexOf("${");
			while (num1 > -1)
			{
				int num2 = text.IndexOf("}", num1);
				if (num2 > num1)
				{
					stringBuilder.Append(text.Substring(num, num1 - num));
					string upper = text.Substring(num1 + 2, num2 - num1 - 2).ToUpper();
					if (!alreadyFoundVariables.ContainsKey(upper))
					{
						if (Variables.ContainsKey(upper))
						{
							stringBuilder.Append(Variables[upper].SampleVariableValue);
						}
						if (upper.Length >= Guid.Empty.ToString().Length)
						{
							Guid empty = Guid.Empty;
							if (Guid.TryParse(upper.Substring(0, Guid.Empty.ToString().Length), out empty))
							{
								Guid guid = Guid.Empty;
								MessageSourceDirection messageSourceDirection = MessageSourceDirection.variable;
								Enum.TryParse<MessageSourceDirection>(upper.Substring(guid.ToString().Length).Trim(), true, out messageSourceDirection);
								ISetting setting = activityHost.GetSetting(empty);
								if (setting != null)
								{
									string message = FunctionHelpers.GetMessage(setting, messageSourceDirection);
									alreadyFoundVariables[upper] = message;
									stringBuilder.Append(FunctionHelpers.RemoveVariablesFromMessage(message, Variables, activityHost, alreadyFoundVariables));
								}
							}
						}
					}
					else
					{
						stringBuilder.Append(alreadyFoundVariables[upper]);
					}
					num = num2 + 1;
				}
				num1 = (num2 <= -1 ? -1 : text.IndexOf("${", num2));
			}
			stringBuilder.Append(text.Substring(num));
			return stringBuilder.ToString();
		}

		public static Guid ReplaceActivity(Guid oldActivityId, Dictionary<Guid, Guid> replacementDictionary)
		{
			if (oldActivityId == Guid.Empty)
			{
				return oldActivityId;
			}
			if (replacementDictionary.ContainsKey(oldActivityId))
			{
				return replacementDictionary[oldActivityId];
			}
			Log.Instance.Warn(string.Concat("Cannot find activity with id ", oldActivityId, " so this binding will not work - though it probably already didn't"));
			return oldActivityId;
		}

		public static void SetDataProviderCombo(DataProviders dataProviders, ComboBox combo)
		{
			switch (dataProviders)
			{
				case DataProviders.SqlClient:
				{
					combo.Text = "Sql Client";
					return;
				}
				case DataProviders.OracleClient:
				{
					combo.Text = "Oracle";
					return;
				}
				case DataProviders.OleDb:
				{
					combo.Text = "OleDb";
					return;
				}
				case DataProviders.Odbc:
				{
					combo.Text = "Odbc";
					return;
				}
			}
			combo.Text = "Sql Client";
		}

		public static void SetMessageTypeCombo(MessageTypes messageType, ComboBox combo)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				{
					messageType = MessageTypes.HL7V2;
					combo.Text = "HL7";
					return;
				}
				case MessageTypes.HL7V2:
				{
					combo.Text = "HL7";
					return;
				}
				case MessageTypes.HL7V3:
				{
					combo.Text = "";
					return;
				}
				case MessageTypes.FHIR:
				{
					combo.Text = "";
					return;
				}
				case MessageTypes.XML:
				{
					combo.Text = "XML";
					return;
				}
				case MessageTypes.CSV:
				{
					combo.Text = "CSV";
					return;
				}
				case MessageTypes.SQL:
				{
					combo.Text = "SQL";
					return;
				}
				case MessageTypes.TextWithVariables:
				case MessageTypes.HL7V2Path:
				case MessageTypes.XPath:
				case MessageTypes.CSVPath:
				case MessageTypes.JSONPath:
				{
					return;
				}
				case MessageTypes.JSON:
				{
					combo.Text = "JSON";
					return;
				}
				case MessageTypes.Text:
				{
					combo.Text = "Text";
					return;
				}
				case MessageTypes.Binary:
				{
					combo.Text = "Binary";
					return;
				}
				default:
				{
					return;
				}
			}
		}
	}
}