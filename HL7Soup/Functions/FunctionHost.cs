using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions
{
	public class FunctionHost : IFunctionHost, IDisposable
	{
		public Dictionary<Guid, IFunctionProvider> DictionaryOfFunctions
		{
			get
			{
				return JustDecompileGenerated_get_DictionaryOfFunctions();
			}
			set
			{
				JustDecompileGenerated_set_DictionaryOfFunctions(value);
			}
		}

		private Dictionary<Guid, IFunctionProvider> JustDecompileGenerated_DictionaryOfFunctions_k__BackingField;

		public Dictionary<Guid, IFunctionProvider> JustDecompileGenerated_get_DictionaryOfFunctions()
		{
			return this.JustDecompileGenerated_DictionaryOfFunctions_k__BackingField;
		}

		private void JustDecompileGenerated_set_DictionaryOfFunctions(Dictionary<Guid, IFunctionProvider> value)
		{
			this.JustDecompileGenerated_DictionaryOfFunctions_k__BackingField = value;
		}

		public Dictionary<Guid, ISetting> DictionaryOfSettings
		{
			get
			{
				return JustDecompileGenerated_get_DictionaryOfSettings();
			}
			set
			{
				JustDecompileGenerated_set_DictionaryOfSettings(value);
			}
		}

		private Dictionary<Guid, ISetting> JustDecompileGenerated_DictionaryOfSettings_k__BackingField;

		public Dictionary<Guid, ISetting> JustDecompileGenerated_get_DictionaryOfSettings()
		{
			return this.JustDecompileGenerated_DictionaryOfSettings_k__BackingField;
		}

		private void JustDecompileGenerated_set_DictionaryOfSettings(Dictionary<Guid, ISetting> value)
		{
			this.JustDecompileGenerated_DictionaryOfSettings_k__BackingField = value;
		}

		public IFunctionProvider ErroredFunction
		{
			get
			{
				return JustDecompileGenerated_get_ErroredFunction();
			}
			set
			{
				JustDecompileGenerated_set_ErroredFunction(value);
			}
		}

		private IFunctionProvider JustDecompileGenerated_ErroredFunction_k__BackingField;

		public IFunctionProvider JustDecompileGenerated_get_ErroredFunction()
		{
			return this.JustDecompileGenerated_ErroredFunction_k__BackingField;
		}

		private void JustDecompileGenerated_set_ErroredFunction(IFunctionProvider value)
		{
			this.JustDecompileGenerated_ErroredFunction_k__BackingField = value;
		}

		public ISetting ErroredFunctionSetting
		{
			get
			{
				return JustDecompileGenerated_get_ErroredFunctionSetting();
			}
			set
			{
				JustDecompileGenerated_set_ErroredFunctionSetting(value);
			}
		}

		private ISetting JustDecompileGenerated_ErroredFunctionSetting_k__BackingField;

		public ISetting JustDecompileGenerated_get_ErroredFunctionSetting()
		{
			return this.JustDecompileGenerated_ErroredFunctionSetting_k__BackingField;
		}

		private void JustDecompileGenerated_set_ErroredFunctionSetting(ISetting value)
		{
			this.JustDecompileGenerated_ErroredFunctionSetting_k__BackingField = value;
		}

		public List<IFunctionProvider> FunctionsProcessedForCurrentMessage
		{
			get
			{
				return JustDecompileGenerated_get_FunctionsProcessedForCurrentMessage();
			}
			set
			{
				JustDecompileGenerated_set_FunctionsProcessedForCurrentMessage(value);
			}
		}

		private List<IFunctionProvider> JustDecompileGenerated_FunctionsProcessedForCurrentMessage_k__BackingField;

		public List<IFunctionProvider> JustDecompileGenerated_get_FunctionsProcessedForCurrentMessage()
		{
			return this.JustDecompileGenerated_FunctionsProcessedForCurrentMessage_k__BackingField;
		}

		protected void JustDecompileGenerated_set_FunctionsProcessedForCurrentMessage(List<IFunctionProvider> value)
		{
			this.JustDecompileGenerated_FunctionsProcessedForCurrentMessage_k__BackingField = value;
		}

		public bool hasErrored
		{
			get
			{
				return JustDecompileGenerated_get_hasErrored();
			}
			set
			{
				JustDecompileGenerated_set_hasErrored(value);
			}
		}

		private bool JustDecompileGenerated_hasErrored_k__BackingField;

		public bool JustDecompileGenerated_get_hasErrored()
		{
			return this.JustDecompileGenerated_hasErrored_k__BackingField;
		}

		private void JustDecompileGenerated_set_hasErrored(bool value)
		{
			this.JustDecompileGenerated_hasErrored_k__BackingField = value;
		}

		public bool hasWorkflowStarted
		{
			get
			{
				return JustDecompileGenerated_get_hasWorkflowStarted();
			}
			set
			{
				JustDecompileGenerated_set_hasWorkflowStarted(value);
			}
		}

		private bool JustDecompileGenerated_hasWorkflowStarted_k__BackingField;

		public bool JustDecompileGenerated_get_hasWorkflowStarted()
		{
			return this.JustDecompileGenerated_hasWorkflowStarted_k__BackingField;
		}

		private void JustDecompileGenerated_set_hasWorkflowStarted(bool value)
		{
			this.JustDecompileGenerated_hasWorkflowStarted_k__BackingField = value;
		}

		public FunctionHost()
		{
			this.DictionaryOfFunctions = new Dictionary<Guid, IFunctionProvider>();
			this.DictionaryOfSettings = new Dictionary<Guid, ISetting>();
		}

		protected void ClearDictionaries()
		{
			this.hasWorkflowStarted = false;
			this.hasErrored = false;
			this.ErroredFunction = null;
			this.ErroredFunctionSetting = null;
			foreach (KeyValuePair<Guid, IFunctionProvider> dictionaryOfFunction in this.DictionaryOfFunctions)
			{
				dictionaryOfFunction.Value.Close();
			}
			this.DictionaryOfFunctions = new Dictionary<Guid, IFunctionProvider>();
			this.DictionaryOfSettings = new Dictionary<Guid, ISetting>();
		}

		public void Dispose()
		{
			this.ClearDictionaries();
		}

		public void Errored(IFunctionProvider erroredFunction, ISetting erroredFunctionSetting)
		{
			this.hasErrored = true;
			this.ErroredFunction = erroredFunction;
			this.ErroredFunctionSetting = erroredFunctionSetting;
		}

		public virtual void WorkflowStarted()
		{
			this.hasWorkflowStarted = true;
		}
	}
}