using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml;

namespace HL7Soup.Functions
{
	public class JSONMessage : FunctionMessage, IDisposable, IJsonMessage, IMessage
	{
		private XmlDocument doc;

		private XmlNode root;

		public override bool IsWellFormed
		{
			get
			{
				return true;
			}
		}

		public override MessageTypes MessageType
		{
			get
			{
				return MessageTypes.JSON;
			}
		}

		public XmlNamespaceManager NamespaceManager
		{
			get;
			set;
		}

		public override string Text
		{
			get
			{
				return JsonConvert.SerializeXmlNode(this.doc);
			}
		}

		public JSONMessage(string text) : base(text)
		{
			this.SetText(text);
		}

		internal void CreateNamespaceManager(Dictionary<string, string> namespaces)
		{
			if (namespaces != null && namespaces.Count > 0)
			{
				XmlNamespaceManager xmlNamespaceManagers = new XmlNamespaceManager(this.doc.NameTable);
				foreach (KeyValuePair<string, string> @namespace in namespaces)
				{
					xmlNamespaceManagers.AddNamespace(@namespace.Key, @namespace.Value);
				}
				this.NamespaceManager = xmlNamespaceManagers;
			}
		}

		public override void Dispose()
		{
			base.Dispose();
		}

		public override IMessage GenerateAcceptMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(string errorMessage)
		{
			return null;
		}

		public override IMessage GenerateErrorMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(string rejectMessage)
		{
			return null;
		}

		public override IMessage GenerateRejectMessage(IReceiverWithResponseSetting setting)
		{
			return null;
		}

		private XmlNode GetNodeByPath(string path)
		{
			return XMLMessage.GetNodeByPath(path, this.root, this.NamespaceManager);
		}

		public string GetStructureAtPath(string path)
		{
			try
			{
				XmlNode nodeByPath = this.GetNodeByPath(path);
				if (nodeByPath != null)
				{
					return JsonConvert.SerializeXmlNode(nodeByPath);
				}
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
			return "";
		}

		public override string GetValueAtPath(string path)
		{
			try
			{
				XmlNode nodeByPath = this.GetNodeByPath(path);
				if (nodeByPath != null)
				{
					return nodeByPath.InnerText;
				}
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
			return "";
		}

		public override void SetStructureAtPath(string path, string value)
		{
			XmlNode nodeByPath = this.GetNodeByPath(path);
			if (nodeByPath != null)
			{
				if (nodeByPath.ParentNode is XmlDocument)
				{
					this.doc = JsonConvert.DeserializeXmlNode(value);
					return;
				}
				string.Concat("{\"Wrapper\":[", value, "]}");
				foreach (XmlNode childNode in JsonConvert.DeserializeXmlNode(value, "DocTopLevel").FirstChild.ChildNodes)
				{
					XmlNode xmlNodes = nodeByPath.OwnerDocument.ImportNode(childNode, true);
					nodeByPath.ParentNode.ReplaceChild(xmlNodes, nodeByPath);
				}
			}
		}

		protected override void SetText(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				this.doc = new XmlDocument()
				{
					XmlResolver = null
				};
				this.root = this.doc.DocumentElement;
				return;
			}
			XmlNode xmlNodes = null;
			try
			{
				xmlNodes = JsonConvert.DeserializeXmlNode(text);
			}
			catch (JsonReaderException jsonReaderException1)
			{
				JsonReaderException jsonReaderException = jsonReaderException1;
				if (text.Contains("${"))
				{
					throw new Exception(string.Concat("Either your json message structure is corrupt or the json parser is upset by the variables you've put into the message.  You can put variables inside double quotes as values, but not into the json message structure.  If you need to add entire sections of json via a variable then use the 'Text' message type to construct your json as it doesn't validate the message. ", jsonReaderException.Message), jsonReaderException);
				}
				throw;
			}
			this.doc = new XmlDocument()
			{
				XmlResolver = null
			};
			this.doc.LoadXml(xmlNodes.OuterXml);
			this.root = this.doc.DocumentElement;
		}

		public override void SetValueAtPath(string toPath, string fromValue)
		{
			XmlNode nodeByPath = this.GetNodeByPath(toPath);
			if (nodeByPath != null)
			{
				nodeByPath.InnerText = fromValue;
			}
		}
	}
}