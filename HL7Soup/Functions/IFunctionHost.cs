using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;

namespace HL7Soup.Functions
{
	public interface IFunctionHost : IDisposable
	{
		Dictionary<Guid, IFunctionProvider> DictionaryOfFunctions
		{
			get;
		}

		Dictionary<Guid, ISetting> DictionaryOfSettings
		{
			get;
		}

		IFunctionProvider ErroredFunction
		{
			get;
		}

		ISetting ErroredFunctionSetting
		{
			get;
		}

		List<IFunctionProvider> FunctionsProcessedForCurrentMessage
		{
			get;
		}

		bool hasErrored
		{
			get;
		}

		bool hasWorkflowStarted
		{
			get;
		}

		void Errored(IFunctionProvider erroredFunction, ISetting erroredFunctionSetting);

		void WorkflowStarted();
	}
}