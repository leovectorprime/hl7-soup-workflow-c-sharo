using System;

namespace HL7Soup.Functions
{
	public enum FunctionProviderStatuses
	{
		Constructed,
		Ready,
		Processing,
		Finalizing,
		RollingBack,
		Canceled,
		Closed,
		Errored
	}
}