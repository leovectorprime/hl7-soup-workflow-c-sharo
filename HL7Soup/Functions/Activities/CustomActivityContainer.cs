using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Activities;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using HL7Soup.Workflow.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions.Activities
{
	public class CustomActivityContainer : Activity, ISender, IFunctionProvider
	{
		private ICustomActivity CustomActivity
		{
			get;
			set;
		}

		public new CustomActivityContainerSetting Setting
		{
			get
			{
				return (CustomActivityContainerSetting)this.baseSetting;
			}
		}

		public CustomActivityContainer()
		{
			HL7Soup.Workflow.Properties.Settings @default = HL7Soup.Workflow.Properties.Settings.Default;
			@default.AlignmentGap2 = @default.AlignmentGap2 + 1;
			HL7Soup.Workflow.Properties.Settings.Default.Save();
		}

		protected override void FunctionAllFunctionsSucceded(WorkflowInstance workflowInstance)
		{
			base.FunctionAllFunctionsSucceded(workflowInstance);
			this.CustomActivity.AllFunctionsSucceded(workflowInstance, workflowInstance.CurrentActivityInstance);
		}

		protected override void FunctionCanceled()
		{
			base.FunctionCanceled();
			this.CustomActivity.Cancel();
		}

		protected override void FunctionClosed()
		{
			base.FunctionClosed();
			if (this.CustomActivity != null)
			{
				this.CustomActivity.Close();
			}
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
			base.FunctionPrepare(workflowInstance);
			Type customActivityType = this.Setting.GetCustomActivityType();
			this.CustomActivity = Activator.CreateInstance(customActivityType) as ICustomActivity;
			if (this.CustomActivity == null)
			{
				throw new Exception(string.Concat("Creating instance of IIntegrationFunctionProvider returned null. ", customActivityType.FullName));
			}
			this.CustomActivity.Prepare(workflowInstance, workflowInstance.CurrentActivityInstance);
		}

		protected override void FunctionRollBack(WorkflowInstance workflowInstance)
		{
			base.FunctionRollBack(workflowInstance);
			this.CustomActivity.RollBack(workflowInstance, workflowInstance.CurrentActivityInstance);
		}

		public void Send(WorkflowInstance workflowInstance)
		{
			if (this.CustomActivity == null)
			{
				throw new ArgumentNullException("CustomActivity");
			}
			MessageLogger.Instance.AddMessage(new MessageEvent((workflowInstance.Message == null ? "" : workflowInstance.Message.Text), workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
			Dictionary<string, string> strs = new Dictionary<string, string>();
			string str = "Parameters:\r\n";
			foreach (CustomActivityParameter value in this.Setting.Parameters.Values)
			{
				string theValue = CustomActivityParameter.GetTheValue(workflowInstance, value.Value);
				str = string.Concat(new string[] { str, "\r\n", value.ParameterName, ": \"", theValue, "\"" });
				strs[value.ParameterName] = theValue;
			}
			if (str.Length > 13)
			{
				MessageLogger.Instance.AddMessage(new MessageEvent((workflowInstance.Message == null ? str : string.Concat(workflowInstance.Message.Text, "\r\n\r\n", str)), workflowInstance, this.Setting.Id, LogItemStatus.Processing, LogItemDataType.Sent));
			}
			if (this.Setting.OutMessage != null)
			{
				if (this.Setting.ResponseMessageType != MessageTypes.Unknown)
				{
					((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessage(this.Setting.ResponseMessageTemplate, this.Setting.ResponseMessageType);
				}
				else
				{
					((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage = FunctionHelpers.GetFunctionMessageDeterminedFromText(this.Setting.ResponseMessageTemplate);
				}
			}
			this.CustomActivity.Process(workflowInstance, workflowInstance.CurrentActivityInstance, strs);
			HL7V2MessageType responseMessage = ((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage as HL7V2MessageType;
			if (responseMessage != null)
			{
				responseMessage.ReloadFromInternalMessageParts();
			}
			string str1 = "Variables:\r\n";
			Dictionary<string, IVariableCreator> variables = this.Setting.GetVariables();
			if (variables != null)
			{
				foreach (KeyValuePair<string, IVariableCreator> variable in variables)
				{
					str1 = string.Concat(new string[] { str1, "\r\n", variable.Key, ": \"", workflowInstance.GetVariable(variable.Key), "\"" });
				}
			}
			if (str1.Length == 12)
			{
				str1 = "";
			}
			if (this.Setting.OutMessage == null)
			{
				MessageLogger.Instance.AddMessage(new MessageEvent(str1, workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Response));
				return;
			}
			MessageLogger.Instance.AddMessage(new MessageEvent((((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage == null ? str1 : string.Concat(((ActivityInstance)workflowInstance.CurrentActivityInstance).ResponseMessage.Text, "\r\n\r\n", str1)), workflowInstance, this.Setting.Id, LogItemStatus.Completed, LogItemDataType.Response));
		}
	}
}