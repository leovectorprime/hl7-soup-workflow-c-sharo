using HL7Soup;
using HL7Soup.Functions;
using System;

namespace HL7Soup.Functions.Activities
{
	public interface IActivity : IFunctionProvider
	{
		void ProcessActivity(WorkflowInstance workflowInstance);
	}
}