using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HL7Soup.Functions.Activities
{
	public class Activity : FunctionProvider, IActivity, IFunctionProvider
	{
		public IActivitySetting Setting
		{
			get
			{
				return (IActivitySetting)this.baseSetting;
			}
		}

		public Activity()
		{
		}

		protected override void FunctionPrepare(WorkflowInstance workflowInstance)
		{
		}

		public void ProcessActivity(WorkflowInstance workflowInstance)
		{
			ISenderSetting setting = this.Setting as ISenderSetting;
			if (setting != null)
			{
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Processing));
				string str = setting.MessageTemplateRuntimeValue(workflowInstance);
				if (str != "" || setting.MessageType == MessageTypes.Text)
				{
					workflowInstance.AddActivity(FunctionHelpers.GetFunctionMessage(str, setting.MessageType), this.Setting.Id);
				}
				else
				{
					workflowInstance.AddActivity(null, this.Setting.Id);
				}
			}
			if (!base.Manager.ProcessFilters(this.Setting.Filters, workflowInstance))
			{
				return;
			}
			if (workflowInstance.CurrentActivityInstance.Filtered)
			{
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, this.Setting.Id, LogItemStatus.Filtered));
				return;
			}
			if (this.Setting.Transformers != Guid.Empty && !base.Manager.ProcessTransformers(this.Setting.Transformers, workflowInstance))
			{
				return;
			}
			if ((FunctionMessage)workflowInstance.Message != null)
			{
				((FunctionMessage)workflowInstance.Message).ProcessVariables(workflowInstance);
			}
			if (setting != null)
			{
				base.Manager.ProcessSender(setting, workflowInstance);
			}
			if (Thread.CurrentThread.Name != "QueueThreadWorker")
			{
				base.Manager.FunctionsProcessedForCurrentMessage.Add(this);
			}
		}
	}
}