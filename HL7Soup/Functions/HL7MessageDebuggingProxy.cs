using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup.Functions
{
	public class HL7MessageDebuggingProxy
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public IHL7Message BaseType
		{
			get;
			set;
		}

		public ICollection<IHL7Segment> DebugHelperSegments
		{
			get
			{
				return this.BaseType.GetSegments();
			}
		}

		public string Text
		{
			get
			{
				return this.BaseType.Text;
			}
		}

		public HL7MessageDebuggingProxy(IHL7Message basetype)
		{
			this.BaseType = basetype;
		}
	}
}