using HL7Soup.HL7Descriptions;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class SubComponentDebuggingProxy
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public SubComponent BaseType
		{
			get;
			set;
		}

		public string DebugHelperDescription
		{
			get
			{
				if (this.BaseType.XPart == null)
				{
					return "Unavailable";
				}
				return this.BaseType.XPart.Description;
			}
		}

		public string LocationCode
		{
			get
			{
				return this.BaseType.LocationCode;
			}
		}

		public string Text
		{
			get
			{
				return this.BaseType.Text;
			}
		}

		public SubComponentDebuggingProxy(SubComponent basetype)
		{
			this.BaseType = basetype;
		}
	}
}