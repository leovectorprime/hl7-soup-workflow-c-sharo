using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class ActivityInstance : IActivityInstance
	{
		private ISetting setting;

		public bool Filtered
		{
			get
			{
				return JustDecompileGenerated_get_Filtered();
			}
			set
			{
				JustDecompileGenerated_set_Filtered(value);
			}
		}

		private bool JustDecompileGenerated_Filtered_k__BackingField;

		public bool JustDecompileGenerated_get_Filtered()
		{
			return this.JustDecompileGenerated_Filtered_k__BackingField;
		}

		public void JustDecompileGenerated_set_Filtered(bool value)
		{
			this.JustDecompileGenerated_Filtered_k__BackingField = value;
		}

		public Guid Id
		{
			get
			{
				return JustDecompileGenerated_get_Id();
			}
			set
			{
				JustDecompileGenerated_set_Id(value);
			}
		}

		private Guid JustDecompileGenerated_Id_k__BackingField;

		public Guid JustDecompileGenerated_get_Id()
		{
			return this.JustDecompileGenerated_Id_k__BackingField;
		}

		internal void JustDecompileGenerated_set_Id(Guid value)
		{
			this.JustDecompileGenerated_Id_k__BackingField = value;
		}

		public IMessage Message
		{
			get
			{
				return JustDecompileGenerated_get_Message();
			}
			set
			{
				JustDecompileGenerated_set_Message(value);
			}
		}

		private IMessage JustDecompileGenerated_Message_k__BackingField;

		public IMessage JustDecompileGenerated_get_Message()
		{
			return this.JustDecompileGenerated_Message_k__BackingField;
		}

		internal void JustDecompileGenerated_set_Message(IMessage value)
		{
			this.JustDecompileGenerated_Message_k__BackingField = value;
		}

		public string Name
		{
			get
			{
				return this.setting.Name;
			}
		}

		public IMessage ResponseMessage
		{
			get
			{
				return JustDecompileGenerated_get_ResponseMessage();
			}
			set
			{
				JustDecompileGenerated_set_ResponseMessage(value);
			}
		}

		private IMessage JustDecompileGenerated_ResponseMessage_k__BackingField;

		public IMessage JustDecompileGenerated_get_ResponseMessage()
		{
			return this.JustDecompileGenerated_ResponseMessage_k__BackingField;
		}

		internal void JustDecompileGenerated_set_ResponseMessage(IMessage value)
		{
			this.JustDecompileGenerated_ResponseMessage_k__BackingField = value;
		}

		public ActivityInstance(IMessage message, Guid functionId)
		{
			this.Id = functionId;
			this.Message = message;
		}

		private ISetting GetSetting()
		{
			if (this.setting == null)
			{
				this.setting = FunctionHelpers.GetSettingById(this.Id);
			}
			return this.setting;
		}
	}
}