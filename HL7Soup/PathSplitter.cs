using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace HL7Soup
{
	[Serializable]
	public class PathSplitter
	{
		private string segmentIndex;

		private string fieldRepeatIndex;

		private string field;

		private string component;

		private string subComponent;

		public string Component
		{
			get
			{
				return this.component;
			}
			set
			{
				value = value.Trim();
				if (!this.IsNumeric(value))
				{
					this.IsValid = false;
					this.IsInvalidBecauseOfError = true;
					this.InvalidReason = string.Concat("The Component must be a number for path '", this.OriginalPath, "'");
				}
				this.component = value;
			}
		}

		public string Field
		{
			get
			{
				return this.field;
			}
			set
			{
				value = value.Trim();
				if (!this.IsNumeric(value))
				{
					this.IsValid = false;
					this.IsInvalidBecauseOfError = true;
					this.InvalidReason = string.Concat("The Field must be a number for path '", this.OriginalPath, "'");
				}
				this.field = value;
			}
		}

		public string FieldRepeatIndex
		{
			get
			{
				return this.fieldRepeatIndex;
			}
			set
			{
				if (value.StartsWith("[") && value.EndsWith("]"))
				{
					if (value.Length < 3)
					{
						this.IsValid = false;
						this.IsInvalidBecauseOfError = true;
						this.InvalidReason = string.Concat("No field repeat index value is provided between the []'s for path '", this.OriginalPath, "'");
					}
					else if (!this.IsNumeric(value.Substring(1, value.Length - 2)))
					{
						this.IsValid = false;
						this.IsInvalidBecauseOfError = true;
						this.InvalidReason = string.Concat("The field repeat index must be a number for path '", this.OriginalPath, "'");
					}
				}
				this.fieldRepeatIndex = value;
			}
		}

		public int FieldRepeatIndexInt
		{
			get
			{
				if (this.fieldRepeatIndex == null || !this.fieldRepeatIndex.StartsWith("["))
				{
					return 1;
				}
				int num = 1;
				int.TryParse(this.fieldRepeatIndex.Trim(new char[] { '[', ']' }), out num);
				if (num == 0)
				{
					throw new Exception("HL7 paths are 1 based arrays.  You cannot have a path with an index of 0.  E.g PID-5[0] is invalid, but PID-5[1] is, and is probably what you are after.");
				}
				return num;
			}
		}

		public string InvalidReason
		{
			get;
			set;
		}

		public bool IsInvalidBecauseOfError
		{
			get;
			set;
		}

		public bool IsValid
		{
			get;
			set;
		}

		public string OriginalPath
		{
			get;
			private set;
		}

		public PathTypes PathTerminationPoint
		{
			get;
			set;
		}

		public bool ReturnWholeSegments
		{
			get;
			set;
		}

		public string Segment
		{
			get;
			set;
		}

		public string SegmentIndex
		{
			get
			{
				return this.segmentIndex;
			}
			set
			{
				if (value.StartsWith("[") && value.EndsWith("]"))
				{
					if (value.Length < 3)
					{
						this.IsValid = false;
						this.IsInvalidBecauseOfError = true;
						this.InvalidReason = string.Concat("No segment index value is provided between the []'s for path '", this.OriginalPath, "'");
					}
					else if (!this.IsNumeric(value.Substring(1, value.Length - 2)))
					{
						this.IsValid = false;
						this.IsInvalidBecauseOfError = true;
						this.InvalidReason = string.Concat("The segment index must be a number for path '", this.OriginalPath, "'");
					}
				}
				this.segmentIndex = value;
			}
		}

		public int SegmentIndexInt
		{
			get
			{
				if (this.segmentIndex == null || !this.segmentIndex.StartsWith("["))
				{
					return 1;
				}
				int num = 1;
				int.TryParse(this.segmentIndex.Trim(new char[] { '[', ']' }), out num);
				if (num == 0)
				{
					throw new Exception("HL7 paths are 1 based arrays.  You cannot have a path with an index of 0.  E.g PID[0] is invalid, but PID[1] is, and is probably what you are after.");
				}
				return num;
			}
		}

		public string SubComponent
		{
			get
			{
				return this.subComponent;
			}
			set
			{
				value = value.Trim();
				if (!this.IsNumeric(value) || value.EndsWith("."))
				{
					this.IsValid = false;
					this.IsInvalidBecauseOfError = true;
					this.InvalidReason = string.Concat("The SubComponent must be a number for path '", this.OriginalPath, "'");
				}
				this.subComponent = value;
			}
		}

		public PathSplitter(string path)
		{
			this.OriginalPath = path;
			this.IsValid = true;
			this.PathTerminationPoint = this.SplitInternal(path);
		}

		public PathSplitter(string path, bool returnWholeSegments)
		{
			this.OriginalPath = path;
			this.IsValid = true;
			this.ReturnWholeSegments = returnWholeSegments;
			this.PathTerminationPoint = this.SplitInternal(path);
		}

		public PathSplitter(SegmentsEnum segment)
		{
			this.Segment = Enum.GetName(typeof(SegmentsEnum), segment);
			this.PathTerminationPoint = PathTypes.Segment;
			this.IsValid = true;
		}

		public PathSplitter(string segment, int field) : this(segment)
		{
			this.Field = field.ToString();
			this.PathTerminationPoint = PathTypes.Field;
		}

		public PathSplitter(SegmentsEnum segment, int field) : this(segment)
		{
			this.Field = field.ToString();
			this.PathTerminationPoint = PathTypes.Field;
		}

		public PathSplitter(SegmentsEnum segment, int field, int component) : this(segment, field)
		{
			this.Component = component.ToString();
			this.PathTerminationPoint = PathTypes.Component;
		}

		public PathSplitter(string segment, int field, int component) : this(segment, field)
		{
			this.Component = component.ToString();
			this.PathTerminationPoint = PathTypes.Component;
		}

		public PathSplitter(SegmentsEnum segment, int field, int component, int subComponent) : this(segment, field, component)
		{
			this.SubComponent = subComponent.ToString();
			this.PathTerminationPoint = PathTypes.SubComponent;
		}

		public PathSplitter(string segment, int field, int component, int subComponent) : this(segment, field, component)
		{
			this.SubComponent = subComponent.ToString();
			this.PathTerminationPoint = PathTypes.SubComponent;
		}

		private bool IsNumeric(string number)
		{
			return Regex.Replace(number, "[^.0-9]", "") == number;
		}

		public void Split(string path)
		{
			this.PathTerminationPoint = this.SplitInternal(path);
		}

		private PathTypes SplitInternal(string path)
		{
			this.IsInvalidBecauseOfError = false;
			this.IsValid = true;
			this.InvalidReason = "";
			if (this.ReturnWholeSegments)
			{
				if (path.Length < 3)
				{
					this.IsValid = false;
					this.InvalidReason = string.Concat("The path '", this.OriginalPath, "' is too short.  A valid HL7 path looks like PID-5.2 or with repeated segments and fields like OBX[3]-5[2].1.1");
					this.IsInvalidBecauseOfError = true;
				}
			}
			else if (path.Length < 4)
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("The path '", this.OriginalPath, "' is too short.  A valid HL7 path looks like PID-5.2 or with repeated segments and fields like OBX[3]-5[2].1.1");
				this.IsInvalidBecauseOfError = true;
			}
			if (path.Length > 3 && path.Substring(3, 1) != "-" && path.Substring(3, 1) != "." && path.Substring(3, 1) != "[")
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("The path '", this.OriginalPath, "' must include a three letter code, then be followed by either a - or a [.  A valid HL7 path looks like PID-5.2 or with repeated segments and fields like OBX[3]-5[2].1.1");
				this.IsInvalidBecauseOfError = true;
				return PathTypes.Segment;
			}
			if (path.Length < 3)
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("The path '", this.OriginalPath, "' is too short.  A valid HL7 path looks like PID-5.2 or with repeated segments and fields like OBX[3]-5[2].1.1");
				this.IsInvalidBecauseOfError = true;
				return PathTypes.None;
			}
			this.Segment = path.Substring(0, 3).ToUpper();
			if (this.ReturnWholeSegments)
			{
				if (path.Length == 3)
				{
					return PathTypes.Segment;
				}
			}
			else if (path.Length == 3)
			{
				return PathTypes.Hyphen;
			}
			int num = 3;
			if (path.Substring(3, 1) == "[")
			{
				num = path.IndexOf("]", 4);
				if (num < 4)
				{
					this.IsValid = false;
					this.InvalidReason = string.Concat("Segment Index not complete for path '", this.OriginalPath, "'");
					this.SegmentIndex = path.Substring(3);
					return PathTypes.SegmentIndex;
				}
				this.SegmentIndex = path.Substring(3, num - 2);
				num++;
				if (path.Length == num)
				{
					return PathTypes.SegmentIndex;
				}
			}
			if (path.Length == num)
			{
				return PathTypes.Hyphen;
			}
			if (path.Length > num && path.Substring(num, 1) == ".")
			{
				path = string.Concat(path.Substring(0, num), "-", (path.Length > num + 1 ? path.Substring(num + 1) : ""));
			}
			if (path.Length > num && path.Substring(num, 1) != "-")
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("A '-' must follow the Segment index for path '", this.OriginalPath, "'");
				this.IsInvalidBecauseOfError = true;
				return PathTypes.Hyphen;
			}
			if (path.Length == num + 1)
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("A Field must be provided after the '-' for path '", this.OriginalPath, "'");
				return PathTypes.Field;
			}
			num++;
			int num1 = num;
			num = path.IndexOfAny(".[".ToCharArray(), num1);
			if (num == -1)
			{
				this.Field = path.Substring(num1);
				return PathTypes.Field;
			}
			this.Field = path.Substring(num1, num - num1);
			if (path.Substring(num, 1) == "[")
			{
				num1 = num;
				num = path.IndexOf("]", num + 1);
				if (num < num1 + 1)
				{
					this.IsValid = false;
					this.InvalidReason = string.Concat("Field Repeat Index not complete for path '", this.OriginalPath, "'");
					this.FieldRepeatIndex = path.Substring(num1);
					return PathTypes.FieldRepeatIndex;
				}
				this.FieldRepeatIndex = path.Substring(num1, num - num1 + 1);
				num++;
				if (path.Length == num)
				{
					if (string.IsNullOrEmpty(this.FieldRepeatIndex))
					{
						return PathTypes.Field;
					}
					return PathTypes.FieldRepeatIndex;
				}
			}
			if (path.Substring(num, 1) != ".")
			{
				this.IsValid = false;
				this.IsInvalidBecauseOfError = true;
				this.InvalidReason = string.Concat("A '.' must be provided after the Field index in order to represent a Component for path '", this.OriginalPath, "'");
				return PathTypes.Dot;
			}
			num1 = num;
			num = path.IndexOf(".", num + 1);
			if (num < num1 + 1)
			{
				this.Component = path.Substring(num1 + 1);
				if (this.Component == "")
				{
					this.IsValid = false;
					this.InvalidReason = string.Concat("A Component must be provided after the '.' for path '", this.OriginalPath, "'.");
				}
				return PathTypes.Component;
			}
			num++;
			this.Component = path.Substring(num1 + 1, num - num1 - 2);
			num++;
			if (num - 1 >= path.Length)
			{
				this.IsValid = false;
				this.InvalidReason = string.Concat("A Sub Component must be provided after the '.' for path '", this.OriginalPath, "'.");
			}
			else
			{
				this.SubComponent = path.Substring(num - 1);
			}
			return PathTypes.SubComponent;
		}

		public string ToComponentTerminatingPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			stringBuilder.Append(this.SegmentIndex);
			if (this.Field != "")
			{
				stringBuilder.Append("-");
				stringBuilder.Append(this.Field);
				stringBuilder.Append(this.FieldRepeatIndex);
				if (this.Component != null)
				{
					stringBuilder.Append(".");
					stringBuilder.Append(this.Component);
				}
			}
			return stringBuilder.ToString();
		}

		public string ToFieldRepeatIndexTerminatingPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			stringBuilder.Append(this.SegmentIndex);
			if (this.Field != "")
			{
				stringBuilder.Append("-");
				stringBuilder.Append(this.Field);
				stringBuilder.Append(this.FieldRepeatIndex);
			}
			return stringBuilder.ToString();
		}

		public string ToFieldTerminatingPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			stringBuilder.Append(this.SegmentIndex);
			if (this.Field != "")
			{
				stringBuilder.Append("-");
				stringBuilder.Append(this.Field);
			}
			return stringBuilder.ToString();
		}

		public string ToSegmentIndexTerminatingPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			stringBuilder.Append(this.SegmentIndex);
			return stringBuilder.ToString();
		}

		public string ToSegmentTerminatingPath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			return stringBuilder.ToString();
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.Segment);
			stringBuilder.Append(this.SegmentIndex);
			if (this.Field != "")
			{
				stringBuilder.Append("-");
				stringBuilder.Append(this.Field);
				stringBuilder.Append(this.FieldRepeatIndex);
				if (this.Component != null)
				{
					stringBuilder.Append(".");
					stringBuilder.Append(this.Component);
					if (this.SubComponent != null)
					{
						stringBuilder.Append(".");
						stringBuilder.Append(this.SubComponent);
					}
				}
			}
			return stringBuilder.ToString();
		}
	}
}