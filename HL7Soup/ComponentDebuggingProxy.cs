using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class ComponentDebuggingProxy
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Component BaseType
		{
			get;
			set;
		}

		public ICollection<IHL7SubComponent> DebugHelperComponents
		{
			get
			{
				return this.BaseType.GetSubComponents();
			}
		}

		public string DebugHelperDescription
		{
			get
			{
				if (this.BaseType.XPart == null)
				{
					return "Unavailable";
				}
				return this.BaseType.XPart.Description;
			}
		}

		public string LocationCode
		{
			get
			{
				return this.BaseType.LocationCode;
			}
		}

		public string Text
		{
			get
			{
				return this.BaseType.Text;
			}
		}

		public ComponentDebuggingProxy(Component basetype)
		{
			this.BaseType = basetype;
		}
	}
}