using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.HL7Descriptions
{
	public class SearchPathResult
	{
		public List<SearchPathResultChild> Children { get; set; } = new List<SearchPathResultChild>();

		public XComponent Component
		{
			get;
			set;
		}

		public XField Field
		{
			get;
			set;
		}

		public XBasePart Part
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public XSegment Segment
		{
			get;
			set;
		}

		public XBasePart SubComponent
		{
			get;
			set;
		}

		public SearchPathResult()
		{
		}
	}
}