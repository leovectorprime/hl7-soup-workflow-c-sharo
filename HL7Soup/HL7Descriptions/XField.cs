using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XField : XBasePart
	{
		public override XElement ChildRootElement
		{
			get
			{
				return base.xsdManager.DataTypesRootElement;
			}
		}

		public override XElement RootElement
		{
			get
			{
				return base.xsdManager.FieldsRootElement;
			}
		}

		public XField(XsdManager xsdManager, XElement childElement, int min, int max) : base(xsdManager, childElement, min, max)
		{
		}

		public override List<XBasePart> GetChildParts(List<XPartReference> partReferences)
		{
			lock (this.RootElement)
			{
				if (base.GetSequenceElements() != null)
				{
					foreach (XPartReference partReference in partReferences)
					{
						XElement elementByName = Helper.GetElementByName(this.ChildRootElement, partReference.RefName);
						if (elementByName == null)
						{
							continue;
						}
						XComponent xComponent = new XComponent(base.xsdManager, elementByName, partReference.Min, partReference.Max);
						this.childParts.Add(xComponent);
					}
				}
			}
			return this.childParts;
		}

		public override XElement SequenceHostingElement()
		{
			if (this.sequenceHostingElement == null)
			{
				this.sequenceHostingElement = Helper.GetComplexTypeByName(this.ChildRootElement, this.DataTypeName);
			}
			return this.sequenceHostingElement;
		}
	}
}