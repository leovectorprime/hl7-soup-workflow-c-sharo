using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XSegment : XBasePart
	{
		private string description = "";

		public override XElement ChildRootElement
		{
			get
			{
				return base.xsdManager.FieldsRootElement;
			}
		}

		public override string Description
		{
			get
			{
				if (this.description == "")
				{
					this.description = XSegment.GetDescription(base.Name);
				}
				return this.description;
			}
		}

		public string DescriptionAndGroup
		{
			get
			{
				return string.Concat(this.Description, (!string.IsNullOrEmpty(this.GroupName) ? string.Concat(" (", this.GroupName, ")") : ""));
			}
		}

		public string GroupName
		{
			get;
			set;
		}

		public override XElement RootElement
		{
			get
			{
				return base.xsdManager.SegmentRootElement;
			}
		}

		public XSegment(XsdManager xsdManager, XElement childElement, int min, int max) : base(xsdManager, childElement, min, max)
		{
		}

		public override List<XBasePart> GetChildParts(List<XPartReference> partReferences)
		{
			List<XBasePart> xBaseParts;
			lock (this.RootElement)
			{
				if (base.GetSequenceElements() != null)
				{
					foreach (XPartReference partReference in partReferences)
					{
						XElement elementByName = Helper.GetElementByName(this.ChildRootElement, partReference.RefName);
						XField xField = new XField(base.xsdManager, elementByName, partReference.Min, partReference.Max);
						this.childParts.Add(xField);
					}
				}
				xBaseParts = this.childParts;
			}
			return xBaseParts;
		}

		public static string GetDescription(string name)
		{
			string str = "";
			if (name != null)
			{
				switch (name)
				{
					case "ACK":
					{
						str = "Acknowledgment";
						break;
					}
					case "ABS":
					{
						str = "Abstract";
						break;
					}
					case "ACC":
					{
						str = "Accident";
						break;
					}
					case "ADD":
					{
						str = "Addendum";
						break;
					}
					case "ADJ":
					{
						str = "Adjustment";
						break;
					}
					case "AFF":
					{
						str = "Professional Affiliation";
						break;
					}
					case "AIG":
					{
						str = "Appointment Information - General Resource";
						break;
					}
					case "AIL":
					{
						str = "Appointment Information - Location Resource";
						break;
					}
					case "AIP":
					{
						str = "Appointment Information - Personnel Resource";
						break;
					}
					case "AIS":
					{
						str = "Appointment Information";
						break;
					}
					case "AL1":
					{
						str = "Patient Allergy Information";
						break;
					}
					case "APR":
					{
						str = "Appointment Preferences";
						break;
					}
					case "ARQ":
					{
						str = "Appointment Request";
						break;
					}
					case "ARV":
					{
						str = "Access Restriction";
						break;
					}
					case "AUT":
					{
						str = "Authorization Information";
						break;
					}
					case "BHS":
					{
						str = "Batch Header";
						break;
					}
					case "BLC":
					{
						str = "Blood Code";
						break;
					}
					case "BLG":
					{
						str = "Billing";
						break;
					}
					case "BPO":
					{
						str = "Blood product order";
						break;
					}
					case "BPX":
					{
						str = "Blood product dispense status";
						break;
					}
					case "BTS":
					{
						str = "Batch Trailer";
						break;
					}
					case "BTX":
					{
						str = "Blood Product Transfusion/Disposition";
						break;
					}
					case "CDM":
					{
						str = "Charge Description Master";
						break;
					}
					case "CER":
					{
						str = "Certificate Detail";
						break;
					}
					case "CM0":
					{
						str = "Clinical Study Master";
						break;
					}
					case "CM1":
					{
						str = "Clinical Study Phase Master";
						break;
					}
					case "CM2":
					{
						str = "Clinical Study Schedule Master";
						break;
					}
					case "CNS":
					{
						str = "Clear Notification";
						break;
					}
					case "CON":
					{
						str = "Consent Segment";
						break;
					}
					case "CSP":
					{
						str = "Clinical Study Phase";
						break;
					}
					case "CSR":
					{
						str = "Clinical Study Registration";
						break;
					}
					case "CSS":
					{
						str = "Clinical Study Data Schedule Segment";
						break;
					}
					case "CTD":
					{
						str = "Contact Data";
						break;
					}
					case "CTI":
					{
						str = "Clinical Trial Identification";
						break;
					}
					case "DB1":
					{
						str = "Disability";
						break;
					}
					case "DG1":
					{
						str = "Diagnosis";
						break;
					}
					case "DMI":
					{
						str = "DRG Master File Information";
						break;
					}
					case "DRG":
					{
						str = "Diagnosis Related Group";
						break;
					}
					case "DSC":
					{
						str = "Continuation Pointer";
						break;
					}
					case "DSP":
					{
						str = "Display Data";
						break;
					}
					case "ECD":
					{
						str = "Equipment Command";
						break;
					}
					case "ECR":
					{
						str = "Equipment Command Response";
						break;
					}
					case "EDU":
					{
						str = "Educational Detail";
						break;
					}
					case "EQP":
					{
						str = "Equipment/log Service";
						break;
					}
					case "EQU":
					{
						str = "Equipment Detail";
						break;
					}
					case "ERR":
					{
						str = "Error";
						break;
					}
					case "EVN":
					{
						str = "Event Type";
						break;
					}
					case "FAC":
					{
						str = "Facility";
						break;
					}
					case "FHS":
					{
						str = "File Header";
						break;
					}
					case "FT1":
					{
						str = "Financial Transaction";
						break;
					}
					case "FTS":
					{
						str = "File Trailer";
						break;
					}
					case "GOL":
					{
						str = "Goal Detail";
						break;
					}
					case "GP1":
					{
						str = "Grouping/Reimbursement - Visit";
						break;
					}
					case "GP2":
					{
						str = "Grouping/Reimbursement - Procedure Line Item";
						break;
					}
					case "GT1":
					{
						str = "Guarantor";
						break;
					}
					case "IAM":
					{
						str = "Patient Adverse Reaction Information";
						break;
					}
					case "IIM":
					{
						str = "Inventory Item Master";
						break;
					}
					case "ILT":
					{
						str = "Material Lot";
						break;
					}
					case "IN1":
					{
						str = "Insurance";
						break;
					}
					case "IN2":
					{
						str = "Insurance Additional Information";
						break;
					}
					case "IN3":
					{
						str = "Insurance Additional Information, Certification";
						break;
					}
					case "INV":
					{
						str = "Inventory Detail";
						break;
					}
					case "IPC":
					{
						str = "Imaging Procedure Control Segment";
						break;
					}
					case "IPR":
					{
						str = "Invoice Processing Results";
						break;
					}
					case "ISD":
					{
						str = "Interaction Status Detail";
						break;
					}
					case "ITM":
					{
						str = "Material Item";
						break;
					}
					case "IVC":
					{
						str = "Invoice Segment";
						break;
					}
					case "IVT":
					{
						str = "Material Location LAN Language Detail";
						break;
					}
					case "LCC":
					{
						str = "Location Charge Code";
						break;
					}
					case "LCH":
					{
						str = "Location Characteristic";
						break;
					}
					case "LDP":
					{
						str = "Location Department";
						break;
					}
					case "LOC":
					{
						str = "Location Identification";
						break;
					}
					case "LRL":
					{
						str = "Location Relationship";
						break;
					}
					case "MFA":
					{
						str = "Master File Acknowledgment";
						break;
					}
					case "MFE":
					{
						str = "Master File Entry";
						break;
					}
					case "MFI":
					{
						str = "Master File Identification";
						break;
					}
					case "MRG":
					{
						str = "Merge Patient Information";
						break;
					}
					case "MSA":
					{
						str = "Message Acknowledgment";
						break;
					}
					case "MSH":
					{
						str = "Message Header";
						break;
					}
					case "NCK":
					{
						str = "System Clock";
						break;
					}
					case "NDS":
					{
						str = "Notification Detail";
						break;
					}
					case "NK1":
					{
						str = "Next of Kin / Associated Parties";
						break;
					}
					case "NPU":
					{
						str = "Bed Status Update";
						break;
					}
					case "NSC":
					{
						str = "Application Status Change";
						break;
					}
					case "NST":
					{
						str = "Application control level statistics";
						break;
					}
					case "NTE":
					{
						str = "Notes and Comments";
						break;
					}
					case "OBR":
					{
						str = "Observation Request";
						break;
					}
					case "OBX":
					{
						str = "Observation/Result";
						break;
					}
					case "ODS":
					{
						str = "Dietary Orders, Supplements, and Preferences";
						break;
					}
					case "ODT":
					{
						str = "Diet Tray Instructions";
						break;
					}
					case "OM1":
					{
						str = "General Segment";
						break;
					}
					case "OM2":
					{
						str = "Numeric Observation";
						break;
					}
					case "OM3":
					{
						str = "Categorical Service/Test/Observation";
						break;
					}
					case "OM4":
					{
						str = "Observations that Require Specimens";
						break;
					}
					case "OM5":
					{
						str = "Observation Batteries (Sets)";
						break;
					}
					case "OM6":
					{
						str = "Observations that are Calculated from Other Observations";
						break;
					}
					case "OM7":
					{
						str = "Additional Basic Attributes";
						break;
					}
					case "ORC":
					{
						str = "Common Order";
						break;
					}
					case "ORG":
					{
						str = "Practitioner Organization Unit";
						break;
					}
					case "OVR":
					{
						str = "Override Segment";
						break;
					}
					case "PCE":
					{
						str = "Patient Charge Cost Center Exceptions";
						break;
					}
					case "PCR":
					{
						str = "Possible Causal Relationship";
						break;
					}
					case "PD1":
					{
						str = "Patient Additional Demographic";
						break;
					}
					case "PDA":
					{
						str = "Patient Death and Autopsy";
						break;
					}
					case "PDC":
					{
						str = "Product Detail Country";
						break;
					}
					case "PEO":
					{
						str = "Product Experience Observation";
						break;
					}
					case "PES":
					{
						str = "Product Experience Sender";
						break;
					}
					case "PID":
					{
						str = "Patient Identification";
						break;
					}
					case "PKG":
					{
						str = "Item Packaging";
						break;
					}
					case "PMT":
					{
						str = "Payment Information";
						break;
					}
					case "PR1":
					{
						str = "Procedures";
						break;
					}
					case "PRA":
					{
						str = "Practitioner Detail";
						break;
					}
					case "PRB":
					{
						str = "Problem Details";
						break;
					}
					case "PRC":
					{
						str = "Pricing";
						break;
					}
					case "PRD":
					{
						str = "Provider Data";
						break;
					}
					case "PSG":
					{
						str = "Product/Service Group";
						break;
					}
					case "PSH":
					{
						str = "Product Summary Header";
						break;
					}
					case "PSL":
					{
						str = "Product/Service Line Item";
						break;
					}
					case "PSS":
					{
						str = "Product/Service Section";
						break;
					}
					case "PTH":
					{
						str = "Pathway";
						break;
					}
					case "PV1":
					{
						str = "Patient Visit";
						break;
					}
					case "PV2":
					{
						str = "Patient Visit - Additional Information";
						break;
					}
					case "PYE":
					{
						str = "Payee Information";
						break;
					}
					case "QAK":
					{
						str = "Query Acknowledgment";
						break;
					}
					case "QID":
					{
						str = "Query Identification";
						break;
					}
					case "QPD":
					{
						str = "Query Parameter Definition";
						break;
					}
					case "QRD":
					{
						str = "Original-Style Query Definition";
						break;
					}
					case "QRF":
					{
						str = "Original style query filter";
						break;
					}
					case "QRI":
					{
						str = "Query Response Instance";
						break;
					}
					case "RCP":
					{
						str = "Response Control Parameter";
						break;
					}
					case "RDF":
					{
						str = "Table Row Definition";
						break;
					}
					case "RDT":
					{
						str = "Table Row Data";
						break;
					}
					case "REL":
					{
						str = "Clinical Relationship Segment";
						break;
					}
					case "RF1":
					{
						str = "Referral Information";
						break;
					}
					case "RFI":
					{
						str = "Request for Information";
						break;
					}
					case "RGS":
					{
						str = "Resource Group";
						break;
					}
					case "RMI":
					{
						str = "Risk Management Incident";
						break;
					}
					case "ROL":
					{
						str = "Role";
						break;
					}
					case "RQ1":
					{
						str = "Requisition Detail-1";
						break;
					}
					case "RQD":
					{
						str = "Requisition Detail";
						break;
					}
					case "RXA":
					{
						str = "Pharmacy/Treatment Administration";
						break;
					}
					case "RXC":
					{
						str = "Pharmacy/Treatment Component Order";
						break;
					}
					case "RXD":
					{
						str = "Pharmacy/Treatment Dispense";
						break;
					}
					case "RXE":
					{
						str = "Pharmacy/Treatment Encoded Order";
						break;
					}
					case "RXG":
					{
						str = "Pharmacy/Treatment Give";
						break;
					}
					case "RXO":
					{
						str = "Pharmacy/Treatment Order";
						break;
					}
					case "RXR":
					{
						str = "Pharmacy/Treatment Route";
						break;
					}
					case "SAC":
					{
						str = "Specimen Container detail";
						break;
					}
					case "SCD":
					{
						str = "Anti-Microbial Cycle Data";
						break;
					}
					case "SCH":
					{
						str = "Scheduling Activity Information";
						break;
					}
					case "SCP":
					{
						str = "Sterilizer Configuration Notification (Anti-Microbial Devices)";
						break;
					}
					case "SDD":
					{
						str = "Sterilization Device Data";
						break;
					}
					case "SFT":
					{
						str = "Software Segment";
						break;
					}
					case "SID":
					{
						str = "Substance Identifier";
						break;
					}
					case "SLT":
					{
						str = "Sterilization Lot";
						break;
					}
					case "SPM":
					{
						str = "Specimen";
						break;
					}
					case "STF":
					{
						str = "Staff Identification";
						break;
					}
					case "STZ":
					{
						str = "Sterilization Parameter";
						break;
					}
					case "TCC":
					{
						str = "Test Code Configuration";
						break;
					}
					case "TCD":
					{
						str = "Test Code Detail";
						break;
					}
					case "TQ1":
					{
						str = "Timing/Quantity";
						break;
					}
					case "TQ2":
					{
						str = "Timing/Quantity Relationship";
						break;
					}
					case "TXA":
					{
						str = "Transcription Document Header UAC User Authentication Credential Segment";
						break;
					}
					case "UB1":
					{
						str = "UB82";
						break;
					}
					case "UB2":
					{
						str = "UB92 Data";
						break;
					}
					case "URD":
					{
						str = "Results/update Definition";
						break;
					}
					case "URS":
					{
						str = "Unsolicited Selection";
						break;
					}
					case "VAR":
					{
						str = "Variance";
						break;
					}
					case "VND":
					{
						str = "Purchasing Vendor";
						break;
					}
					case "ZU1":
					{
						str = "Additional PV info";
						break;
					}
					case "ZU3":
					{
						str = "Attendance Details";
						break;
					}
					case "ZU4":
					{
						str = "Waiting List";
						break;
					}
					case "ZU6":
					{
						str = "Labour & Delivery";
						break;
					}
					case "ZU7":
					{
						str = "Birth Details";
						break;
					}
					case "ZU8":
					{
						str = "Miscellaneous Demographic Details";
						break;
					}
				}
			}
			return str;
		}

		public override XElement SequenceHostingElement()
		{
			if (this.sequenceHostingElement == null)
			{
				this.sequenceHostingElement = Helper.GetComplexTypeByName(this.RootElement, base.ContentTypeName);
			}
			return this.sequenceHostingElement;
		}
	}
}