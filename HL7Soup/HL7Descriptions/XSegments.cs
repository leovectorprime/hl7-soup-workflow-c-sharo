using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XSegments
	{
		public XFields Fields;

		private static XNamespace aw;

		private Dictionary<string, XSegment> segments = new Dictionary<string, XSegment>();

		private bool allSegmentsLoaded;

		private Dictionary<string, XSegment> segmentsWithoutMessageType = new Dictionary<string, XSegment>();

		public HL7Soup.HL7Descriptions.XsdManager XsdManager
		{
			get;
			set;
		}

		static XSegments()
		{
			XSegments.aw = "http://www.w3.org/2001/XMLSchema";
		}

		public XSegments(HL7Soup.HL7Descriptions.XsdManager xsdManager)
		{
			this.XsdManager = xsdManager;
			this.Fields = new XFields(xsdManager);
		}

		public XSegment GetByName(string name)
		{
			if (!this.segments.ContainsKey(name))
			{
				XSegment byName = XSegments.GetByName(this.XsdManager, name);
				if (byName == null)
				{
					return null;
				}
				this.segments[name] = byName;
			}
			return this.segments[name];
		}

		public static XSegment GetByName(HL7Soup.HL7Descriptions.XsdManager xsdManager, string name)
		{
			XElement elementByName = Helper.GetElementByName(xsdManager.SegmentRootElement, name);
			if (elementByName == null)
			{
				return null;
			}
			return new XSegment(xsdManager, elementByName, 0, 0);
		}

		public List<XBasePart> GetFieldsBelongingToSegment(string segmentName)
		{
			return this.GetByName(segmentName).GetChildParts();
		}

		public List<XSegment> GetLoadedSegments()
		{
			return this.segments.Values.ToList<XSegment>();
		}

		public List<XSegment> LoadAllSegmentsWithoutMessageType()
		{
			if (!this.allSegmentsLoaded)
			{
				foreach (XElement xElement in 
					from el in this.XsdManager.SegmentRootElement.Elements(XSegments.aw + "element")
					select el)
				{
					XSegment xSegment = new XSegment(this.XsdManager, xElement, -2147483648, -2147483648);
					if (this.segmentsWithoutMessageType.ContainsKey(xSegment.Name))
					{
						continue;
					}
					this.segmentsWithoutMessageType[xSegment.Name] = xSegment;
				}
				this.allSegmentsLoaded = true;
			}
			return this.segmentsWithoutMessageType.Values.ToList<XSegment>();
		}
	}
}