using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HL7Soup.HL7Descriptions
{
	[Description("Lookup Item")]
	[DisplayName("Lookup Item")]
	[Serializable]
	public class XDataTableItem
	{
		public string Description
		{
			get;
			set;
		}

		public string DisplayText
		{
			get
			{
				return this.ToString();
			}
		}

		public string Value
		{
			get;
			set;
		}

		public XDataTableItem()
		{
		}

		public override string ToString()
		{
			if (this.Description == null)
			{
				return "";
			}
			string description = this.Description;
			int num = description.IndexOf(" - ");
			if (num > 0)
			{
				description = description.Substring(num + 2);
			}
			if (string.IsNullOrWhiteSpace(this.Value))
			{
				if (string.IsNullOrWhiteSpace(description))
				{
					return "";
				}
				return description;
			}
			if (string.IsNullOrWhiteSpace(description))
			{
				return this.Value;
			}
			return string.Concat(this.Value, " - ", description);
		}
	}
}