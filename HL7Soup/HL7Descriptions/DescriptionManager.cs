using HL7Soup;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace HL7Soup.HL7Descriptions
{
	public class DescriptionManager : DependencyObject
	{
		private XMessageType currentMessageType;

		public readonly static DependencyProperty SegmentsForCurrentMessageTypeProperty;

		public readonly static DependencyProperty CurrentMessageTypeDescriptionProperty;

		public readonly static DependencyProperty CurrentSegmentDescriptionProperty;

		public readonly static DependencyProperty CurrentFieldDescriptionProperty;

		public readonly static DependencyProperty CurrentComponentDescriptionProperty;

		public readonly static DependencyProperty CurrentSubComponentDescriptionProperty;

		public readonly static DependencyProperty CurrentSegmentDocumentationProperty;

		public readonly static DependencyProperty CurrentPartDescriptionProperty;

		public readonly static DependencyProperty CurrentDataTableProperty;

		private string customDataTypesFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\CustomDataTypes.dat");

		private string systemDataTypesFileName = "SystemDataTypes.dat";

		private static Dictionary<string, DescriptionManager> AllVersions;

		public XComponent CurrentComponent
		{
			get;
			set;
		}

		public string CurrentComponentDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentComponentDescriptionProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentComponentDescriptionProperty, value);
			}
		}

		public XDataTable CurrentDataTable
		{
			get
			{
				return (XDataTable)base.GetValue(DescriptionManager.CurrentDataTableProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentDataTableProperty, value);
			}
		}

		public XField CurrentField
		{
			get;
			set;
		}

		public string CurrentFieldDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentFieldDescriptionProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentFieldDescriptionProperty, value);
			}
		}

		public XMessageType CurrentMessageType
		{
			get
			{
				return this.currentMessageType;
			}
			set
			{
				if (value != null && (this.currentMessageType == null || this.currentMessageType.Name != value.Name))
				{
					this.SegmentsForCurrentMessageType = value.GetAllSegments();
				}
				this.currentMessageType = value;
			}
		}

		public string CurrentMessageTypeDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentMessageTypeDescriptionProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentMessageTypeDescriptionProperty, value);
			}
		}

		public XBasePart CurrentPart
		{
			get;
			set;
		}

		public string CurrentPartDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentPartDescriptionProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentPartDescriptionProperty, value);
			}
		}

		public XSegment CurrentSegment
		{
			get;
			set;
		}

		public string CurrentSegmentDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentSegmentDescriptionProperty);
			}
			set
			{
				if (value == "")
				{
					value = "Unknown Segment";
				}
				base.SetValue(DescriptionManager.CurrentSegmentDescriptionProperty, value);
				this.CurrentSegmentDocumentation = DocumentationManager.Instance.GetDocumentationForSegment(this.CurrentSegment);
			}
		}

		public string CurrentSegmentDocumentation
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentSegmentDocumentationProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentSegmentDocumentationProperty, value);
			}
		}

		public XComponent CurrentSubComponent
		{
			get;
			set;
		}

		public string CurrentSubComponentDescription
		{
			get
			{
				return (string)base.GetValue(DescriptionManager.CurrentSubComponentDescriptionProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.CurrentSubComponentDescriptionProperty, value);
			}
		}

		public XDataTables DataTables
		{
			get;
			set;
		}

		private XFields Fields
		{
			get;
			set;
		}

		private XMessageTypes MessageTypes
		{
			get;
			set;
		}

		public XSegments Segments
		{
			get;
			set;
		}

		public List<XSegment> SegmentsForCurrentMessageType
		{
			get
			{
				return (List<XSegment>)base.GetValue(DescriptionManager.SegmentsForCurrentMessageTypeProperty);
			}
			set
			{
				base.SetValue(DescriptionManager.SegmentsForCurrentMessageTypeProperty, value);
			}
		}

		public HL7Soup.HL7Descriptions.XsdManager XsdManager
		{
			get;
			set;
		}

		static DescriptionManager()
		{
			DescriptionManager.SegmentsForCurrentMessageTypeProperty = DependencyProperty.Register("SegmentsForCurrentMessageType", typeof(List<XSegment>), typeof(DescriptionManager), new PropertyMetadata(null));
			DescriptionManager.CurrentMessageTypeDescriptionProperty = DependencyProperty.Register("CurrentMessageTypeDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata(""));
			DescriptionManager.CurrentSegmentDescriptionProperty = DependencyProperty.Register("CurrentSegmentDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata("No Segments"));
			DescriptionManager.CurrentFieldDescriptionProperty = DependencyProperty.Register("CurrentFieldDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata("none"));
			DescriptionManager.CurrentComponentDescriptionProperty = DependencyProperty.Register("CurrentComponentDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata("none"));
			DescriptionManager.CurrentSubComponentDescriptionProperty = DependencyProperty.Register("CurrentSubComponentDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata("none"));
			DescriptionManager.CurrentSegmentDocumentationProperty = DependencyProperty.Register("CurrentSegmentDocumentation", typeof(string), typeof(DescriptionManager), new PropertyMetadata(""));
			DescriptionManager.CurrentPartDescriptionProperty = DependencyProperty.Register("CurrentPartDescription", typeof(string), typeof(DescriptionManager), new PropertyMetadata("none"));
			DescriptionManager.CurrentDataTableProperty = DependencyProperty.Register("CurrentDataTable", typeof(XDataTable), typeof(DescriptionManager), new PropertyMetadata(null));
			DescriptionManager.AllVersions = new Dictionary<string, DescriptionManager>();
		}

		public DescriptionManager(string version)
		{
			string str = "HL7Soup.Workflow.HL7Descriptions.Xsd.";
			string str1 = string.Concat(str, "_2._5._1.");
			if (!string.IsNullOrEmpty(version))
			{
				string str2 = string.Concat("_", version.Replace(".", "._"), ".");
				if (Assembly.GetExecutingAssembly().GetManifestResourceInfo(string.Concat(str, str2, "messages.xsd")) != null)
				{
					str1 = string.Concat(str, str2);
				}
			}
			HL7Soup.HL7Descriptions.XsdManager xsdManager = new HL7Soup.HL7Descriptions.XsdManager(str1);
			this.Segments = new XSegments(xsdManager);
			this.Fields = new XFields(xsdManager);
			this.MessageTypes = new XMessageTypes(xsdManager);
			try
			{
				this.LoadDataTables(xsdManager.UseITKTables);
			}
			catch (SerializationException serializationException)
			{
				Log.Instance.Error<SerializationException>("An error occured loading the datatables.  This can likely be resolved by removing the CustomDataTypes.dat file at %programdata%\\hl7soup.  Please contact support@hl7soup.com for assistance.", serializationException);
				throw;
			}
		}

		public static void CreateAppdataDirectory()
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup");
			if (!Directory.Exists(str))
			{
				Directory.CreateDirectory(str);
			}
			str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\CustomSchema");
			if (!Directory.Exists(str))
			{
				Directory.CreateDirectory(str);
			}
			str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_6ydf2bfls3ryzlcurixpdffg1v4tenpui");
			if (!Directory.Exists(str))
			{
				Directory.CreateDirectory(str);
			}
		}

		public List<FinPartByNameResult> FindPartsByName(string searchText)
		{
			FinPartByNameResult finPartByNameResult;
			List<FinPartByNameResult> finPartByNameResults = new List<FinPartByNameResult>();
			foreach (XSegment loadedSegment in this.Segments.GetLoadedSegments())
			{
				if (loadedSegment.Description.IndexOf(searchText, 0, StringComparison.InvariantCultureIgnoreCase) > -1)
				{
					finPartByNameResults.Add(new FinPartByNameResult()
					{
						Part = loadedSegment,
						Path = loadedSegment.Name,
						Name = loadedSegment.Description,
						Type = "Segment"
					});
				}
				int num = 0;
				foreach (XBasePart childPart in loadedSegment.GetChildParts())
				{
					num++;
					if (childPart.Description.IndexOf(searchText, 0, StringComparison.InvariantCultureIgnoreCase) > -1)
					{
						finPartByNameResults.Add(new FinPartByNameResult()
						{
							Part = childPart,
							Path = string.Concat(loadedSegment.Name, "-", num),
							Name = childPart.Description,
							Type = "Field"
						});
					}
					int num1 = 0;
					foreach (XBasePart xBasePart in childPart.GetChildParts())
					{
						num1++;
						if (xBasePart.Description.IndexOf(searchText, 0, StringComparison.InvariantCultureIgnoreCase) > -1)
						{
							finPartByNameResult = new FinPartByNameResult()
							{
								Part = xBasePart,
								Path = string.Concat(new object[] { loadedSegment.Name, "-", num, ".", num1 }),
								Name = string.Concat(childPart.Description, ".", xBasePart.Description),
								Type = "Component"
							};
							finPartByNameResults.Add(finPartByNameResult);
						}
						int num2 = 0;
						foreach (XBasePart childPart1 in xBasePart.GetChildParts())
						{
							num2++;
							if (childPart1.Description.IndexOf(searchText, 0, StringComparison.InvariantCultureIgnoreCase) <= -1)
							{
								continue;
							}
							finPartByNameResult = new FinPartByNameResult()
							{
								Part = childPart1,
								Path = string.Concat(new object[] { loadedSegment.Name, "-", num, ".", num1, ".", num2 }),
								Name = string.Concat(childPart.Description, ".", xBasePart.Description, childPart1.Description),
								Type = "SubComponent"
							};
							finPartByNameResults.Add(finPartByNameResult);
						}
					}
				}
			}
			return finPartByNameResults;
		}

		public XBasePart GetComponent(Component component)
		{
			if (component != null)
			{
				XField field = this.GetField(component.Parent as Field);
				if (field != null)
				{
					List<XBasePart> childParts = field.GetChildParts();
					if (childParts.Count > component.Index)
					{
						return childParts[component.Index];
					}
				}
			}
			return null;
		}

		public XDataTable GetDataTable(string id)
		{
			if (id != "")
			{
				if (id.StartsWith("HL7"))
				{
					id = id.Substring(3);
				}
				if (this.DataTables.ContainsKey(id))
				{
					return this.DataTables[id];
				}
			}
			return null;
		}

		public string GetDataTableDescription(BasePart part)
		{
			if (part != null && part.XPart != null)
			{
				XDataTable dataTable = this.GetDataTable(part.DataTableId);
				string text = part.Text;
				if (dataTable != null && dataTable.Items != null && dataTable.Items.ContainsKey(text))
				{
					return dataTable.Items[text].Description.Replace("&", "&amp;");
				}
			}
			return "";
		}

		public string GetDataTableDisplayText(BasePart part)
		{
			TextBlock textBlock = new TextBlock();
			if (part != null && part.XPart != null)
			{
				XDataTable dataTable = this.GetDataTable(part.DataTableId);
				string text = part.Text;
				if (dataTable != null && dataTable.Items != null && dataTable.Items.ContainsKey(text))
				{
					return dataTable.Items[text].DisplayText;
				}
			}
			return "";
		}

		public string GetDataTypeName(string dataType)
		{
			if (dataType != null)
			{
				if (dataType == "AD")
				{
					return "Address";
				}
				if (dataType == "AUI")
				{
					return "Authorization Information";
				}
				if (dataType == "CCD")
				{
					return "Charge Code and Date";
				}
				if (dataType == "CCP")
				{
					return "Channel Calibration Parameters";
				}
				if (dataType == "CD")
				{
					return "Channel Definition";
				}
				if (dataType == "CE")
				{
					return "Coded Element";
				}
				if (dataType == "CF")
				{
					return "Coded Element with Formatted Values";
				}
				if (dataType == "CNE")
				{
					return "Coded with No Exceptions";
				}
				if (dataType == "CNN")
				{
					return "Composite ID and Name Simplified";
				}
				if (dataType == "CP")
				{
					return "Composite Price";
				}
				if (dataType == "CQ")
				{
					return "Composite Quantity with Units";
				}
				if (dataType == "CSU")
				{
					return "Channel Sensitivity";
				}
				if (dataType == "CWE")
				{
					return "Coded with Exceptions";
				}
				if (dataType == "CX")
				{
					return "Extended Composite ID + Check Digit";
				}
				if (dataType == "DDI")
				{
					return "Daily Deductible Information";
				}
				if (dataType == "DIN")
				{
					return "Date and Institution Name";
				}
				if (dataType == "DLD")
				{
					return "Discharge Location and Date";
				}
				if (dataType == "DLN")
				{
					return "Driver_s License Number";
				}
				if (dataType == "DLT")
				{
					return "Delta";
				}
				if (dataType == "DR")
				{
					return "Date/Time Range";
				}
				if (dataType == "DT")
				{
					return "Date";
				}
				if (dataType == "DTM")
				{
					return "Date/Time";
				}
				if (dataType == "DTN")
				{
					return "Day Type and Number";
				}
				if (dataType == "ED")
				{
					return "Encapsulated Data";
				}
				if (dataType == "EI")
				{
					return "Entity Identifier";
				}
				if (dataType == "EIP")
				{
					return "Entity Identifier Pair";
				}
				if (dataType == "ELD")
				{
					return "Error Location and Description";
				}
				if (dataType == "ERL")
				{
					return "Error Location";
				}
				if (dataType == "FC")
				{
					return "Financial Class";
				}
				if (dataType == "FN")
				{
					return "Family Name";
				}
				if (dataType == "FT")
				{
					return "Formatted Text Data";
				}
				if (dataType == "GTS")
				{
					return "General Timing Specification";
				}
				if (dataType == "HD")
				{
					return "Hierarchic Designator";
				}
				if (dataType == "ICD")
				{
					return "Insurance Certification Definition";
				}
				if (dataType == "ID")
				{
					return "Coded values for HL7 tables";
				}
				if (dataType == "IS")
				{
					return "Coded value for user-defined tables";
				}
				if (dataType == "JCC")
				{
					return "Job Code/Class";
				}
				if (dataType == "LA1")
				{
					return "Location with Address Variation 1";
				}
				if (dataType == "LA2")
				{
					return "Location with Address Variation 2";
				}
				if (dataType == "MA")
				{
					return "Multiplexed Array";
				}
				if (dataType == "MO")
				{
					return "Money";
				}
				if (dataType == "MOC")
				{
					return "Money and Code";
				}
				if (dataType == "MOP")
				{
					return "Money or Percentage";
				}
				if (dataType == "MSG")
				{
					return "Message Type";
				}
				if (dataType == "NA")
				{
					return "Numeric Array";
				}
				if (dataType == "NDL")
				{
					return "Name with Date and Location";
				}
				if (dataType == "NM")
				{
					return "Numeric";
				}
				if (dataType == "NR")
				{
					return "Numeric Range";
				}
				if (dataType == "OCD")
				{
					return "Occurrence Code and Date";
				}
				if (dataType == "OSD")
				{
					return "Order Sequence Definition";
				}
				if (dataType == "OSP")
				{
					return "Occurrence Span Code and Date";
				}
				if (dataType == "PIP")
				{
					return "Practitioner Institutional Privileges";
				}
				if (dataType == "PL")
				{
					return "Person Location";
				}
				if (dataType == "PLN")
				{
					return "Practitioner License ID Number";
				}
				if (dataType == "PPN")
				{
					return "Performing Person Time Stamp";
				}
				if (dataType == "PRL")
				{
					return "Parent Result Link";
				}
				if (dataType == "PT")
				{
					return "Processing Type";
				}
				if (dataType == "PTA")
				{
					return "Policy Type and Amount";
				}
				if (dataType == "QIP")
				{
					return "Query Input Parameter List";
				}
				if (dataType == "QSC")
				{
					return "Query Selection Criteria";
				}
				if (dataType == "RCD")
				{
					return "Row Column Definition";
				}
				if (dataType == "RFR")
				{
					return "Reference Range";
				}
				if (dataType == "RI")
				{
					return "Repeat Interval";
				}
				if (dataType == "RMC")
				{
					return "Room Coverage";
				}
				if (dataType == "RP")
				{
					return "Reference Pointer";
				}
				if (dataType == "RPT")
				{
					return "Repeat Pattern";
				}
				if (dataType == "SAD")
				{
					return "Street Address";
				}
				if (dataType == "SCV")
				{
					return "Scheduling Class Value Pair";
				}
				if (dataType == "SI")
				{
					return "Sequence ID";
				}
				if (dataType == "SN")
				{
					return "Structured Numeric";
				}
				if (dataType == "SPD")
				{
					return "Specialty Description";
				}
				if (dataType == "SPS")
				{
					return "Specimen Source";
				}
				if (dataType == "SRT")
				{
					return "Sort Order";
				}
				if (dataType == "ST")
				{
					return "String Data";
				}
				if (dataType == "TM")
				{
					return "Time";
				}
				if (dataType == "TQ")
				{
					return "Timing Quantity";
				}
				if (dataType == "TS")
				{
					return "Time Stamp";
				}
				if (dataType == "TX")
				{
					return "Text Data";
				}
				if (dataType == "UVC")
				{
					return "UB Value Code and Amount";
				}
				if (dataType == "VARIES")
				{
					return "Variable Datatype";
				}
				if (dataType == "VH")
				{
					return "Visiting Hours";
				}
				if (dataType == "VID")
				{
					return "Version Identifier";
				}
				if (dataType == "VR")
				{
					return "Value Range";
				}
				if (dataType == "WVI")
				{
					return "Channel Identifier";
				}
				if (dataType == "WVS")
				{
					return "Waveform Source";
				}
				if (dataType == "XAD")
				{
					return "Extended Address";
				}
				if (dataType == "XCN")
				{
					return "Extended ID and Name for Persons";
				}
				if (dataType == "XON")
				{
					return "Extended Name and ID for Organizations";
				}
				if (dataType == "XPN")
				{
					return "Extended Person Name";
				}
				if (dataType == "XTN")
				{
					return "Extended Telecommunication Number";
				}
			}
			return "";
		}

		internal static DescriptionManager GetDescriptionManager(string version)
		{
			if (DescriptionManager.AllVersions.ContainsKey(version))
			{
				return DescriptionManager.AllVersions[version];
			}
			if (Application.Current == null)
			{
				DescriptionManager.AllVersions[version] = new DescriptionManager(version);
			}
			else
			{
				Application.Current.Dispatcher.Invoke(() => DescriptionManager.AllVersions[version] = new DescriptionManager(version));
			}
			return DescriptionManager.AllVersions[version];
		}

		public string GetDescriptionWebPage(string path)
		{
			string str = "http://www.hl7soup.com/HL7TutorialMSH.html";
			if (path != null)
			{
				if (path == "MSH-1" || path == "MSH-2")
				{
					return string.Concat(str, "#MSH-1");
				}
				if (path == "MSH-3" || path == "MSH-4" || path == "MSH-5" || path == "MSH-6")
				{
					return string.Concat(str, "#MSH-3");
				}
				if (path == "MSH-7")
				{
					return string.Concat(str, "#MSH-7");
				}
				if (path == "MSH-8")
				{
					return string.Concat(str, "#MSH-8");
				}
				if (path == "MSH-9" || path == "MSH-9.1" || path == "MSH-9.2" || path == "MSH-9.3")
				{
					return string.Concat(str, "#MSH-9");
				}
				if (path == "MSH-10")
				{
					return string.Concat(str, "#MSH-10");
				}
				if (path == "MSH-11")
				{
					return string.Concat(str, "#MSH-11");
				}
				if (path == "MSH-12")
				{
					return string.Concat(str, "#MSH-12");
				}
				if (path == "MSH-13")
				{
					return string.Concat(str, "#MSH-13");
				}
				if (path == "MSH-14")
				{
					return string.Concat(str, "#MSH-14");
				}
				if (path == "MSH-15")
				{
					return string.Concat(str, "#MSH-15");
				}
				if (path == "MSH-16")
				{
					return string.Concat(str, "#MSH-16");
				}
				if (path == "MSH-17")
				{
					return string.Concat(str, "#MSH-17");
				}
				if (path == "MSH-18")
				{
					return string.Concat(str, "#MSH-18");
				}
				if (path == "MSH-19")
				{
					return string.Concat(str, "#MSH-19");
				}
				if (path == "MSH-20")
				{
					return string.Concat(str, "#MSH-20");
				}
				if (path == "MSH-21")
				{
					return string.Concat(str, "#MSH-21");
				}
			}
			return "";
		}

		public XField GetField(Field field)
		{
			return this.Fields.GetByName(field.GetDescriptionName());
		}

		private void GetMessageTypeAndDescription(Message message, ref XMessageType tmpMessageType, ref string tmpMessageTypeDescription)
		{
			string messageType = message.MessageType;
			if (string.IsNullOrEmpty(messageType))
			{
				tmpMessageTypeDescription = "";
				return;
			}
			tmpMessageType = this.MessageTypes.GetByName(messageType);
			if (tmpMessageType != null)
			{
				tmpMessageTypeDescription = tmpMessageType.Description;
				return;
			}
			string str = string.Concat(message.GetPartNiceText(new PathSplitter("MSH", 9, 1)), "_", message.GetPartNiceText(new PathSplitter("MSH", 9, 2)));
			if (str == "_")
			{
				str = "";
			}
			string partNiceText = message.GetPartNiceText(new PathSplitter("MSH", 9, 3));
			if (partNiceText == "")
			{
				partNiceText = this.GetMessageTypeMagically(str);
				if (partNiceText != "")
				{
					tmpMessageType = this.MessageTypes.GetByName(partNiceText);
				}
			}
			else
			{
				tmpMessageType = this.MessageTypes.GetByName(partNiceText);
			}
			tmpMessageTypeDescription = XMessageType.GetMessageTypeDescription(str);
		}

		public string GetMessageTypeDescription(Message message)
		{
			XMessageType xMessageType = null;
			string str = "";
			this.GetMessageTypeAndDescription(message, ref xMessageType, ref str);
			return str;
		}

		private string GetMessageTypeMagically(string messageType)
		{
			if (messageType != null)
			{
				if (messageType == "ADT_A04" || messageType == "ADT_A08" || messageType == "ADT_A13")
				{
					return "ADT_A01";
				}
				if (messageType == "ADT_A14" || messageType == "ADT_A28" || messageType == "ADT_A31")
				{
					return "ADT_A05";
				}
				if (messageType == "ADT_A07")
				{
					return "ADT_A06";
				}
				if (messageType == "ADT_A10" || messageType == "ADT_A11")
				{
					return "ADT_A09";
				}
				if (messageType == "ADT_A22" || messageType == "ADT_A23" || messageType == "ADT_A25" || messageType == "ADT_A26" || messageType == "ADT_A27" || messageType == "ADT_A29" || messageType == "ADT_A32" || messageType == "ADT_A33")
				{
					return "ADT_A21";
				}
				if (messageType == "ADT_A34" || messageType == "ADT_A35" || messageType == "ADT_A36" || messageType == "ADT_A46" || messageType == "ADT_A47" || messageType == "ADT_A48" || messageType == "ADT_A49")
				{
					return "ADT_A30";
				}
				if (messageType == "ADT_A40" || messageType == "ADT_A41" || messageType == "ADT_A42")
				{
					return "ADT_A39";
				}
				if (messageType == "ADT_A44")
				{
					return "ADT_A43";
				}
				if (messageType == "ADT_A51")
				{
					return "ADT_A50";
				}
				if (messageType == "ADT_A53")
				{
					return "ADT_A52";
				}
				if (messageType == "ADT_A55")
				{
					return "ADT_A54";
				}
				if (messageType == "ADT_A62")
				{
					return "ADT_A61";
				}
				if (messageType == "CRM_C02" || messageType == "CRM_C03" || messageType == "CRM_C04" || messageType == "CRM_C05" || messageType == "CRM_C06" || messageType == "CRM_C07" || messageType == "CRM_C08")
				{
					return "CRM_C01";
				}
				if (messageType == "CSU_C10" || messageType == "CSU_C11" || messageType == "CSU_C12")
				{
					return "CSU_C09";
				}
				if (messageType == "LSU_U13")
				{
					return "LSU_U12";
				}
				if (messageType == "MDM_T03" || messageType == "MDM_T05" || messageType == "MDM_T07" || messageType == "MDM_T09" || messageType == "MDM_T11")
				{
					return "MDM_T01";
				}
				if (messageType == "MDM_T04" || messageType == "MDM_T06" || messageType == "MDM_T08" || messageType == "MDM_T10")
				{
					return "MDM_T02";
				}
				if (messageType == "MFK_M02" || messageType == "MFK_M03" || messageType == "MFK_M04" || messageType == "MFK_M05" || messageType == "MFK_M06" || messageType == "MFK_M07" || messageType == "MFK_M08" || messageType == "MFK_M09" || messageType == "MFK_M10" || messageType == "MFK_M11")
				{
					return "MFK_M01";
				}
				if (messageType == "MFQ_M02" || messageType == "MFQ_M03" || messageType == "MFQ_M04" || messageType == "MFQ_M05" || messageType == "MFQ_M06")
				{
					return "MFQ_M01";
				}
				if (messageType == "MFR_M02" || messageType == "MFR_M03")
				{
					return "MFR_M01";
				}
				if (messageType == "PEX_P08")
				{
					return "PEX_P07";
				}
				if (messageType == "PGL_PC7" || messageType == "PGL_PC8")
				{
					return "PGL_PC6";
				}
				if (messageType == "PMU_B02")
				{
					return "PMU_B01";
				}
				if (messageType == "PMU_B05" || messageType == "PMU_B06")
				{
					return "PMU_B04";
				}
				if (messageType == "PPG_PCC" || messageType == "PPG_PCH" || messageType == "PPG_PCJ")
				{
					return "PPG_PCG";
				}
				if (messageType == "PPP_PCD")
				{
					return "PPP_PCB";
				}
				if (messageType == "PPR_PC2" || messageType == "PPR_PC3")
				{
					return "PPR_PC1";
				}
				if (messageType == "QBP_Q22" || messageType == "QBP_Q23" || messageType == "QBP_Q24" || messageType == "QBP_Q25")
				{
					return "QBP_Q21";
				}
				if (messageType == "QCN_J02")
				{
					return "QCN_J01";
				}
				if (messageType == "QRY_PC9" || messageType == "QRY_PCE" || messageType == "QRY_PCK")
				{
					return "QRY_PC4";
				}
				if (messageType == "QRY_Q26" || messageType == "QRY_Q27" || messageType == "QRY_Q28" || messageType == "QRY_Q29" || messageType == "QRY_Q30")
				{
					return "QRY_Q01";
				}
				if (messageType == "RDE_OO25")
				{
					return "RDE_O11";
				}
				if (messageType == "REF_I13" || messageType == "REF_I14" || messageType == "REF_I15")
				{
					return "REF_I12";
				}
				if (messageType == "RPA_I09" || messageType == "RPA_I10" || messageType == "RPA_I11")
				{
					return "RPA_I08";
				}
				if (messageType == "RPI_I04")
				{
					return "RPI_I01";
				}
				if (messageType == "RQA_I09" || messageType == "RQA_I10" || messageType == "RQA_I11")
				{
					return "RQA_I08";
				}
				if (messageType == "RQA_I06")
				{
					return "RQC_I05";
				}
				if (messageType == "RQI_I02" || messageType == "RQI_I03" || messageType == "RQI_I07")
				{
					return "RQI_I01";
				}
				if (messageType == "RRI_I13" || messageType == "RRI_I14" || messageType == "RRI_I15")
				{
					return "RRI_I12";
				}
				if (messageType == "RSP_K24")
				{
					return "RSP_K23";
				}
				if (messageType == "SDR_S36")
				{
					return "SDR_S31";
				}
				if (messageType == "SDR_SDR_S37")
				{
					return "SDR_S32";
				}
				if (messageType == "SIU_S13" || messageType == "SIU_S14" || messageType == "SIU_S15" || messageType == "SIU_S16" || messageType == "SIU_S17" || messageType == "SIU_S18" || messageType == "SIU_S19" || messageType == "SIU_S20" || messageType == "SIU_S21" || messageType == "SIU_S22" || messageType == "SIU_S23" || messageType == "SIU_S24" || messageType == "SIU_S26")
				{
					return "SIU_S12";
				}
				if (messageType == "SLR_S29" || messageType == "SLR_S30" || messageType == "SLR_S34" || messageType == "SLR_S35")
				{
					return "SLR_S28";
				}
				if (messageType == "SRM_S02" || messageType == "SRM_S03" || messageType == "SRM_S04" || messageType == "SRM_S05" || messageType == "SRM_S06" || messageType == "SRM_S07" || messageType == "SRM_S08" || messageType == "SRM_S09" || messageType == "SRM_S10" || messageType == "SRM_S11")
				{
					return "SRM_S01";
				}
				if (messageType == "SRR_S02" || messageType == "SRR_S03" || messageType == "SRR_S04" || messageType == "SRR_S05" || messageType == "SRR_S06" || messageType == "SRR_S07" || messageType == "SRR_S08" || messageType == "SRR_S09" || messageType == "SRR_S10" || messageType == "SRR_S11")
				{
					return "SRR_S01";
				}
				if (messageType == "TCU_U11")
				{
					return "TCU_U10";
				}
			}
			return "";
		}

		public SearchPathResult GetPathDescription(PathSplitter splitter)
		{
			SearchPathResult searchPathResult = new SearchPathResult();
			if (splitter.IsValid || splitter.Segment != null && splitter.ToString().Length <= 4)
			{
				XSegment byName = this.Segments.GetByName(splitter.Segment);
				if (byName == null)
				{
					this.Segments.LoadAllSegmentsWithoutMessageType();
					byName = this.Segments.GetByName(splitter.Segment);
				}
				if (byName != null)
				{
					searchPathResult.Segment = byName;
					if (!string.IsNullOrEmpty(splitter.Field))
					{
						int num = int.Parse(splitter.Field) - 1;
						List<XBasePart> childParts = byName.GetChildParts();
						if (childParts.Count > num && num > -1)
						{
							XBasePart item = childParts[num];
							searchPathResult.Field = (XField)item;
							if (!string.IsNullOrEmpty(splitter.Component))
							{
								int num1 = int.Parse(splitter.Component) - 1;
								List<XBasePart> xBaseParts = item.GetChildParts();
								if (xBaseParts.Count > num1 && num1 > -1)
								{
									XBasePart xBasePart = xBaseParts[num1];
									searchPathResult.Component = (XComponent)xBasePart;
									if (!string.IsNullOrEmpty(splitter.SubComponent))
									{
										int num2 = int.Parse(splitter.SubComponent) - 1;
										if (xBasePart.GetChildParts().Count > num2 && num2 > -1)
										{
											XBasePart item1 = xBasePart.GetChildParts()[num2];
											searchPathResult.Path = string.Concat(new string[] { splitter.Segment, "-", splitter.Field, ".", splitter.Component, ".", splitter.SubComponent });
											searchPathResult.SubComponent = item1;
											searchPathResult.Part = item1;
											return searchPathResult;
										}
									}
									searchPathResult.Path = string.Concat(new string[] { splitter.Segment, "-", splitter.Field, ".", splitter.Component });
									searchPathResult.Part = xBasePart;
									List<XBasePart> childParts1 = xBasePart.GetChildParts();
									if (childParts1 != null)
									{
										int num3 = 0;
										foreach (XBasePart childPart in childParts1)
										{
											num3++;
											searchPathResult.Children.Add(new SearchPathResultChild()
											{
												Path = string.Concat(searchPathResult.Path, ".", num3.ToString()),
												Description = childPart.Description
											});
										}
									}
									return searchPathResult;
								}
							}
							searchPathResult.Path = string.Concat(splitter.Segment, "-", splitter.Field);
							searchPathResult.Part = item;
							List<XBasePart> xBaseParts1 = item.GetChildParts();
							if (xBaseParts1 != null)
							{
								int num4 = 0;
								foreach (XBasePart childPart1 in xBaseParts1)
								{
									num4++;
									searchPathResult.Children.Add(new SearchPathResultChild()
									{
										Path = string.Concat(searchPathResult.Path, ".", num4.ToString()),
										Description = childPart1.Description
									});
								}
							}
							return searchPathResult;
						}
					}
					searchPathResult.Path = splitter.Segment;
					searchPathResult.Part = byName;
					List<XBasePart> childParts2 = byName.GetChildParts();
					if (childParts2 != null)
					{
						int num5 = 0;
						foreach (XBasePart xBasePart1 in childParts2)
						{
							num5++;
							searchPathResult.Children.Add(new SearchPathResultChild()
							{
								Path = string.Concat(searchPathResult.Path, "-", num5.ToString()),
								Description = xBasePart1.Description
							});
						}
					}
					return searchPathResult;
				}
			}
			return null;
		}

		public XSegment GetSegment(Segment segment)
		{
			return this.Segments.GetByName(segment.GetDescriptionName());
		}

		public XBasePart GetSubComponent(SubComponent subComponent)
		{
			if (subComponent != null)
			{
				XBasePart component = this.GetComponent(subComponent.Parent as Component);
				if (component != null)
				{
					List<XBasePart> childParts = component.GetChildParts();
					if (childParts.Count > subComponent.Index)
					{
						return childParts[subComponent.Index];
					}
				}
			}
			return null;
		}

		internal bool IsValidSegmentHeader(string header)
		{
			if (header.Length == 3 && header == header.ToUpper() && (this.Segments.GetByName(header) != null || header.StartsWith("Z")))
			{
				return true;
			}
			return false;
		}

		internal bool IsValidSegmentHeaderForCurrentMessageType(string header)
		{
			bool flag;
			if (header.Length == 3 && header == header.ToUpper())
			{
				if (this.SegmentsForCurrentMessageType != null)
				{
					List<XSegment>.Enumerator enumerator = this.SegmentsForCurrentMessageType.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							if (enumerator.Current.Name != header)
							{
								continue;
							}
							flag = true;
							return flag;
						}
						goto Label0;
					}
					finally
					{
						((IDisposable)enumerator).Dispose();
					}
					return flag;
				}
			Label0:
				if (header.StartsWith("Z"))
				{
					return true;
				}
			}
			return false;
		}

		public void LoadDataTables(bool useITKTables)
		{
			DescriptionManager.CreateAppdataDirectory();
			string str = this.customDataTypesFileName;
			Stream manifestResourceStream = null;
			if (useITKTables)
			{
				manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("HL7Soup.Workflow.HL7Descriptions.Xsd.DataTables.UK.CustomDataTypes.dat");
			}
			if (manifestResourceStream == null)
			{
				if (File.Exists(this.customDataTypesFileName))
				{
					manifestResourceStream = new FileStream(str, FileMode.Open, FileAccess.Read, FileShare.Read);
				}
				else
				{
					str = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, this.systemDataTypesFileName);
					if (File.Exists(str))
					{
						manifestResourceStream = new FileStream(str, FileMode.Open, FileAccess.Read, FileShare.Read);
					}
					else
					{
						manifestResourceStream = Assembly.GetAssembly(typeof(Message)).GetManifestResourceStream("HL7Soup.Workflow.SystemDataTypes.dat");
					}
				}
			}
			this.DataTables = (XDataTables)(new BinaryFormatter()).Deserialize(manifestResourceStream);
			manifestResourceStream.Close();
		}

		private void PopulateCurrentPart()
		{
			if (this.CurrentSubComponent != null)
			{
				this.CurrentPart = this.CurrentSubComponent;
				this.CurrentPartDescription = this.CurrentSubComponentDescription;
				return;
			}
			if (this.CurrentComponent != null)
			{
				this.CurrentPart = this.CurrentComponent;
				this.CurrentPartDescription = this.CurrentComponentDescription;
				return;
			}
			if (this.CurrentField != null)
			{
				this.CurrentPart = this.CurrentField;
				this.CurrentPartDescription = this.CurrentFieldDescription;
				return;
			}
			if (this.CurrentSegment == null)
			{
				return;
			}
			this.CurrentPart = this.CurrentSegment;
			this.CurrentPartDescription = this.CurrentSegmentDescription;
		}

		public void SaveDataTables()
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			DescriptionManager.CreateAppdataDirectory();
			using (Stream fileStream = new FileStream(this.customDataTypesFileName, FileMode.Create, FileAccess.Write, FileShare.None))
			{
				binaryFormatter.Serialize(fileStream, this.DataTables);
				fileStream.Close();
			}
		}

		private void SetCurrentDataTable(string id)
		{
			XDataTable dataTable = this.GetDataTable(id);
			if (dataTable != this.CurrentDataTable)
			{
				this.CurrentDataTable = dataTable;
			}
		}

		public void UpateDataTable(string Id, List<XDataTableItem> dt)
		{
			this.DataTables[Id].Items = new Dictionary<string, XDataTableItem>();
			foreach (XDataTableItem xDataTableItem in dt)
			{
				this.DataTables[Id].Items[xDataTableItem.Value] = xDataTableItem;
			}
			this.SaveDataTables();
			if (this.DataTableUpdated != null)
			{
				this.DataTableUpdated(this, null);
			}
		}

		public void UpdateCurrent(Message message)
		{
			string fullLocationCode;
			XMessageType xMessageType = null;
			string str = "";
			this.GetMessageTypeAndDescription(message, ref xMessageType, ref str);
			this.CurrentMessageType = xMessageType;
			this.CurrentMessageTypeDescription = str;
			this.CurrentSegment = null;
			this.CurrentField = null;
			this.CurrentComponent = null;
			this.CurrentSubComponent = null;
			this.CurrentPart = null;
			this.CurrentSegmentDescription = "";
			message.CurrentSegmentDescription = "";
			message.CurrentSegmentDocumentation = "";
			this.CurrentFieldDescription = "";
			this.CurrentComponentDescription = "";
			this.CurrentSubComponentDescription = "";
			string dataTableId = "";
			if (message.CurrentSegment != null)
			{
				this.CurrentSegment = this.Segments.GetByName(message.CurrentSegment.Header);
				if (this.CurrentSegment != null)
				{
					this.CurrentSegmentDescription = this.CurrentSegment.Description;
					message.CurrentSegmentDescription = this.CurrentSegment.Description;
					message.CurrentSegmentDocumentation = DocumentationManager.Instance.GetDocumentationForSegment(this.CurrentSegment);
					if (message.CurrentField != null && message.CurrentField.Index >= 0)
					{
						List<XBasePart> fieldsBelongingToSegment = this.Segments.GetFieldsBelongingToSegment(this.CurrentSegment.Name);
						if (fieldsBelongingToSegment.Count > message.CurrentField.Index)
						{
							this.CurrentField = (XField)fieldsBelongingToSegment[message.CurrentField.Index];
							if (this.CurrentField != null)
							{
								this.CurrentFieldDescription = this.CurrentField.Description;
								dataTableId = this.CurrentField.DataTableId;
								if (this.CurrentField.DataTableId == "")
								{
									fullLocationCode = message.CurrentField.FullLocationCode;
									if (fullLocationCode != null && fullLocationCode == "MSH-12")
									{
										dataTableId = "HL70104";
									}
								}
								if (message.CurrentComponent != null)
								{
									List<XBasePart> childParts = this.CurrentField.GetChildParts();
									if (childParts.Count > message.CurrentComponent.Index)
									{
										this.CurrentComponent = (XComponent)childParts[message.CurrentComponent.Index];
										if (this.CurrentComponent != null)
										{
											this.CurrentComponentDescription = this.CurrentComponent.Description;
											dataTableId = this.CurrentComponent.DataTableId;
											if (this.CurrentComponent.DataTableId == "")
											{
												fullLocationCode = message.CurrentComponent.FullLocationCode;
												if (fullLocationCode != null)
												{
													if (fullLocationCode == "MSH-9.1")
													{
														dataTableId = "HL70076";
													}
													else if (fullLocationCode == "MSH-9.2")
													{
														dataTableId = "HL70003";
													}
													else if (fullLocationCode == "MSH-9.3")
													{
														dataTableId = "HL70354";
													}
												}
											}
											if (message.CurrentSubComponent != null)
											{
												List<XBasePart> xBaseParts = this.CurrentComponent.GetChildParts();
												if (xBaseParts.Count > message.CurrentSubComponent.Index)
												{
													this.CurrentSubComponent = (XComponent)xBaseParts[message.CurrentSubComponent.Index];
													if (this.CurrentSubComponent != null)
													{
														this.CurrentSubComponentDescription = this.CurrentSubComponent.Description;
														dataTableId = this.CurrentSubComponent.DataTableId;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			this.SetCurrentDataTable(dataTableId);
			this.PopulateCurrentPart();
		}

		public event DataTableUpdatedEventHandler DataTableUpdated;
	}
}