using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public static class Helper
	{
		private static XNamespace aw;

		static Helper()
		{
			Helper.aw = "http://www.w3.org/2001/XMLSchema";
		}

		public static XElement GetComplexTypeByName(XElement fromElement, string name)
		{
			IEnumerable<XElement> xElements = 
				from el in fromElement.Elements(Helper.aw + "complexType")
				where (string)el.Attribute("name") == name
				select el;
			if (xElements == null)
			{
				return null;
			}
			return xElements.FirstOrDefault<XElement>();
		}

		public static XElement GetElementByName(XElement fromElement, string name)
		{
			IEnumerable<XElement> xElements = 
				from el in fromElement.Elements(Helper.aw + "element")
				where (string)el.Attribute("name") == name
				select el;
			if (xElements == null)
			{
				return null;
			}
			return xElements.FirstOrDefault<XElement>();
		}
	}
}