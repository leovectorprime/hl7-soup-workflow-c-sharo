using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XsdManager
	{
		private static XNamespace aw;

		private XDocument segmentDoc;

		private XDocument fieldDoc;

		private XDocument dataTypesDoc;

		private XDocument messagesDoc;

		public XElement DataTypesRootElement
		{
			get;
			set;
		}

		public XElement FieldsRootElement
		{
			get;
			set;
		}

		public XElement MessagesRootElement
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public XElement SegmentRootElement
		{
			get;
			set;
		}

		public bool UseITKTables
		{
			get;
			set;
		}

		static XsdManager()
		{
			XsdManager.aw = "http://www.w3.org/2001/XMLSchema";
		}

		public XsdManager(string path)
		{
			this.Path = path;
			System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\CustomSchema");
			this.segmentDoc = this.LoadDocument(path, "segments.xsd", "ITKEventsSegments-2012-05.xsd");
			this.fieldDoc = this.LoadDocument(path, "fields.xsd", "ITKEventsFields-2012-05.xsd");
			this.dataTypesDoc = this.LoadDocument(path, "datatypes.xsd", "ITKEventsDatatypes-2012-05.xsd");
			this.messagesDoc = this.LoadDocument(path, "messages.xsd", "");
			this.SegmentRootElement = this.segmentDoc.Elements(XsdManager.aw + "schema").FirstOrDefault<XElement>();
			this.FieldsRootElement = this.fieldDoc.Elements(XsdManager.aw + "schema").FirstOrDefault<XElement>();
			this.DataTypesRootElement = this.dataTypesDoc.Elements(XsdManager.aw + "schema").FirstOrDefault<XElement>();
			this.MessagesRootElement = this.messagesDoc.Elements(XsdManager.aw + "schema").FirstOrDefault<XElement>();
		}

		private XDocument LoadDocument(string path, string name, string altName)
		{
			string str = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\CustomSchema\\");
			string str1 = System.IO.Path.Combine(str, name);
			if (File.Exists(str1))
			{
				return XDocument.Load(str1);
			}
			if (altName != "")
			{
				str1 = System.IO.Path.Combine(str, altName);
				if (File.Exists(str1))
				{
					this.UseITKTables = true;
					return XDocument.Load(str1);
				}
			}
			return XDocument.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat(path, name)), LoadOptions.PreserveWhitespace);
		}

		internal XElement LoadDocument(string xsdName)
		{
			string str = string.Concat("ITKEvents_", xsdName, "-2012-05.xsd");
			string str1 = string.Concat(this.Path, xsdName, ".xsd");
			if (Assembly.GetExecutingAssembly().GetManifestResourceInfo(str1) != null)
			{
				XDocument xDocument = this.LoadDocument(this.Path, string.Concat(xsdName, ".xsd"), str);
				if (xDocument != null)
				{
					return xDocument.Elements(XsdManager.aw + "schema").FirstOrDefault<XElement>();
				}
			}
			return null;
		}
	}
}