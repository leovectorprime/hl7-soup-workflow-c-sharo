using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace HL7Soup.HL7Descriptions
{
	public class DocumentationManager
	{
		private static DocumentationManager instance;

		private Dictionary<string, string> segmentDescriptions = new Dictionary<string, string>();

		public static DocumentationManager Instance
		{
			get
			{
				if (DocumentationManager.instance == null)
				{
					DocumentationManager.instance = new DocumentationManager();
				}
				return DocumentationManager.instance;
			}
		}

		public Dictionary<string, string> SegmentDescriptions
		{
			get
			{
				Dictionary<string, string> strs = new Dictionary<string, string>();
				Dictionary<string, string> strs1 = strs;
				this.segmentDescriptions = strs;
				return strs1;
			}
		}

		private DocumentationManager()
		{
			this.LoadDocumentation();
		}

		public string GetDocumentationForSegment(XSegment CurrentSegment)
		{
			if (CurrentSegment == null || !this.segmentDescriptions.ContainsKey(CurrentSegment.Name))
			{
				return "";
			}
			return this.segmentDescriptions[CurrentSegment.Name].TrimStart(Environment.NewLine.ToCharArray()).Trim().Replace("  ", " ").Replace(string.Concat(Environment.NewLine, "  "), Environment.NewLine).Replace(Environment.NewLine, string.Concat(Environment.NewLine, Environment.NewLine));
		}

		private void LoadDocumentation()
		{
			Stream manifestResourceStream = this.GetType().Module.Assembly.GetManifestResourceStream("HL7Soup.Workflow.HL7Descriptions.SegmentDescriptions.xml");
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(manifestResourceStream);
			manifestResourceStream.Close();
			foreach (XmlNode childNode in xmlDocument.ChildNodes[0].ChildNodes)
			{
				this.segmentDescriptions.Add(childNode.Name, childNode.InnerText);
			}
		}
	}
}