using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	[Serializable]
	public class XDataTables : Dictionary<string, XDataTable>
	{
		public XDataTables()
		{
		}

		public XDataTables(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public static XDataTables CreateFromCSV(string path)
		{
			XDataTables xDataTable = new XDataTables();
			XDataTable xDataTable1 = null;
			string[] strArrays = File.ReadAllLines(path);
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string[] strArrays1 = strArrays[i].Split(",".ToArray<char>());
				if (!string.IsNullOrWhiteSpace(strArrays1[2]))
				{
					if (xDataTable1 != null)
					{
						xDataTable.Add(xDataTable1.Id, xDataTable1);
					}
					xDataTable1 = new XDataTable()
					{
						Name = strArrays1[2],
						Type = strArrays1[0]
					};
				}
				else
				{
					XDataTableItem xDataTableItem = new XDataTableItem()
					{
						Value = strArrays1[3]
					};
					if (strArrays1.Count<string>() > 5)
					{
						for (int j = 5; j < (int)strArrays1.Length; j++)
						{
							strArrays1[4] = string.Concat(strArrays1[4], strArrays1[j]);
						}
					}
					xDataTableItem.Description = strArrays1[4].Replace("\"", "");
					string str = string.Concat("0000", strArrays1[1]);
					xDataTable1.Id = str.Substring(str.Length - 4);
					if (xDataTableItem.Value != "�")
					{
						xDataTable1.Items.Add(xDataTableItem.Value, xDataTableItem);
					}
				}
			}
			xDataTable.Add(xDataTable1.Id, xDataTable1);
			return xDataTable;
		}

		internal static XDataTables CreateFromXML(string filePath)
		{
			XDataTables xDataTable;
			try
			{
				XDataTables xDataTable1 = new XDataTables();
				foreach (XElement xElement in XDocument.Load(filePath).Element("vocabularyIndex").Elements())
				{
					string value = xElement.Attribute("name").Value;
					int num = value.IndexOf("-");
					string str = xElement.Attribute("id").Value;
					XDataTable xDataTable2 = new XDataTable()
					{
						Name = value.Substring(num + 1),
						Type = "User",
						Id = value.Substring(0, num)
					};
					XDocument xDocument = null;
					try
					{
						xDocument = XDocument.Load(string.Concat("C:\\Users\\Jason\\Desktop\\HL7V2_DMS_V1.1_Final\\HL7V2_DMS_V1.1_Final\\Vocabulary\\HL7v2\\XML\\", value.Substring(num + 1).Replace("/", "_").Replace("'", ""), "-v1.0.xml"));
					}
					catch (Exception exception)
					{
					}
					foreach (XElement xElement1 in xDocument.Element("vocabulary").Elements())
					{
						if (xElement1.Name != "concept")
						{
							continue;
						}
						XDataTableItem xDataTableItem = new XDataTableItem()
						{
							Value = xElement1.Attribute("code").Value,
							Description = xElement1.Element("displayName").Value
						};
						xDataTable2.Items.Add(xDataTableItem.Value, xDataTableItem);
					}
					xDataTable1.Add(xDataTable2.Id, xDataTable2);
				}
				xDataTable = xDataTable1;
			}
			catch (Exception exception1)
			{
				throw;
			}
			return xDataTable;
		}
	}
}