using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.HL7Descriptions
{
	public class SearchPathResultChild
	{
		public string Description
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public SearchPathResultChild()
		{
		}
	}
}