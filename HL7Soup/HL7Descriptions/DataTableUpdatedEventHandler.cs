using System;

namespace HL7Soup.HL7Descriptions
{
	public delegate void DataTableUpdatedEventHandler(object sender, EventArgs e);
}