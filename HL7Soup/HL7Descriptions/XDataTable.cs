using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.HL7Descriptions
{
	[Serializable]
	public class XDataTable : INotifyPropertyChanged
	{
		private Dictionary<string, XDataTableItem> items;

		private string type;

		private string id;

		private string name;

		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("Id"));
				}
			}
		}

		public Dictionary<string, XDataTableItem> Items
		{
			get
			{
				return this.items;
			}
			set
			{
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ObservableItems"));
				}
			}
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("Name"));
				}
			}
		}

		public ObservableCollection<XDataTableItem> ObservableItems
		{
			get
			{
				if (this.items == null || this.items.Values == null)
				{
					return null;
				}
				return new ObservableCollection<XDataTableItem>(this.items.Values.ToList<XDataTableItem>());
			}
		}

		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("Type"));
				}
			}
		}

		public XDataTable()
		{
			this.items = new Dictionary<string, XDataTableItem>();
		}

		public override string ToString()
		{
			return string.Concat(this.Id, " - ", this.Name);
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}