using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XFields
	{
		private static XNamespace aw;

		private Dictionary<string, XField> fields = new Dictionary<string, XField>();

		public HL7Soup.HL7Descriptions.XsdManager XsdManager
		{
			get;
			set;
		}

		static XFields()
		{
			XFields.aw = "http://www.w3.org/2001/XMLSchema";
		}

		public XFields(HL7Soup.HL7Descriptions.XsdManager xsdManager)
		{
			this.XsdManager = xsdManager;
		}

		public XField GetByName(string name)
		{
			if (!this.fields.ContainsKey(name))
			{
				XField fieldByName = XFields.GetFieldByName(this.XsdManager, name);
				if (fieldByName == null)
				{
					return null;
				}
				this.fields[name] = fieldByName;
			}
			return this.fields[name];
		}

		public static XField GetFieldByName(HL7Soup.HL7Descriptions.XsdManager xsdManager, string name)
		{
			XElement elementByName = Helper.GetElementByName(xsdManager.FieldsRootElement, name);
			if (elementByName == null)
			{
				return null;
			}
			return new XField(xsdManager, elementByName, 0, 0);
		}

		public List<XField> GetLoadedFields()
		{
			return this.fields.Values.ToList<XField>();
		}
	}
}