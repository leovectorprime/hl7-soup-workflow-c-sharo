using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public abstract class XBasePart
	{
		private static XNamespace aw;

		private static XNamespace hl7ns;

		private static bool hl7nsSet;

		private XElement contentElement;

		protected XElement sequenceHostingElement;

		private string description = "";

		private string dataTableId = "";

		private string dataTypeName = "";

		private bool sequenceElementsAreNull;

		private IEnumerable<XElement> sequenceElements;

		protected List<XBasePart> childParts = new List<XBasePart>();

		private List<XPartReference> childPartReferences = new List<XPartReference>();

		public XElement BasePartElement
		{
			get;
			set;
		}

		public abstract XElement ChildRootElement
		{
			get;
		}

		public string ContentTypeName
		{
			get;
			set;
		}

		public virtual string DataTableId
		{
			get
			{
				if (!string.IsNullOrEmpty(this.dataTableId))
				{
					return this.dataTableId;
				}
				if (this.ContentElement() == null)
				{
					this.dataTableId = "";
					return this.dataTableId;
				}
				XElement xElement = this.ContentElement().Elements(XBasePart.aw + "annotation").FirstOrDefault<XElement>();
				if (xElement != null)
				{
					XElement xElement1 = xElement.Elements(XBasePart.aw + "appinfo").FirstOrDefault<XElement>();
					if (xElement1 != null)
					{
						XElement xElement2 = xElement1.Elements(XBasePart.hl7ns + "Table").FirstOrDefault<XElement>();
						if (xElement2 != null)
						{
							this.dataTableId = xElement2.Value;
						}
						else if (this.childParts != null && this.childParts.Count > 0)
						{
							xElement = this.childParts[0].ContentElement().Elements(XBasePart.aw + "annotation").FirstOrDefault<XElement>();
							if (xElement != null)
							{
								xElement1 = xElement.Elements(XBasePart.aw + "appinfo").FirstOrDefault<XElement>();
								if (xElement1 != null)
								{
									xElement2 = xElement1.Elements(XBasePart.hl7ns + "Table").FirstOrDefault<XElement>();
									if (xElement2 != null)
									{
										this.dataTableId = xElement2.Value;
									}
								}
							}
						}
					}
				}
				return this.dataTableId;
			}
		}

		public virtual string DataTypeName
		{
			get
			{
				if (!string.IsNullOrEmpty(this.dataTypeName))
				{
					return this.dataTypeName;
				}
				if (this.ContentElement() == null)
				{
					this.dataTypeName = "ST";
					return this.dataTypeName;
				}
				XElement xElement = this.ContentElement().Elements(XBasePart.aw + "complexContent").FirstOrDefault<XElement>() ?? this.ContentElement().Elements(XBasePart.aw + "simpleContent").FirstOrDefault<XElement>();
				if (xElement != null)
				{
					XElement xElement1 = xElement.Elements(XBasePart.aw + "extension").FirstOrDefault<XElement>();
					if (xElement1 != null)
					{
						XAttribute xAttribute = xElement1.Attribute("base");
						if (xAttribute != null)
						{
							this.dataTypeName = xAttribute.Value;
						}
					}
				}
				return this.dataTypeName;
			}
		}

		public virtual string Description
		{
			get
			{
				if (!string.IsNullOrEmpty(this.description))
				{
					return this.description;
				}
				if (this.ContentElement() == null)
				{
					this.description = "Missing Def";
					return this.description;
				}
				XElement xElement = this.ContentElement().Elements(XBasePart.aw + "annotation").FirstOrDefault<XElement>();
				if (xElement != null)
				{
					XElement xElement1 = xElement.Elements(XBasePart.aw + "documentation").FirstOrDefault<XElement>();
					if (xElement1 != null)
					{
						this.description = xElement1.Value;
					}
				}
				return this.description;
			}
		}

		public int Max
		{
			get;
			set;
		}

		public int Min
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public abstract XElement RootElement
		{
			get;
		}

		protected XsdManager xsdManager
		{
			get;
			set;
		}

		static XBasePart()
		{
			XBasePart.aw = "http://www.w3.org/2001/XMLSchema";
			XBasePart.hl7ns = "urn:com.sun:encoder-hl7-1.0";
			XBasePart.hl7nsSet = false;
		}

		public XBasePart(XsdManager xsdManager, XElement basePartElement, int min, int max)
		{
			this.xsdManager = xsdManager;
			if (!XBasePart.hl7nsSet && xsdManager.FieldsRootElement != null && xsdManager.FieldsRootElement.Name != null && xsdManager.FieldsRootElement.FirstAttribute != null)
			{
				foreach (XAttribute xAttribute in xsdManager.FieldsRootElement.Attributes())
				{
					if (xAttribute.Name.LocalName != "hl7")
					{
						continue;
					}
					XBasePart.hl7ns = xAttribute.Value;
					XBasePart.hl7nsSet = true;
				}
			}
			this.BasePartElement = basePartElement;
			if (this.BasePartElement != null)
			{
				this.Name = this.BasePartElement.Attributes("name").First<XAttribute>().Value.ToString();
				this.ContentTypeName = this.BasePartElement.Attributes("type").First<XAttribute>().Value.ToString();
			}
			this.Min = min;
			this.Max = max;
		}

		public XElement ContentElement()
		{
			if (this.contentElement == null)
			{
				this.contentElement = Helper.GetComplexTypeByName(this.RootElement, this.ContentTypeName);
			}
			return this.contentElement;
		}

		public List<XPartReference> GetChildPartReferences()
		{
			if (this.childPartReferences.Count > 0)
			{
				return this.childPartReferences;
			}
			if (this.GetSequenceElements() != null)
			{
				ConcurrentQueue<XPartReference> xPartReferences = new ConcurrentQueue<XPartReference>();
				int num = 0;
				foreach (XElement sequenceElement in this.GetSequenceElements())
				{
					num++;
					XPartReference xPartReference = new XPartReference();
					XAttribute xAttribute = sequenceElement.Attribute("ref") ?? sequenceElement.Attribute("name");
					xPartReference.RefName = xAttribute.Value;
					XAttribute xAttribute1 = sequenceElement.Attribute("minOccurs");
					if (xAttribute1 != null)
					{
						xPartReference.Min = int.Parse(xAttribute1.Value);
					}
					XAttribute xAttribute2 = sequenceElement.Attribute("maxOccurs");
					if (xAttribute2 == null)
					{
						xPartReference.Max = 2147483647;
					}
					else if (xAttribute2.Value != "unbounded")
					{
						xPartReference.Max = int.Parse(xAttribute2.Value);
					}
					else
					{
						xPartReference.Max = 2147483647;
					}
					int num1 = 0;
					int.TryParse(xPartReference.RefName.Substring(4), out num1);
					while (num1 > num)
					{
						xPartReferences.Enqueue(new XPartReference()
						{
							RefName = string.Concat(xPartReference.RefName.Substring(0, 3), ".", num.ToString())
						});
						num++;
					}
					xPartReferences.Enqueue(xPartReference);
				}
				this.childPartReferences = xPartReferences.ToList<XPartReference>();
			}
			return this.childPartReferences;
		}

		public List<XBasePart> GetChildParts()
		{
			if (this.childParts.Count > 0)
			{
				return this.childParts;
			}
			this.childParts = this.GetChildParts(this.GetChildPartReferences());
			return this.childParts;
		}

		public abstract List<XBasePart> GetChildParts(List<XPartReference> partReferences);

		protected IEnumerable<XElement> GetSequenceElements()
		{
			if (this.sequenceElementsAreNull)
			{
				return null;
			}
			if (this.sequenceElements == null && this.SequenceHostingElement() != null)
			{
				XElement xElement = this.SequenceHostingElement().Element(XBasePart.aw + "sequence");
				if (xElement != null)
				{
					this.sequenceElements = 
						from el in xElement.Elements(XBasePart.aw + "element")
						select el;
				}
			}
			this.sequenceElementsAreNull = this.sequenceElements == null;
			return this.sequenceElements;
		}

		public abstract XElement SequenceHostingElement();

		public override string ToString()
		{
			return string.Format("name = {0}, description = {1}, datatypeName = {2}, min = {3}, max = {4}", new object[] { this.Name, this.Description, this.DataTypeName, this.Min, this.Max });
		}
	}
}