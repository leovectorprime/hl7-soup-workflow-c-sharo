using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace HL7Soup.HL7Descriptions
{
	public class XMessageTypes
	{
		private static XNamespace aw;

		private Dictionary<string, XMessageType> messageTypes = new Dictionary<string, XMessageType>();

		private List<string> allMessageTypes = new List<string>();

		public HL7Soup.HL7Descriptions.XsdManager XsdManager
		{
			get;
			set;
		}

		static XMessageTypes()
		{
			XMessageTypes.aw = "http://www.w3.org/2001/XMLSchema";
		}

		public XMessageTypes(HL7Soup.HL7Descriptions.XsdManager xsdManager)
		{
			this.XsdManager = xsdManager;
		}

		public List<string> GetAllMessageTypes()
		{
			if (this.allMessageTypes.Count == 0)
			{
				XElement xElement = (
					from ele in this.XsdManager.MessagesRootElement.Elements(XMessageTypes.aw + "group")
					select ele).FirstOrDefault<XElement>();
				if (xElement != null)
				{
					XElement xElement1 = (
						from ele in xElement.Elements(XMessageTypes.aw + "choice")
						select ele).FirstOrDefault<XElement>();
					if (xElement1 != null)
					{
						foreach (XElement xElement2 in 
							from el in xElement1.Elements(XMessageTypes.aw + "element")
							select el)
						{
							this.allMessageTypes.Add(xElement2.Attribute("ref").Value);
						}
					}
				}
			}
			return this.allMessageTypes;
		}

		public XMessageType GetByName(string name)
		{
			name = name.Replace("^", "_");
			if (!this.messageTypes.ContainsKey(name))
			{
				if (XMessageTypes.GetElementByName(this.XsdManager.MessagesRootElement, name) == null)
				{
					return null;
				}
				XElement xElement = this.XsdManager.LoadDocument(name);
				if (xElement == null)
				{
					return null;
				}
				XMessageType messageTypeByName = XMessageTypes.GetMessageTypeByName(this.XsdManager, xElement, name);
				if (messageTypeByName == null)
				{
					return null;
				}
				this.messageTypes[name] = messageTypeByName;
			}
			return this.messageTypes[name];
		}

		public static XElement GetElementByName(XElement rootElement, string name)
		{
			XElement xElement = (
				from ele in rootElement.Elements(XMessageTypes.aw + "group")
				select ele).FirstOrDefault<XElement>();
			if (xElement != null)
			{
				XElement xElement1 = (
					from ele in xElement.Elements(XMessageTypes.aw + "choice")
					select ele).FirstOrDefault<XElement>();
				if (xElement1 != null)
				{
					IEnumerable<XElement> xElements = 
						from el in xElement1.Elements(XMessageTypes.aw + "element")
						where (string)el.Attribute("ref") == name
						select el;
					if (xElements != null)
					{
						return xElements.FirstOrDefault<XElement>();
					}
				}
			}
			return null;
		}

		public List<XMessageType> GetLoadedMessageTypes()
		{
			return this.messageTypes.Values.ToList<XMessageType>();
		}

		public static XMessageType GetMessageTypeByName(HL7Soup.HL7Descriptions.XsdManager xsdManager, XElement rootElement, string name)
		{
			XElement elementByName = Helper.GetElementByName(rootElement, name);
			if (elementByName == null)
			{
				return null;
			}
			return new XMessageType(xsdManager, rootElement, elementByName, 0, 0);
		}
	}
}