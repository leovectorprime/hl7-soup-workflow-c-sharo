using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.HL7Descriptions
{
	public class XPartReference
	{
		public int Max
		{
			get;
			set;
		}

		public int Min
		{
			get;
			set;
		}

		public string RefName
		{
			get;
			set;
		}

		public XPartReference()
		{
		}
	}
}