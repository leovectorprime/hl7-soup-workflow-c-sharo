using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.HL7Descriptions
{
	public class FinPartByNameResult
	{
		public string Name
		{
			get;
			set;
		}

		public XBasePart Part
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public FinPartByNameResult()
		{
		}
	}
}