using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class PositionInTheDocument
	{
		public int Length
		{
			get;
			set;
		}

		public int Start
		{
			get;
			set;
		}

		public PositionInTheDocument(int start, int length)
		{
			this.Start = start;
			this.Length = length;
		}
	}
}