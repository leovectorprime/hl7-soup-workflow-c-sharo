using System;

namespace HL7Soup.Extensions
{
	public enum SettingTypes
	{
		Activity,
		Sender,
		Receiver,
		Filter,
		Transformer,
		MessageType,
		EditorSendingBehaviour,
		AutomaticSender,
		SourceProvider,
		ClientResponseDestination
	}
}