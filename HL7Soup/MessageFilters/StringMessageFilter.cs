using HL7Soup;
using HL7Soup.HL7Descriptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageFilters
{
	[Serializable]
	public class StringMessageFilter : MessageFilter
	{
		private string[] inListItems;

		private string inListCurrentValue;

		public bool CaseSensitive
		{
			get;
			set;
		}

		public StringMessageFilterComparers Comparer
		{
			get;
			set;
		}

		public override string CriteriaText
		{
			get
			{
				string enumDescription = Helpers.GetEnumDescription(this.Comparer);
				if (this.Comparer == StringMessageFilterComparers.Empty)
				{
					return "empty";
				}
				if (this.Comparer == StringMessageFilterComparers.InMessage)
				{
					return "in messsage";
				}
				if (this.Comparer == StringMessageFilterComparers.InDataTable)
				{
					return "in data table";
				}
				if (this.Comparer == StringMessageFilterComparers.IsTitleCase)
				{
					return "in Title Case";
				}
				if (this.Comparer == StringMessageFilterComparers.IsUpperCase)
				{
					return "in UPPERCASE";
				}
				if (this.Comparer == StringMessageFilterComparers.IsLowerCase)
				{
					return "in lowercase";
				}
				return string.Concat(enumDescription, " ", this.StringValue.Substring(0, Math.Min(20, this.StringValue.Length)), (this.StringValue.Length > 20 ? "..." : ""));
			}
		}

		public string StringValue
		{
			get
			{
				if (base.Value == null)
				{
					return "";
				}
				return base.Value.ToString();
			}
		}

		public StringMessageFilter()
		{
		}

		public override bool Compare(BasePart part)
		{
			if (part != null)
			{
				return this.Compare(part.Text, part);
			}
			if (this.Comparer != StringMessageFilterComparers.InMessage)
			{
				return false;
			}
			return false.Invert(base.Not);
		}

		public override bool Compare(string valueText)
		{
			if (valueText != null)
			{
				return this.Compare(valueText, null);
			}
			if (this.Comparer != StringMessageFilterComparers.InMessage)
			{
				return false;
			}
			return false.Invert(base.Not);
		}

		private bool Compare(string valueText, BasePart part)
		{
			if (this.Comparer == StringMessageFilterComparers.InMessage)
			{
				return true.Invert(base.Not);
			}
			string str = valueText;
			StringComparison stringComparison = StringComparison.InvariantCultureIgnoreCase;
			if (this.CaseSensitive)
			{
				stringComparison = StringComparison.InvariantCulture;
			}
			switch (this.Comparer)
			{
				case StringMessageFilterComparers.Equals:
				{
					return this.StringValue.Equals(str.ToString(), stringComparison).Invert(base.Not);
				}
				case StringMessageFilterComparers.Contains:
				{
					if (this.CaseSensitive)
					{
						return str.ToString().Contains(this.StringValue).Invert(base.Not);
					}
					return (CultureInfo.InvariantCulture.CompareInfo.IndexOf(str.ToString(), this.StringValue, CompareOptions.IgnoreCase) >= 0).Invert(base.Not);
				}
				case StringMessageFilterComparers.StartsWith:
				{
					return str.ToString().StartsWith(this.StringValue, stringComparison).Invert(base.Not);
				}
				case StringMessageFilterComparers.EndsWith:
				{
					return str.ToString().EndsWith(this.StringValue, stringComparison).Invert(base.Not);
				}
				case StringMessageFilterComparers.LengthGreaterThan:
				{
					return (str.ToString().Length > int.Parse((base.Value != null ? base.Value.ToString() : "0"))).Invert(base.Not);
				}
				case StringMessageFilterComparers.LengthLessThan:
				{
					return (str.ToString().Length < int.Parse((base.Value != null ? base.Value.ToString() : "0"))).Invert(base.Not);
				}
				case StringMessageFilterComparers.Empty:
				{
					return string.IsNullOrWhiteSpace(str.ToString()).Invert(base.Not);
				}
				case StringMessageFilterComparers.InMessage:
				{
					return true;
				}
				case StringMessageFilterComparers.InDataTable:
				{
					if (part == null)
					{
						throw new Exception("Sorry, filters checking if the value is in a datatable are not allowed in a workflow.");
					}
					if (!part.HasDataTable || str.ToString() == "" || str.ToString() == "\"\"")
					{
						return false;
					}
					bool flag = false;
					XDataTable getCurrentDataTable = part.GetCurrentDataTable;
					if (getCurrentDataTable == null || getCurrentDataTable.Items.Count <= 0)
					{
						return false;
					}
					foreach (KeyValuePair<string, XDataTableItem> item in part.GetCurrentDataTable.Items)
					{
						if (!item.Key.Equals(str.ToString(), stringComparison))
						{
							continue;
						}
						flag = true;
						return flag.Invert(base.Not);
					}
					return flag.Invert(base.Not);
				}
				case StringMessageFilterComparers.IsTitleCase:
				{
					if (string.IsNullOrWhiteSpace(str))
					{
						return false;
					}
					return Helpers.IsTitleCase(str.ToString()).Invert(base.Not);
				}
				case StringMessageFilterComparers.IsUpperCase:
				{
					if (string.IsNullOrWhiteSpace(str))
					{
						return false;
					}
					return Helpers.IsUpperCase(str.ToString()).Invert(base.Not);
				}
				case StringMessageFilterComparers.IsLowerCase:
				{
					if (string.IsNullOrWhiteSpace(str))
					{
						return false;
					}
					return Helpers.IsLowerCase(str.ToString()).Invert(base.Not);
				}
				case StringMessageFilterComparers.InList:
				{
					if (this.inListCurrentValue != this.StringValue)
					{
						this.inListItems = this.StringValue.Split(new char[] { ',' });
						this.inListCurrentValue = this.StringValue;
					}
					if (this.inListItems == null || this.inListItems.Length == 0)
					{
						return false;
					}
					return this.inListItems.Any<string>((string x) => x.Equals(str.ToString(), stringComparison)).Invert(base.Not);
				}
				default:
				{
					return true;
				}
			}
		}

		public override bool ComparerUsesValue()
		{
			StringMessageFilterComparers comparer = this.Comparer;
			if (comparer > StringMessageFilterComparers.LengthLessThan && comparer != StringMessageFilterComparers.InList)
			{
				return false;
			}
			return true;
		}

		public override string ToString()
		{
			return string.Concat((base.IsConjunctionVisible ? string.Concat(Enum.GetName(typeof(Conjunctions), base.Conjunction), " ") : ""), base.Path, " ", this.CriteriaText);
		}

		public override void Validate()
		{
			base.IsValid = true;
			base.Validate();
			bool isValid = base.IsValid;
			if (isValid && (this.Comparer == StringMessageFilterComparers.LengthGreaterThan || this.Comparer == StringMessageFilterComparers.LengthLessThan))
			{
				int num = -1;
				if (!int.TryParse(this.StringValue, out num))
				{
					isValid = false;
				}
			}
			if (base.IsValid != isValid)
			{
				base.IsValid = isValid;
			}
		}
	}
}