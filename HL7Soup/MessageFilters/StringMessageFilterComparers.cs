using System;
using System.ComponentModel;

namespace HL7Soup.MessageFilters
{
	public enum StringMessageFilterComparers
	{
		[Description("equal to")]
		Equals,
		[Description("containing")]
		Contains,
		[Description("starting with")]
		StartsWith,
		[Description("ending with")]
		EndsWith,
		[Description("longer than")]
		LengthGreaterThan,
		[Description("shorter than")]
		LengthLessThan,
		[Description("empty")]
		Empty,
		[Description("in message")]
		InMessage,
		[Description("in data table")]
		InDataTable,
		[Description("in Title Case")]
		IsTitleCase,
		[Description("in UPPER case")]
		IsUpperCase,
		[Description("in lower case")]
		IsLowerCase,
		[Description("in list (csv)")]
		InList
	}
}