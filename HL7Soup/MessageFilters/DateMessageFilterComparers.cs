using System;
using System.ComponentModel;

namespace HL7Soup.MessageFilters
{
	public enum DateMessageFilterComparers
	{
		[Description("=")]
		Equals,
		[Description(">")]
		GreaterThan,
		[Description("<")]
		LessThan,
		[Description(">=")]
		GreaterThanOrEqualTo,
		[Description("<=")]
		LessThanOrEqualTo,
		[Description("empty")]
		Empty,
		[Description("in message")]
		InMessage,
		[Description("invalid date")]
		InvalidDate
	}
}