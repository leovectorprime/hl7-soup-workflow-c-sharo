using System;
using System.ComponentModel;

namespace HL7Soup.MessageFilters
{
	public enum ValidMessageFilterComparers
	{
		[Description("message is valid")]
		Valid,
		[Description("message is invalid")]
		Invalid
	}
}