using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageFilters
{
	[Serializable]
	public class ValidMessageFilter : MessageFilter
	{
		public ValidMessageFilterComparers Comparer
		{
			get;
			set;
		}

		public override string CriteriaText
		{
			get
			{
				if (this.Comparer == ValidMessageFilterComparers.Valid)
				{
					return "valid message";
				}
				return "invalid message";
			}
		}

		public ValidMessageFilter()
		{
		}

		public override bool Compare(string valueText)
		{
			throw new NotImplementedException();
		}

		public override bool Compare(BasePart part)
		{
			if (part == null)
			{
				if (this.Comparer == ValidMessageFilterComparers.Invalid)
				{
					return true;
				}
				return false;
			}
			if (this.Comparer != ValidMessageFilterComparers.Invalid)
			{
				return part.Message.IsValid;
			}
			return !part.Message.IsValid;
		}

		public override string ToString()
		{
			return string.Concat(base.Path, " ", this.CriteriaText);
		}

		public override void Validate()
		{
		}
	}
}