using System;
using System.ComponentModel;

namespace HL7Soup.MessageFilters
{
	public enum Conjunctions
	{
		[Description("and")]
		And,
		[Description("or")]
		Or
	}
}