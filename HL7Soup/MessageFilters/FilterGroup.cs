using HL7Soup;
using System;

namespace HL7Soup.MessageFilters
{
	[Serializable]
	public class FilterGroup : MessageFilter
	{
		public override string CriteriaText
		{
			get
			{
				return Helpers.GetEnumDescription(base.Conjunction);
			}
		}

		public FilterGroup()
		{
		}

		public override bool Compare(BasePart part)
		{
			return true;
		}

		public override bool Compare(string valueText)
		{
			return true;
		}
	}
}