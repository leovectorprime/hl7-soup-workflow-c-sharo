using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.MessageFilters
{
	[Serializable]
	public abstract class MessageFilter : INotifyPropertyChanged
	{
		private string path;

		private string toPath;

		private bool isConjunctionVisible;

		private HL7Soup.PathSplitter pathSplitter;

		private bool includesRepeatFields = true;

		private bool isValid = true;

		private Guid fromSetting;

		private MessageTypes fromType;

		private MessageSourceDirection fromDirection = MessageSourceDirection.variable;

		private Guid toSetting;

		private MessageTypes toType;

		private MessageSourceDirection toDirection = MessageSourceDirection.variable;

		[NonSerialized]
		public IActivityHost ActivityHost;

		public Conjunctions Conjunction
		{
			get;
			set;
		}

		public abstract string CriteriaText
		{
			get;
		}

		public MessageSourceDirection FromDirection
		{
			get
			{
				return this.fromDirection;
			}
			set
			{
				this.fromDirection = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromDirection"));
				}
			}
		}

		public Dictionary<string, string> FromNamespaces
		{
			get;
			set;
		}

		public Guid FromSetting
		{
			get
			{
				return this.fromSetting;
			}
			set
			{
				this.fromSetting = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromSetting"));
				}
			}
		}

		public MessageTypes FromType
		{
			get
			{
				return this.fromType;
			}
			set
			{
				this.fromType = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FromType"));
				}
			}
		}

		public bool IncludesRepeatFields
		{
			get
			{
				return this.includesRepeatFields;
			}
			set
			{
				this.includesRepeatFields = value;
			}
		}

		public bool IsConjunctionVisible
		{
			get
			{
				return this.isConjunctionVisible;
			}
			set
			{
				if (this.isConjunctionVisible != value)
				{
					this.isConjunctionVisible = value;
					if (this.PropertyChanged != null)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("IsConjunctionVisible"));
					}
				}
			}
		}

		public bool IsValid
		{
			get
			{
				return this.isValid;
			}
			set
			{
				this.isValid = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
				}
			}
		}

		public bool JustReceived
		{
			get;
			set;
		}

		public bool Not
		{
			get;
			set;
		}

		public string NotString
		{
			get
			{
				if (this.Not)
				{
					return "not";
				}
				return "is";
			}
			set
			{
				this.Not = value.ToString() == "not";
			}
		}

		public string Path
		{
			get
			{
				return this.path;
			}
			set
			{
				this.path = value;
				this.pathSplitter = null;
			}
		}

		public HL7Soup.PathSplitter PathSplitter
		{
			get
			{
				if (this.pathSplitter == null)
				{
					this.pathSplitter = new HL7Soup.PathSplitter(this.Path);
				}
				return this.pathSplitter;
			}
		}

		public MessageSourceDirection ToDirection
		{
			get
			{
				return this.toDirection;
			}
			set
			{
				this.toDirection = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ToDirection"));
				}
			}
		}

		public Dictionary<string, string> ToNamespaces
		{
			get;
			set;
		}

		public string ToPath
		{
			get
			{
				return this.toPath;
			}
			set
			{
				this.toPath = value;
				this.pathSplitter = null;
			}
		}

		public Guid ToSetting
		{
			get
			{
				return this.toSetting;
			}
			set
			{
				this.toSetting = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ToSetting"));
				}
			}
		}

		public MessageTypes ToType
		{
			get
			{
				return this.toType;
			}
			set
			{
				this.toType = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ToType"));
				}
			}
		}

		public object Value
		{
			get;
			set;
		}

		protected MessageFilter()
		{
		}

		public MessageFilter Clone()
		{
			return (MessageFilter)this.MemberwiseClone();
		}

		public abstract bool Compare(BasePart part);

		public abstract bool Compare(string valueText);

		public virtual bool ComparerUsesValue()
		{
			return false;
		}

		public static bool EvaluateConditions(WorkflowInstance workflowInstance, List<MessageFilter> filters)
		{
			bool? nullable;
			bool flag;
			if (filters == null || filters.Count == 0)
			{
				return false;
			}
			bool? nullable1 = null;
			bool flag1 = true;
			bool flag2 = false;
			List<MessageFilter>.Enumerator enumerator = filters.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					MessageFilter current = enumerator.Current;
					FilterGroup filterGroup = current as FilterGroup;
					if (filterGroup == null)
					{
						if (flag2)
						{
							continue;
						}
						if (current.Conjunction == Conjunctions.Or && nullable1.HasValue)
						{
							nullable = nullable1;
							if (!(nullable.GetValueOrDefault() & nullable.HasValue))
							{
								nullable1 = new bool?(true);
							}
							else
							{
								flag2 = true;
								continue;
							}
						}
						if (!flag1)
						{
							continue;
						}
						nullable = nullable1;
						if (!nullable.GetValueOrDefault() & nullable.HasValue)
						{
							continue;
						}
						nullable1 = new bool?(!current.Process(workflowInstance));
					}
					else
					{
						if (filterGroup.Conjunction != Conjunctions.Or)
						{
							nullable = nullable1;
							if (!nullable.GetValueOrDefault() & nullable.HasValue)
							{
								flag1 = false;
							}
						}
						else
						{
							if (flag1)
							{
								nullable = nullable1;
								flag = true;
								if (nullable.GetValueOrDefault() == flag & nullable.HasValue)
								{
									flag = true;
									return flag;
								}
							}
							flag1 = true;
						}
						nullable1 = null;
						flag2 = false;
					}
				}
				if (!flag1)
				{
					return false;
				}
				return nullable1.Value;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return flag;
		}

		internal string GetResultText()
		{
			return string.Concat(this.NotString, " ", this.CriteriaText);
		}

		public virtual string GetTheFromValue(WorkflowInstance workflowInstance)
		{
			return MessageFilter.GetTheValue(workflowInstance, this.Path, this.FromSetting, this.FromDirection, this.FromNamespaces);
		}

		public virtual string GetTheToValue(WorkflowInstance workflowInstance)
		{
			return MessageFilter.GetTheValue(workflowInstance, this.ToPath, this.ToSetting, this.ToDirection, this.ToNamespaces);
		}

		public static string GetTheValue(WorkflowInstance workflowInstance, string path, Guid setting, MessageSourceDirection direction, Dictionary<string, string> namespaces)
		{
			string valueAtPath = "";
			if (direction != MessageSourceDirection.variable)
			{
				IMessage message = workflowInstance.GetMessage(setting, direction);
				if (message != null)
				{
					XMLMessage xMLMessage = message as XMLMessage;
					if (xMLMessage != null)
					{
						xMLMessage.CreateNamespaceManager(namespaces);
					}
					valueAtPath = message.GetValueAtPath(path);
				}
			}
			else
			{
				foreach (string listOfVariablesInText in FunctionHelpers.GetListOfVariablesInText(path))
				{
					path = path.Replace(string.Concat("${", listOfVariablesInText, "}"), workflowInstance.GetVariable(listOfVariablesInText));
				}
				valueAtPath = path;
			}
			return valueAtPath;
		}

		public virtual bool Process(WorkflowInstance workflowInstance)
		{
			string theFromValue = this.GetTheFromValue(workflowInstance);
			if (this.ComparerUsesValue())
			{
				if (this.ToPath == null && this.Value != null)
				{
					this.ToPath = this.Value.ToString();
					this.ToDirection = MessageSourceDirection.variable;
					this.ToType = MessageTypes.TextWithVariables;
					this.Value = null;
				}
				this.Value = this.GetTheToValue(workflowInstance);
			}
			return !this.Compare(theFromValue);
		}

		public static void ShowHideConditionalConjunctions(IList<MessageFilter> messageFilters)
		{
			bool flag = true;
			for (int i = 0; i < messageFilters.Count; i++)
			{
				if (!flag || messageFilters[i] is FilterGroup)
				{
					messageFilters[i].IsConjunctionVisible = true;
				}
				else
				{
					messageFilters[i].IsConjunctionVisible = false;
				}
				flag = (!(messageFilters[i] is FilterGroup) ? false : true);
			}
		}

		public virtual void Validate()
		{
			this.pathSplitter = null;
			if (this.fromType == MessageTypes.Unknown || this.fromType == MessageTypes.HL7V2Path || this.fromType == MessageTypes.HL7V2)
			{
				this.IsValid = this.PathSplitter.IsValid;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}