using HL7Soup;
using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageFilters
{
	[Serializable]
	public class DateMessageFilter : MessageFilter
	{
		public DateMessageFilterComparers Comparer
		{
			get;
			set;
		}

		public override string CriteriaText
		{
			get
			{
				if (this.Comparer == DateMessageFilterComparers.Empty)
				{
					return "empty";
				}
				if (this.Comparer == DateMessageFilterComparers.InMessage)
				{
					return "in messsage";
				}
				if (this.Comparer == DateMessageFilterComparers.InvalidDate)
				{
					return "invalid date";
				}
				return string.Concat(Helpers.GetEnumDescription(this.Comparer), " ", (base.Value != null ? Helpers.ParseISO8601StringToString(base.Value.ToString()) : "Not Set"));
			}
		}

		public DateTime DateValue
		{
			get
			{
				if (base.Value == null)
				{
					return DateTime.MinValue;
				}
				return Helpers.ParseISO8601String((base.Value != null ? base.Value.ToString() : ""));
			}
			set
			{
				base.Value = Helpers.GetIso8601String(value);
			}
		}

		public string FullDateTime
		{
			get
			{
				string fullDateTimePattern = CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern;
				return string.Format(string.Concat("{0:", fullDateTimePattern, "}"), this.DateValue);
			}
		}

		public DateMessageFilter()
		{
		}

		public override bool Compare(string valueText)
		{
			throw new NotImplementedException();
		}

		public override bool Compare(BasePart part)
		{
			if (part == null)
			{
				if (this.Comparer != DateMessageFilterComparers.InMessage)
				{
					return false;
				}
				return false.Invert(base.Not);
			}
			if (this.Comparer == DateMessageFilterComparers.InMessage)
			{
				return true.Invert(base.Not);
			}
			string text = part.Text;
			if (this.Comparer == DateMessageFilterComparers.Empty)
			{
				return (text.ToString() == "").Invert(base.Not);
			}
			if (text.ToString() == "" || text.ToString() == "\"\"")
			{
				return false;
			}
			switch (this.Comparer)
			{
				case DateMessageFilterComparers.Equals:
				{
					return (this.DateValue == Helpers.ParseISO8601String(text.ToString())).Invert(base.Not);
				}
				case DateMessageFilterComparers.GreaterThan:
				{
					return (this.DateValue < Helpers.ParseISO8601String(text.ToString())).Invert(base.Not);
				}
				case DateMessageFilterComparers.LessThan:
				{
					return (this.DateValue > Helpers.ParseISO8601String(text.ToString())).Invert(base.Not);
				}
				case DateMessageFilterComparers.GreaterThanOrEqualTo:
				{
					return (this.DateValue <= Helpers.ParseISO8601String(text.ToString())).Invert(base.Not);
				}
				case DateMessageFilterComparers.LessThanOrEqualTo:
				{
					return (this.DateValue >= Helpers.ParseISO8601String(text.ToString())).Invert(base.Not);
				}
				case DateMessageFilterComparers.Empty:
				case DateMessageFilterComparers.InMessage:
				{
					return true;
				}
				case DateMessageFilterComparers.InvalidDate:
				{
					return (Helpers.ParseISO8601StringToString(text.ToString(), "x") == "x").Invert(base.Not);
				}
				default:
				{
					return true;
				}
			}
		}

		public override bool ComparerUsesValue()
		{
			if (this.Comparer <= DateMessageFilterComparers.LessThanOrEqualTo)
			{
				return true;
			}
			return false;
		}

		public override string ToString()
		{
			return string.Concat(base.Path, " ", this.CriteriaText);
		}

		public override void Validate()
		{
			base.Validate();
			if (base.IsValid && this.Comparer != DateMessageFilterComparers.InvalidDate && this.Comparer != DateMessageFilterComparers.Empty && this.Comparer != DateMessageFilterComparers.InMessage && this.DateValue == DateTime.MinValue)
			{
				base.IsValid = false;
			}
		}
	}
}