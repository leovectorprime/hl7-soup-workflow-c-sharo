using System;

namespace HL7Soup
{
	public interface IConnectionSetting
	{
		HL7Soup.ConnectionDirection ConnectionDirection
		{
			get;
			set;
		}

		string ConnectionTypeName
		{
			get;
		}

		string Details
		{
			get;
		}

		string Name
		{
			get;
			set;
		}
	}
}