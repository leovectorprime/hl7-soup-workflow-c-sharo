using HL7Soup.Functions;
using HL7Soup.HL7Descriptions;
using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace HL7Soup
{
	public class DocumentManager : DependencyObject, INotifyPropertyChanged, IDisposable
	{
		public readonly static DependencyProperty CurrentDocumentIndexProperty;

		private bool IsLoading;

		private string endline = new string(new char[] { '\u001C', '\r' });

		private string endline2 = new string('\u001C', 1);

		private Message currentMessage;

		public string FilePath;

		public readonly static DependencyProperty firstIndexProperty;

		public readonly static DependencyProperty LastIndexProperty;

		private int highestInternalId;

		private string filter = "";

		private BackgroundWorker ValidationBackgroundWorker;

		public bool AutomaticValidation
		{
			get;
			set;
		}

		public int CurrentDocumentIndex
		{
			get
			{
				return (int)base.GetValue(DocumentManager.CurrentDocumentIndexProperty);
			}
			set
			{
				base.SetValue(DocumentManager.CurrentDocumentIndexProperty, value);
			}
		}

		public Message CurrentMessage
		{
			get
			{
				return this.currentMessage;
			}
			private set
			{
				this.currentMessage = value;
				this.IsTempMessage = false;
				if (this.MessageLoaded != null)
				{
					this.MessageLoaded(this, null);
				}
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentMessage"));
				}
			}
		}

		public ObservableCollection<Message> CurrentMessages
		{
			get
			{
				if (!this.IsFiltering)
				{
					return this.Messages;
				}
				if (this.FilteredMessages == null)
				{
					this.FilteredMessages = new ObservableCollection<Message>();
				}
				return this.FilteredMessages;
			}
		}

		public ObservableCollection<Message> FilteredMessages
		{
			get;
			set;
		}

		public int FirstIndex
		{
			get
			{
				return (int)base.GetValue(DocumentManager.firstIndexProperty);
			}
			set
			{
				base.SetValue(DocumentManager.firstIndexProperty, value);
			}
		}

		public bool HasChangedSinceLastSave
		{
			get;
			set;
		}

		public bool HasFilters
		{
			get
			{
				if (this.filter != null && !string.IsNullOrWhiteSpace(this.filter))
				{
					return true;
				}
				if (this.MessageFilters == null)
				{
					return false;
				}
				return this.MessageFilters.Count != 0;
			}
		}

		public bool IsFiltering
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(this.filter))
				{
					return true;
				}
				return this.MessageFilters.Count > 0;
			}
		}

		public bool IsTempMessage
		{
			get;
			set;
		}

		public int LastIndex
		{
			get
			{
				return (int)base.GetValue(DocumentManager.LastIndexProperty);
			}
			set
			{
				base.SetValue(DocumentManager.LastIndexProperty, value);
			}
		}

		public ObservableCollection<MessageFilter> MessageFilters
		{
			get;
			set;
		}

		public ObservableCollection<Message> Messages
		{
			get;
			set;
		}

		public MessageTypes MessageType { get; set; } = MessageTypes.HL7V2;

		public string Summary
		{
			get
			{
				string str = (this.Messages.Count == 1 ? string.Concat("There is ", this.Messages.Count, " message") : string.Concat("There are ", this.Messages.Count, " messages"));
				if (this.FilteredMessages != null && this.HasFilters)
				{
					object[] count = new object[] { str, ".  ", this.Messages.Count - this.FilteredMessages.Count, null, null, null, null };
					count[3] = (this.FilteredMessages.Count == 1 ? " has" : " have");
					count[4] = " been filtered out, leaving ";
					count[5] = this.FilteredMessages.Count;
					count[6] = (this.FilteredMessages.Count == 1 ? " current message." : " current messages.");
					str = string.Concat(count);
				}
				return str;
			}
		}

		public bool SwappingCurrentMessage
		{
			get;
			set;
		}

		static DocumentManager()
		{
			DocumentManager.CurrentDocumentIndexProperty = DependencyProperty.Register("CurrentDocumentIndex", typeof(int), typeof(DocumentManager), new PropertyMetadata((object)0));
			DocumentManager.firstIndexProperty = DependencyProperty.Register("firstIndex", typeof(int), typeof(DocumentManager), new PropertyMetadata((object)0));
			DocumentManager.LastIndexProperty = DependencyProperty.Register("LastIndex", typeof(int), typeof(DocumentManager), new PropertyMetadata((object)0));
		}

		public DocumentManager()
		{
			this.Messages = new ObservableCollection<Message>();
			this.MessageFilters = new ObservableCollection<MessageFilter>();
			this.AutomaticValidation = true;
		}

		public void AddMessage(Message message)
		{
			message.InternalID = this.GetHighestInternalId(true) + 1;
			this.Messages.Insert(0, message);
			this.FilterAddedMessages(message);
			this.HasChangedSinceLastSave = true;
			ValidationManager.Instance.ValidateMessage(message);
		}

		public void AddMessages(List<Message> messages)
		{
			foreach (Message message in messages)
			{
				message.InternalID = this.GetHighestInternalId(false) + 1;
			}
			this.Messages.AddRange<Message>(messages);
			foreach (Message message1 in messages)
			{
				this.FilterAddedMessages(message1);
				ValidationManager.Instance.ValidateMessage(message1);
			}
			this.HasChangedSinceLastSave = true;
		}

		public void Delete(Message message)
		{
			this.Messages.Remove(message);
			this.FilterDeleteMessages(message);
			this.HasChangedSinceLastSave = true;
			message.Dispose();
		}

		public void DeleteAll()
		{
			foreach (Message message in this.Messages)
			{
				message.Dispose();
			}
			this.Messages.Clear();
			this.HasChangedSinceLastSave = true;
		}

		public void Dispose()
		{
			this.DeleteAll();
		}

		public void FilterAddedMessages(Message message)
		{
			if (!string.IsNullOrWhiteSpace(this.filter))
			{
				if (this.FilteredMessages == null)
				{
					this.FilteredMessages = new ObservableCollection<Message>(this.GetFilteredMessages(this.Messages));
					return;
				}
				foreach (Message filteredMessage in this.GetFilteredMessages(new List<Message>()
				{
					message
				}))
				{
					this.FilteredMessages.Insert(0, filteredMessage);
				}
			}
		}

		public void FilterDeleteMessages(Message message)
		{
			if (!string.IsNullOrWhiteSpace(this.filter))
			{
				if (this.FilteredMessages == null)
				{
					this.FilteredMessages = new ObservableCollection<Message>(this.GetFilteredMessages(this.Messages));
					return;
				}
				foreach (Message filteredMessage in this.GetFilteredMessages(new List<Message>()
				{
					message
				}))
				{
					this.FilteredMessages.Remove(filteredMessage);
				}
			}
		}

		public void FilterMessages()
		{
			if (!string.IsNullOrWhiteSpace(this.filter) || this.MessageFilters.Count > 0)
			{
				this.FilteredMessages = new ObservableCollection<Message>(this.GetFilteredMessages(this.Messages));
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FilteredMessages"));
				}
			}
			else if (this.FilteredMessages != null)
			{
				this.FilteredMessages.Clear();
			}
			else
			{
				this.FilteredMessages = new ObservableCollection<Message>();
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("FilteredMessages"));
				}
			}
			bool flag = false;
			if (this.FilteredMessages != null)
			{
				int num = 0;
				foreach (Message filteredMessage in this.FilteredMessages)
				{
					if (filteredMessage.InternalID != this.CurrentMessage.InternalID)
					{
						num++;
					}
					else
					{
						flag = true;
						if (this.CurrentMessage != filteredMessage)
						{
							try
							{
								this.SwappingCurrentMessage = true;
								this.CurrentMessage = filteredMessage;
							}
							finally
							{
								this.SwappingCurrentMessage = false;
							}
						}
						this.CurrentDocumentIndex = num;
						goto Label0;
					}
				}
			Label0:
				if (!flag && this.FilteredMessages.Count > 0 && this.CurrentMessage != this.FilteredMessages[0])
				{
					try
					{
						this.SwappingCurrentMessage = true;
						this.CurrentMessage = this.FilteredMessages[0];
						this.CurrentDocumentIndex = 0;
					}
					finally
					{
						this.SwappingCurrentMessage = false;
					}
				}
			}
		}

		public string GetAllMessagesText()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (Message currentMessage in this.CurrentMessages)
			{
				stringBuilder.Append(currentMessage.Text);
				stringBuilder.AppendLine();
			}
			return stringBuilder.ToString();
		}

		private IEnumerable<Message> GetFilteredMessages(IEnumerable<Message> messages)
		{
			if (this.MessageFilters.Count <= 0)
			{
				return 
					from p in messages
					where p.Text.IndexOf(this.filter, 0, StringComparison.InvariantCultureIgnoreCase) > -1
					select p;
			}
			List<Message> messages1 = new List<Message>();
			foreach (Message message in messages)
			{
				bool flag = false;
			Label0:
				foreach (MessageFilter messageFilter in this.MessageFilters)
				{
					if (!messageFilter.IsValid)
					{
						continue;
					}
					List<BasePart> baseParts = null;
					baseParts = (!messageFilter.IncludesRepeatFields ? new List<BasePart>()
					{
						message.GetPart(messageFilter.PathSplitter)
					} : message.GetParts(messageFilter.PathSplitter));
					foreach (BasePart basePart in baseParts)
					{
						if (messageFilter.Compare(basePart))
						{
							continue;
						}
						flag = true;
						goto Label0;
					}
				}
				if (flag)
				{
					continue;
				}
				messages1.Add(message);
			}
			if (string.IsNullOrWhiteSpace(this.filter))
			{
				return messages1;
			}
			return 
				from p in messages1
				where p.Text.IndexOf(this.filter, 0, StringComparison.InvariantCultureIgnoreCase) > -1
				select p;
		}

		private int GetHighestInternalId(bool recount)
		{
			if (this.highestInternalId == 0 || recount)
			{
				int internalID = 0;
				foreach (Message message in this.Messages)
				{
					if (message.InternalID <= internalID)
					{
						continue;
					}
					internalID = message.InternalID;
				}
				this.highestInternalId = internalID;
			}
			else
			{
				this.highestInternalId++;
			}
			return this.highestInternalId;
		}

		public void Load(string filePath)
		{
			try
			{
				this.IsLoading = true;
				Helpers.SetBusyState();
				if (filePath != null && (File.Exists(filePath) || filePath.Contains<char>('>')))
				{
					this.FilePath = filePath;
					this.CurrentDocumentIndex = 0;
				}
				this.LoadDocuments();
				this.MoveToIndex(0);
			}
			finally
			{
				this.IsLoading = false;
			}
		}

		private void LoadDocuments()
		{
			string str;
			char[] chrArray = new char[] { '\u001C', '\r' };
			string str1 = "";
			if (this.FilePath == null)
			{
				return;
			}
			string[] strArrays = this.FilePath.Split(new char[] { '>' });
			if (strArrays.Count<string>() > 1)
			{
				this.FilePath = "";
			}
			Encoding encoding = Helpers.GetEncoding(false);
			string[] strArrays1 = strArrays;
			for (int i = 0; i < (int)strArrays1.Length; i++)
			{
				str = strArrays1[i];
				if (File.Exists(str))
				{
					if (this.MessageType == MessageTypes.Binary)
					{
						goto Label1;
					}
					int num = -1;
					StringBuilder stringBuilder = new StringBuilder();
					string str2 = "\r";
					string str3 = File.ReadAllText(str, encoding);
					int num1 = str3.IndexOf("\r");
					int num2 = str3.IndexOf("\r\n");
					str2 = (num2 > num1 || num2 == -1 ? "\r" : "\r\n");
					string[] strArrays2 = str3.Split(new char[] { '\r' });
					for (int j = 0; j < (int)strArrays2.Length; j++)
					{
						string str4 = strArrays2[j];
						if (this.MessageType != MessageTypes.CSV)
						{
							string[] strArrays3 = str4.Split(new char[] { '\v' });
							for (int k = 0; k < (int)strArrays3.Length; k++)
							{
								string str5 = strArrays3[k].Trim();
								if (str5.Length > 3 && str5.TrimStart(Array.Empty<char>()).Substring(0, 3).ToUpper() == "MSH")
								{
									num++;
								}
								if (str5.Length > 3 && str5.TrimStart(Array.Empty<char>()).Substring(0, 3).ToUpper() == "MSH")
								{
									str1 = stringBuilder.ToString();
									if (!string.IsNullOrWhiteSpace(str1))
									{
										this.Messages.Insert(0, new Message(this.RemoveFSCharacter(str1), false, this.Messages.Count + 1));
									}
									stringBuilder = new StringBuilder();
								}
								if (!string.IsNullOrWhiteSpace(str5) && !str5.StartsWith("--"))
								{
									stringBuilder.Append(str5);
									stringBuilder.Append(str2);
								}
							}
						}
						else
						{
							string str6 = str4.Trim();
							if (str6 != "")
							{
								this.Messages.Insert(0, new Message(str6, false, this.Messages.Count + 1)
								{
									ActualSegmentSeperator = ""
								});
							}
						}
					}
					this.CurrentDocumentIndex = num;
					if (stringBuilder.ToString().TrimStart(Array.Empty<char>()).Length > 3 && stringBuilder.ToString().TrimStart(Array.Empty<char>()).Substring(0, 3).ToUpper() == "MSH")
					{
						str1 = stringBuilder.ToString();
						if (!string.IsNullOrWhiteSpace(str1))
						{
							this.Messages.Insert(0, new Message(this.RemoveFSCharacter(stringBuilder.ToString()), false, this.Messages.Count + 1));
						}
					}
					if (this.Messages.Count == 0)
					{
						this.Messages.Insert(0, new Message(stringBuilder.ToString(), false, this.Messages.Count + 1));
					}
				}
				this.currentMessage = this.Messages[0];
				this.IsTempMessage = false;
			Label0:
			}
			this.ValidateAllDocuments();
			return;
			using (FileStream fileStream = File.OpenRead(str))
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					fileStream.CopyTo(memoryStream);
					this.Messages.Insert(0, new Message(this.RemoveFSCharacter(Convert.ToBase64String(memoryStream.ToArray())), false, this.Messages.Count + 1));
					goto Label0;
				}
			}
		}

		private void MoveNext()
		{
			this.CurrentDocumentIndex = this.CurrentDocumentIndex + 1;
			if (this.IsFiltering)
			{
				if (this.CurrentDocumentIndex >= this.CurrentMessages.Count)
				{
					this.CurrentDocumentIndex = this.CurrentMessages.Count - 1;
				}
				this.CurrentMessage = this.CurrentMessages[this.CurrentDocumentIndex];
				return;
			}
			if (this.Messages.Count > 0)
			{
				if (this.CurrentDocumentIndex >= this.Messages.Count)
				{
					this.CurrentDocumentIndex = this.Messages.Count - 1;
				}
				this.CurrentMessage = this.Messages[this.CurrentDocumentIndex];
			}
		}

		public void MoveNext(string currentMessageText)
		{
			this.SaveCurrent(currentMessageText);
			this.MoveNext();
		}

		private void MovePrevious()
		{
			this.CurrentDocumentIndex = this.CurrentDocumentIndex - 1;
			if (this.CurrentDocumentIndex < 0)
			{
				this.CurrentDocumentIndex = 0;
			}
			if (this.IsFiltering)
			{
				if (this.CurrentMessages.Count > 0)
				{
					this.CurrentMessage = this.CurrentMessages[this.CurrentDocumentIndex];
					return;
				}
			}
			else if (this.Messages.Count > 0)
			{
				this.CurrentMessage = this.Messages[this.CurrentDocumentIndex];
			}
		}

		public void MovePrevious(string currentMessageText)
		{
			this.SaveCurrent(currentMessageText);
			this.MovePrevious();
		}

		private void MoveToIndex(int index)
		{
			this.CurrentDocumentIndex = index;
			if (this.IsFiltering)
			{
				if (this.CurrentDocumentIndex > this.CurrentMessages.Count)
				{
					this.CurrentDocumentIndex = this.CurrentMessages.Count - 1;
				}
				if (this.CurrentDocumentIndex == -1)
				{
					return;
				}
				this.CurrentMessage = this.CurrentMessages[this.CurrentDocumentIndex];
				return;
			}
			if (this.Messages.Count > 0)
			{
				if (this.CurrentDocumentIndex > this.Messages.Count)
				{
					this.CurrentDocumentIndex = this.Messages.Count - 1;
				}
				this.CurrentMessage = this.Messages[this.CurrentDocumentIndex];
			}
		}

		private void ProcessValidationResults(Dictionary<int, bool> messageValidationResults)
		{
			if (messageValidationResults != null)
			{
				foreach (Message message in this.Messages)
				{
					if (!messageValidationResults.ContainsKey(message.InternalID) || message.IsValid == messageValidationResults[message.InternalID])
					{
						continue;
					}
					message.IsValid = messageValidationResults[message.InternalID];
				}
			}
		}

		private string RemoveFSCharacter(string messageText)
		{
			if (messageText.EndsWith(this.endline))
			{
				return messageText.Substring(0, messageText.Length - 2);
			}
			if (!messageText.EndsWith(this.endline2))
			{
				return messageText;
			}
			return messageText.Substring(0, messageText.Length - 1);
		}

		public void Save(string currentMessageText)
		{
			this.SaveCurrent(currentMessageText);
			string tempFileName = Path.GetTempFileName();
			Encoding encoding = Helpers.GetEncoding(false);
			if (this.CurrentMessage == null)
			{
				string str = currentMessageText.TrimEnd("\r\n".ToCharArray());
				str = str.TrimEnd("\r".ToCharArray());
				str = str.TrimEnd("\n".ToCharArray());
				str = string.Concat(str, "\r", Environment.NewLine);
				File.AppendAllText(tempFileName, str, encoding);
			}
			else
			{
				for (int i = this.Messages.Count - 1; i >= 0; i--)
				{
					string text = this.Messages[i].Text;
					text = text.TrimEnd("\r\n".ToCharArray());
					text = text.TrimEnd("\r".ToCharArray());
					text = text.TrimEnd("\n".ToCharArray());
					text = string.Concat(text, "\r", Environment.NewLine);
					File.AppendAllText(tempFileName, text, encoding);
				}
			}
			if (File.Exists(this.FilePath))
			{
				File.Delete(this.FilePath);
			}
			File.Move(tempFileName, this.FilePath);
			File.Delete(tempFileName);
			this.HasChangedSinceLastSave = false;
		}

		public void SaveCurrent(string messageText)
		{
			if (this.CurrentMessage != null)
			{
				this.CurrentMessage.Text = messageText;
			}
		}

		public void SetCurrentDocument(string previousMessageText, Message message)
		{
			this.SaveCurrent(previousMessageText);
			int num = this.Messages.IndexOf(message);
			if (this.IsFiltering)
			{
				num = this.CurrentMessages.IndexOf(message);
			}
			this.MoveToIndex(num);
		}

		public void SetFilter(string searchString)
		{
			this.filter = searchString.ToLower();
			this.FilterMessages();
		}

		public void ShowTempMessage(string messageText, string messageTitle)
		{
			this.ShowTempMessage(new Message(messageText, true), messageTitle);
		}

		public void ShowTempMessage(Message message, string messageTitle)
		{
			this.CurrentMessage = message;
			this.IsTempMessage = true;
			message.DescriptionManager.CurrentMessageTypeDescription = messageTitle;
		}

		public static List<Message> SplitTextIntoMessages(string text)
		{
			List<Message> messages = new List<Message>();
			text = text.Replace("\v", "");
			text = text.Replace('\u001C'.ToString(), "\r");
			text = text.Replace("\r\n", "\r");
			text = text.Replace("\r ", "\r");
			text = text.Replace("\r\r", "\r");
			foreach (string str in Regex.Split(string.Concat("\r", text), "\rMSH|\r\nMSH").Skip<string>(1))
			{
				messages.Add(new Message(string.Concat("MSH", str), true));
			}
			return messages;
		}

		public void SwapCurrentMessage(Message message)
		{
			this.SwapCurrentMessage(message, this.Messages);
			if (this.IsFiltering)
			{
				this.SwapCurrentMessage(message, this.FilteredMessages);
			}
		}

		private void SwapCurrentMessage(Message message, ObservableCollection<Message> messagesList)
		{
			int num = -1;
			int num1 = 0;
			foreach (Message message1 in messagesList)
			{
				if (message1.InternalID == message.InternalID)
				{
					num = num1;
				}
				num1++;
			}
			if (num > -1)
			{
				if (messagesList[num].Text != message.Text)
				{
					bool isValid = message.IsValid;
					message = new Message(message.Text, true, message.InternalID)
					{
						IsValid = isValid
					};
					try
					{
						this.SwappingCurrentMessage = true;
						messagesList[num] = message;
					}
					finally
					{
						this.SwappingCurrentMessage = false;
					}
					this.currentMessage = message;
					this.IsTempMessage = false;
					if (!this.IsLoading)
					{
						this.HasChangedSinceLastSave = true;
						return;
					}
				}
			}
			else if (!this.IsLoading)
			{
				this.HasChangedSinceLastSave = true;
			}
		}

		public void ValidateAllDocuments()
		{
			if (this.AutomaticValidation && Settings.Default.ValidateMessages)
			{
				if (this.ValidationBackgroundWorker != null)
				{
					this.ValidationBackgroundWorker.DoWork -= new DoWorkEventHandler(this.ValidateAllDocuments);
					this.ValidationBackgroundWorker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(this.ValidateAllDocumentsCompleted);
					this.ValidationBackgroundWorker.ProgressChanged -= new ProgressChangedEventHandler(this.ValidateAllDocuments_ProgressChanged);
					this.ValidationBackgroundWorker.CancelAsync();
				}
				this.ValidationBackgroundWorker = new BackgroundWorker()
				{
					WorkerReportsProgress = true,
					WorkerSupportsCancellation = true
				};
				this.ValidationBackgroundWorker.DoWork += new DoWorkEventHandler(this.ValidateAllDocuments);
				this.ValidationBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.ValidateAllDocumentsCompleted);
				this.ValidationBackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(this.ValidateAllDocuments_ProgressChanged);
				this.ValidationBackgroundWorker.RunWorkerAsync();
			}
		}

		private void ValidateAllDocuments(object sender, DoWorkEventArgs e)
		{
			try
			{
				Dictionary<int, bool> nums = new Dictionary<int, bool>();
				int num = 0;
				while (num < this.Messages.Count)
				{
					Message item = null;
					lock (this.Messages)
					{
						if (this.Messages.Count > num)
						{
							item = this.Messages[num];
						}
					}
					nums.Add(item.InternalID, ValidationManager.Instance.IsMessageValid(item));
					if (num == 30)
					{
						this.ValidationBackgroundWorker.ReportProgress(1, nums);
					}
					if (!e.Cancel)
					{
						num++;
					}
					else
					{
						return;
					}
				}
				e.Result = nums;
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
		}

		private void ValidateAllDocuments_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			this.ProcessValidationResults((Dictionary<int, bool>)e.UserState);
		}

		private void ValidateAllDocumentsCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			this.ProcessValidationResults((Dictionary<int, bool>)e.Result);
		}

		public event MessageLoadedEventHandler MessageLoaded;

		public event PropertyChangedEventHandler PropertyChanged;
	}
}