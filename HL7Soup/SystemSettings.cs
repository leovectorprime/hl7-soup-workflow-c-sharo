using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Transformers;
using HL7Soup.Integrations;
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public sealed class SystemSettings
	{
		private static volatile SystemSettings instance;

		private static object syncRoot;

		private List<TransformerType> customTransfomerTypes;

		private List<Type> customActivityTypes;

		public HL7Soup.FunctionSettings FunctionSettings
		{
			get;
			set;
		}

		public static SystemSettings Instance
		{
			get
			{
				if (SystemSettings.instance == null)
				{
					lock (SystemSettings.syncRoot)
					{
						if (SystemSettings.instance == null)
						{
							SystemSettings.instance = new SystemSettings();
						}
						SystemSettings.instance.WorkflowInstanceIdDictionary = new Dictionary<Guid, int>();
					}
				}
				return SystemSettings.instance;
			}
		}

		public bool NotReadOnlyMode
		{
			get
			{
				return !this.ReadOnlyMode;
			}
		}

		public bool ReadOnlyMode
		{
			get;
			set;
		}

		public Dictionary<Guid, int> WorkflowInstanceIdDictionary
		{
			get;
			private set;
		}

		static SystemSettings()
		{
			SystemSettings.syncRoot = new object();
		}

		private SystemSettings()
		{
		}

		public IAutomaticSendSetting GetAutomaticSendConnectionById(Guid id)
		{
			IAutomaticSendSetting automaticSendSetting;
			if (this.FunctionSettings == null || this.FunctionSettings.AutomaticSenderSettings == null || this.FunctionSettings.AutomaticSenderSettings.Count <= 0)
			{
				throw new Exception("Cannot continue without any Automatic Send connections");
			}
			using (IEnumerator<IAutomaticSendSetting> enumerator = this.FunctionSettings.AutomaticSenderSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					IAutomaticSendSetting current = enumerator.Current;
					if (id != current.Id)
					{
						continue;
					}
					automaticSendSetting = current;
					return automaticSendSetting;
				}
				return this.FunctionSettings.AutomaticSenderSettings[0];
			}
			return automaticSendSetting;
		}

		public IAutomaticSendSetting GetAutomaticSendConnectionBySendConnection(IEditorSendingBehaviourSetting setting)
		{
			if (setting == null)
			{
				throw new ArgumentNullException("setting");
			}
			return this.GetAutomaticSendConnectionById(setting.AutomaticSendSettings);
		}

		internal IEnumerable<Type> GetCustomActivityTypes()
		{
			if (this.customActivityTypes != null)
			{
				return SystemSettings.instance.customActivityTypes;
			}
			SystemSettings.Instance.customActivityTypes = SystemSettings.GetImpletmentationsOfInterfaceFromCustomLibs(typeof(ICustomActivity));
			return SystemSettings.Instance.customActivityTypes;
		}

		public IEnumerable<TransformerType> GetCustomTransformerTypes()
		{
			if (this.customTransfomerTypes != null)
			{
				return SystemSettings.instance.customTransfomerTypes;
			}
			this.customTransfomerTypes = new List<TransformerType>();
			foreach (Type impletmentationsOfInterfaceFromCustomLib in SystemSettings.GetImpletmentationsOfInterfaceFromCustomLibs(typeof(ICustomTransformer)))
			{
				string name = impletmentationsOfInterfaceFromCustomLib.Name;
				DisplayNameAttribute displayNameAttribute = impletmentationsOfInterfaceFromCustomLib.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault<object>() as DisplayNameAttribute;
				name = (displayNameAttribute != null ? displayNameAttribute.DisplayName : impletmentationsOfInterfaceFromCustomLib.Name);
				this.customTransfomerTypes.Add(new TransformerType(impletmentationsOfInterfaceFromCustomLib, name));
			}
			return SystemSettings.Instance.customTransfomerTypes;
		}

		private static List<Type> GetImpletmentationsOfInterfaceFromCustomLibs(Type interfaceType)
		{
			List<Type> types = new List<Type>();
			string str = string.Concat(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "\\Custom Libraries\\");
			Log.Instance.Debug(string.Concat("Searching for Custom Libraries in ", str));
			FileInfo[] files = (new DirectoryInfo(str)).GetFiles("*.dll");
			for (int i = 0; i < (int)files.Length; i++)
			{
				FileInfo fileInfo = files[i];
				try
				{
					Log.Instance.Debug(string.Concat("Custom Library dll found ", fileInfo.FullName));
					Type[] typeArray = Assembly.LoadFrom(fileInfo.FullName).GetTypes();
					for (int j = 0; j < (int)typeArray.Length; j++)
					{
						Type type = typeArray[j];
						Log.Instance.Debug(string.Concat("Custom Library type found ", type.FullName));
						interfaceType.IsAssignableFrom(type);
						if (interfaceType.IsAssignableFrom(type))
						{
							Log.Instance.Info(string.Concat("Custom Library found and loaded ", type.FullName));
							types.Add(type);
						}
					}
				}
				catch (BadImageFormatException badImageFormatException)
				{
				}
			}
			return types;
		}

		public ISourceProviderSetting GetSourceProviderById(Guid id)
		{
			ISourceProviderSetting sourceProviderSetting;
			if (this.FunctionSettings == null || this.FunctionSettings.SourceProviderSettings == null || this.FunctionSettings.SourceProviderSettings.Count <= 0)
			{
				throw new Exception("Cannot continue without any Source Provider connections");
			}
			using (IEnumerator<ISourceProviderSetting> enumerator = this.FunctionSettings.SourceProviderSettings.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ISourceProviderSetting current = enumerator.Current;
					if (id != current.Id)
					{
						continue;
					}
					sourceProviderSetting = current;
					return sourceProviderSetting;
				}
				return this.FunctionSettings.SourceProviderSettings[0];
			}
			return sourceProviderSetting;
		}

		public ISourceProviderSetting GetSourceProviderBySendConnection(IEditorSendingBehaviourSetting setting)
		{
			if (setting == null)
			{
				throw new ArgumentNullException("setting");
			}
			return this.GetSourceProviderById(setting.SourceProviderSettings);
		}

		public static bool InterfaceFilter(Type typeObj, object criteriaObj)
		{
			return typeObj.ToString() == criteriaObj.ToString();
		}

		public void SetWorkflowInstanceIdDictionary(Dictionary<Guid, int> settings)
		{
			this.WorkflowInstanceIdDictionary = settings;
		}
	}
}