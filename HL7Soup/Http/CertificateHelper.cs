using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Collections;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace HL7Soup.Http
{
	public class CertificateHelper
	{
		public CertificateHelper()
		{
		}

		public static void AddToCertificateStore(X509Certificate2 certificate)
		{
			CertificateHelper.AddToCertificateStore(certificate, StoreName.My, StoreLocation.LocalMachine);
		}

		public static void AddToCertificateStore(string certPath, string password)
		{
			X509Certificate2 x509Certificate2 = new X509Certificate2(certPath, password);
			CertificateHelper.AddToCertificateStore(new X509Certificate2(certPath, password));
		}

		public static void AddToCertificateStore(X509Certificate2 certificate, StoreName storeName, StoreLocation storeLocation)
		{
			X509Store x509Store = new X509Store(storeName, storeLocation);
			x509Store.Open(OpenFlags.ReadWrite);
			x509Store.Add(certificate);
			x509Store.Close();
		}

		public static bool DoesCertificateExist(string thumbprint)
		{
			return CertificateHelper.DoesCertificateExist(thumbprint, StoreName.My, StoreLocation.LocalMachine);
		}

		public static bool DoesCertificateExist(string thumbprint, StoreName storeName, StoreLocation storeLocation)
		{
			X509Store x509Store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
			x509Store.Open(OpenFlags.ReadOnly);
			if (x509Store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false).Count > 0)
			{
				return true;
			}
			return false;
		}

		public static X509Certificate2 GenerateSslCertificate()
		{
			RsaKeyPairGenerator rsaKeyPairGenerator = new RsaKeyPairGenerator();
			rsaKeyPairGenerator.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), 1024));
			AsymmetricCipherKeyPair asymmetricCipherKeyPair = rsaKeyPairGenerator.GenerateKeyPair();
			AsymmetricKeyParameter @public = asymmetricCipherKeyPair.get_Public();
			RsaPrivateCrtKeyParameters @private = (RsaPrivateCrtKeyParameters)asymmetricCipherKeyPair.get_Private();
			X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
			x509V3CertificateGenerator.SetSerialNumber(BigInteger.ProbablePrime(120, new Random()));
			x509V3CertificateGenerator.SetSubjectDN(new X509Name("CN=Popokey"));
			x509V3CertificateGenerator.SetIssuerDN(new X509Name(string.Concat("CN=", HttpHelpers.AssemblyName)));
			DateTime date = DateTime.UtcNow.Date;
			x509V3CertificateGenerator.SetNotAfter(date.AddYears(12));
			date = DateTime.Now;
			x509V3CertificateGenerator.SetNotBefore(date.Subtract(new TimeSpan(7, 0, 0, 0)));
			x509V3CertificateGenerator.SetSignatureAlgorithm("SHA256WithRSA");
			x509V3CertificateGenerator.SetPublicKey(@public);
			Org.BouncyCastle.X509.X509Certificate x509Certificate = x509V3CertificateGenerator.Generate(@private);
			AsymmetricAlgorithm dotNetKey = CertificateHelper.ToDotNetKey(@private);
			return new X509Certificate2(DotNetUtilities.ToX509Certificate(x509Certificate))
			{
				PrivateKey = dotNetKey
			};
		}

		private static AsymmetricAlgorithm ToDotNetKey(RsaPrivateCrtKeyParameters privateKey)
		{
			RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(new CspParameters()
			{
				KeyContainerName = Guid.NewGuid().ToString(),
				KeyNumber = 1,
				Flags = CspProviderFlags.UseMachineKeyStore
			});
			RSAParameters rSAParameter = new RSAParameters()
			{
				Modulus = privateKey.get_Modulus().ToByteArrayUnsigned(),
				P = privateKey.get_P().ToByteArrayUnsigned(),
				Q = privateKey.get_Q().ToByteArrayUnsigned(),
				DP = privateKey.get_DP().ToByteArrayUnsigned(),
				DQ = privateKey.get_DQ().ToByteArrayUnsigned(),
				InverseQ = privateKey.get_QInv().ToByteArrayUnsigned(),
				D = privateKey.get_Exponent().ToByteArrayUnsigned(),
				Exponent = privateKey.get_PublicExponent().ToByteArrayUnsigned()
			};
			rSACryptoServiceProvider.ImportParameters(rSAParameter);
			return rSACryptoServiceProvider;
		}
	}
}