using HL7Soup;
using NLog;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;

namespace HL7Soup.Http
{
	public static class HttpHelpers
	{
		public const string AssemblyGuidForHL7Soup = "d6928ca8-e8bc-4ff7-9099-85a168c96061";

		public const string AssemblyGuidForIntegrationHost = "c5198a6b-9e11-4535-a634-2b47154dcfc5";

		public static string AssemblyGuid
		{
			get
			{
				Assembly entryAssembly = Assembly.GetEntryAssembly();
				if (entryAssembly == null)
				{
					throw new Exception("Could not determine the entry assembly.  Please contact HL7 Soup Support with this error.");
				}
				object[] customAttributes = entryAssembly.GetCustomAttributes(typeof(GuidAttribute), false);
				if (customAttributes.Length == 0)
				{
					throw new Exception("Could not determine the entry assembly, you need to add an [assembly: Guid(\"yourguid\")] attribute to the assemblyInfo.cs.  Please contact HL7 Soup Support if you are experiencing this error as a user.");
				}
				return ((GuidAttribute)customAttributes[0]).Value.ToUpper();
			}
		}

		public static string AssemblyName
		{
			get
			{
				Assembly entryAssembly = Assembly.GetEntryAssembly();
				if (entryAssembly == null)
				{
					throw new Exception("Could not determine the entry assembly.  Please contact HL7 Soup Support with this error.");
				}
				return entryAssembly.GetName().Name;
			}
		}

		public static void AddUrlRegistrationForNonAdministators(string address)
		{
			HttpHelpers.AddUrlRegistrationForNonAdministators(address, HttpHelpers.GetEveryoneUserName());
		}

		public static void AddUrlRegistrationForNonAdministators(string address, string userName)
		{
			HttpHelpers.RemoveAllConflictingRegistrations(address);
			if (!HttpHelpers.IsLocalHostUrl(address))
			{
				string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
				string str1 = string.Concat("http add urlacl url=", address, " user=", userName);
				int num = 0;
				string str2 = Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
				string str3 = str2.Replace(" ", "");
				if (!str3.Contains("SSLCertificatesuccessfullydeleted") && num != 0)
				{
					if (!str3.Contains("Cannotcreateafilewhenthatfilealreadyexists"))
					{
						throw new Exception(string.Concat(new string[] { "An unknown repsonse was received when attempting to add the address\r\n\r\nYou can try running the command yourself in an elevated console window using:\r\n", str, " ", str1, "\r\n\r\nThe response was:\r\n", str2, "\r\n\r\nPress ctrl+c to copy this message" }));
					}
					throw new Exception("Cannot Register this address as there is already something else here. Try a different port.\r\n\r\nDiagnose by running:\r\nNetsh http show urlacl\r\nin a command window to see the currently registered addresses.\r\n\r\nNote that HTTP and HTTPS cannot exist for the same port.");
				}
			}
		}

		public static void BindPortToCertificate(int port, string assemblyGuid)
		{
			HttpHelpers.BindPortToCertificate(port, assemblyGuid, "");
		}

		public static void BindPortToCertificate(int port, string assemblyGuid, string thumbprint)
		{
			X509Certificate2 item = null;
			if (string.IsNullOrEmpty(thumbprint))
			{
				thumbprint = SharedSettings.DefaultSSLCertificateThumbprint;
				if (string.IsNullOrEmpty(thumbprint))
				{
					item = HttpHelpers.GenerateHL7SoupDefaultCertificate();
					thumbprint = SharedSettings.DefaultSSLCertificateThumbprint;
				}
			}
			if (item == null)
			{
				X509Store x509Store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				x509Store.Open(OpenFlags.ReadOnly);
				X509Certificate2Collection x509Certificate2Collection = x509Store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
				if (x509Certificate2Collection.Count > 0)
				{
					item = x509Certificate2Collection[0];
				}
				if (item == null)
				{
					throw new Exception(string.Concat("Could not find certificate with thumbprint '", thumbprint, "' in the Local Machine, Certificate Store in the Personal folder.  Please either provide a different certificate thumbprint, locate and install certificate into the machines Computer Personal folder, or select the default HL7 Soup certificate."));
				}
			}
			HttpHelpers.BindPortToCertificate(port, item, assemblyGuid, false);
		}

		public static void BindPortToCertificate(int port, StoreLocation storeLocation, StoreName storeName, X509FindType x509FindType, string thumbPrint, string password, string assemblyGuid, bool openCmdWindowOnFailure)
		{
			X509Store x509Store = new X509Store(storeName, storeLocation);
			x509Store.Open(OpenFlags.ReadOnly);
			X509Certificate2Collection x509Certificate2Collection = x509Store.Certificates.Find(x509FindType, thumbPrint, false);
			if (x509Certificate2Collection.Count <= 0)
			{
				throw new Exception(string.Concat(new string[] { "Certificate not found in ", Helpers.GetEnumDescription(storeLocation), " with thumbprint ", thumbPrint, ".  Please add the certificate or change the workflow to use the default certificate." }));
			}
			HttpHelpers.BindPortToCertificate(port, x509Certificate2Collection[0], assemblyGuid, openCmdWindowOnFailure);
		}

		public static void BindPortToCertificate(int port, string certPath, string password, string assemblyGuid, bool openCmdWindowOnFailure)
		{
			X509Certificate2 x509Certificate2 = new X509Certificate2(certPath, password);
			HttpHelpers.BindPortToCertificate(port, new X509Certificate2(certPath, password), assemblyGuid, openCmdWindowOnFailure);
		}

		private static void BindPortToCertificate(int port, X509Certificate2 certificate, string assemblyGuid, bool openCmdWindowOnFailure)
		{
			bool flag = false;
			string sslCertificateBindingDetailsForPort = HttpHelpers.GetSslCertificateBindingDetailsForPort(port);
			if (HttpHelpers.GetIsBoundFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort))
			{
				flag = true;
				bool flag1 = false;
				HttpHelpers.MyAssemblies assemblyFromSslCertificateBindingDetails = HttpHelpers.GetAssemblyFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort);
				if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.UnknownAssembly)
				{
					throw new Exception("Cannot bind the SSL certificate to this port because another application is already bound here.");
				}
				if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.IntegrationHost && assemblyGuid != "c5198a6b-9e11-4535-a634-2b47154dcfc5")
				{
					flag1 = true;
				}
				if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.HL7Soup && assemblyGuid != "d6928ca8-e8bc-4ff7-9099-85a168c96061")
				{
					flag1 = true;
				}
				if (certificate.Thumbprint.ToLower() != HttpHelpers.GetThumbprintFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort).ToLower())
				{
					flag1 = true;
				}
				if (flag1)
				{
					flag = false;
					HttpHelpers.DeletePortBindingToCertificate(port);
				}
			}
			if (!flag)
			{
				string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
				string str1 = string.Format("http add sslcert ipport=0.0.0.0:{0} certhash={1} appid={{{2}}}", port, certificate.Thumbprint, new Guid(assemblyGuid));
				int num = 0;
				Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
				if (num > 0)
				{
					if (openCmdWindowOnFailure)
					{
						Process process = new Process();
						process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "cmd.exe");
						process.StartInfo.Arguments = string.Format(string.Concat("/K netsh.exe ", str1), Array.Empty<object>());
						process.Start();
					}
					throw new Exception(string.Concat(new object[] { "Failed to bind port ", port, ".  Please run this command line in an elevated console to view the error if not already displayed.\r\n\r\n", str, " ", str1, "\r\n\r\nCtrl+c copies this message\r\n\r\n\r\nCommon Errors and Solutions:\r\n\r\n*SSL Certificate add failed, Error: 1312 A specified logon session does not exist - Your certificate might not be installed.  Double click the certificate file to import. Make sure it is imported to Local Machine under the Personal store.\r\n\r\n*SSL Certificate add failed, Error: 183 Cannot create a file when that file already exists. - Have you already registered this certificate here?  Veiw the existing registrations and remove it with this if you need to.\r\nnetsh http delete sslcert ipport=0.0.0.0:", port, "\r\nIt could be that the certificate is already bound here correctly and might already work.  Try it.  If all else fails, try another port.\r\n\r\n*SSL Certificate add failed, Error: 1312 - Shift the certificate into the Personal certificate store." }));
				}
			}
		}

		public static void DeletePortBindingToCertificate(int port)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Format("http delete sslcert ipport=0.0.0.0:{0}", port);
			int num = 0;
			string str2 = Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
			if (num > 0)
			{
				object[] objArray = new object[] { "Failed to delete port binding ", port, ". '", null, null };
				objArray[3] = (string.IsNullOrEmpty(str2) ? "You need to be running as an administrator (local system)" : str2);
				objArray[4] = "'";
				throw new Exception(string.Concat(objArray));
			}
		}

		public static void DeleteUrlRegistrationForNonAdministrators(string url)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Format(string.Concat("http Delete urlacl url=", url), Array.Empty<object>());
			Helpers.ExecuteProcessAndGetTextString(str, str1);
		}

		public static void DeleteUrlRegistrationNonAdministratorsIfSslChanged(string url)
		{
			if (url.StartsWith("https"))
			{
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttp(url)))
				{
					HttpHelpers.DeleteUrlRegistrationForNonAdministrators(url.Replace("https", "http"));
					return;
				}
			}
			else if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttps(url)))
			{
				HttpHelpers.DeleteUrlRegistrationForNonAdministrators(url.Replace("http:", "https:"));
			}
		}

		private static void ElevateAndEnableUrlToBeOpenedWithoutElevation(string url, string applicationId)
		{
			HttpHelpers.ElevateAndEnableUrlToBeOpenedWithoutElevation(url, applicationId, "");
		}

		private static void ElevateAndEnableUrlToBeOpenedWithoutElevation(string url, string applicationId, string certificateThumbprint)
		{
			if (!string.IsNullOrEmpty(certificateThumbprint) && !CertificateHelper.DoesCertificateExist(certificateThumbprint))
			{
				throw new Exception(string.Concat("Couldn't find certificate with thumbprint '", certificateThumbprint, "' in the Local Machine, Certificate Store in the Personal folder.  Please either provide a different certificate thumbprint, locate and install certificate into the machines Computer Personal folder, or select the default HL7 Soup certificate."));
			}
			string str = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "UrlBinderForHL7Soup.exe");
			if (!File.Exists(str))
			{
				throw new Exception(string.Concat("Could not locate the application UrlBinderForHL7Soup.exe.\r\nPlease add it to\r\n", AppDomain.CurrentDomain.BaseDirectory));
			}
			string str1 = string.Format(string.Concat(new string[] { "url=", url, " applicationId=", applicationId, " thumbprint=", certificateThumbprint }), Array.Empty<object>());
			int num = 0;
			Helpers.ExecuteProcessAndGetTextString(str, str1, true, out num);
			if (num != 0)
			{
				string lastUrlBinderError = SharedSettings.LastUrlBinderError;
				HL7Soup.Log.Instance.Error(string.Concat(new object[] { "ElevateAndEnableUrlToBeOpenedWithoutElevation returned the exit code ", num, ". The error was '", lastUrlBinderError, "'" }));
				throw new Exception(lastUrlBinderError);
			}
		}

		public static void EnableUrlToBeOpenedWithoutElevation(string url, string applicationId)
		{
			HttpHelpers.EnableUrlToBeOpenedWithoutElevation(url, applicationId, "");
		}

		public static void EnableUrlToBeOpenedWithoutElevation(string url, string applicationId, string certificateThumbprint)
		{
			if (!url.EndsWith("/"))
			{
				url = string.Concat(url, "/");
			}
			if (!HttpHelpers.IsUrlAbleToBeOpenedWithoutElevation(url, applicationId, certificateThumbprint))
			{
				HttpHelpers.ElevateAndEnableUrlToBeOpenedWithoutElevation(url, applicationId, certificateThumbprint);
			}
		}

		private static X509Certificate2 GenerateHL7SoupDefaultCertificate()
		{
			X509Certificate2 x509Certificate2 = CertificateHelper.GenerateSslCertificate();
			CertificateHelper.AddToCertificateStore(x509Certificate2);
			SharedSettings.DefaultSSLCertificateThumbprint = x509Certificate2.Thumbprint;
			return x509Certificate2;
		}

		private static HttpHelpers.MyAssemblies GetAssemblyFromSslCertificateBindingDetails(string text)
		{
			string str = text.ToString().Replace(" ", "");
			if (str.IndexOf("Thesystemcannotfindthefilespecified.", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				return HttpHelpers.MyAssemblies.NotBound;
			}
			if (str.IndexOf("ApplicationID:{c5198a6b-9e11-4535-a634-2b47154dcfc5}", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				return HttpHelpers.MyAssemblies.IntegrationHost;
			}
			if (str.IndexOf("ApplicationID:{d6928ca8-e8bc-4ff7-9099-85a168c96061}", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				return HttpHelpers.MyAssemblies.HL7Soup;
			}
			return HttpHelpers.MyAssemblies.UnknownAssembly;
		}

		public static string GetEveryoneUserName()
		{
			return ((NTAccount)(new SecurityIdentifier(WellKnownSidType.WorldSid, null)).Translate(typeof(NTAccount))).Value;
		}

		private static bool GetIsBoundFromSslCertificateBindingDetails(string text)
		{
			if (text.IndexOf("Thesystemcannotfindthefilespecified.", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				return false;
			}
			if (text.IndexOf("IP:port:0.0.0.0:", StringComparison.InvariantCultureIgnoreCase) > -1)
			{
				return true;
			}
			return false;
		}

		public static int GetPortFromUrl(string url)
		{
			return (new Uri(HttpHelpers.ReplaceDomainInUrl(url, "localhost"))).Port;
		}

		private static string GetSslCertificateBindingDetailsForPort(int port)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Format(string.Concat("http show sslcert ipport=0.0.0.0:", port), Array.Empty<object>());
			return Helpers.ExecuteProcessAndGetTextString(str, str1).ToString().Replace(" ", "");
		}

		private static string GetThumbprintFromSslCertificateBindingDetails(string text)
		{
			string str = "NotFound";
			int length = text.IndexOf("CertificateHash:", StringComparison.InvariantCultureIgnoreCase);
			if (length > 0)
			{
				int num = text.IndexOf("\r", length);
				if (num == -1)
				{
					num = text.Length;
				}
				length += "CertificateHash:".Length;
				str = text.Substring(length, num - length);
			}
			return str;
		}

		public static string GetUrlAsHttp(string url)
		{
			return url.Replace("https:", "http:");
		}

		public static string GetUrlAsHttps(string url)
		{
			return url.Replace("http:", "https:");
		}

		public static bool IsLocalHostUrl(string url)
		{
			if (url.ToLower().Contains("://localhost"))
			{
				return true;
			}
			return url.Contains("://127.0.0.1");
		}

		public static bool IsUrlAbleToBeOpenedWithoutElevation(string url, string applicationId, string certificateThumbprint)
		{
			if (HttpHelpers.IsLocalHostUrl(url))
			{
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttp(HttpHelpers.ReplaceDomainInUrl(url, "*"))))
				{
					return false;
				}
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttps(url)))
				{
					return false;
				}
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttp(HttpHelpers.ReplaceDomainInUrl(url, "+"))))
				{
					return false;
				}
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttps(url)))
				{
					return false;
				}
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttp(HttpHelpers.ReplaceDomainInUrl(url, Environment.MachineName))))
				{
					return false;
				}
				if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttps(url)))
				{
					return false;
				}
			}
			else if (!HttpHelpers.IsUrlRegisteredForNonAdministrators(url))
			{
				return false;
			}
			if (HttpHelpers.IsUrlSsl(url))
			{
				string sslCertificateBindingDetailsForPort = HttpHelpers.GetSslCertificateBindingDetailsForPort(HttpHelpers.GetPortFromUrl(url));
				if (HttpHelpers.GetIsBoundFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort))
				{
					HttpHelpers.MyAssemblies assemblyFromSslCertificateBindingDetails = HttpHelpers.GetAssemblyFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort);
					if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.UnknownAssembly)
					{
						return false;
					}
					if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.IntegrationHost && applicationId.ToLower() != "c5198a6b-9e11-4535-a634-2b47154dcfc5")
					{
						return false;
					}
					if (assemblyFromSslCertificateBindingDetails == HttpHelpers.MyAssemblies.HL7Soup && applicationId.ToLower() != "d6928ca8-e8bc-4ff7-9099-85a168c96061")
					{
						return false;
					}
					if (string.IsNullOrEmpty(certificateThumbprint))
					{
						certificateThumbprint = SharedSettings.DefaultSSLCertificateThumbprint;
						if (certificateThumbprint == null)
						{
							return false;
						}
					}
					if (certificateThumbprint.ToLower() != HttpHelpers.GetThumbprintFromSslCertificateBindingDetails(sslCertificateBindingDetailsForPort).ToLower())
					{
						return false;
					}
				}
			}
			return true;
		}

		public static bool IsUrlRegisteredForNonAdministrators(string url)
		{
			if (Helpers.ExecuteProcessAndGetTextString(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe"), string.Format(string.Concat("http show urlacl url=", url), Array.Empty<object>())).ToString().Replace(" ", "").IndexOf(url, StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				return true;
			}
			return false;
		}

		public static bool IsUrlSsl(string url)
		{
			return url.ToLower().StartsWith("https://");
		}

		private static void RemoveAllBindingsToThisURL(string url)
		{
			if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttp(url)))
			{
				HttpHelpers.DeleteUrlRegistrationForNonAdministrators(HttpHelpers.GetUrlAsHttp(url));
			}
			if (HttpHelpers.IsUrlRegisteredForNonAdministrators(HttpHelpers.GetUrlAsHttps(url)))
			{
				HttpHelpers.DeleteUrlRegistrationForNonAdministrators(HttpHelpers.GetUrlAsHttps(url));
			}
		}

		public static void RemoveAllConflictingRegistrations(string url)
		{
			if (HttpHelpers.IsLocalHostUrl(url))
			{
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "*"));
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "+"));
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, Environment.MachineName));
				return;
			}
			HttpHelpers.DeleteUrlRegistrationNonAdministratorsIfSslChanged(url);
			if (url.Contains("://*"))
			{
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, Environment.MachineName));
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "+"));
				return;
			}
			if (url.Contains("://+"))
			{
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "*"));
				HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, Environment.MachineName));
				return;
			}
			HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "*"));
			HttpHelpers.RemoveAllBindingsToThisURL(HttpHelpers.ReplaceDomainInUrl(url, "+"));
		}

		public static string ReplaceDomainInUrl(string url, string newDomain)
		{
			int num = url.IndexOf("://");
			if (num < 0)
			{
				throw new Exception("Invalid url, you must have a ://");
			}
			string str = url.Substring(num + 3);
			int num1 = (str.IndexOf(":") == -1 ? str.Length : str.IndexOf(":"));
			int num2 = (str.IndexOf("/") == -1 ? str.Length : str.IndexOf("/"));
			int num3 = Math.Min(Math.Min(str.Length, num1), num2);
			return string.Concat(url.Substring(0, num + 3), newDomain, str.Substring(num3));
		}

		public static void WithoutElevatingEnableUrlToBeOpenedWithoutElevation(string url, string applicationId)
		{
			HttpHelpers.WithoutElevatingEnableUrlToBeOpenedWithoutElevation(url, applicationId, "");
		}

		public static void WithoutElevatingEnableUrlToBeOpenedWithoutElevation(string url, string applicationId, string certificateThumbprint)
		{
			if (!url.EndsWith("/"))
			{
				url = string.Concat(url, "/");
			}
			if (!HttpHelpers.IsUrlRegisteredForNonAdministrators(url))
			{
				HttpHelpers.AddUrlRegistrationForNonAdministators(url);
			}
			if (HttpHelpers.IsUrlSsl(url))
			{
				HttpHelpers.BindPortToCertificate(HttpHelpers.GetPortFromUrl(url), applicationId, certificateThumbprint);
			}
		}

		private enum MyAssemblies
		{
			NotBound,
			UnknownAssembly,
			HL7Soup,
			IntegrationHost
		}
	}
}