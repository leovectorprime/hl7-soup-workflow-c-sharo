using System;

namespace HL7Soup
{
	public enum PartTypes
	{
		unknown,
		Segment,
		Field,
		RepeatField,
		Component,
		SubComponent
	}
}