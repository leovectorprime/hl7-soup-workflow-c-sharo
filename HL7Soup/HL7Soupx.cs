using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class HL7Soupx
	{
		public string HL7Message
		{
			get;
			set;
		}

		public HL7Soup.Message Message
		{
			get;
			private set;
		}

		public int MyProperty
		{
			get;
			set;
		}

		public HL7Soupx()
		{
		}

		public void SetMessage(string message)
		{
			this.Message = new HL7Soup.Message(message, true);
		}

		public override string ToString()
		{
			return string.Concat(string.Concat(string.Concat("Current Field = ", (this.Message.CurrentSegment == null || this.Message.CurrentSegment.CurrentField == null ? "null" : this.Message.CurrentSegment.CurrentField.ToString())), "\r\nCurrent Segment header = ", (this.Message.CurrentSegment != null ? this.Message.CurrentSegment.Header.ToString() : "null")), "\r\nlocation Code = ", this.Message.GetCurrentPath());
		}
	}
}