using System;

namespace HL7Soup
{
	public delegate void RaiseMessageEventHandler(object sender, string text);
}