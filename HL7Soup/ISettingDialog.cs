using HL7Soup.Dialogs;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public interface ISettingDialog
	{
		IActivityHost ActivityHost
		{
			get;
			set;
		}

		List<ISettingDialog> AlternativeSettingDialogList
		{
			get;
			set;
		}

		bool Disabled
		{
			get;
		}

		bool HasBeenLoaded
		{
			get;
			set;
		}

		bool IsValid
		{
			get;
			set;
		}

		Guid SettingId
		{
			get;
		}

		string SettingTypeDisplayName
		{
			get;
		}

		string ValidationMessage
		{
			get;
			set;
		}

		void ForceSettingID(Guid settingId);

		ISetting GetConnectionSetting();

		void HighlightPath(string path, MessageTypes messageType);

		void SetConnectionSetting(ISetting connection);

		void SetDisabled(bool disabled);

		void Validate();

		event DialogValidatedEventHandler DialogValidated;
	}
}