using System;

namespace HL7Soup
{
	public delegate void TextUpdatedEventHandler(object sender, TextUpdatedEventArgs e);
}