using HL7Soup.HL7Descriptions;
using HL7Soup.MessageHighlighters;
using HL7Soup.Workflow.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Windows;

namespace HL7Soup
{
	public class Message : BasePart, IComparable, IComparer, IEquatable<HL7Soup.Message>, IDisposable
	{
		private bool IsShown;

		public readonly static DependencyProperty HeaderProperty;

		public readonly static DependencyProperty MessageTypeProperty;

		private string currentSegmentDescriptionProperty = "";

		private string currentSegmentDocumentation = "";

		public readonly static DependencyProperty IsValidProperty;

		private string segmentSeperator = "\r\n|\r|\n";

		public string ActualSegmentSeperator = "\r";

		private string fieldSeperator = "|";

		private string componentSeperator = "^";

		private string subComponentSeperator = "&";

		private string fieldRepeatSeperator = "~";

		private string escapeCharacterSeperator = "\\";

		public readonly static DependencyProperty TitleProperty;

		public readonly static DependencyProperty HL7VersionProperty;

		public readonly static DependencyProperty MessageStoryProperty;

		public readonly static DependencyProperty JustReceivedProperty;

		public ObservableCollection<BasePart> AllSegments
		{
			get
			{
				ObservableCollection<BasePart> observableCollection = new ObservableCollection<BasePart>();
				if (base.ChildParts != null)
				{
					foreach (BasePart childPart in base.ChildParts)
					{
						if (childPart == null)
						{
							continue;
						}
						observableCollection.Add(childPart);
					}
				}
				return observableCollection;
			}
		}

		public int BeginUpdateCount
		{
			get;
			private set;
		}

		public string ComponentSeperator
		{
			get
			{
				return this.componentSeperator;
			}
		}

		public Component CurrentComponent
		{
			get
			{
				if (!this.HasCurrentComponent)
				{
					return null;
				}
				return this.CurrentSegment.CurrentField.CurrentComponent;
			}
		}

		public Field CurrentField
		{
			get
			{
				if (!this.HasCurrentField)
				{
					return null;
				}
				return this.CurrentSegment.CurrentField;
			}
		}

		public BasePart CurrentPart
		{
			get
			{
				if (this.HasCurrentSubComponent)
				{
					return this.CurrentSubComponent;
				}
				if (this.HasCurrentComponent)
				{
					return this.CurrentComponent;
				}
				if (this.HasCurrentField)
				{
					return this.CurrentField;
				}
				if (!this.HasCurrentSegment)
				{
					return null;
				}
				return this.CurrentSegment;
			}
		}

		public Segment CurrentSegment
		{
			get
			{
				return (Segment)base.CurrentChildPart;
			}
		}

		public string CurrentSegmentDescription
		{
			get
			{
				return this.currentSegmentDescriptionProperty;
			}
			set
			{
				if (value == "")
				{
					value = "Unknown Segment";
				}
				this.currentSegmentDescriptionProperty = value;
				base.RaisePropertyChanged("CurrentSegmentDescription");
			}
		}

		public string CurrentSegmentDocumentation
		{
			get
			{
				return this.currentSegmentDocumentation;
			}
			set
			{
				if (value == "")
				{
					value = "Unknown Segment";
				}
				this.currentSegmentDocumentation = value;
				base.RaisePropertyChanged("CurrentSegmentDocumentation");
			}
		}

		public SubComponent CurrentSubComponent
		{
			get
			{
				if (!this.HasCurrentSubComponent)
				{
					return null;
				}
				return this.CurrentSegment.CurrentField.CurrentComponent.CurrentSubComponent;
			}
		}

		public HL7Soup.HL7Descriptions.DescriptionManager DescriptionManager
		{
			get;
			set;
		}

		public string EscapeCharacterSeperator
		{
			get
			{
				return this.escapeCharacterSeperator;
			}
		}

		public string FieldRepeatSeperator
		{
			get
			{
				return this.fieldRepeatSeperator;
			}
		}

		public string FieldSeperator
		{
			get
			{
				return this.fieldSeperator;
			}
		}

		public string GetMessageStory
		{
			get
			{
				if (string.IsNullOrEmpty(this.MessageStory))
				{
					this.UpdateMessageStory();
				}
				return this.MessageStory;
			}
		}

		public bool HasCurrentComponent
		{
			get
			{
				if (base.Message.CurrentSegment == null || base.Message.CurrentSegment.CurrentField == null)
				{
					return false;
				}
				return base.Message.CurrentSegment.CurrentField.CurrentComponent != null;
			}
		}

		public bool HasCurrentField
		{
			get
			{
				if (base.Message.CurrentSegment == null)
				{
					return false;
				}
				return base.Message.CurrentSegment.CurrentField != null;
			}
		}

		public bool HasCurrentSegment
		{
			get
			{
				return base.Message.CurrentSegment != null;
			}
		}

		public bool HasCurrentSubComponent
		{
			get
			{
				if (base.Message.CurrentSegment == null || base.Message.CurrentSegment.CurrentField == null || base.Message.CurrentSegment.CurrentField.CurrentComponent == null)
				{
					return false;
				}
				return base.Message.CurrentSegment.CurrentField.CurrentComponent.CurrentSubComponent != null;
			}
		}

		public string Header
		{
			get
			{
				return (string)base.GetValue(HL7Soup.Message.HeaderProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.HeaderProperty, value);
			}
		}

		public string HL7Version
		{
			get
			{
				return (string)base.GetValue(HL7Soup.Message.HL7VersionProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.HL7VersionProperty, value);
			}
		}

		public int InternalID
		{
			get;
			set;
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(HL7Soup.Message.IsValidProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.IsValidProperty, value);
			}
		}

		public bool JustReceived
		{
			get
			{
				return (bool)base.GetValue(HL7Soup.Message.JustReceivedProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.JustReceivedProperty, value);
			}
		}

		public string MessageStory
		{
			get
			{
				return (string)base.GetValue(HL7Soup.Message.MessageStoryProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.MessageStoryProperty, value);
			}
		}

		public string MessageType
		{
			get
			{
				return (string)base.GetValue(HL7Soup.Message.MessageTypeProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.MessageTypeProperty, value);
			}
		}

		public string MessageTypeDescription
		{
			get
			{
				if (this.DescriptionManager == null)
				{
					return this.MessageType;
				}
				return this.DescriptionManager.GetMessageTypeDescription(this);
			}
		}

		public string MessageTypeDescriptionForMessagesList
		{
			get
			{
				if (Settings.Default.MessageListDescriptionField != "Patient")
				{
					if (Settings.Default.MessageListDescriptionField != "ControlId")
					{
						return this.MessageTypeDescription;
					}
					return this.GetPartText(new PathSplitter(SegmentsEnum.MSH, 10, 1));
				}
				string partNiceText = this.GetPartNiceText(new PathSplitter(SegmentsEnum.PID, 3, 1));
				if (string.IsNullOrEmpty(partNiceText))
				{
					partNiceText = this.GetPartNiceText(new PathSplitter(SegmentsEnum.PID, 2, 1));
				}
				string str = string.Concat(this.GetPartNiceText(new PathSplitter(SegmentsEnum.PID, 5, 1)), ", ", this.GetPartNiceText(new PathSplitter(SegmentsEnum.PID, 5, 2)));
				if (!string.IsNullOrEmpty(partNiceText))
				{
					str = string.Concat("(", partNiceText, ") ", str);
				}
				return str;
			}
		}

		public bool OnlyUpdateChangesMode
		{
			get;
			set;
		}

		public bool PopulatingDataTableForField
		{
			get;
			set;
		}

		public DateTime ReceivedDate
		{
			get;
			set;
		}

		public string ReceivedDateString
		{
			get
			{
				if (this.ReceivedDate != DateTime.MinValue)
				{
					return string.Format("{0:HH:mm:ss}", this.ReceivedDate);
				}
				return Helpers.ParseISO8601StringToString(this.GetPartNiceText(new PathSplitter(SegmentsEnum.MSH, 7)));
			}
		}

		public string ReceivedDateTooltip
		{
			get
			{
				if (this.ReceivedDate == DateTime.MinValue)
				{
					return string.Concat("Message Date (MSH-7), ", Helpers.GetTimeAgo(this.GetPartNiceText(new PathSplitter(SegmentsEnum.MSH, 7))));
				}
				return string.Concat("Received time.  The Message date (MSH-7) is ", Helpers.ParseISO8601StringToString(this.GetPartNiceText(new PathSplitter(SegmentsEnum.MSH, 7))));
			}
		}

		public Dictionary<string, Segment> Segments
		{
			get;
			private set;
		}

		public string SegmentSeperator
		{
			get
			{
				return this.segmentSeperator;
			}
		}

		public char[] Seperators
		{
			get;
			set;
		}

		public static string SeperatorText
		{
			get;
			set;
		}

		public string SubComponentSeperator
		{
			get
			{
				return this.subComponentSeperator;
			}
		}

		public string Title
		{
			get
			{
				return (string)base.GetValue(HL7Soup.Message.TitleProperty);
			}
			set
			{
				base.SetValue(HL7Soup.Message.TitleProperty, value);
			}
		}

		public List<ValidationResult> ValidationResults
		{
			get;
			set;
		}

		static Message()
		{
			HL7Soup.Message.HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(HL7Soup.Message), new PropertyMetadata(""));
			HL7Soup.Message.MessageTypeProperty = DependencyProperty.Register("MessageType", typeof(string), typeof(HL7Soup.Message), new PropertyMetadata(""));
			HL7Soup.Message.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(HL7Soup.Message), new PropertyMetadata(true));
			HL7Soup.Message.TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(HL7Soup.Message), new PropertyMetadata("The Title"));
			HL7Soup.Message.HL7VersionProperty = DependencyProperty.Register("HL7Version", typeof(string), typeof(HL7Soup.Message), new PropertyMetadata("2.5.1"));
			HL7Soup.Message.MessageStoryProperty = DependencyProperty.Register("MessageStory", typeof(string), typeof(HL7Soup.Message), new PropertyMetadata(null));
			HL7Soup.Message.JustReceivedProperty = DependencyProperty.Register("JustReceived", typeof(bool), typeof(HL7Soup.Message), new PropertyMetadata(false));
		}

		public Message(string text, bool isShown, int internalId) : this(text, isShown)
		{
			this.InternalID = internalId;
		}

		public Message(string text, bool isShown) : base(text, null, "", 0, 0, null)
		{
			this.IsShown = isShown;
			base.Load();
		}

		public void BeginUpdate()
		{
			this.BeginUpdateCount = this.BeginUpdateCount + 1;
		}

		public int CompareTo(object obj)
		{
			return ((HL7Soup.Message)obj).InternalID.CompareTo(this.InternalID);
		}

		private void CreateNewDescriptionManager(string version)
		{
			if (this.DescriptionManager != null)
			{
				this.DescriptionManager.DataTableUpdated -= new DataTableUpdatedEventHandler(this.DescriptionManager_DataTableUpdated);
			}
			this.DescriptionManager = HL7Soup.HL7Descriptions.DescriptionManager.GetDescriptionManager(version);
			if (this.IsShown)
			{
				this.DescriptionManager.DataTableUpdated += new DataTableUpdatedEventHandler(this.DescriptionManager_DataTableUpdated);
			}
		}

		internal BasePart CreatePart(PathSplitter path, string fromValue)
		{
			if (path.IsInvalidBecauseOfError || !path.IsValid)
			{
				throw new Exception(string.Concat("Invalid HL7 Path.  ", path.InvalidReason));
			}
			if (path.PathTerminationPoint == PathTypes.SegmentIndex || path.PathTerminationPoint == PathTypes.Segment)
			{
				int segmentIndexInt = path.SegmentIndexInt;
				if (segmentIndexInt > 1)
				{
					int num = this.Segments.Count<KeyValuePair<string, Segment>>((KeyValuePair<string, Segment> x) => x.Key.StartsWith(path.Segment));
					if (num + 1 < segmentIndexInt)
					{
						StringBuilder stringBuilder = new StringBuilder(base.Text);
						for (int i = 0; i < segmentIndexInt - (num + 1); i++)
						{
							stringBuilder.Append(string.Concat("\r", path.Segment, base.Message.FieldSeperator, base.Message.FieldSeperator));
						}
						this.SetTextQuietly(stringBuilder.ToString());
					}
				}
				this.SetTextQuietly(string.Concat(base.Text, "\r", fromValue));
				this.Reload();
				return this.Segments.Values.Last<Segment>();
			}
			if (path.PathTerminationPoint == PathTypes.FieldRepeatIndex)
			{
				PathSplitter pathSplitter = new PathSplitter(path.ToFieldTerminatingPath());
				BasePart part = this.GetPart(pathSplitter);
				if (part == null)
				{
					part = this.CreatePart(pathSplitter, pathSplitter.Field);
					part.Load();
				}
				int count = ((Field)part).GetRelatedRepeatField().Count;
				count++;
				if (count > 1)
				{
					pathSplitter = new PathSplitter(string.Concat(new object[] { pathSplitter, "[", count, "]" }));
					part = this.GetPart(pathSplitter);
				}
				string str = "".PadRight(path.FieldRepeatIndexInt - count, Convert.ToChar(base.Message.FieldRepeatSeperator));
				part.SetTextQuietly(string.Concat(part.Text, str));
				part.Reload();
				part.RebuildParentTextQuietly();
				part.Parent.Reload();
				return this.GetPart(path);
			}
			if (path.PathTerminationPoint == PathTypes.Component)
			{
				BasePart basePart = null;
				if (!string.IsNullOrEmpty(path.FieldRepeatIndex))
				{
					PathSplitter pathSplitter1 = new PathSplitter(path.ToFieldRepeatIndexTerminatingPath());
					basePart = this.GetPart(pathSplitter1);
					if (basePart == null)
					{
						basePart = this.CreatePart(pathSplitter1, pathSplitter1.FieldRepeatIndex);
						if (basePart == null)
						{
							throw new Exception(string.Format("Couldn't add repeat field to fieldPath {0} at index {1}", pathSplitter1, pathSplitter1.FieldRepeatIndex));
						}
						basePart.Load();
					}
				}
				else
				{
					PathSplitter pathSplitter2 = new PathSplitter(path.ToFieldTerminatingPath());
					basePart = this.GetPart(pathSplitter2);
					if (basePart == null)
					{
						basePart = this.CreatePart(pathSplitter2, pathSplitter2.Field);
						if (basePart == null)
						{
							throw new Exception(string.Format("Couldn't add Component to ComponentPath {0} at Field {1}", pathSplitter2, pathSplitter2.Field));
						}
						basePart.Load();
					}
				}
				int count1 = basePart.ChildParts.Count;
				if (count1 == 0)
				{
					count1 = 1;
				}
				string str1 = "".PadRight(int.Parse(path.Component) - count1, Convert.ToChar(base.Message.ComponentSeperator));
				basePart.SetTextQuietly(string.Concat(basePart.Text, str1));
				basePart.Reload();
				return this.GetPart(path);
			}
			if (path.PathTerminationPoint == PathTypes.SubComponent)
			{
				PathSplitter pathSplitter3 = new PathSplitter(path.ToComponentTerminatingPath());
				BasePart part1 = this.GetPart(pathSplitter3);
				if (part1 == null)
				{
					part1 = this.CreatePart(pathSplitter3, pathSplitter3.Component);
					part1.Load();
				}
				int num1 = part1.ChildParts.Count;
				if (num1 == 0)
				{
					num1 = 1;
				}
				string str2 = "".PadRight(int.Parse(path.SubComponent) - num1, Convert.ToChar(base.Message.SubComponentSeperator));
				part1.SetTextQuietly(string.Concat(part1.Text, str2));
				part1.Reload();
				return this.GetPart(path);
			}
			if (path.PathTerminationPoint != PathTypes.Field)
			{
				return null;
			}
			PathSplitter pathSplitter4 = new PathSplitter(path.ToSegmentIndexTerminatingPath(), true);
			BasePart basePart1 = this.GetPart(pathSplitter4);
			if (basePart1 == null)
			{
				basePart1 = this.CreatePart(pathSplitter4, pathSplitter4.Segment);
				basePart1.Load();
			}
			string str3 = "".PadRight(int.Parse(path.Field) - (basePart1.ChildParts[basePart1.ChildParts.Count - 1].Index + 1), Convert.ToChar(base.Message.FieldSeperator));
			basePart1.SetTextQuietly(string.Concat(basePart1.Text, str3));
			basePart1.Reload();
			return this.GetPart(path);
		}

		private void DescriptionManager_DataTableUpdated(object sender, EventArgs e)
		{
			if (this.DataTableUpdated != null)
			{
				this.DataTableUpdated(sender, e);
			}
		}

		public void Dispose()
		{
			this.DescriptionManager.DataTableUpdated -= new DataTableUpdatedEventHandler(this.DescriptionManager_DataTableUpdated);
		}

		public void EndUpdate()
		{
			this.BeginUpdateCount = this.BeginUpdateCount - 1;
			if (this.BeginUpdateCount < 1)
			{
				this.BeginUpdateCount = 0;
				this.Reload();
			}
		}

		public static string FixMessageSeperators(HL7Soup.Message message)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (message.ChildParts != null && message.ChildParts.Count == 1 && message.Text.Contains("MSH"))
			{
				int start = 0;
				message.DescriptionManager.UpdateCurrent(message);
				((Segment)message.AllSegments[0]).Load();
				List<BasePart> childParts = message.ChildParts[0].ChildParts;
				if (childParts == null)
				{
					return "";
				}
				if (childParts.Count > 30)
				{
					foreach (BasePart childPart in childParts)
					{
						string str = childPart.Text.TrimStart(Array.Empty<char>());
						if (childPart.Text.Length != 4 || !childPart.Text.StartsWith(" "))
						{
							if (str.Length <= 4 || !(str.Substring(str.Length - 4, 1) == " "))
							{
								continue;
							}
							str = str.Substring(str.Length - 3, 3);
							if (!message.DescriptionManager.IsValidSegmentHeader(str))
							{
								continue;
							}
							int length = childPart.Text.Length - 3;
							stringBuilder.Append(message.Text.Substring(start, childPart.PositionInTheDocument.Start - start + length - 1));
							stringBuilder.Append("\r");
							start = childPart.PositionInTheDocument.Start + length;
						}
						else
						{
							if (!message.DescriptionManager.IsValidSegmentHeader(str) || !(str != "MSH"))
							{
								continue;
							}
							stringBuilder.Append(message.Text.Substring(start, childPart.PositionInTheDocument.Start - start - 1));
							stringBuilder.Append("\r");
							start = childPart.PositionInTheDocument.Start + childPart.PositionInTheDocument.Length - 3;
						}
					}
					if (start == 0)
					{
						foreach (BasePart basePart in childParts)
						{
							string str1 = basePart.Text.TrimStart(Array.Empty<char>());
							if (str1.Length <= 2)
							{
								continue;
							}
							str1 = str1.Substring(str1.Length - 3, 3);
							if (!(str1 != "MSH") || !message.DescriptionManager.IsValidSegmentHeaderForCurrentMessageType(str1))
							{
								continue;
							}
							int num = basePart.Text.Length - 3;
							stringBuilder.Append(message.Text.Substring(start, basePart.PositionInTheDocument.Start - start + num));
							stringBuilder.Append("\r");
							start = basePart.PositionInTheDocument.Start + num;
						}
					}
					if (start > 0)
					{
						stringBuilder.Append(message.Text.Substring(start).TrimStart(Array.Empty<char>()));
					}
				}
			}
			return stringBuilder.ToString();
		}

		public PartTypes GetCurrentPartType()
		{
			if (this.HasCurrentSubComponent)
			{
				return PartTypes.SubComponent;
			}
			if (this.HasCurrentComponent)
			{
				return PartTypes.Component;
			}
			if (this.HasCurrentField)
			{
				return PartTypes.Field;
			}
			if (this.HasCurrentSegment)
			{
				return PartTypes.Segment;
			}
			return PartTypes.unknown;
		}

		public override string GetCurrentPath()
		{
			if (base.CurrentChildPart == null)
			{
				return this.Header;
			}
			return base.CurrentChildPart.GetCurrentPath();
		}

		public override string GetDescriptionName()
		{
			return "";
		}

		public string GetMessageHeader(string message)
		{
			string[] strArrays = message.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				string str1 = str.Trim();
				if (str1.ToUpper().StartsWith("MSH"))
				{
					if (str1.Length > 3)
					{
						this.fieldSeperator = str1.Substring(3, 1);
					}
					string[] strArrays1 = (new Segment(str, this, "MSH", 0, 0, 0, null)).SplitSegmentIntoFields(str);
					if ((int)strArrays1.Length > 1)
					{
						this.SetSeperators(strArrays1[1]);
					}
					if ((int)strArrays1.Length > 8)
					{
						this.MessageType = strArrays1[8];
					}
					if ((int)strArrays1.Length > 11 && this.HL7Version != strArrays1[11])
					{
						this.HL7Version = strArrays1[11];
						this.CreateNewDescriptionManager(this.HL7Version);
						base.RaiseDescriptionManagerChanged();
					}
					if (this.DescriptionManager == null)
					{
						this.CreateNewDescriptionManager("2.5.1");
					}
					return str;
				}
			}
			if (this.DescriptionManager == null)
			{
				this.CreateNewDescriptionManager("2.5.1");
			}
			return "";
		}

		public override BasePart GetPart(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Segment))
			{
				BasePart segment = this.GetSegment(path);
				if (path.PathTerminationPoint == PathTypes.Segment || path.PathTerminationPoint == PathTypes.SegmentIndex || path.PathTerminationPoint == PathTypes.Hyphen)
				{
					return segment;
				}
				if (segment != null)
				{
					BasePart part = segment.GetPart(path);
					if (part != null)
					{
						return part;
					}
				}
			}
			return null;
		}

		public string GetPartNiceText(string segment, int field)
		{
			PathSplitter pathSplitter = new PathSplitter(segment, field);
			return SecurityElement.Escape(this.GetPartText(pathSplitter).Trim());
		}

		public string GetPartNiceText(string segment, int field, int component)
		{
			PathSplitter pathSplitter = new PathSplitter(segment, field, component);
			return SecurityElement.Escape(this.GetPartText(pathSplitter).Trim());
		}

		public string GetPartNiceText(PathSplitter path)
		{
			return SecurityElement.Escape(this.GetPartText(path).Trim());
		}

		public List<BasePart> GetParts(PathSplitter path)
		{
			BasePart part = this.GetPart(path);
			List<BasePart> baseParts = new List<BasePart>()
			{
				part
			};
			Field field = part as Field;
			if (field != null && string.IsNullOrEmpty(path.FieldRepeatIndex) && field.HasSiblings())
			{
				baseParts.AddRange(field.GetRelatedRepeatField());
			}
			return baseParts;
		}

		public object[] GetPartsText(List<PathSplitter> paths)
		{
			List<string> strs = new List<string>();
			foreach (PathSplitter path in paths)
			{
				strs.Add(this.GetPartText(path));
			}
			return strs.ToArray();
		}

		public string GetPartText(PathSplitter path)
		{
			BasePart part = this.GetPart(path);
			if (part == null)
			{
				return "";
			}
			return part.Text;
		}

		public BasePart GetSegment(PathSplitter path)
		{
			string str = (path.SegmentIndex == "[0]" || path.SegmentIndex == "[1]" ? path.Segment : string.Concat(path.Segment, path.SegmentIndex));
			if (!this.Segments.ContainsKey(str))
			{
				return null;
			}
			return this.Segments[str];
		}

		public Dictionary<string, Segment> GetSegments(HL7Soup.Message message)
		{
			return this.GetSegments(message.Text);
		}

		public Dictionary<string, Segment> GetSegments(string text)
		{
			string[] strArrays = this.SplitMessageIntoSegment(text);
			Dictionary<string, Segment> strs = new Dictionary<string, Segment>();
			int length = 0;
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				int num = strArrays[i].Length;
				string str = strArrays[i].TrimStart(Array.Empty<char>());
				int length1 = strArrays[i].Length - str.Length;
				str = str.TrimEnd(Array.Empty<char>());
				if (!string.IsNullOrWhiteSpace(str))
				{
					int num1 = 1;
					string header = Segment.GetHeader(str);
					string str1 = header;
					while (true)
					{
						if (!strs.ContainsKey((num1 == 1 ? str1 : string.Concat(str1, "[", num1.ToString(), "]"))))
						{
							break;
						}
						num1++;
					}
					if (num1 > 1)
					{
						str1 = string.Concat(str1, "[", num1.ToString(), "]");
					}
					Segment segment = new Segment(str, base.Message, str1, length + length1, 0, length1, this.DescriptionManager.Segments.GetByName(header));
					strs.Add(str1, segment);
				}
				length = length + num + this.ActualSegmentSeperator.Length;
			}
			return strs;
		}

		protected override void LoadChildParts()
		{
			this.Segments = this.GetSegments(this);
			base.ChildParts = this.Segments.Values.Cast<BasePart>().ToList<BasePart>();
		}

		internal void RaiseTextUpdated(BasePart basePart, int oldLength)
		{
			if (this.TextUpdated != null)
			{
				this.TextUpdated(basePart, new TextUpdatedEventArgs()
				{
					OldLength = oldLength
				});
			}
		}

		public override void Reload()
		{
			if (this.BeginUpdateCount > 0)
			{
				return;
			}
			this.Header = this.GetMessageHeader(base.Text);
			this.ActualSegmentSeperator = "\r";
			base.Reload();
			if (this.IsShown)
			{
				this.ValidationResults = ValidationManager.Instance.GetValidationResults(this);
				this.UpdateMessageStory();
			}
			base.RaiseAllSegmentsChanged();
			if (this.Reloaded != null)
			{
				this.Reloaded(this, null);
			}
		}

		public override void SetAsCurrentItem()
		{
			this.UpdateDescriptionManager();
		}

		public void SetCurrentLocation(int location, int lineNumber)
		{
			if (!base.IsLoaded)
			{
				base.Load();
			}
			int num = 0;
			string[] strArrays = this.SplitMessageIntoSegment(base.Text);
			for (int i = 0; i < lineNumber; i++)
			{
				if (string.IsNullOrWhiteSpace(strArrays[i]))
				{
					num++;
					if (i == lineNumber - 1)
					{
						base.CursorLocation = location;
						base.CurrentChildPart = null;
						this.UpdateDescriptionManager();
						return;
					}
				}
			}
			lineNumber -= num;
			base.CursorLocation = location;
			if (base.ChildParts != null && base.ChildParts.Count > 0 && lineNumber - 1 < base.ChildParts.Count)
			{
				Segment item = (Segment)base.ChildParts[lineNumber - 1];
				location -= item.FirstNonSpaceCharacter;
				item.SetCurrentLocation(location);
				if (base.CurrentChildPart != item)
				{
					base.CurrentChildPart = item;
				}
			}
			this.UpdateDescriptionManager();
			base.RaiseCurrentPartChanged();
		}

		public override HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Segment))
			{
				BasePart segment = this.GetSegment(path);
				if (segment != null)
				{
					if (!segment.Equals(base.CurrentChildPart))
					{
						base.CurrentChildPart = segment;
					}
					if (base.CurrentChildPart != null)
					{
						HL7Soup.PositionInTheDocument positionInTheDocument = base.CurrentChildPart.SetCurrentPath(path);
						if (positionInTheDocument == null)
						{
							return new HL7Soup.PositionInTheDocument(this.CurrentSegment.LocationOfFirstCharacter, this.CurrentSegment.Text.Length);
						}
						HL7Soup.PositionInTheDocument start = positionInTheDocument;
						start.Start = start.Start + this.CurrentSegment.LocationOfFirstCharacter;
						return positionInTheDocument;
					}
				}
			}
			return null;
		}

		public void SetMessage(string text)
		{
			base.SetText(text);
			this.Reload();
		}

		public void SetSeperators(string seperatorText)
		{
			HL7Soup.Message.SeperatorText = seperatorText;
			char[] charArray = seperatorText.ToCharArray();
			this.Seperators = charArray;
			if (this.Seperators == null)
			{
				this.fieldSeperator = "|";
				this.segmentSeperator = "\r\n|\r|\n";
				this.componentSeperator = "^";
				this.subComponentSeperator = "&";
				this.fieldRepeatSeperator = "~";
				this.escapeCharacterSeperator = "\\";
			}
			if (charArray.Length != 0)
			{
				this.componentSeperator = charArray[0].ToString();
				if ((int)charArray.Length > 1)
				{
					this.fieldRepeatSeperator = charArray[1].ToString();
					if ((int)charArray.Length > 2)
					{
						this.escapeCharacterSeperator = charArray[2].ToString();
						if ((int)charArray.Length > 3)
						{
							this.subComponentSeperator = charArray[3].ToString();
						}
					}
				}
			}
		}

		public string[] SplitMessageIntoSegment(string text)
		{
			return base.Split(text, this.SegmentSeperator);
		}

		public string[] SplitMessageIntoSegment(HL7Soup.Message message)
		{
			return this.SplitMessageIntoSegment(message.Text);
		}

		int System.Collections.IComparer.Compare(object a, object b)
		{
			HL7Soup.Message message = (HL7Soup.Message)a;
			HL7Soup.Message message1 = (HL7Soup.Message)b;
			if (message.InternalID > message1.InternalID)
			{
				return 1;
			}
			if (message.InternalID < message1.InternalID)
			{
				return -1;
			}
			return 0;
		}

		bool System.IEquatable<HL7Soup.Message>.Equals(HL7Soup.Message other)
		{
			if (other == null)
			{
				return false;
			}
			if (this == other)
			{
				return true;
			}
			return this.InternalID == other.InternalID;
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.Segments == null)
			{
				return "Empty Message";
			}
			foreach (KeyValuePair<string, Segment> segment in this.Segments)
			{
				stringBuilder.Append(segment.Value.ToString());
				stringBuilder.Append(this.ActualSegmentSeperator);
			}
			return stringBuilder.ToString();
		}

		public void UpdateDescriptionManager()
		{
			base.Message.DescriptionManager.UpdateCurrent(base.Message);
			if (this.DescriptionManagerUpdated != null)
			{
				this.DescriptionManagerUpdated(this, null);
			}
		}

		public void UpdateMessageStory()
		{
			StoryBuilder storyBuilder = new StoryBuilder(this.DescriptionManager, this.ValidationResults);
			this.MessageStory = storyBuilder.BuildStory(this.Segments.Values.ToList<Segment>());
		}

		public event DataTableUpdatedEventHandler DataTableUpdated;

		public event EventHandler DescriptionManagerUpdated;

		public event EventHandler Reloaded;

		public event TextUpdatedEventHandler TextUpdated;
	}
}