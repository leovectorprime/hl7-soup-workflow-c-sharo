using System;

namespace HL7Soup.MessageLog
{
	public enum LogItemDataType
	{
		Sent,
		Received,
		Response,
		Error,
		None
	}
}