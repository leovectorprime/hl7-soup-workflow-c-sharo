using HL7Soup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	[JsonObject]
	[Serializable]
	public class LogItem : ILogItem
	{
		public Dictionary<Guid, ActivityLogItem> Activities
		{
			get;
			set;
		}

		public DateTime CompletedDate
		{
			get;
			set;
		}

		public string CompletedDateString
		{
			get
			{
				if (this.CompletedDate == DateTime.MinValue)
				{
					return "---- -- --";
				}
				if (this.CompletedDate > DateTime.Today)
				{
					return string.Format("{0:HH:mm:ss}", this.CompletedDate);
				}
				return string.Format("{0:g}", this.CompletedDate);
			}
		}

		public string CompletedDateTooltip
		{
			get
			{
				if (this.CompletedDate == DateTime.MinValue)
				{
					return "Not completed yet.  Try clicking refresh to see if it's completed now.";
				}
				return string.Concat("Completed processing message ", Helpers.GetTimeAgo(this.CompletedDate));
			}
		}

		public string Duration
		{
			get
			{
				if (this.CompletedDate == DateTime.MinValue)
				{
					return "";
				}
				TimeSpan completedDate = this.CompletedDate - this.StartDate;
				return string.Format("{0:g}ms", (int)completedDate.TotalMilliseconds);
			}
		}

		public string ErrorMessage
		{
			get;
			set;
		}

		public Guid Id
		{
			get;
			set;
		}

		public int Instance
		{
			get;
			set;
		}

		public string ResponseMessage
		{
			get;
			set;
		}

		public string SourceMessage
		{
			get;
			set;
		}

		public DateTime StartDate
		{
			get;
			set;
		}

		public string StartDateString
		{
			get
			{
				if (this.StartDate == DateTime.MinValue)
				{
					return "---- -- --";
				}
				if (this.StartDate > DateTime.Today)
				{
					return string.Format("{0:HH:mm:ss}", this.StartDate);
				}
				return string.Format("{0:g}", this.StartDate);
			}
		}

		public string StartDateTooltip
		{
			get
			{
				if (this.StartDate == DateTime.MinValue)
				{
					return "Even I don't know when this was sent :(";
				}
				return string.Concat("Started processing message ", Helpers.GetTimeAgo(this.StartDate));
			}
		}

		public LogItemStatus Status
		{
			get;
			set;
		}

		public string StatusText
		{
			get
			{
				return Enum.GetName(typeof(LogItemStatus), this.Status);
			}
		}

		public List<Variable> Variables
		{
			get;
			set;
		}

		public LogItem()
		{
			this.Activities = new Dictionary<Guid, ActivityLogItem>();
		}
	}
}