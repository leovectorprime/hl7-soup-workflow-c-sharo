using System;

namespace HL7Soup.MessageLog
{
	public enum LogItemStatus
	{
		Processing,
		Completed,
		Errored,
		Cancelled,
		Filtered
	}
}