using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	public class ErrorEvent : MessageEvent
	{
		public string Error
		{
			get;
			set;
		}

		public ErrorEvent(string message, Guid rootFunctionId, int rootFunctionInstanceId, Guid functionId, string error)
		{
			base.RootFunctionId = rootFunctionId;
			base.FunctionId = functionId;
			this.Error = error;
			base.Instance = rootFunctionInstanceId;
		}
	}
}