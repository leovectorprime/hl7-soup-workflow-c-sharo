using HL7Soup;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	public sealed class MessageLogger
	{
		private static volatile MessageLogger instance;

		private static object syncRoot;

		public readonly static DateTime nodate;

		private ConcurrentDictionary<Guid, ConcurrentDictionary<int, ILogItem>> Logs = new ConcurrentDictionary<Guid, ConcurrentDictionary<int, ILogItem>>();

		public static MessageLogger Instance
		{
			get
			{
				if (MessageLogger.instance == null)
				{
					lock (MessageLogger.syncRoot)
					{
						if (MessageLogger.instance == null)
						{
							MessageLogger.instance = new MessageLogger();
						}
					}
				}
				return MessageLogger.instance;
			}
		}

		static MessageLogger()
		{
			MessageLogger.syncRoot = new object();
			MessageLogger.nodate = new DateTime(1900, 1, 1);
		}

		private MessageLogger()
		{
		}

		public void AddMessage(MessageEvent messageEvent)
		{
			ConcurrentDictionary<int, ILogItem> orAdd = this.Logs.GetOrAdd(messageEvent.RootFunctionId, new ConcurrentDictionary<int, ILogItem>());
			MessageLogger.TrunctateMessageLog(orAdd);
			LogItem logItem = new LogItem();
			ActivityLogItem activityLogItem = new ActivityLogItem();
			MessageLogger.CreateNewInstanceValues(messageEvent, activityLogItem);
			activityLogItem.Id = messageEvent.FunctionId;
			logItem.Activities[messageEvent.FunctionId] = activityLogItem;
			MessageLogger.CreateNewInstanceValues(messageEvent, logItem);
			logItem.Id = messageEvent.RootFunctionId;
			logItem.Instance = messageEvent.Instance;
			orAdd.AddOrUpdate(messageEvent.Instance, logItem, (int key, ILogItem existingVal) => {
				MessageLogger.UpdateInstanceValues(messageEvent, existingVal, logItem);
				Dictionary<Guid, ActivityLogItem> guids = new Dictionary<Guid, ActivityLogItem>();
				foreach (KeyValuePair<Guid, ActivityLogItem> activity in existingVal.Activities)
				{
					guids.Add(activity.Key, activity.Value);
				}
				if (guids.ContainsKey(messageEvent.FunctionId))
				{
					activityLogItem = guids[messageEvent.FunctionId];
					MessageLogger.UpdateInstanceValues(messageEvent, activityLogItem, logItem);
				}
				guids[messageEvent.FunctionId] = activityLogItem;
				existingVal.Activities = guids;
				return existingVal;
			});
		}

		private bool ContainsSearchText(bool searchActivities, bool searchResponse, string searchText, Dictionary<Guid, ActivityLogItem>.ValueCollection values)
		{
			if (string.IsNullOrEmpty(searchText))
			{
				return true;
			}
			if (values == null || values.Count == 0)
			{
				return false;
			}
			ActivityLogItem activityLogItem = values.ElementAt<ActivityLogItem>(0);
			if (activityLogItem.SourceMessage.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > -1)
			{
				return true;
			}
			if (searchResponse && activityLogItem.ResponseMessage != null && activityLogItem.ResponseMessage.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > -1)
			{
				return true;
			}
			if (!searchActivities || values.Count == 1)
			{
				return false;
			}
			for (int i = 1; i < values.Count; i++)
			{
				ActivityLogItem activityLogItem1 = values.ElementAt<ActivityLogItem>(0);
				if (activityLogItem1.SourceMessage.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > -1)
				{
					return true;
				}
				if (searchResponse && activityLogItem1.ResponseMessage != null && activityLogItem1.ResponseMessage.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) > -1)
				{
					return true;
				}
			}
			return false;
		}

		private static void CreateNewInstanceValues(MessageEvent messageEvent, LogItem logItem)
		{
			logItem.Id = messageEvent.FunctionId;
			logItem.Status = messageEvent.Status;
			logItem.StartDate = messageEvent.Date;
			logItem.Variables = messageEvent.Variables;
			if (messageEvent.DataType == LogItemDataType.Sent || messageEvent.DataType == LogItemDataType.Received)
			{
				logItem.SourceMessage = messageEvent.Message;
			}
		}

		private bool ErrorsOnly(bool errorsOnly, ILogItem logItem)
		{
			if (!errorsOnly)
			{
				return true;
			}
			return logItem.Status == LogItemStatus.Errored;
		}

		public ConcurrentDictionary<int, ILogItem> GetLog(Guid id)
		{
			if (!this.Logs.ContainsKey(id))
			{
				return new ConcurrentDictionary<int, ILogItem>();
			}
			return this.Logs[id];
		}

		public LogResults GetLogs(Guid workflowId, int skip, int take, DateTime startDate, DateTime endDate, bool searchActivities, bool searchResponse, bool errorsOnly, string searchText)
		{
			ConcurrentDictionary<int, ILogItem> log = MessageLogger.Instance.GetLog(workflowId);
			LogResults logResult = new LogResults()
			{
				ServerCount = log.Values.Count<ILogItem>()
			};
			if (startDate > MessageLogger.nodate)
			{
				if (endDate <= MessageLogger.nodate)
				{
					IEnumerable<ILogItem> logItems = log.Values.Where<ILogItem>((ILogItem p) => {
						if (!(p.StartDate > startDate) || !this.ErrorsOnly(errorsOnly, p))
						{
							return false;
						}
						return this.ContainsSearchText(searchActivities, searchResponse, searchText, p.Activities.Values);
					});
					logResult.ServerCount = logItems.Count<ILogItem>();
					logResult.LogItems = (
						from p in logItems
						orderby p.Instance descending
						select p).Skip<ILogItem>(skip).Take<ILogItem>(take).Cast<LogItem>();
				}
				else
				{
					IEnumerable<ILogItem> logItems1 = log.Values.Where<ILogItem>((ILogItem p) => {
						if (!(p.StartDate > startDate) || !(p.StartDate < endDate) || !this.ErrorsOnly(errorsOnly, p))
						{
							return false;
						}
						return this.ContainsSearchText(searchActivities, searchResponse, searchText, p.Activities.Values);
					});
					logResult.ServerCount = logItems1.Count<ILogItem>();
					logResult.LogItems = (
						from p in logItems1
						orderby p.Instance descending
						select p).Skip<ILogItem>(skip).Take<ILogItem>(take).Cast<LogItem>();
				}
			}
			else if (endDate > MessageLogger.nodate)
			{
				IEnumerable<ILogItem> logItems2 = log.Values.Where<ILogItem>((ILogItem p) => {
					if (!(p.StartDate < endDate) || !this.ErrorsOnly(errorsOnly, p))
					{
						return false;
					}
					return this.ContainsSearchText(searchActivities, searchResponse, searchText, p.Activities.Values);
				});
				logResult.ServerCount = logItems2.Count<ILogItem>();
				logResult.LogItems = (
					from p in logItems2
					orderby p.Instance descending
					select p).Skip<ILogItem>(skip).Take<ILogItem>(take).Cast<LogItem>();
			}
			else if (!(!string.IsNullOrEmpty(searchText) | errorsOnly))
			{
				logResult.LogItems = (
					from p in log.Values
					orderby p.Instance descending
					select p).Skip<ILogItem>(skip).Take<ILogItem>(take).Cast<LogItem>();
				logResult.ServerCount = log.Values.Count<ILogItem>();
			}
			else
			{
				IEnumerable<ILogItem> logItems3 = log.Values.Where<ILogItem>((ILogItem p) => {
					if (!this.ErrorsOnly(errorsOnly, p))
					{
						return false;
					}
					return this.ContainsSearchText(searchActivities, searchResponse, searchText, p.Activities.Values);
				});
				logResult.ServerCount = logItems3.Count<ILogItem>();
				logResult.LogItems = (
					from p in logItems3
					orderby p.Instance descending
					select p).Skip<ILogItem>(skip).Take<ILogItem>(take).Cast<LogItem>();
			}
			return logResult;
		}

		public void Load()
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "IntegrationHost.Server\\ServerLogs.dat");
			if (File.Exists(str))
			{
				MessageLogger.Instance.Logs = (ConcurrentDictionary<Guid, ConcurrentDictionary<int, ILogItem>>)Helpers.Deserialize(str);
			}
		}

		public void RemoveLogsForMissingWorkflows(List<Guid> knownWorkflows)
		{
			List<Guid> guids = new List<Guid>();
			foreach (KeyValuePair<Guid, ConcurrentDictionary<int, ILogItem>> log in this.Logs)
			{
				Guid key = log.Key;
				bool flag = false;
				foreach (Guid knownWorkflow in knownWorkflows)
				{
					if (knownWorkflow != key)
					{
						continue;
					}
					flag = true;
					goto Label0;
				}
			Label0:
				if (flag)
				{
					continue;
				}
				guids.Add(key);
			}
			foreach (Guid guid in guids)
			{
				this.RemoveWorkflowLog(guid);
			}
		}

		public void RemoveWorkflowLog(Guid id)
		{
			if (this.Logs.ContainsKey(id))
			{
				ConcurrentDictionary<int, ILogItem> nums = null;
				this.Logs.TryRemove(id, out nums);
			}
		}

		public void Save()
		{
			Helpers.Serialize(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "IntegrationHost.Server\\ServerLogs.dat"), MessageLogger.Instance.Logs);
		}

		private static void TrunctateMessageLog(ConcurrentDictionary<int, ILogItem> FunctionLog)
		{
		Label0:
			while (FunctionLog.Count > 1100)
			{
				foreach (ILogItem logItem in 
					from p in FunctionLog.Values
					orderby p.Instance
					select p)
				{
					ILogItem logItem1 = null;
					FunctionLog.TryRemove(logItem.Instance, out logItem1);
					if (FunctionLog.Count > 1000)
					{
						continue;
					}
					goto Label0;
				}
			}
		}

		private static void UpdateInstanceValues(MessageEvent messageEvent, ILogItem existingVal, LogItem logItem)
		{
			existingVal.Status = logItem.Status;
			if (messageEvent.Status == LogItemStatus.Completed)
			{
				existingVal.CompletedDate = messageEvent.Date;
			}
			existingVal.Variables = messageEvent.Variables;
			if ((messageEvent.DataType == LogItemDataType.Sent || messageEvent.DataType == LogItemDataType.Received) && !string.IsNullOrEmpty(messageEvent.Message))
			{
				existingVal.SourceMessage = messageEvent.Message;
				return;
			}
			if (messageEvent.DataType == LogItemDataType.Error && !string.IsNullOrEmpty(messageEvent.Message))
			{
				existingVal.ErrorMessage = messageEvent.Message;
				return;
			}
			if (messageEvent.DataType == LogItemDataType.Response && !string.IsNullOrEmpty(messageEvent.Message))
			{
				existingVal.ResponseMessage = messageEvent.Message;
			}
		}
	}
}