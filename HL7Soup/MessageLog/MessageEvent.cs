using HL7Soup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	[JsonObject]
	[Serializable]
	public class MessageEvent : IMessageEvent
	{
		public LogItemDataType DataType
		{
			get;
			set;
		}

		public DateTime Date
		{
			get;
			set;
		}

		public Guid FunctionId
		{
			get;
			set;
		}

		public int Instance
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public Guid RootFunctionId
		{
			get;
			set;
		}

		public LogItemStatus Status
		{
			get;
			set;
		}

		public List<Variable> Variables
		{
			get;
			set;
		}

		public MessageEvent(string message, WorkflowInstance workflowInstance, Guid functionId, LogItemStatus status, LogItemDataType dataType) : this()
		{
			this.Message = message;
			this.RootFunctionId = workflowInstance.Id;
			this.FunctionId = functionId;
			this.Instance = workflowInstance.InstanceId;
			this.Status = status;
			this.DataType = dataType;
			this.AddVariables(workflowInstance);
		}

		public MessageEvent(WorkflowInstance workflowInstance, Guid functionId, LogItemStatus status) : this()
		{
			this.Message = "";
			this.RootFunctionId = workflowInstance.Id;
			this.FunctionId = functionId;
			this.Instance = workflowInstance.InstanceId;
			this.Status = status;
			this.DataType = LogItemDataType.None;
			this.AddVariables(workflowInstance);
		}

		public MessageEvent()
		{
			this.Date = DateTime.Now;
		}

		private void AddVariables(WorkflowInstance workflowInstance)
		{
			this.Variables = workflowInstance.GetLoggerVariables();
		}
	}
}