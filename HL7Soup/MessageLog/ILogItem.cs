using System;
using System.Collections.Generic;

namespace HL7Soup.MessageLog
{
	public interface ILogItem
	{
		Dictionary<Guid, ActivityLogItem> Activities
		{
			get;
			set;
		}

		DateTime CompletedDate
		{
			get;
			set;
		}

		string ErrorMessage
		{
			get;
			set;
		}

		Guid Id
		{
			get;
			set;
		}

		int Instance
		{
			get;
			set;
		}

		string ResponseMessage
		{
			get;
			set;
		}

		string SourceMessage
		{
			get;
			set;
		}

		DateTime StartDate
		{
			get;
			set;
		}

		LogItemStatus Status
		{
			get;
			set;
		}

		List<Variable> Variables
		{
			get;
			set;
		}
	}
}