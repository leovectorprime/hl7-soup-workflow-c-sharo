using System;

namespace HL7Soup.MessageLog
{
	internal interface IMessageEvent
	{
		Guid FunctionId
		{
			get;
			set;
		}

		int Instance
		{
			get;
			set;
		}

		Guid RootFunctionId
		{
			get;
			set;
		}
	}
}