using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using Newtonsoft.Json;
using System;

namespace HL7Soup.MessageLog
{
	[JsonObject]
	[Serializable]
	public class ActivityLogItem : LogItem
	{
		public string Name
		{
			get
			{
				ISetting settingById = FunctionHelpers.GetSettingById(base.Id);
				if (settingById != null)
				{
					return settingById.Name;
				}
				return base.Id.ToString();
			}
		}

		public ActivityLogItem()
		{
		}
	}
}