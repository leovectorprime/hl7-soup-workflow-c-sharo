using Newtonsoft.Json;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	[JsonObject]
	[Serializable]
	public class Variable
	{
		public string Name
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

		public Variable()
		{
		}
	}
}