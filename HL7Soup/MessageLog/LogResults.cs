using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageLog
{
	public class LogResults
	{
		public IEnumerable<LogItem> LogItems
		{
			get;
			set;
		}

		public int ServerCount
		{
			get;
			set;
		}

		public LogResults()
		{
		}
	}
}