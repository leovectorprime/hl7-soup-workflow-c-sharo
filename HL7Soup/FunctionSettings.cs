using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;

namespace HL7Soup
{
	public sealed class FunctionSettings : ApplicationSettingsBase
	{
		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IActivitySetting> ActivitySettings
		{
			get
			{
				return (ObservableCollection<IActivitySetting>)this["ActivitySettings"];
			}
			set
			{
				this["ActivitySettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IAutomaticSendSetting> AutomaticSenderSettings
		{
			get
			{
				return (ObservableCollection<IAutomaticSendSetting>)this["AutomaticSenderSettings"];
			}
			set
			{
				this["AutomaticSenderSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IClientResponseDestinationSetting> ClientResponseDestinationSettings
		{
			get
			{
				return (ObservableCollection<IClientResponseDestinationSetting>)this["ClientResponseDestinationSettings"];
			}
			set
			{
				this["ClientResponseDestinationSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public IEditorSendingBehaviourSetting DefaultEditorSendingBehaviour
		{
			get
			{
				return (IEditorSendingBehaviourSetting)this["DefaultEditorSendingBehaviour"];
			}
			set
			{
				this["DefaultEditorSendingBehaviour"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public IReceiverSetting DefaultReceiver
		{
			get
			{
				return (IReceiverSetting)this["DefaultReceiver"];
			}
			set
			{
				this["DefaultReceiver"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IEditorSendingBehaviourSetting> EditorSendingBehaviourSettings
		{
			get
			{
				return (ObservableCollection<IEditorSendingBehaviourSetting>)this["EditorSendingBehaviourSettings"];
			}
			set
			{
				this["EditorSendingBehaviourSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IFilterSetting> FilterSettings
		{
			get
			{
				return (ObservableCollection<IFilterSetting>)this["FilterSettings"];
			}
			set
			{
				this["FilterSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IMessageTypeSetting> MessageTypeSettings
		{
			get
			{
				return (ObservableCollection<IMessageTypeSetting>)this["MessageTypeSettings"];
			}
			set
			{
				this["MessageTypeSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IReceiverSetting> ReceiverSettings
		{
			get
			{
				return (ObservableCollection<IReceiverSetting>)this["ReceiverSettings"];
			}
			set
			{
				this["ReceiverSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<ISenderSetting> SenderSettings
		{
			get
			{
				return (ObservableCollection<ISenderSetting>)this["SenderSettings"];
			}
			set
			{
				this["SenderSettings"] = value;
			}
		}

		[DefaultSettingValue("2")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public int SettingsVersion
		{
			get
			{
				return (int)this["SettingsVersion"];
			}
			set
			{
				this["SettingsVersion"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<ISourceProviderSetting> SourceProviderSettings
		{
			get
			{
				return (ObservableCollection<ISourceProviderSetting>)this["SourceProviderSettings"];
			}
			set
			{
				this["SourceProviderSettings"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<ITransformerSetting> TransformerSettings
		{
			get
			{
				return (ObservableCollection<ITransformerSetting>)this["TransformerSettings"];
			}
			set
			{
				this["TransformerSettings"] = value;
			}
		}

		public FunctionSettings()
		{
		}

		public void UpgradeAllWorkflowsToTheCurrentVersion()
		{
			foreach (IReceiverSetting receiverSetting in SystemSettings.Instance.FunctionSettings.ReceiverSettings)
			{
				receiverSetting.Upgrade();
			}
			foreach (IActivitySetting activitySetting in SystemSettings.Instance.FunctionSettings.ActivitySettings)
			{
				activitySetting.Upgrade();
			}
			foreach (IFilterSetting filterSetting in SystemSettings.Instance.FunctionSettings.FilterSettings)
			{
				filterSetting.Upgrade();
			}
			foreach (IMessageTypeSetting messageTypeSetting in SystemSettings.Instance.FunctionSettings.MessageTypeSettings)
			{
				messageTypeSetting.Upgrade();
			}
			foreach (ISenderSetting senderSetting in SystemSettings.Instance.FunctionSettings.SenderSettings)
			{
				senderSetting.Upgrade();
			}
			foreach (ITransformerSetting transformerSetting in SystemSettings.Instance.FunctionSettings.TransformerSettings)
			{
				transformerSetting.Upgrade();
			}
			SystemSettings.Instance.FunctionSettings.Save();
		}
	}
}