using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;

namespace HL7Soup
{
	public interface ISendReceiveManager : IFunctionHost, IDisposable
	{
		HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		void ProcessActivities(List<Guid> activities, WorkflowInstance workflowInstance);

		bool ProcessFilters(Guid filters, WorkflowInstance workflowInstance);

		void ProcessSender(ISenderSetting senderSetting, WorkflowInstance workflowInstance);

		bool ProcessTransformers(Guid transformerId, WorkflowInstance workflowInstance);

		void RaiseMessageReceived(Message ack);

		void SendMessage(ISenderSetting senderSetting, WorkflowInstance workflowInstance);
	}
}