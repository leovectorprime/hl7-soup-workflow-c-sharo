using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(ComponentDebuggingProxy))]
	public class Component : BasePart, IHL7Component, IHL7Part
	{
		public SubComponent CurrentSubComponent
		{
			get
			{
				return (SubComponent)base.CurrentChildPart;
			}
		}

		private string DebuggerDisplay
		{
			get
			{
				string path = this.GetPath();
				string str = "";
				string str1 = "                                                            ";
				if (base.XPart != null)
				{
					str = Helpers.TruncateString(base.XPart.Description, Math.Max(27 - this.GetPath().Length, 12));
				}
				return string.Concat(new string[] { path, "(", string.Concat(str, ") ", str1).Substring(0, 60 - (int)((double)(path.Length + str.Length) * 0.85)), "  =", base.Text });
			}
		}

		public Dictionary<string, SubComponent> SubComponents
		{
			get;
			set;
		}

		public Component(string text, BasePart parent, string locationCode, int index, int documentLocation, XBasePart xPart) : base(text, parent, locationCode, index, documentLocation, xPart)
		{
			base.LocationOfFirstCharacter = documentLocation;
		}

		public override string GetCurrentPath()
		{
			if (base.CurrentChildPart == null)
			{
				return base.LocationCode;
			}
			return string.Concat(base.LocationCode, ".", base.CurrentChildPart.GetCurrentPath());
		}

		public override string GetDescriptionName()
		{
			if (base.Parent == null)
			{
				return "";
			}
			return base.Parent.GetDescriptionName();
		}

		public override BasePart GetPart(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.SubComponent))
			{
				BasePart subComponent = this.GetSubComponent(path);
				if (path.PathTerminationPoint == PathTypes.SubComponent)
				{
					return subComponent;
				}
				if (subComponent != null)
				{
					BasePart part = subComponent.GetPart(path);
					if (part != null)
					{
						return part;
					}
				}
			}
			return null;
		}

		public override string GetPath()
		{
			string path = "";
			if (base.Parent != null)
			{
				path = base.Parent.GetPath();
			}
			return string.Concat(path, ".", base.LocationCode);
		}

		public BasePart GetSubComponent(PathSplitter path)
		{
			if (!this.SubComponents.ContainsKey(path.SubComponent))
			{
				return null;
			}
			return this.SubComponents[path.SubComponent];
		}

		public IHL7SubComponent GetSubComponent(int subComponentLocation)
		{
			base.Load();
			return this.GetSubComponent(subComponentLocation.ToString());
		}

		public IHL7SubComponent GetSubComponent(string locationCode)
		{
			base.Load();
			if (!this.SubComponents.ContainsKey(locationCode))
			{
				return null;
			}
			return this.SubComponents[locationCode];
		}

		public Dictionary<string, SubComponent> GetSubComponents(Component component)
		{
			return this.GetSubComponents(component.Text);
		}

		public Dictionary<string, SubComponent> GetSubComponents(string text)
		{
			string[] strArrays = this.SplitComponentsIntoSubComponents(text);
			Dictionary<string, SubComponent> strs = new Dictionary<string, SubComponent>();
			if (strArrays.GetUpperBound(0) > 0)
			{
				int num = 0;
				int length = 0;
				string[] strArrays1 = strArrays;
				for (int i = 0; i < (int)strArrays1.Length; i++)
				{
					string str = strArrays1[i];
					int num1 = num + 1;
					SubComponent subComponent = new SubComponent(str, this, num1.ToString(), num, length, base.GetDefinitionByIndex(num));
					num1 = num + 1;
					strs.Add(num1.ToString(), subComponent);
					num++;
					length = length + str.Length + 1;
				}
			}
			return strs;
		}

		public IHL7SubComponents GetSubComponents()
		{
			base.Load();
			return new HL7Soup.SubComponents(this.SubComponents.Values.ToList<IHL7SubComponent>());
		}

		void HL7Soup.Integrations.IHL7Part.SetText(string text)
		{
			this.SetTextQuietly(text);
			this.RebuildParentTextQuietly();
		}

		protected override void LoadChildParts()
		{
			this.SubComponents = this.GetSubComponents(this);
			base.HasChildren = this.SubComponents.Count > 0;
			base.ChildParts = this.SubComponents.Values.Cast<BasePart>().ToList<BasePart>();
		}

		public override void Reload()
		{
			base.Reload();
		}

		public override HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.SubComponent))
			{
				base.CurrentChildPart = this.GetSubComponent(path);
				if (base.CurrentChildPart != null)
				{
					HL7Soup.PositionInTheDocument positionInTheDocument = base.CurrentChildPart.SetCurrentPath(path);
					if (positionInTheDocument == null)
					{
						return new HL7Soup.PositionInTheDocument(this.CurrentSubComponent.LocationOfFirstCharacter, this.CurrentSubComponent.Text.Length);
					}
					HL7Soup.PositionInTheDocument start = positionInTheDocument;
					start.Start = start.Start + this.CurrentSubComponent.LocationOfFirstCharacter;
					return positionInTheDocument;
				}
			}
			return null;
		}

		public void SetTextEncoded(string text)
		{
			((IHL7Part)this).SetText(Helpers.HL7Encode(text));
		}

		public string[] SplitComponentsIntoSubComponents(string text)
		{
			return base.Split(text, base.Message.SubComponentSeperator);
		}

		public override string ToString()
		{
			string text = "";
			if (this.SubComponents == null)
			{
				text = base.Text;
			}
			else
			{
				foreach (BasePart childPart in base.ChildParts)
				{
					text = (!childPart.HasChildren ? string.Concat(text, childPart.Text, base.Message.SubComponentSeperator) : string.Concat(text, childPart.ToString(), base.Message.SubComponentSeperator));
				}
			}
			if (text.EndsWith(base.Message.SubComponentSeperator))
			{
				text = text.Substring(0, text.Length - 1);
			}
			return text;
		}
	}
}