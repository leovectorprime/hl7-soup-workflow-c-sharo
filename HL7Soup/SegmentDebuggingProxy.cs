using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class SegmentDebuggingProxy
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Segment BaseType
		{
			get;
			set;
		}

		public string DebugHelperDescription
		{
			get
			{
				if (this.BaseType.XPart == null)
				{
					return "Unavailable";
				}
				return this.BaseType.XPart.Description;
			}
		}

		public ICollection<IHL7Field> DebugHelperFields
		{
			get
			{
				return this.BaseType.GetFields();
			}
		}

		public string Header
		{
			get
			{
				return this.BaseType.Header;
			}
		}

		public string LocationCode
		{
			get
			{
				return this.BaseType.LocationCode;
			}
		}

		public string Text
		{
			get
			{
				return this.BaseType.Text;
			}
		}

		public SegmentDebuggingProxy(Segment basetype)
		{
			this.BaseType = basetype;
		}
	}
}