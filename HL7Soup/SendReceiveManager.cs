using HL7Soup.Functions;
using HL7Soup.Functions.Activities;
using HL7Soup.Functions.EditorSendingBehaviours;
using HL7Soup.Functions.Filters;
using HL7Soup.Functions.Receivers;
using HL7Soup.Functions.Senders;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.SourceProviders;
using HL7Soup.Functions.Transformers;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace HL7Soup
{
	public class SendReceiveManager : FunctionHost, ISendReceiveManager, IFunctionHost, IDisposable
	{
		public Encoding MessageEncoding = Helpers.GetEncoding(true);

		private BackgroundWorker receiverWorker;

		private BackgroundWorker autoSenderWorker;

		private BackgroundWorker activityProcessorWorker;

		private BackgroundWorker responseProcessorWorker;

		private ISourceProvider CurrentSourceProvider;

		private IReceiver CurrentReceiver;

		private Queue<string> ResponseQueue = new Queue<string>();

		private Queue<WorkflowInstance> ActivityQueue = new Queue<WorkflowInstance>();

		public IEditorSendingBehaviourSetting AutomaticSendClientBehaviourSettings
		{
			get;
			set;
		}

		public HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		public bool IsHosted
		{
			get;
			set;
		}

		public bool IsReceiving
		{
			get;
			set;
		}

		public bool IsSendingAutomatically
		{
			get;
			set;
		}

		public IReceiverSetting ReceiveConnection
		{
			get;
			set;
		}

		public SendReceiveManager()
		{
		}

		private void autoSenderWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			this.autoSenderWorker = null;
			this.IsSendingAutomatically = false;
			if (this.SendAutomaticallyStopped != null)
			{
				this.SendAutomaticallyStopped(this, new EventArgs());
			}
		}

		private void Error(string p)
		{
			if (this.RaiseError != null)
			{
				this.RaiseError(this, p);
			}
		}

		public int GetNextWorkflowInstanceID(Guid rootFunctionId)
		{
			int item;
			lock (this)
			{
				if (!SystemSettings.Instance.WorkflowInstanceIdDictionary.ContainsKey(rootFunctionId))
				{
					SystemSettings.Instance.WorkflowInstanceIdDictionary[rootFunctionId] = 1;
				}
				Dictionary<Guid, int> workflowInstanceIdDictionary = SystemSettings.Instance.WorkflowInstanceIdDictionary;
				Guid guid = rootFunctionId;
				item = workflowInstanceIdDictionary[guid];
				workflowInstanceIdDictionary[guid] = item + 1;
				item = item;
			}
			return item;
		}

		private void GetResponseMessage(WorkflowInstance workflowInstance, IReceiverWithResponseSetting receiveAndRespondConnectionSetting)
		{
			if (receiveAndRespondConnectionSetting != null)
			{
				if (receiveAndRespondConnectionSetting.ReturnCustomResponse)
				{
					lock (workflowInstance.Activities)
					{
						try
						{
							workflowInstance.ForceCurrentActivityToTheReceiver = true;
							workflowInstance.SetReponseMessage(FunctionHelpers.GetFunctionMessage(receiveAndRespondConnectionSetting.ResponseMessageTemplateRuntimeValue(workflowInstance), receiveAndRespondConnectionSetting.MessageType));
							if (receiveAndRespondConnectionSetting.Transformers != Guid.Empty && !receiveAndRespondConnectionSetting.TransformersNotAvailable)
							{
								this.ProcessTransformers(receiveAndRespondConnectionSetting.Transformers, workflowInstance);
							}
							((FunctionMessage)workflowInstance.ResponseMessage).ProcessVariables(workflowInstance);
						}
						finally
						{
							workflowInstance.ForceCurrentActivityToTheReceiver = false;
						}
					}
				}
				if (base.hasErrored)
				{
					workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Activities[0].Message).GenerateErrorMessage(workflowInstance.ErrorMessage));
					return;
				}
				if (receiveAndRespondConnectionSetting.ReturnApplicationAccept)
				{
					IMessage message = null;
					message = (!workflowInstance.CheckActivitiesAfterReceiverFiltered() ? ((FunctionMessage)workflowInstance.Activities[0].Message).GenerateAcceptMessage(receiveAndRespondConnectionSetting) : ((FunctionMessage)workflowInstance.Activities[0].Message).GenerateRejectMessage("Rejected because all activities filtered this message"));
					workflowInstance.SetReponseMessage(message);
					return;
				}
				if (receiveAndRespondConnectionSetting.ReturnApplicationReject)
				{
					workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Activities[0].Message).GenerateRejectMessage(receiveAndRespondConnectionSetting));
					return;
				}
				if (receiveAndRespondConnectionSetting.ReturnApplicationError)
				{
					workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Activities[0].Message).GenerateErrorMessage(receiveAndRespondConnectionSetting));
					return;
				}
				if (receiveAndRespondConnectionSetting.ReturnResponseFromActivity)
				{
					IActivityInstance activityInstance = workflowInstance.GetActivityInstance(receiveAndRespondConnectionSetting.ReturnResponseFromActivityId);
					if (activityInstance != null)
					{
						workflowInstance.SetReponseMessage(activityInstance.ResponseMessage);
						return;
					}
					workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Activities[0].Message).GenerateRejectMessage(string.Concat("Invalid response type.  Could not return value from the activity '", receiveAndRespondConnectionSetting.ReturnResponseFromActivityName, "' because it was not executed.  This probably is because the activity was filtered out.")));
				}
			}
		}

		private void Log(string p)
		{
			if (this.RaiseMessage != null)
			{
				this.RaiseMessage(this, p);
			}
		}

		public void ProcessActivities(List<Guid> activities, WorkflowInstance workflowInstance)
		{
			foreach (Guid activity in activities)
			{
				IActivitySetting activitySettingById = FunctionHelpers.GetActivitySettingById(activity, this);
				if (activitySettingById == null)
				{
					continue;
				}
				IActivity activityInstance = FunctionHelpers.GetActivityInstance(activitySettingById, this, workflowInstance);
				ISettingCanBeDisabled settingCanBeDisabled = activitySettingById;
				if (settingCanBeDisabled != null && !settingCanBeDisabled.DisableNotAvailable && settingCanBeDisabled.Disabled)
				{
					return;
				}
				else if (string.IsNullOrEmpty(activityInstance.ErrorMessage))
				{
					activityInstance.ProcessActivity(workflowInstance);
				}
				else
				{
					return;
				}
			}
		}

		public bool ProcessFilters(Guid filterId, WorkflowInstance workflowInstance)
		{
			if (filterId == Guid.Empty)
			{
				return true;
			}
			IFilterSetting filterSettingById = FunctionHelpers.GetFilterSettingById(filterId);
			if (filterSettingById == null)
			{
				throw new Exception(string.Concat("Cannot find filter ", filterId.ToString()));
			}
			return FunctionHelpers.GetFilterInstance(filterSettingById, this, workflowInstance).FilterMessage(workflowInstance);
		}

		private void ProcessReceivedMessage(WorkflowInstance workflowInstance)
		{
			object text;
			IReceiverWithResponseSetting receiveConnection = this.ReceiveConnection as IReceiverWithResponseSetting;
			if (!((FunctionMessage)workflowInstance.Message).IsWellFormed && receiveConnection != null)
			{
				workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Message).GenerateErrorMessage(receiveConnection));
				return;
			}
			if (receiveConnection != null && receiveConnection.ReponsePriority == ReturnResponsePriority.UponArrival)
			{
				this.GetResponseMessage(workflowInstance, receiveConnection);
				this.QueueProcessingActivities(workflowInstance);
				return;
			}
			if (!this.ProcessTransformers(this.ReceiveConnection.Filters, workflowInstance))
			{
				throw new Exception(string.Concat("Error occured in the Receivers Transformers that prevent this workflow from running. ", workflowInstance.ErrorMessage));
			}
			this.ProcessFilters(this.ReceiveConnection.Filters, workflowInstance);
			if (workflowInstance.CurrentActivityInstance.Filtered)
			{
				if (receiveConnection != null)
				{
					workflowInstance.SetReponseMessage(((FunctionMessage)workflowInstance.Message).GenerateRejectMessage("Message rejected by receiving activity"));
				}
				return;
			}
			if (receiveConnection != null && receiveConnection.ReponsePriority == ReturnResponsePriority.AfterValidation && workflowInstance.ResponseMessage == null)
			{
				this.GetResponseMessage(workflowInstance, receiveConnection);
				this.QueueProcessingActivities(workflowInstance);
				return;
			}
			if (this.ReceiveConnection.AddIncomingMessageToCurrentTab || this.IsHosted)
			{
				BackgroundWorker backgroundWorker = this.receiverWorker;
				IMessage message = workflowInstance.Message;
				if (message != null)
				{
					text = message.Text;
				}
				else
				{
					text = null;
				}
				backgroundWorker.ReportProgress(0, text);
			}
			this.ProcessActivities(this.ReceiveConnection.Activities, workflowInstance);
			if (workflowInstance.ResponseMessage == null || receiveConnection.ReturnApplicationError || receiveConnection.ReturnApplicationReject || receiveConnection.ReturnCustomResponse)
			{
				this.GetResponseMessage(workflowInstance, receiveConnection);
			}
		}

		public void ProcessSender(Guid senderId, WorkflowInstance workflowInstance)
		{
			this.ProcessSender(FunctionHelpers.GetSenderSettingById(senderId), workflowInstance);
		}

		public void ProcessSender(ISenderSetting senderSetting, WorkflowInstance workflowInstance)
		{
			this.SendMessage(senderSetting, workflowInstance);
		}

		public bool ProcessTransformers(Guid transformerId, WorkflowInstance workflowInstance)
		{
			if (transformerId == Guid.Empty)
			{
				return true;
			}
			ITransformerSetting transformerSettingById = FunctionHelpers.GetTransformerSettingById(transformerId);
			if (transformerSettingById == null)
			{
				throw new Exception(string.Concat("Cannot find transformer ", transformerId.ToString()));
			}
			return FunctionHelpers.GetTransformerInstance(transformerSettingById, this, workflowInstance).Transform(workflowInstance);
		}

		private void QueueProcessingActivities(WorkflowInstance workflowInstance)
		{
			if (workflowInstance != null)
			{
				lock (this.ActivityQueue)
				{
					this.ActivityQueue.Enqueue(workflowInstance);
				}
			}
			if (this.activityProcessorWorker == null || !this.activityProcessorWorker.IsBusy)
			{
				this.activityProcessorWorker = new BackgroundWorker()
				{
					WorkerReportsProgress = true,
					WorkerSupportsCancellation = true
				};
				this.activityProcessorWorker.DoWork += new DoWorkEventHandler((object sender, DoWorkEventArgs e) => {
					if (Thread.CurrentThread.Name != "QueueThreadWorker")
					{
						Thread.CurrentThread.Name = "QueueThreadWorker";
					}
					while (this.ActivityQueue.Count > 0)
					{
						WorkflowInstance workflowInstance1 = this.ActivityQueue.Dequeue();
						if (this.ReceiveConnection.AddIncomingMessageToCurrentTab || this.IsHosted)
						{
							this.receiverWorker.ReportProgress(0, workflowInstance1.Message.Text);
						}
						this.ProcessActivities(this.ReceiveConnection.Activities, workflowInstance1);
					}
				});
				this.activityProcessorWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object sender, RunWorkerCompletedEventArgs eventArgs) => {
					this.activityProcessorWorker = null;
					if (this.ActivityQueue.Count > 0)
					{
						this.QueueProcessingActivities(null);
					}
				});
				lock (this.ActivityQueue)
				{
					if (!this.activityProcessorWorker.IsBusy)
					{
						this.activityProcessorWorker.RunWorkerAsync();
					}
				}
			}
		}

		private void QueueResponseEvents(string message)
		{
			if (message != null)
			{
				lock (this.ResponseQueue)
				{
					this.ResponseQueue.Enqueue(message);
				}
			}
			if (this.responseProcessorWorker == null || !this.responseProcessorWorker.IsBusy)
			{
				this.responseProcessorWorker = new BackgroundWorker()
				{
					WorkerReportsProgress = true,
					WorkerSupportsCancellation = true
				};
				this.responseProcessorWorker.DoWork += new DoWorkEventHandler((object sender, DoWorkEventArgs e) => {
					if (Thread.CurrentThread.Name != "QueueThreadWorker")
					{
						Thread.CurrentThread.Name = "QueueThreadWorker";
					}
					while (this.ResponseQueue.Count > 0)
					{
						string str = "";
						lock (this.ResponseQueue)
						{
							str = this.ResponseQueue.Dequeue();
						}
						Message message1 = null;
						if (Application.Current == null)
						{
							message1 = new Message(str, false);
						}
						else
						{
							Application.Current.Dispatcher.Invoke(() => message1 = new Message(str, false));
						}
						if (this.MessageReceived == null)
						{
							continue;
						}
						this.MessageReceived(this, message1);
					}
				});
				this.responseProcessorWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object sender, RunWorkerCompletedEventArgs eventArgs) => {
					this.responseProcessorWorker = null;
					if (this.ResponseQueue.Count > 0)
					{
						this.QueueResponseEvents(null);
					}
				});
				lock (this.ResponseQueue)
				{
					if (!this.responseProcessorWorker.IsBusy)
					{
						this.responseProcessorWorker.RunWorkerAsync();
					}
				}
			}
		}

		public void RaiseMessageReceived(Message ack)
		{
			if (this.MessageReceived != null)
			{
				this.MessageReceived(this, ack);
			}
		}

		private void receiver_MessageReceived(object sender, ref WorkflowInstance workflowInstance)
		{
			this.ProcessReceivedMessage(workflowInstance);
		}

		public void ReprocessMessage(IReceiverSetting connection, string message)
		{
			try
			{
				base.FunctionsProcessedForCurrentMessage = new List<IFunctionProvider>();
				IReceiverSetting receiverSettingById = FunctionHelpers.GetReceiverSettingById(connection.Id);
				WorkflowInstance workflowInstance = new WorkflowInstance(receiverSettingById.Id, this.GetNextWorkflowInstanceID(receiverSettingById.Id));
				workflowInstance.AddActivity(new HL7V2MessageType(message), receiverSettingById.Id);
				IReceiver receiverInstance = FunctionHelpers.GetReceiverInstance(receiverSettingById, this, workflowInstance, true);
				MessageLogger.Instance.AddMessage(new MessageEvent(message, workflowInstance, receiverSettingById.Id, LogItemStatus.Processing, LogItemDataType.Received));
				this.ProcessActivities(receiverSettingById.Activities, workflowInstance);
				IReceiverWithResponseSetting receiverWithResponseSetting = receiverSettingById as IReceiverWithResponseSetting;
				if (receiverWithResponseSetting != null)
				{
					this.GetResponseMessage(workflowInstance, receiverWithResponseSetting);
					MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance.ResponseMessage.Text, workflowInstance, receiverSettingById.Id, LogItemStatus.Processing, LogItemDataType.Response));
				}
				MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, receiverSettingById.Id, LogItemStatus.Completed));
				this.RootFunctionFinishedProcessing(workflowInstance, receiverInstance);
				if (base.hasErrored)
				{
					MessageLogger.Instance.AddMessage(new MessageEvent(base.ErroredFunction.ErrorMessage, workflowInstance, receiverSettingById.Id, LogItemStatus.Errored, LogItemDataType.Error));
					if (this.RaiseError != null)
					{
						this.RaiseError(this, base.ErroredFunction.ErrorMessage);
					}
				}
			}
			finally
			{
				base.ClearDictionaries();
			}
		}

		private void RootFunctionFinishedProcessing(WorkflowInstance workflowInstance, IFunctionProvider provider)
		{
			if (base.hasErrored)
			{
				foreach (IFunctionProvider functionsProcessedForCurrentMessage in base.FunctionsProcessedForCurrentMessage)
				{
					functionsProcessedForCurrentMessage.RollBack(workflowInstance);
				}
			}
			else
			{
				foreach (IFunctionProvider functionProvider in base.FunctionsProcessedForCurrentMessage)
				{
					functionProvider.AllFunctionsSucceded(workflowInstance);
				}
			}
		}

		private void RunAutomaticSendingLoop()
		{
			try
			{
				try
				{
					IAutomaticSendSetting automaticSendConnectionBySendConnection = SystemSettings.Instance.GetAutomaticSendConnectionBySendConnection(this.AutomaticSendClientBehaviourSettings);
					IEditorSendingBehaviourSetting editorSendingBehaviourSettingById = FunctionHelpers.GetEditorSendingBehaviourSettingById(this.AutomaticSendClientBehaviourSettings.Id);
					WorkflowInstance workflowInstance = new WorkflowInstance(editorSendingBehaviourSettingById.Id, this.GetNextWorkflowInstanceID(editorSendingBehaviourSettingById.Id));
					IEditorSendingBehaviour editorSendingBehaviourInstance = FunctionHelpers.GetEditorSendingBehaviourInstance(editorSendingBehaviourSettingById, this, workflowInstance);
					DateTime now = DateTime.Now;
					while (!this.autoSenderWorker.CancellationPending)
					{
						if (now > DateTime.Now)
						{
							continue;
						}
						now = DateTime.Now + automaticSendConnectionBySendConnection.Delay;
						base.FunctionsProcessedForCurrentMessage = new List<IFunctionProvider>();
						this.CurrentSourceProvider = editorSendingBehaviourInstance.AutomaticSend(workflowInstance);
						if (!this.CurrentSourceProvider.Pending)
						{
							this.RootFunctionFinishedProcessing(workflowInstance, this.CurrentSourceProvider);
						}
						if (this.CurrentSourceProvider.Finished && !automaticSendConnectionBySendConnection.InfinateLoop)
						{
							return;
						}
						else if (base.hasErrored)
						{
							if (this.RaiseError != null)
							{
								this.RaiseError(this, base.ErroredFunction.ErrorMessage);
							}
							MessageLogger.Instance.AddMessage(new MessageEvent(base.ErroredFunction.ErrorMessage, workflowInstance, editorSendingBehaviourSettingById.Id, LogItemStatus.Errored, LogItemDataType.Error));
							return;
						}
						else if (!this.CurrentSourceProvider.Pending)
						{
							workflowInstance = new WorkflowInstance(editorSendingBehaviourSettingById.Id, this.GetNextWorkflowInstanceID(editorSendingBehaviourSettingById.Id));
						}
						else
						{
							Thread.Sleep(10);
						}
					}
				}
				catch (Exception exception)
				{
					Log.Instance.Error(exception.ToString());
					throw;
				}
			}
			finally
			{
				base.ClearDictionaries();
			}
		}

		private void RunReceiverLoop()
		{
			if (this.ReceiveConnection.Disabled)
			{
				if (this.RaiseError != null)
				{
					this.RaiseError(this, string.Concat("Cannot start receiving because ", this.ReceiveConnection.Name, " is not enabled.  Please edit it and set it to enabled."));
				}
				this.receiverWorker.CancelAsync();
			}
			IReceiverSetting receiveConnection = this.ReceiveConnection;
			WorkflowInstance workflowInstance = new WorkflowInstance(receiveConnection.Id, this.GetNextWorkflowInstanceID(receiveConnection.Id));
			this.CurrentReceiver = FunctionHelpers.GetReceiverInstance(receiveConnection, this, workflowInstance);
			if (base.hasErrored)
			{
				if (this.RaiseError != null)
				{
					this.RaiseError(this, base.ErroredFunction.ErrorMessage);
				}
				workflowInstance.Dispose();
				return;
			}
			this.WorkflowStarted();
			try
			{
				this.CurrentReceiver.MessageReceived -= new MessageReceivedEventHandler(this.receiver_MessageReceived);
				this.CurrentReceiver.MessageReceived += new MessageReceivedEventHandler(this.receiver_MessageReceived);
				while (!this.receiverWorker.CancellationPending)
				{
					base.FunctionsProcessedForCurrentMessage = new List<IFunctionProvider>();
					this.CurrentReceiver.GetNext(workflowInstance);
					if (this.CurrentReceiver.Pending)
					{
						Thread.Sleep(1);
					}
					else
					{
						this.RootFunctionFinishedProcessing(workflowInstance, this.CurrentReceiver);
						if (!this.receiverWorker.CancellationPending)
						{
							LogItemStatus logItemStatu = LogItemStatus.Completed;
							if (workflowInstance.Filtered)
							{
								logItemStatu = LogItemStatus.Filtered;
							}
							MessageLogger.Instance.AddMessage(new MessageEvent(workflowInstance, receiveConnection.Id, logItemStatu));
						}
					}
					if (this.CurrentReceiver.Finished)
					{
						workflowInstance.Dispose();
						return;
					}
					else if (!base.hasErrored)
					{
						if (this.CurrentReceiver.Pending)
						{
							continue;
						}
						workflowInstance.Dispose();
						workflowInstance = new WorkflowInstance(receiveConnection.Id, this.GetNextWorkflowInstanceID(receiveConnection.Id));
					}
					else
					{
						MessageLogger.Instance.AddMessage(new MessageEvent(base.ErroredFunction.ErrorMessage, workflowInstance, receiveConnection.Id, LogItemStatus.Errored, LogItemDataType.Error));
						if (this.RaiseError != null)
						{
							this.RaiseError(this, base.ErroredFunction.ErrorMessage);
						}
						workflowInstance.Dispose();
						return;
					}
				}
			}
			finally
			{
				base.ClearDictionaries();
				this.CurrentReceiver.MessageReceived -= new MessageReceivedEventHandler(this.receiver_MessageReceived);
			}
		}

		public void RunSingleMessageSend(ISenderSetting connection, string message)
		{
			try
			{
				base.FunctionsProcessedForCurrentMessage = new List<IFunctionProvider>();
				IEditorSendingBehaviourSetting editorSendingBehaviourSettingById = FunctionHelpers.GetEditorSendingBehaviourSettingById(connection.Id);
				WorkflowInstance workflowInstance = new WorkflowInstance(editorSendingBehaviourSettingById.Id, this.GetNextWorkflowInstanceID(editorSendingBehaviourSettingById.Id));
				workflowInstance.AddActivity(new HL7V2MessageType(message), editorSendingBehaviourSettingById.Id);
				IEditorSendingBehaviour editorSendingBehaviourInstance = FunctionHelpers.GetEditorSendingBehaviourInstance(editorSendingBehaviourSettingById, this, workflowInstance);
				editorSendingBehaviourInstance.Send(workflowInstance);
				this.RootFunctionFinishedProcessing(workflowInstance, editorSendingBehaviourInstance);
				if (base.hasErrored)
				{
					MessageLogger.Instance.AddMessage(new MessageEvent(base.ErroredFunction.ErrorMessage, workflowInstance, editorSendingBehaviourSettingById.Id, LogItemStatus.Errored, LogItemDataType.Error));
					if (this.RaiseError != null)
					{
						this.RaiseError(this, base.ErroredFunction.ErrorMessage);
					}
				}
			}
			finally
			{
				base.ClearDictionaries();
			}
		}

		public void SendMessage(ISenderSetting senderSetting, WorkflowInstance workflowInstance)
		{
			FunctionHelpers.GetSenderInstance(senderSetting, this, workflowInstance).Send(workflowInstance);
		}

		public void StartReceiving(IReceiverSetting connection)
		{
			this.ReceiveConnection = connection;
			if (connection.Activities != null && connection.Activities.Count > Settings.Default.AlignmentGap)
			{
				Settings.Default.AlignmentGap = connection.Activities.Count + 1;
				Settings.Default.Save();
			}
			this.StartReceiving();
		}

		public void StartReceiving()
		{
			if (this.ReceiveConnection == null)
			{
				throw new NullReferenceException("Receive connection setting must be provided,");
			}
			if (this.receiverWorker == null)
			{
				this.receiverWorker = new BackgroundWorker()
				{
					WorkerReportsProgress = true,
					WorkerSupportsCancellation = true
				};
				this.receiverWorker.ProgressChanged += new ProgressChangedEventHandler((object sender, ProgressChangedEventArgs eventArgs) => {
					if (this.MessageReceived != null)
					{
						this.QueueResponseEvents((string)eventArgs.UserState);
					}
				});
				this.receiverWorker.DoWork += new DoWorkEventHandler((object sender, DoWorkEventArgs e) => this.RunReceiverLoop());
				this.receiverWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object sender, RunWorkerCompletedEventArgs eventArgs) => {
					base.ClearDictionaries();
					this.receiverWorker = null;
					this.IsReceiving = false;
					if (this.ReceivingStopped != null)
					{
						this.ReceivingStopped(this, this.ReceiveConnection.Id);
					}
				});
				this.receiverWorker.RunWorkerAsync();
				this.IsReceiving = true;
				if (this.ReceivingStarted != null)
				{
					this.ReceivingStarted(this, new EventArgs());
				}
			}
		}

		public void StartSendingAutomatically(IEditorSendingBehaviourSetting connection, HL7Soup.DocumentManager documentManger)
		{
			if (connection == null)
			{
				throw new NullReferenceException("Send connection setting must be provided.");
			}
			if (documentManger == null)
			{
				throw new NullReferenceException("Document Manager must be provided.");
			}
			this.AutomaticSendClientBehaviourSettings = connection;
			this.DocumentManager = documentManger;
			this.StartSendingAutomatically();
		}

		public void StartSendingAutomatically()
		{
			if (this.AutomaticSendClientBehaviourSettings == null)
			{
				throw new NullReferenceException("Send connection setting must be set.");
			}
			if (this.autoSenderWorker == null)
			{
				this.autoSenderWorker = new BackgroundWorker();
				this.autoSenderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.autoSenderWorker_RunWorkerCompleted);
				this.autoSenderWorker.WorkerReportsProgress = true;
				this.autoSenderWorker.WorkerSupportsCancellation = true;
				this.autoSenderWorker.ProgressChanged += new ProgressChangedEventHandler((object sender, ProgressChangedEventArgs eventArgs) => {
					if (this.MessageSentAutomatically != null)
					{
						this.MessageSentAutomatically(this, new Message((string)eventArgs.UserState, false));
					}
				});
				this.autoSenderWorker.DoWork += new DoWorkEventHandler((object sender, DoWorkEventArgs e) => this.RunAutomaticSendingLoop());
				this.autoSenderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler((object sender, RunWorkerCompletedEventArgs eventArgs) => {
				});
				this.autoSenderWorker.RunWorkerAsync();
				this.IsSendingAutomatically = true;
				if (this.SendAutomaticallyStarted != null)
				{
					this.SendAutomaticallyStarted(this, new EventArgs());
				}
			}
		}

		public void StopReceiving()
		{
			if (this.receiverWorker != null)
			{
				if (this.CurrentReceiver != null)
				{
					this.CurrentReceiver.Cancel();
				}
				if (this.receiverWorker != null)
				{
					this.receiverWorker.CancelAsync();
				}
			}
		}

		public void StopSendingAutomatically()
		{
			if (this.autoSenderWorker != null)
			{
				if (this.CurrentSourceProvider != null)
				{
					this.CurrentSourceProvider.Cancel();
				}
				this.autoSenderWorker.CancelAsync();
			}
		}

		private bool ValidateMessage(Message m)
		{
			return true;
		}

		public override void WorkflowStarted()
		{
			base.WorkflowStarted();
			if (this.ReceivingStartedWorkflow != null)
			{
				this.ReceivingStartedWorkflow(this, new EventArgs());
			}
		}

		public event MessageEventHandler MessageReceived;

		public event MessageEventHandler MessageSentAutomatically;

		public event RaiseErrorEventHandler RaiseError;

		public event RaiseMessageEventHandler RaiseMessage;

		public event EventHandler ReceivingStarted;

		public event EventHandler ReceivingStartedWorkflow;

		public event EventHandler<Guid> ReceivingStopped;

		public event EventHandler SendAutomaticallyStarted;

		public event EventHandler SendAutomaticallyStopped;
	}
}