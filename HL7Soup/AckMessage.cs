using System;
using System.Windows.Media;

namespace HL7Soup
{
	public class AckMessage : HL7Soup.Message
	{
		private string acknowledgmentCode = "";

		public string AcknowledgmentCode
		{
			get
			{
				if (this.acknowledgmentCode == "")
				{
					this.acknowledgmentCode = base.GetPartText(new PathSplitter(SegmentsEnum.MSA, 1));
				}
				return this.acknowledgmentCode;
			}
		}

		public string AcknowledgmentCodeDescription
		{
			get
			{
				string acknowledgmentCode = this.AcknowledgmentCode;
				if (acknowledgmentCode != null)
				{
					if (acknowledgmentCode == "AA")
					{
						return "Application Accept";
					}
					if (acknowledgmentCode == "AE")
					{
						return "Application Error";
					}
					if (acknowledgmentCode == "AR")
					{
						return "Application Reject";
					}
				}
				return "acknowledgmentCode";
			}
		}

		public Color BackgroundDarkColor
		{
			get
			{
				string acknowledgmentCode = this.AcknowledgmentCode;
				if (acknowledgmentCode != null)
				{
					if (acknowledgmentCode == "AA")
					{
						return (Color)ColorConverter.ConvertFromString("#FF007ACC");
					}
					if (acknowledgmentCode == "AE")
					{
						return Color.FromArgb(255, 204, 0, 0);
					}
					if (acknowledgmentCode == "AR")
					{
						return Color.FromArgb(255, 62, 62, 62);
					}
				}
				return Color.FromArgb(255, 0, 0, 0);
			}
		}

		public Color BackgroundLightColor
		{
			get
			{
				string acknowledgmentCode = this.AcknowledgmentCode;
				if (acknowledgmentCode != null)
				{
					if (acknowledgmentCode == "AA")
					{
						return (Color)ColorConverter.ConvertFromString("#5530AAFC");
					}
					if (acknowledgmentCode == "AE")
					{
						return Color.FromArgb(85, 236, 32, 32);
					}
					if (acknowledgmentCode == "AR")
					{
						return Color.FromArgb(85, 94, 94, 94);
					}
				}
				return Color.FromArgb(85, 255, 255, 255);
			}
		}

		public string TextMessage
		{
			get
			{
				string partText = base.GetPartText(new PathSplitter(SegmentsEnum.MSA, 3));
				if (partText == "")
				{
					partText = this.AcknowledgmentCodeDescription;
				}
				return partText;
			}
		}

		public AckMessage(string text, bool isShown) : base(text, isShown)
		{
		}
	}
}