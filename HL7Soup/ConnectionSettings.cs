using System;
using System.Collections.ObjectModel;
using System.Configuration;

namespace HL7Soup
{
	public sealed class ConnectionSettings : ApplicationSettingsBase
	{
		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public IConnectionSetting DefaultReceiveConnection
		{
			get
			{
				return (IConnectionSetting)this["DefaultReceiveConnection"];
			}
			set
			{
				this["DefaultReceiveConnection"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public IConnectionSetting DefaultSendConnection
		{
			get
			{
				return (IConnectionSetting)this["DefaultSendConnection"];
			}
			set
			{
				this["DefaultSendConnection"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IConnectionSetting> ReceiveConnections
		{
			get
			{
				return (ObservableCollection<IConnectionSetting>)this["ReceiveConnections"];
			}
			set
			{
				this["ReceiveConnections"] = value;
			}
		}

		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.Binary)]
		[UserScopedSetting]
		public ObservableCollection<IConnectionSetting> SendConnections
		{
			get
			{
				return (ObservableCollection<IConnectionSetting>)this["SendConnections"];
			}
			set
			{
				this["SendConnections"] = value;
			}
		}

		public ConnectionSettings()
		{
		}
	}
}