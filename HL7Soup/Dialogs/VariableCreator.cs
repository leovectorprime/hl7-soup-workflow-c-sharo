using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Dialogs
{
	[Serializable]
	public class VariableCreator : IVariableCreator
	{
		public bool SampleValueIsDefaultValue
		{
			get;
			set;
		}

		public string SampleVariableValue
		{
			get;
			set;
		}

		public string VariableName
		{
			get;
			set;
		}

		public VariableCreator(string variableName, string sampleVariableValue, bool sampleValueIsDefaultValue)
		{
			this.VariableName = variableName;
			this.SampleVariableValue = sampleVariableValue;
			this.SampleValueIsDefaultValue = sampleValueIsDefaultValue;
		}

		public VariableCreator(string variableName, string sampleVariableValue)
		{
			this.VariableName = variableName;
			this.SampleVariableValue = sampleVariableValue;
		}

		public IVariableCreator Clone()
		{
			return (IVariableCreator)this.MemberwiseClone();
		}
	}
}