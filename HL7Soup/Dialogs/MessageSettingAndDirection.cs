using HL7Soup.Functions.Settings;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Dialogs
{
	public class MessageSettingAndDirection
	{
		public HL7Soup.Functions.Settings.MessageSourceDirection MessageSourceDirection
		{
			get;
			set;
		}

		public ISetting Setting
		{
			get;
			set;
		}

		public Guid SettingId
		{
			get
			{
				if (this.Setting == null)
				{
					return Guid.Empty;
				}
				return this.Setting.Id;
			}
		}

		public MessageSettingAndDirection()
		{
		}

		public override string ToString()
		{
			if (this.Setting == null)
			{
				return "Setting not set";
			}
			if (!(this.Setting is ISenderSetting))
			{
				return this.Setting.Name;
			}
			if (this.MessageSourceDirection == HL7Soup.Functions.Settings.MessageSourceDirection.inbound)
			{
				return string.Concat(this.Setting.Name, " Response");
			}
			return string.Concat(this.Setting.Name, " Sent");
		}
	}
}