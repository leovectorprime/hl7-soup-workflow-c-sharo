using System;

namespace HL7Soup.Dialogs
{
	public interface IActivityUI
	{
		Guid Id
		{
			get;
		}
	}
}