using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Transformers;
using System;
using System.Collections.Generic;

namespace HL7Soup.Dialogs
{
	public interface IActivityHost
	{
		ISettingDialog CurrentDialog
		{
			get;
		}

		Guid CurrentSettingId
		{
			get;
		}

		ISetting RootSetting
		{
			get;
		}

		Dictionary<Guid, ISetting> SettingsBeingDeleted
		{
			get;
		}

		Dictionary<Guid, ISetting> SettingsBeingEdited
		{
			get;
		}

		string WorkflowPatternName
		{
			get;
			set;
		}

		void ActivityAdded(IActivityUI activityUI);

		void ActivityRemoved(IActivityUI activityUI);

		void BuildTree();

		ITransformerAction CreateNewTransformerActionInstance(TransformerType transformerType);

		ISetting GetActivity(Guid settingId);

		IActivityUI GetActivityUIForSetting(Guid id);

		List<Guid> GetAllActivitiesAtRootLevel();

		List<ISetting> GetAllActivityInstances();

		List<MessageSettingAndDirection> GetAllBindableActivitiesForActivities(Guid fromActivity);

		List<MessageSettingAndDirection> GetAllBindableActivitiesForFiltersOrTransformers(Guid fromActivity);

		List<TransformerType> GetAllTransformerTypes();

		Dictionary<string, IVariableCreator> GetAllVariables();

		Dictionary<string, IVariableCreator> GetAllVariables(Guid lastActivityId);

		ISettingDialog GetCurrentDialogForSetting(ISetting setting);

		string GetCurrentMessage();

		IVariableCreator GetExistingVariable(CreateVariableTransformerAction potentialVariable, Guid lastActivityId);

		MessageTypes GetMessageType(Guid setting, MessageSourceDirection direction);

		MessageTypes GetMessageType(ISetting setting, MessageSourceDirection direction);

		ISettingDialog GetOrCreateTransformerDialog(ISetting activitySettingWithTransformers);

		ISetting GetParentActivity(Guid settingId);

		ISetting GetSetting(Guid id);

		void HighlightCurrentPath(string path, MessageTypes messageType);

		bool IsSettingDialogValid(Guid id);

		ISetting NavigateToFilterOfActivity(ISetting activitySettingWithTransformers);

		ISetting NavigateToTransformerOfActivity(ISetting activitySettingWithTransformers);

		void RebuildTree();

		void RefreshBindingTree();

		void SetSetting(ISetting setting);

		void ShowCorrectTabsForActivity(ISetting setting);
	}
}