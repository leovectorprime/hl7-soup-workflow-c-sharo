using System;
using System.Collections.Generic;
using System.IO;

namespace HL7Soup
{
	public static class SharedSettings
	{
		private static string settingPath;

		private static Dictionary<string, string> runtimeValues;

		public static string CodeEditorPath
		{
			get
			{
				return SharedSettings.GetSetting("CodeEditorPath");
			}
			set
			{
				SharedSettings.SetSetting("CodeEditorPath", value);
			}
		}

		public static string DefaultSSLCertificateThumbprint
		{
			get
			{
				return SharedSettings.GetSetting("DefaultSSLCertificateThumbprint");
			}
			set
			{
				SharedSettings.SetSetting("DefaultSSLCertificateThumbprint", value);
			}
		}

		public static string HostUrl
		{
			get
			{
				return SharedSettings.GetSetting("HostUrl");
			}
			set
			{
				SharedSettings.SetSetting("HostUrl", value);
			}
		}

		public static string IntegrationWorkflowDesignerPath
		{
			get
			{
				return SharedSettings.GetSetting("IntegrationWorkflowDesignerPath");
			}
			set
			{
				SharedSettings.SetSetting("IntegrationWorkflowDesignerPath", value);
			}
		}

		public static string LastUrlBinderError
		{
			get
			{
				return SharedSettings.GetSettingFromFile("LastUrlBinderError");
			}
			set
			{
				SharedSettings.SetSetting("LastUrlBinderError", value);
			}
		}

		static SharedSettings()
		{
			SharedSettings.settingPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Popokey\\SharedSettings\\");
			SharedSettings.runtimeValues = new Dictionary<string, string>();
		}

		private static void EnsureDirectoryExists()
		{
			if (!Directory.Exists(SharedSettings.settingPath))
			{
				Directory.CreateDirectory(SharedSettings.settingPath);
			}
		}

		private static string GetFilePath(string name)
		{
			return Path.Combine(SharedSettings.settingPath, string.Concat(name, ".txt"));
		}

		private static string GetSetting(string name)
		{
			string item;
			lock (SharedSettings.settingPath)
			{
				if (!SharedSettings.runtimeValues.ContainsKey(name))
				{
					SharedSettings.EnsureDirectoryExists();
					string filePath = SharedSettings.GetFilePath(name);
					string str = null;
					if (File.Exists(filePath))
					{
						str = File.ReadAllText(filePath);
					}
					SharedSettings.runtimeValues[name] = str;
					item = str;
				}
				else
				{
					item = SharedSettings.runtimeValues[name];
				}
			}
			return item;
		}

		private static string GetSettingFromFile(string name)
		{
			string str;
			lock (SharedSettings.settingPath)
			{
				SharedSettings.EnsureDirectoryExists();
				string filePath = SharedSettings.GetFilePath(name);
				string str1 = null;
				if (File.Exists(filePath))
				{
					str1 = File.ReadAllText(filePath);
				}
				str = str1;
			}
			return str;
		}

		private static void SetSetting(string name, string value)
		{
			lock (SharedSettings.settingPath)
			{
				SharedSettings.runtimeValues[name] = value;
				SharedSettings.EnsureDirectoryExists();
				File.WriteAllText(SharedSettings.GetFilePath(name), value);
			}
		}
	}
}