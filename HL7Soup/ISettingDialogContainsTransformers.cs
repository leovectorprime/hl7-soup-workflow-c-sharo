using System;

namespace HL7Soup
{
	public interface ISettingDialogContainsTransformers : ISettingDialog
	{
		Guid Transformers
		{
			get;
			set;
		}
	}
}