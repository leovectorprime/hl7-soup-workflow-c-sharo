using HL7Soup.HL7Descriptions;
using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using HL7Soup.Workflow.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace HL7Soup
{
	public class StoryBuilder
	{
		public HL7Soup.HL7Descriptions.DescriptionManager DescriptionManager
		{
			get;
			set;
		}

		public List<ValidationResult> ValidationResults
		{
			get;
			set;
		}

		public StoryBuilder(HL7Soup.HL7Descriptions.DescriptionManager descriptionManager, List<ValidationResult> validationResults)
		{
			this.DescriptionManager = descriptionManager;
			this.ValidationResults = validationResults;
		}

		private static string An(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			int num = 0;
			if (text.StartsWith("<"))
			{
				int num1 = text.IndexOf(">") + 1;
				if (num1 > 0)
				{
					num = num1;
				}
			}
			if ("aeiou".IndexOf(text.Substring(num, 1), StringComparison.InvariantCultureIgnoreCase) >= 0)
			{
				return string.Concat("an ", text);
			}
			return string.Concat("a ", text);
		}

		private static string Body(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			string newLine = " ";
			if (Settings.Default.MessageStoryFontSize > 1)
			{
				newLine = Environment.NewLine;
			}
			return string.Concat(new object[] { newLine, "<Run FontSize=\"", Settings.Default.MessageStoryFontSize + 8, "px\">", text, "</Run>" });
		}

		public string BuildStory(List<Segment> segments)
		{
			string str;
			bool flag = false;
			StringBuilder stringBuilder = new StringBuilder();
			if (this.ValidationResults != null)
			{
				foreach (ValidationResult validationResult in this.ValidationResults)
				{
					if (!validationResult.Result || !validationResult.MessageHighlighter.InvalidatesMessage)
					{
						continue;
					}
					stringBuilder.AppendLine(this.CreateValidationErrorText(validationResult));
				}
				if (stringBuilder.Length > 0)
				{
					stringBuilder.AppendLine();
				}
			}
			int num = 0;
			foreach (Segment segment in segments)
			{
				num++;
				if (segment.LocationCode.Length > 2)
				{
					string str1 = segment.LocationCode.Substring(0, 3);
					if (str1 != null)
					{
						switch (str1)
						{
							case "PID":
							{
								stringBuilder.AppendLine(this.GetPid(segment));
								break;
							}
							case "PV1":
							{
								stringBuilder.AppendLine(this.GetPv1(segment));
								break;
							}
							case "SCH":
							{
								stringBuilder.AppendLine(this.GetSch(segment));
								break;
							}
							case "ORC":
							{
								stringBuilder.AppendLine(this.GetOrc(segment));
								break;
							}
							case "OBR":
							{
								stringBuilder.AppendLine(this.GetObr(segment));
								break;
							}
							case "AIS":
							{
								stringBuilder.AppendLine(this.GetAis(segment));
								break;
							}
							case "AIL":
							{
								stringBuilder.AppendLine(this.GetAil(segment));
								break;
							}
							case "AIP":
							{
								stringBuilder.AppendLine(this.GetAip(segment));
								break;
							}
							case "AIG":
							{
								stringBuilder.AppendLine(this.GetAig(segment));
								break;
							}
							case "ROL":
							{
								stringBuilder.AppendLine(this.GetRol(segment));
								break;
							}
							case "IN1":
							{
								stringBuilder.AppendLine(this.GetIn1(segment));
								break;
							}
							case "TXA":
							{
								stringBuilder.AppendLine(this.GetTxa(segment));
								break;
							}
							case "RF1":
							{
								stringBuilder.AppendLine(this.GetRF1(segment));
								break;
							}
							case "PRD":
							{
								stringBuilder.AppendLine(this.GetPrd(segment));
								break;
							}
							case "MSA":
							{
								stringBuilder.AppendLine(this.GetMsa(segment));
								break;
							}
							case "MSH":
							{
								flag = true;
								break;
							}
						}
					}
				}
				if (num <= 100)
				{
					continue;
				}
				str = stringBuilder.ToString();
				if (str.Length > 1)
				{
					str = str.Substring(0, str.Length - 2);
				}
				else if (flag)
				{
					str = StoryBuilder.Body("No interpretations are available for any segment in this message.");
				}
				return str;
			}
			str = stringBuilder.ToString();
			if (str.Length > 1)
			{
				str = str.Substring(0, str.Length - 2);
			}
			else if (flag)
			{
				str = StoryBuilder.Body("No interpretations are available for any segment in this message.");
			}
			return str;
		}

		public static string Capital(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			char chr = text.First<char>();
			return string.Concat(chr.ToString().ToUpper(), string.Join<char>("", text.Skip<char>(1)).ToLower());
		}

		public static string CapitalFirstLetter(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			if (text.StartsWith("<"))
			{
				int num = text.IndexOf(">") + 1;
				if (num > 0)
				{
					return string.Concat(text.Substring(0, num), text.Substring(num, 1).ToUpper(), text.Substring(num + 1));
				}
			}
			return string.Concat(text.Substring(0, 1).ToUpper(), text.Substring(1));
		}

		public static string CapitalSpace(string text)
		{
			return StoryBuilder.Capital(StoryBuilder.Space(text));
		}

		private string CreateValidationErrorText(ValidationResult validationResult)
		{
			return StoryBuilder.HyperLink(StoryBuilder.ValidationErrorBody(string.Concat(new string[] { StoryBuilder.Space(validationResult.FieldName), "(", validationResult.Path, ") ", validationResult.MessageHighlighter.MessageFilter.GetResultText(), this.HasText(".  ", validationResult.MessageHighlighter.UserText) })), validationResult.Path.ToString());
		}

		private string DeleteLastComma(string text)
		{
			text = text.TrimEnd(Array.Empty<char>());
			if (!text.EndsWith(","))
			{
				return text;
			}
			return text.Substring(0, text.Length - 1);
		}

		private string ExistsThen(string condition, string texttoShow)
		{
			if (string.IsNullOrWhiteSpace(condition))
			{
				return "";
			}
			return texttoShow;
		}

		private string GetAig(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Appointment Information - General Resource Segment (", segment.LocationCode, "):")), string.Concat(str, "3"));
			string str2 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(6), string.Concat(str, "6"));
			string str3 = StoryBuilder.Sustitute(StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7), string.Concat(str, "7")), StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7, 2), string.Concat(str, "7.2")));
			string str4 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4), string.Concat(str, "4"));
			string str5 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4, 2), string.Concat(str, "4.2"));
			string str6 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(14), string.Concat(str, "14"));
			string str7 = StoryBuilder.HyperLinkInBody(this.HasText("actioned as ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower()), string.Concat(str, "2"));
			string str8 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(13)).ToLower(), string.Concat(str, "13"));
			string str9 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(8), ""), string.Concat(str, "8"));
			string str10 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(11), string.Concat(str, "11"));
			string str11 = StoryBuilder.Sustitute(StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(12), string.Concat(str, "12")), StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(12, 2), string.Concat(str, "12.2")));
			string str12 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(9), string.Concat(str, "9"));
			string str13 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(10), string.Concat(str, "10"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("", StoryBuilder.Space(str2), StoryBuilder.Space(str3)), StoryBuilder.Space(str4), StoryBuilder.Space(str5), StoryBuilder.HasText("in ", StoryBuilder.Space(str6), "status, "), StoryBuilder.HasText("", StoryBuilder.Space(str7), ""), this.HasText("starting at ", StoryBuilder.Space(str9)), StoryBuilder.Space(StoryBuilder.HasText("lasting ", str10, str11)), StoryBuilder.HasText("offset by ", StoryBuilder.Space(str12), str13), StoryBuilder.HasText(", ", str8, "") }))), StoryBuilder.Paragraph());
		}

		private string GetAil(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Appointment Information - Location Resource Segment (", segment.LocationCode, "):")), string.Concat(str, "3"));
			string location = this.GetLocation(segment, 3);
			string str2 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(12), string.Concat(str, "12"));
			string str3 = StoryBuilder.HyperLinkInBody(this.HasText("actioned as ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower()), string.Concat(str, "2"));
			string str4 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(11)).ToLower(), string.Concat(str, "11"));
			string str5 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(6), ""), string.Concat(str, "6"));
			string str6 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(9), string.Concat(str, "9"));
			string str7 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(10), string.Concat(str, "10"));
			string str8 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7), string.Concat(str, "7"));
			string str9 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(8), string.Concat(str, "8"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.NewLine(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("in ", StoryBuilder.Space(str2), "status, "), StoryBuilder.HasText("", StoryBuilder.Space(str3), ""), this.HasText("starting at ", StoryBuilder.Space(str5)), StoryBuilder.Space(StoryBuilder.HasText("lasting ", str6, str7)), StoryBuilder.HasText("offset by ", StoryBuilder.Space(str8), str9), StoryBuilder.HasText(", ", str4, "") })), StoryBuilder.HasText("Location: ", location, ""))), StoryBuilder.Paragraph());
		}

		private string GetAip(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("AIP - Appointment Information - Personnel Resource Segment (", segment.LocationCode, "):")), string.Concat(str, "3"));
			string doctor = this.GetDoctor(segment, 3);
			string str2 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4, 2), string.Concat(str, "4.2"));
			string str3 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(12), string.Concat(str, "12"));
			string str4 = StoryBuilder.HyperLinkInBody(this.HasText("actioned as ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower()), string.Concat(str, "2"));
			string str5 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(11)).ToLower(), string.Concat(str, "11"));
			string str6 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(6), ""), string.Concat(str, "6"));
			string str7 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(9), string.Concat(str, "9"));
			string str8 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(10), string.Concat(str, "10"));
			string str9 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7), string.Concat(str, "7"));
			string str10 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(8), string.Concat(str, "8"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.NewLine(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("in ", StoryBuilder.Space(str3), "status, "), StoryBuilder.HasText("", StoryBuilder.Space(str4), ""), this.HasText("starting at ", StoryBuilder.Space(str6)), StoryBuilder.Space(StoryBuilder.HasText("lasting ", str7, str8)), StoryBuilder.HasText("offset by ", StoryBuilder.Space(str9), str10), StoryBuilder.HasText(", ", str5, "") })), StoryBuilder.HasText(StoryBuilder.Sustitute("Personal: ", StoryBuilder.HasText("", str2, ": ")), doctor, ""))), StoryBuilder.Paragraph());
		}

		private string GetAis(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Appointment Information - Service Segment (", segment.LocationCode, "):")), string.Concat(str, "3"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(3, 1)), string.Concat(str, "3.1"));
			string str3 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(3, 2), string.Concat(str, "3.2"));
			string str4 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(10), string.Concat(str, "10"));
			string str5 = StoryBuilder.HyperLinkInBody(this.HasText("actioned as ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower()), string.Concat(str, "2"));
			string str6 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(9)).ToLower(), string.Concat(str, "9"));
			string str7 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(4), ""), string.Concat(str, "4"));
			string str8 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7), string.Concat(str, "7"));
			string str9 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(8), string.Concat(str, "8"));
			string str10 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(5), string.Concat(str, "5"));
			string str11 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(6), string.Concat(str, "6"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("", StoryBuilder.Space(str2), ""), StoryBuilder.Space(str3), StoryBuilder.HasText("in ", StoryBuilder.Space(str4), "status, "), StoryBuilder.HasText("", StoryBuilder.Space(str5), ""), this.HasText("starting at ", StoryBuilder.Space(str7)), StoryBuilder.Space(StoryBuilder.HasText("lasting ", str8, str9)), StoryBuilder.HasText("offset by ", StoryBuilder.Space(str10), str11), StoryBuilder.HasText(", ", str6, "") }))), StoryBuilder.Paragraph());
		}

		private string GetDoctor(Segment segment, int field)
		{
			string str = string.Concat(new object[] { segment.LocationCode, "-", field, "." });
			string str1 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(field, 1), string.Concat(str, "1"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 3)), string.Concat(str, "3"));
			string str3 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 2)), string.Concat(str, "2"));
			return string.Concat(StoryBuilder.Space(str2), StoryBuilder.Space(str3), StoryBuilder.Id(str1));
		}

		private string GetIn1(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Insurance (", segment.LocationCode, "):")), string.Concat(str, "36"));
			string str2 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4), string.Concat(str, "4"));
			string str3 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(36)), string.Concat(str, "36"));
			string str4 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(12), ""), string.Concat(str, "12"));
			string str5 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(12), ""), string.Concat(str, "12"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(StoryBuilder.Space(str3), this.HasText("Insured by ", StoryBuilder.Space(str2)), StoryBuilder.HasText("starting ", StoryBuilder.Space(str4), this.HasText("and expiring ", StoryBuilder.Space(str5)))))), StoryBuilder.Paragraph());
		}

		private string GetLocation(Segment segment, int field)
		{
			string str = string.Concat(new object[] { segment.LocationCode, "-", field, "." });
			string str1 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(field, 1)), string.Concat(str, "1"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 2)), string.Concat(str, "2"));
			string str3 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 3)), string.Concat(str, "3"));
			string str4 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 4)), string.Concat(str, "4"));
			string str5 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 7)), string.Concat(str, "7"));
			string str6 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(field, 8)), string.Concat(str, "8"));
			return string.Concat(new string[] { StoryBuilder.Space(str1), StoryBuilder.HasText("room ", str2, " "), StoryBuilder.HasText("bed ", str3, " "), StoryBuilder.Space(str4), StoryBuilder.HasText("building ", str5, " "), StoryBuilder.Space(str6) });
		}

		private string GetMsa(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(2)), string.Concat(str, "2"));
			string str2 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Message Acknowledgment (", segment.LocationCode, "):")), string.Concat(str, "2"));
			string str3 = StoryBuilder.HyperLinkInBody(this.HasText("Coded as ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(1)).ToLower()), string.Concat(str, "1"));
			string str4 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(3), string.Concat(str, "3"));
			string str5 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4), string.Concat(str, "4"));
			string str6 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(5), string.Concat(str, "5"));
			string str7 = StoryBuilder.HyperLinkInBody(this.HasText("Error due to ", this.DescriptionManager.GetDataTableDescription(segment.GetPart(6)).ToLower()), string.Concat(str, "6"));
			return string.Concat(str2, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.Space(str1), StoryBuilder.Space(str4), StoryBuilder.HasText("expected sequence number ", StoryBuilder.Space(str5), ", "), this.HasText("delayed acknowledgment type ", StoryBuilder.Space(str6)), StoryBuilder.Space(str3), StoryBuilder.NewLine(StoryBuilder.Space(str7)) }))), StoryBuilder.Paragraph());
		}

		private string GetObr(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Observation Request Segment (", segment.LocationCode, "):")), string.Concat(str, "4"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.HasText("(Placer ID ", segment.GetPartNiceText(2), ")"), string.Concat(str, "2"));
			string str3 = StoryBuilder.HyperLinkInBody(StoryBuilder.HasText("(Filler ID ", segment.GetPartNiceText(3), ")"), string.Concat(str, "3"));
			string str4 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(4, 1)), string.Concat(str, "4.1"));
			string str5 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(4, 2), string.Concat(str, "4.2"));
			string str6 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(6)), string.Concat(str, "6"));
			string str7 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			string str8 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(14)), string.Concat(str, "14"));
			string doctor = this.GetDoctor(segment, 10);
			string str9 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(16), string.Concat(str, "16"));
			return string.Concat(str1, StoryBuilder.Body(string.Concat(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("", StoryBuilder.Space(str9), "ordered "), StoryBuilder.Space(str4), StoryBuilder.Space(str5), StoryBuilder.Space(str2), StoryBuilder.Space(str3), this.HasText("requested ", StoryBuilder.Space(str6)), this.HasText("observed ", StoryBuilder.Space(str7)), this.HasText("specimen received ", StoryBuilder.Space(str8)) })), StoryBuilder.NewLine(StoryBuilder.HasText("Collected by: ", doctor, "")))), StoryBuilder.Paragraph());
		}

		private string GetOrc(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Common Order (", segment.LocationCode, "):")), string.Concat(str, "2"));
			string str2 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(1)).ToLower(), string.Concat(str, "1"));
			string str3 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(5)).ToLower(), string.Concat(str, "5"));
			string str4 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(6)).ToLower(), string.Concat(str, "6"));
			string str5 = StoryBuilder.HyperLinkInBody(StoryBuilder.HasText("(Placer ID ", segment.GetPartNiceText(2), ")"), string.Concat(str, "2"));
			string str6 = StoryBuilder.HyperLinkInBody(StoryBuilder.HasText("(Filler ID ", segment.GetPartNiceText(3), ")"), string.Concat(str, "3"));
			string str7 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(9)), string.Concat(str, "9"));
			string doctor = this.GetDoctor(segment, 10);
			string doctor1 = this.GetDoctor(segment, 11);
			string doctor2 = this.GetDoctor(segment, 12);
			string str8 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(21), string.Concat(str, "21"));
			return string.Concat(str1, StoryBuilder.Body(string.Concat(StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.HasText("", StoryBuilder.Space(str8), "initiated "), StoryBuilder.An(StoryBuilder.Space(str2)), StoryBuilder.Space(str5), StoryBuilder.Space(str6), StoryBuilder.Space(this.HasText("with the status ", str3)), StoryBuilder.Space(this.HasText("requests response of ", str4)), StoryBuilder.Space(str7) })), StoryBuilder.NewLine(StoryBuilder.HasText("Entered by: ", doctor, "")), StoryBuilder.NewLine(StoryBuilder.HasText("Verified by: ", doctor1, "")), StoryBuilder.NewLine(StoryBuilder.HasText("Ordering Provider: ", doctor2, "")))), StoryBuilder.Paragraph());
		}

		private string GetPid(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(3), string.Concat(str, "3"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(5, 2)), string.Concat(str, "5.2"));
			string str3 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(5, 1)), string.Concat(str, "5.1"));
			string str4 = StoryBuilder.HyperLinkInBody(Helpers.GetAge(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			string str5 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(8)).ToLower(), string.Concat(str, "8"));
			string str6 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(10)).ToLower(), string.Concat(str, "10"));
			string str7 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(16)).ToLower(), string.Concat(str, "16"));
			string str8 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(11, 3)), string.Concat(str, "11.3"));
			return string.Concat(StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Patient (", segment.LocationCode, "):")), "PID-3"), StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(StoryBuilder.Sustitute("unknown ", string.Concat(StoryBuilder.Space(str2), StoryBuilder.Space(str3), StoryBuilder.Id(str1))), this.HasText("is a ", StoryBuilder.HasText("", str4, string.Concat(new string[] { " year old ", StoryBuilder.Space(str7), StoryBuilder.Space(str6), StoryBuilder.Space(str5), this.HasText("living in ", StoryBuilder.Space(str8)) })))))), StoryBuilder.Paragraph());
		}

		private string GetPrd(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Provider Data (", segment.LocationCode, "):")), string.Concat(str, "7"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			string str3 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(1)).ToLower(), string.Concat(str, "1"));
			string str4 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(6)).ToLower(), string.Concat(str, "6"));
			string str5 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(8), ""), string.Concat(str, "8"));
			string str6 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(8)), string.Concat(str, "8"));
			string str7 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(9), ""), string.Concat(str, "9"));
			string str8 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(9)), string.Concat(str, "9"));
			string str9 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(2, 2)), string.Concat(str, "2.2"));
			string str10 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(2, 1)), string.Concat(str, "2.1"));
			string str11 = StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(3, 3)), string.Concat(str, "3.3"));
			return string.Concat(str1, StoryBuilder.Body(string.Concat(StoryBuilder.CapitalFirstLetter(string.Concat(StoryBuilder.HasText("The ", str3, " is "), StoryBuilder.CapitalFirstLetter(StoryBuilder.Sustitute("unknown ", string.Concat(StoryBuilder.Space(str9), StoryBuilder.Space(str10), str2))), StoryBuilder.HasText("from ", StoryBuilder.Space(str11), ""), StoryBuilder.HasText("prefers contact by", StoryBuilder.Space(str4), ""))), StoryBuilder.NewLine(StoryBuilder.HasText("Effective start: ", str5, string.Concat(" (", str6, ")"))), StoryBuilder.NewLine(StoryBuilder.HasText("Effective end: ", str7, string.Concat(" (", str8, ")"))))), StoryBuilder.Paragraph());
		}

		private string GetPv1(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Visit (", segment.LocationCode, "):")), string.Concat(str, "19"));
			string doctor = this.GetDoctor(segment, 7);
			string doctor1 = this.GetDoctor(segment, 8);
			string doctor2 = this.GetDoctor(segment, 9);
			string str2 = this.GetDoctor(segment, 17);
			string str3 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(39), string.Concat(str, "39"));
			string str4 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(10)).ToLower(), string.Concat(str, "10"));
			string str5 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(44)), string.Concat(str, "44"));
			string str6 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(44), ""), string.Concat(str, "44"));
			string partNiceText = segment.GetPartNiceText(2);
			string upper = partNiceText.ToUpper();
			partNiceText = (upper == null || !(upper == "N") && !(upper == "U") ? StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower(), string.Concat(str, "2")) : "");
			return string.Concat(str1, StoryBuilder.Body(string.Concat(new string[] { StoryBuilder.CapitalFirstLetter(string.Concat(StoryBuilder.HasText("", StoryBuilder.An(partNiceText), " visit "), StoryBuilder.HasText("occured ", str6, string.Concat(" ", StoryBuilder.Id(str5))), this.HasText("for ", str4), this.HasText("at ", str3))), StoryBuilder.NewLine(StoryBuilder.HasText("Attending Doctor: ", doctor, "")), StoryBuilder.NewLine(StoryBuilder.HasText("Referring Doctor: ", doctor1, "")), StoryBuilder.NewLine(StoryBuilder.HasText("Consulting Doctor: ", doctor2, "")), StoryBuilder.NewLine(StoryBuilder.HasText("Admitting Doctor: ", str2, "")) })), StoryBuilder.Paragraph());
		}

		private string GetRF1(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Referral Infomation (", segment.LocationCode, "):")), string.Concat(str, "6"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(12)), string.Concat(str, "6"));
			string str3 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(3, 2).ToLower(), string.Concat(str, "3.2"));
			string str4 = StoryBuilder.HyperLink(StoryBuilder.Header(StoryBuilder.CapitalFirstLetter(this.DescriptionManager.GetDataTableDescription(segment.GetPart(1)).ToLower())), string.Concat(str, "1"));
			string str5 = StoryBuilder.HyperLinkInBody(StoryBuilder.Replace(this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower(), "routine", "routinely"), string.Concat(str, "2"));
			string str6 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(3)).ToLower(), string.Concat(str, "3"));
			string str7 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(4)).ToLower(), string.Concat(str, "4"));
			string str8 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(5)).ToLower(), string.Concat(str, "5"));
			string str9 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(10)).ToLower(), string.Concat(str, "10"));
			string str10 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(7), ""), string.Concat(str, "7"));
			string str11 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			string str12 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(8), ""), string.Concat(str, "8"));
			string str13 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(8)), string.Concat(str, "8"));
			string str14 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(9), ""), string.Concat(str, "9"));
			string str15 = StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(9)), string.Concat(str, "9"));
			string str16 = StoryBuilder.HasText("", str4, StoryBuilder.Header(" request for "));
			string[] strArrays = new string[] { StoryBuilder.Space(str2), null, null, null, null };
			strArrays[1] = StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.An(StoryBuilder.HasText("", str8, " referral ")), this.HasText("to ", StoryBuilder.Sustitute(StoryBuilder.Space(str6), StoryBuilder.Space(str3))), StoryBuilder.HasText("requesting ", StoryBuilder.Space(str7), ""), StoryBuilder.HasText("is required ", StoryBuilder.Space(str5), ""), StoryBuilder.HasText("due to ", StoryBuilder.Space(str9), "") }));
			strArrays[2] = StoryBuilder.NewLine(StoryBuilder.HasText("Effective from: ", str10, string.Concat(" (", str11, ")")));
			strArrays[3] = StoryBuilder.NewLine(StoryBuilder.HasText("Expires on: ", str12, string.Concat(" (", str13, ")")));
			strArrays[4] = StoryBuilder.NewLine(StoryBuilder.HasText("Processed on: ", str14, string.Concat(" (", str15, ")")));
			return string.Concat(str16, str1, StoryBuilder.Body(string.Concat(strArrays)), StoryBuilder.Paragraph());
		}

		private string GetRol(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Role (", segment.LocationCode, "):")), string.Concat(str, "1"));
			string str2 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower(), string.Concat(str, "2"));
			string str3 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(3)).ToLower(), string.Concat(str, "3"));
			string doctor = this.GetDoctor(segment, 4);
			string str4 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(5), ""), string.Concat(str, "5"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(StoryBuilder.Space(str2), StoryBuilder.Space(doctor), this.HasText("in to role of ", StoryBuilder.Space(str3)), StoryBuilder.HasText("starting ", StoryBuilder.Space(str4), "")))), StoryBuilder.Paragraph());
		}

		private string GetSch(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Appointment (", segment.LocationCode, "):")), string.Concat(str, "2"));
			StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(12, 3)), string.Concat(str, "12.3"));
			StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(12, 2)), string.Concat(str, "12.2"));
			string str2 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(25)).ToLower(), string.Concat(str, "25"));
			string str3 = StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(11, 4), ""), string.Concat(str, "11.4"));
			string str4 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(9), string.Concat(str, "9"));
			string str5 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(10), string.Concat(str, "10"));
			string str6 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(7)).ToLower(), string.Concat(str, "7"));
			if (str6 == "")
			{
				str6 = StoryBuilder.HyperLinkInBody(segment.GetPartNiceText(7, 2), string.Concat(str, "7.2"));
			}
			StoryBuilder.HyperLinkInBody(Helpers.GetAge(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(8)).ToLower(), string.Concat(str, "8"));
			StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(10)).ToLower(), string.Concat(str, "10"));
			StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(16)).ToLower(), string.Concat(str, "16"));
			StoryBuilder.HyperLinkInBody(StoryBuilder.Capital(segment.GetPartNiceText(11, 3)), string.Concat(str, "11.3"));
			return string.Concat(str1, StoryBuilder.Body(StoryBuilder.CapitalFirstLetter(string.Concat(this.HasText("", StoryBuilder.Space(str2)), this.HasText("starting ", StoryBuilder.Space(str3)), StoryBuilder.Space(StoryBuilder.HasText("lasting ", str4, str5)), this.HasText("for ", StoryBuilder.Space(str6))))), StoryBuilder.Paragraph());
		}

		private string GetTxa(Segment segment)
		{
			string str = string.Concat(segment.LocationCode, "-");
			string str1 = StoryBuilder.HyperLink(StoryBuilder.Header(string.Concat("Transcription Document Header (", segment.LocationCode, "):")), string.Concat(str, "12"));
			string str2 = StoryBuilder.HyperLinkInBody(StoryBuilder.Id(segment.GetPartNiceText(12)), string.Concat(str, "12"));
			string str3 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(2)).ToLower(), string.Concat(str, "2"));
			string str4 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(3)).ToLower(), string.Concat(str, "3"));
			string str5 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(17)).ToLower(), string.Concat(str, "17"));
			string str6 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(18)).ToLower(), string.Concat(str, "18"));
			string str7 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(19)).ToLower(), string.Concat(str, "19"));
			string str8 = StoryBuilder.HyperLinkInBody(this.DescriptionManager.GetDataTableDescription(segment.GetPart(20)).ToLower(), string.Concat(str, "20"));
			string doctor = this.GetDoctor(segment, 5);
			string doctor1 = this.GetDoctor(segment, 9);
			string doctor2 = this.GetDoctor(segment, 10);
			string doctor3 = this.GetDoctor(segment, 11);
			StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(4), ""), string.Concat(str, "4"));
			StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(4)), string.Concat(str, "4"));
			StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(6), ""), string.Concat(str, "6"));
			StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(6)), string.Concat(str, "6"));
			StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(7), ""), string.Concat(str, "7"));
			StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(7)), string.Concat(str, "7"));
			StoryBuilder.HyperLinkInBody(Helpers.ParseISO8601StringToString(segment.GetPartNiceText(7), ""), string.Concat(str, "7"));
			StoryBuilder.HyperLinkInBody(Helpers.GetTimeAgo(segment.GetPartNiceText(8)), string.Concat(str, "8"));
			string[] strArrays = new string[] { StoryBuilder.Space(str2), null, null, null, null, null };
			strArrays[1] = StoryBuilder.CapitalFirstLetter(string.Concat(new string[] { StoryBuilder.An(StoryBuilder.Space(str3)), this.HasText("of type ", StoryBuilder.Space(str4)), StoryBuilder.HasText("in the status of ", StoryBuilder.Space(str5), ""), StoryBuilder.HasText("that is ", StoryBuilder.Space(str6), ""), StoryBuilder.HasText(string.Concat(this.ExistsThen(str6, "and "), "is "), StoryBuilder.Space(str7), ""), StoryBuilder.HasText(string.Concat(this.NotExistsThen(str6, this.ExistsThen(str7, "and ")), "is stored as "), StoryBuilder.Space(str8), "") }));
			strArrays[2] = StoryBuilder.NewLine(StoryBuilder.HasText("Entered by: ", doctor, ""));
			strArrays[3] = StoryBuilder.NewLine(StoryBuilder.HasText("Originated from: ", doctor1, ""));
			strArrays[4] = StoryBuilder.NewLine(StoryBuilder.HasText("Assigned Authenticator: ", doctor2, ""));
			strArrays[5] = StoryBuilder.NewLine(StoryBuilder.HasText("Transcribed by: ", doctor3, ""));
			return string.Concat(str1, StoryBuilder.Body(string.Concat(strArrays)), StoryBuilder.Paragraph());
		}

		private string HasText(string left, string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(left, text);
		}

		private static string HasText(string left, string text, string right)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(left, text, right);
		}

		private static string Header(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(new object[] { "<Run FontSize=\"", Settings.Default.MessageStoryFontSize + 10, "px\">", text, "</Run>" });
		}

		private static string HyperLink(string text, string path)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(new string[] { "<Hyperlink Command=\"Help\" CommandParameter=\"", path, "\">", text, "</Hyperlink>" });
		}

		private static string HyperLinkInBody(string text, string path)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(new object[] { "</Run><Hyperlink FontSize=\"", Settings.Default.MessageStoryFontSize + 8, "px\" Command=\"Help\" CommandParameter=\"", path, "\">", text, "</Hyperlink><Run FontSize=\"", Settings.Default.MessageStoryFontSize + 8, "px\">" });
		}

		private static string Id(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat("(", text, ") ");
		}

		private static string NewLine(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(Environment.NewLine, text);
		}

		private static string NewLine(string text1, string text2)
		{
			if (string.IsNullOrWhiteSpace(text1))
			{
				return text2;
			}
			return string.Concat(text1, Environment.NewLine, text2);
		}

		private string NotExistsThen(string condition, string texttoShow)
		{
			if (!string.IsNullOrWhiteSpace(condition))
			{
				return "";
			}
			return texttoShow;
		}

		private static string Paragraph()
		{
			return Environment.NewLine;
		}

		private static string Replace(string text, string unwantedText, string substituteText)
		{
			if (text == unwantedText)
			{
				return substituteText;
			}
			return text;
		}

		private static string Space(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(text, " ");
		}

		private static string Sustitute(string substituteText, string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return substituteText;
			}
			return text;
		}

		private static string ValidationErrorBody(string text)
		{
			if (string.IsNullOrWhiteSpace(text))
			{
				return "";
			}
			return string.Concat(new object[] { "<Run Foreground=\"Red\" FontSize=\"", Settings.Default.MessageStoryFontSize + 8, "px\">", text, "</Run>" });
		}
	}
}