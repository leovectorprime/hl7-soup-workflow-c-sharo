using HL7Soup.HL7Descriptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Windows;

namespace HL7Soup
{
	public abstract class BasePart : DependencyObject, INotifyPropertyChanged
	{
		protected List<XBasePart> XDefinitions;

		private List<BasePart> childParts;

		private bool istimerrunning;

		private bool istimer1running;

		private string text;

		private BasePart currentChildPart;

		private bool hasChildren;

		public List<BasePart> ChildParts
		{
			get
			{
				return this.childParts;
			}
			protected set
			{
				if (!this.Message.OnlyUpdateChangesMode)
				{
					this.childParts = value;
					this.OnChildPartsChanged();
					return;
				}
				if (this.childParts == null || value == null || this.childParts.Count != value.Count)
				{
					if (this.childParts == null || value == null || this.childParts.Count == value.Count)
					{
						this.OnChildPartsChanged();
						this.childParts = value;
						return;
					}
					this.childParts = value;
					this.Message.RaiseCurrentSegmentChanged();
					this.OnChildPartsChanged();
					return;
				}
				int count = this.childParts.Count;
				for (int i = 0; i < this.childParts.Count; i++)
				{
					this.childParts[i].XPart = value[i].XPart;
					this.childParts[i].LocationCode = value[i].LocationCode;
					this.childParts[i].PositionInTheDocument = value[i].PositionInTheDocument;
					if (this.childParts[i].Text != value[i].Text)
					{
						this.childParts[i].SetText(value[i].Text);
						this.childParts[i].Reload();
					}
				}
				this.OnChildPartsChanged();
			}
		}

		public BasePart CurrentChildPart
		{
			get
			{
				return this.currentChildPart;
			}
			protected set
			{
				this.currentChildPart = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentChildPart"));
					if (this is HL7Soup.Message)
					{
						this.RaiseCurrentSegmentChanged();
					}
					if (this is Segment)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentField"));
					}
					if (this is Field)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentComponent"));
					}
					if (this is HL7Soup.Component)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentSubComponent"));
					}
				}
			}
		}

		public int CursorLocation
		{
			get;
			protected set;
		}

		public string DataTableId
		{
			get
			{
				if (this.XPart == null)
				{
					return null;
				}
				if (this is HL7Soup.Component)
				{
					Field parent = this.Parent as Field;
					if (parent != null && parent.LocationCode == "9")
					{
						string fullLocationCode = this.FullLocationCode;
						if (fullLocationCode != null)
						{
							if (fullLocationCode == "MSH-9.1")
							{
								return "HL70076";
							}
							if (fullLocationCode == "MSH-9.2")
							{
								return "HL70003";
							}
							if (fullLocationCode == "MSH-9.3")
							{
								return "HL70354";
							}
						}
					}
				}
				return this.XPart.DataTableId;
			}
		}

		public string Description
		{
			get;
			set;
		}

		public string FullLocationCode
		{
			get;
			protected set;
		}

		public XDataTable GetCurrentAdjustedDataTable
		{
			get
			{
				Func<KeyValuePair<string, XDataTableItem>, bool> func;
				Func<KeyValuePair<string, XDataTableItem>, bool> func1 = null;
				Func<KeyValuePair<string, XDataTableItem>, bool> func2 = null;
				this.Message.PopulatingDataTableForField = true;
				XDataTable getCurrentDataTable = this.GetCurrentDataTable;
				if (getCurrentDataTable != null && this.DataTableId == "HL70003")
				{
					XDataTable xDataTable = new XDataTable();
					List<string> suitableMSH92 = XMessageType.GetSuitableMSH92(this.Message.GetPartText(new PathSplitter(SegmentsEnum.MSH, 9, 1)));
					if (suitableMSH92.Count > 0)
					{
						Dictionary<string, XDataTableItem> items = getCurrentDataTable.Items;
						Func<KeyValuePair<string, XDataTableItem>, bool> func3 = func1;
						if (func3 == null)
						{
							Func<KeyValuePair<string, XDataTableItem>, bool> func4 = (KeyValuePair<string, XDataTableItem> p) => suitableMSH92.Contains(p.Value.Value);
							func = func4;
							func1 = func4;
							func3 = func;
						}
						foreach (KeyValuePair<string, XDataTableItem> keyValuePair in items.Where<KeyValuePair<string, XDataTableItem>>(func3))
						{
							xDataTable.Items.Add(keyValuePair.Key, keyValuePair.Value);
						}
						Dictionary<string, XDataTableItem> strs = getCurrentDataTable.Items;
						Func<KeyValuePair<string, XDataTableItem>, bool> func5 = func2;
						if (func5 == null)
						{
							Func<KeyValuePair<string, XDataTableItem>, bool> func6 = (KeyValuePair<string, XDataTableItem> p) => !suitableMSH92.Contains(p.Value.Value);
							func = func6;
							func2 = func6;
							func5 = func;
						}
						foreach (KeyValuePair<string, XDataTableItem> keyValuePair1 in strs.Where<KeyValuePair<string, XDataTableItem>>(func5))
						{
							xDataTable.Items.Add(keyValuePair1.Key, keyValuePair1.Value);
						}
						return xDataTable;
					}
				}
				return getCurrentDataTable;
			}
		}

		public XDataTable GetCurrentDataTable
		{
			get
			{
				if (this.XPart == null)
				{
					return null;
				}
				return this.Message.DescriptionManager.GetDataTable(this.DataTableId);
			}
		}

		public string GetCurrentDataTableValue
		{
			get
			{
				string dataTableDisplayText = this.Message.DescriptionManager.GetDataTableDisplayText(this);
				if (!string.IsNullOrWhiteSpace(dataTableDisplayText))
				{
					return dataTableDisplayText;
				}
				return this.text;
			}
		}

		public bool HasChildren
		{
			get
			{
				return this.hasChildren;
			}
			set
			{
				this.hasChildren = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("HasChildren"));
				}
			}
		}

		public bool HasDataTable
		{
			get
			{
				return !string.IsNullOrEmpty(this.DataTableId);
			}
		}

		public int Index
		{
			get;
			set;
		}

		public bool IsDate
		{
			get
			{
				if (this.XPart == null || this.XPart.DataTypeName == null)
				{
					return false;
				}
				if (this.XPart.DataTypeName.ToUpper() == "TS")
				{
					return true;
				}
				return this.XPart.DataTypeName.ToUpper() == "DT";
			}
		}

		public bool IsLoaded
		{
			get;
			set;
		}

		public string LocationCode
		{
			get
			{
				return JustDecompileGenerated_get_LocationCode();
			}
			set
			{
				JustDecompileGenerated_set_LocationCode(value);
			}
		}

		private string JustDecompileGenerated_LocationCode_k__BackingField;

		public string JustDecompileGenerated_get_LocationCode()
		{
			return this.JustDecompileGenerated_LocationCode_k__BackingField;
		}

		protected void JustDecompileGenerated_set_LocationCode(string value)
		{
			this.JustDecompileGenerated_LocationCode_k__BackingField = value;
		}

		public int LocationOfFirstCharacter
		{
			get;
			set;
		}

		public HL7Soup.Message Message
		{
			get;
			private set;
		}

		public BasePart Parent
		{
			get;
			private set;
		}

		public HL7Soup.PositionInTheDocument PositionInTheDocument
		{
			get;
			set;
		}

		public string Text
		{
			get
			{
				return JustDecompileGenerated_get_Text();
			}
			set
			{
				JustDecompileGenerated_set_Text(value);
			}
		}

		public string JustDecompileGenerated_get_Text()
		{
			return this.text;
		}

		public void JustDecompileGenerated_set_Text(string value)
		{
			if (this.text != value)
			{
				int length = this.text.Length;
				this.text = value;
				this.RebuildParentTextQuietly();
				this.RecalculatePositionInTheDocument();
				this.Message.RaiseTextUpdated(this, length);
				if (this.FullLocationCode == "MSH-9.1" && this.PropertyChanged != null && this.Parent != null)
				{
					BasePart part = this.Parent.GetPart(new PathSplitter(SegmentsEnum.MSH, 9, 2));
					if (part != null)
					{
						part.PropertyChanged(part, new PropertyChangedEventArgs("GetCurrentAdjustedDataTable"));
					}
				}
				this.Message.UpdateMessageStory();
			}
		}

		public XBasePart XPart
		{
			get;
			set;
		}

		public BasePart(string text, BasePart parent, string locationCode, int index, int locationOfFirstCharacter, XBasePart xPart)
		{
			if (parent is HL7Soup.Message)
			{
				this.Message = (HL7Soup.Message)parent;
				this.PositionInTheDocument = new HL7Soup.PositionInTheDocument(locationOfFirstCharacter, text.Length);
				this.FullLocationCode = locationCode;
			}
			else if (parent != null)
			{
				this.Message = parent.Message;
				if (parent.PositionInTheDocument == null)
				{
					this.PositionInTheDocument = new HL7Soup.PositionInTheDocument(locationOfFirstCharacter, text.Length);
				}
				else
				{
					if (locationOfFirstCharacter == 2 && parent is Segment)
					{
						locationOfFirstCharacter = 3;
					}
					this.PositionInTheDocument = new HL7Soup.PositionInTheDocument(locationOfFirstCharacter + parent.PositionInTheDocument.Start, text.Length);
				}
				if (!(parent is Segment))
				{
					this.FullLocationCode = string.Concat(parent.FullLocationCode, ".", locationCode);
				}
				else if (!string.IsNullOrEmpty(locationCode))
				{
					this.FullLocationCode = string.Concat(parent.FullLocationCode, "-", locationCode);
				}
				else
				{
					this.FullLocationCode = parent.FullLocationCode;
				}
			}
			else
			{
				this.Message = (HL7Soup.Message)this;
				this.FullLocationCode = locationCode;
			}
			this.LocationOfFirstCharacter = locationOfFirstCharacter;
			this.Index = index;
			this.text = text;
			this.LocationCode = locationCode;
			this.Parent = parent;
			if (xPart != null)
			{
				this.Description = xPart.Description;
				this.XDefinitions = xPart.GetChildParts();
			}
			this.XPart = xPart;
		}

		public abstract string GetCurrentPath();

		protected XBasePart GetDefinitionByIndex(int index)
		{
			if (this.XDefinitions == null || index < 0 || this.XDefinitions.Count <= index)
			{
				return null;
			}
			return this.XDefinitions[index];
		}

		public abstract string GetDescriptionName();

		public abstract BasePart GetPart(PathSplitter path);

		public virtual string GetPath()
		{
			string path = "";
			if (this.Parent != null)
			{
				path = this.Parent.GetPath();
			}
			return string.Concat(path, this.LocationCode);
		}

		public bool HasSiblings()
		{
			if (this.Parent == null)
			{
				return false;
			}
			return this.Parent.ChildParts.Count > 1;
		}

		public void Load()
		{
			if (!this.IsLoaded)
			{
				this.Reload();
			}
		}

		protected abstract void LoadChildParts();

		protected virtual void OnChildPartsChanged()
		{
		}

		protected void RaiseAllSegmentsChanged()
		{
			if (!(this is HL7Soup.Message))
			{
				this.Message.RaiseAllSegmentsChanged();
				return;
			}
			if (this.istimer1running)
			{
				return;
			}
			this.istimer1running = true;
			System.Timers.Timer timer = new System.Timers.Timer(200)
			{
				AutoReset = true
			};
			timer.Elapsed += new ElapsedEventHandler((object argument0, ElapsedEventArgs argument1) => {
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("AllSegments"));
				}
				timer.Stop();
				this.istimer1running = false;
			});
			timer.Start();
		}

		public void RaiseCurrentPartChanged()
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentPart"));
			}
		}

		protected void RaiseCurrentSegmentChanged()
		{
			if (!(this is HL7Soup.Message))
			{
				this.Message.RaiseCurrentPartChanged();
				return;
			}
			if (this.istimerrunning)
			{
				return;
			}
			this.istimerrunning = true;
			System.Timers.Timer timer = new System.Timers.Timer(200)
			{
				AutoReset = true
			};
			timer.Elapsed += new ElapsedEventHandler((object argument0, ElapsedEventArgs argument1) => {
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentSegment"));
				}
				timer.Stop();
				this.istimerrunning = false;
			});
			timer.Start();
		}

		public void RaiseDescriptionManagerChanged()
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("DescriptionManager"));
			}
		}

		public void RaisePropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public virtual void RebuildParentTextQuietly()
		{
			if (this.Message.BeginUpdateCount > 0)
			{
				return;
			}
			if (this.Parent != null)
			{
				this.Parent.SetTextQuietly(this.Parent.ToString());
				this.Parent.RebuildParentTextQuietly();
			}
		}

		public virtual void RecalculatePositionInTheDocument()
		{
			this.Message.RecalculatePositionInTheDocument(0, 0);
		}

		public virtual int RecalculatePositionInTheDocument(int locationOfFirstCharacterInParentPart, int positionInWholeDocument)
		{
			this.UpdatePositionInDocument(this, locationOfFirstCharacterInParentPart, positionInWholeDocument);
			this.Load();
			if (this.childParts != null)
			{
				int length = 1;
				if (this is HL7Soup.Message)
				{
					length = this.Message.ActualSegmentSeperator.Length;
				}
				locationOfFirstCharacterInParentPart = 0;
				foreach (BasePart childPart in this.childParts)
				{
					positionInWholeDocument = childPart.RecalculatePositionInTheDocument(locationOfFirstCharacterInParentPart, positionInWholeDocument);
					locationOfFirstCharacterInParentPart = locationOfFirstCharacterInParentPart + childPart.text.Length + length;
					positionInWholeDocument = positionInWholeDocument + childPart.text.Length + length;
				}
			}
			return this.PositionInTheDocument.Start;
		}

		public virtual void Reload()
		{
			this.LoadChildParts();
			this.IsLoaded = true;
		}

		public virtual void SetAsCurrentItem()
		{
			if (this.Parent != null)
			{
				this.Parent.currentChildPart = this;
				this.Parent.SetAsCurrentItem();
			}
		}

		public virtual BasePart SetCurrentLocation(int location)
		{
			this.Load();
			this.CursorLocation = location;
			int length = 0;
			int num = 0;
			for (int i = 0; i < this.ChildParts.Count - 1; i++)
			{
				length = length + this.ChildParts[i].Text.Length + 1;
				if (i == 0 && this.ChildParts[i].Text == "MSH")
				{
					length--;
				}
				if (i == 1 && this.ChildParts[i].Text == this.Message.FieldSeperator)
				{
					length--;
				}
				if (length > location)
				{
					if (this.CurrentChildPart != this.ChildParts[i])
					{
						this.CurrentChildPart = this.ChildParts[i];
					}
					if ((this.CurrentChildPart.Text.Contains(this.Message.ComponentSeperator) || this.CurrentChildPart.Text.Contains(this.Message.SubComponentSeperator)) && (i != 2 || !(this.CurrentChildPart.Text == HL7Soup.Message.SeperatorText)))
					{
						this.CurrentChildPart.SetCurrentLocation(location - num);
					}
					return this.CurrentChildPart;
				}
				num = length;
			}
			if (this.ChildParts.Count < 1)
			{
				return null;
			}
			if (this.CurrentChildPart != this.ChildParts[this.ChildParts.Count - 1])
			{
				this.CurrentChildPart = this.ChildParts[this.ChildParts.Count - 1];
			}
			if (this.CurrentChildPart.Text.Contains(this.Message.ComponentSeperator) || this.CurrentChildPart.Text.Contains(this.Message.SubComponentSeperator))
			{
				this.CurrentChildPart.SetCurrentLocation(location - num);
			}
			return this.CurrentChildPart;
		}

		public abstract HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path);

		protected void SetText(string text)
		{
			this.text = text;
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("Text"));
			}
		}

		public virtual void SetTextQuietly(string text)
		{
			this.text = text;
		}

		public string[] Split(string text, string delimiter)
		{
			string[] strArrays = null;
			strArrays = ("\r\n|\r|\n" != delimiter ? text.Split(delimiter.ToCharArray()) : Regex.Split(text, delimiter));
			return strArrays;
		}

		public override string ToString()
		{
			return this.Text;
		}

		public static void Unload(BasePart basepart)
		{
			basepart.childParts = new List<BasePart>();
			basepart.IsLoaded = false;
		}

		public void UpdatePositionInDocument(BasePart parent, int locationOfFirstCharacterInLine, int positionInWholeDocument)
		{
			if (parent is HL7Soup.Message)
			{
				this.PositionInTheDocument = new HL7Soup.PositionInTheDocument(positionInWholeDocument, this.text.Length);
			}
			else if (parent != null)
			{
				if (positionInWholeDocument == 4)
				{
					positionInWholeDocument = 3;
					locationOfFirstCharacterInLine = 3;
				}
				else if (positionInWholeDocument == 5)
				{
					positionInWholeDocument = 4;
					locationOfFirstCharacterInLine = 4;
				}
				this.PositionInTheDocument = new HL7Soup.PositionInTheDocument(positionInWholeDocument, this.text.Length);
			}
			this.LocationOfFirstCharacter = locationOfFirstCharacterInLine;
		}

		public virtual event PropertyChangedEventHandler PropertyChanged;
	}
}