using NLog;
using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace HL7Soup
{
	public static class Log
	{
		public static Logger Instance
		{
			get;
			private set;
		}

		static Log()
		{
			LogManager.ReconfigExistingLoggers();
			Log.Instance = LogManager.GetCurrentClassLogger();
		}

		public static void ExceptionHandler(Exception ex)
		{
			Log.Instance.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}
	}
}