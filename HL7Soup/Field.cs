using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(FieldDebuggingProxy))]
	public class Field : BasePart, IHL7Field, IHL7Part
	{
		public Dictionary<string, Component> Components
		{
			get;
			set;
		}

		public Component CurrentComponent
		{
			get
			{
				return (Component)base.CurrentChildPart;
			}
		}

		private string DebuggerDisplay
		{
			get
			{
				string path = this.GetPath();
				string str = "";
				string str1 = "                                                            ";
				if (base.XPart != null)
				{
					str = Helpers.TruncateString(base.XPart.Description, Math.Max(27 - this.GetPath().Length, 12));
				}
				return string.Concat(new string[] { path, "(", string.Concat(str, ") ", str1).Substring(0, 60 - (int)((double)(path.Length + str.Length) * 0.85)), "  =", base.Text });
			}
		}

		public bool IsFieldRepeated
		{
			get
			{
				return JustDecompileGenerated_get_IsFieldRepeated();
			}
			set
			{
				JustDecompileGenerated_set_IsFieldRepeated(value);
			}
		}

		private bool JustDecompileGenerated_IsFieldRepeated_k__BackingField;

		public bool JustDecompileGenerated_get_IsFieldRepeated()
		{
			return this.JustDecompileGenerated_IsFieldRepeated_k__BackingField;
		}

		public void JustDecompileGenerated_set_IsFieldRepeated(bool value)
		{
			this.JustDecompileGenerated_IsFieldRepeated_k__BackingField = value;
		}

		public bool IsHeader
		{
			get
			{
				return base.LocationCode == "";
			}
		}

		public bool IsRepeatField
		{
			get
			{
				return base.LocationCode.Contains("[");
			}
		}

		public string RepeatGroup
		{
			get;
			set;
		}

		public Field(string text, BasePart parent, string locationCode, int index, int documentLocation, XBasePart xPart) : base(text, parent, locationCode, index, documentLocation, xPart)
		{
		}

		public BasePart GetComponent(PathSplitter path)
		{
			if (!this.Components.ContainsKey(path.Component))
			{
				return null;
			}
			return this.Components[path.Component];
		}

		public IHL7Component GetComponent(int componentLocation)
		{
			base.Load();
			return this.GetComponent(componentLocation.ToString());
		}

		public IHL7Component GetComponent(int componentLocation, int repeatIndex)
		{
			base.Load();
			string str = componentLocation.ToString();
			if (repeatIndex > 1)
			{
				str = string.Concat(new object[] { str, "[", repeatIndex, "]" });
			}
			return this.GetComponent(str);
		}

		public IHL7Component GetComponent(string locationCode)
		{
			base.Load();
			if (!this.Components.ContainsKey(locationCode))
			{
				return null;
			}
			return this.Components[locationCode];
		}

		public Dictionary<string, Component> GetComponents(Field field)
		{
			return this.GetComponents(field.Text);
		}

		public Dictionary<string, Component> GetComponents(string text)
		{
			if (text == HL7Soup.Message.SeperatorText)
			{
				return new Dictionary<string, Component>();
			}
			string[] strArrays = this.SplitFieldsIntoComponents(text);
			Dictionary<string, Component> strs = new Dictionary<string, Component>();
			if (strArrays.GetUpperBound(0) > 0 || strArrays[0].StartsWith(base.Message.SubComponentSeperator))
			{
				int num = 0;
				int length = 0;
				string[] strArrays1 = strArrays;
				for (int i = 0; i < (int)strArrays1.Length; i++)
				{
					string str = strArrays1[i];
					int num1 = num + 1;
					Component component = new Component(str, this, num1.ToString(), num, length, base.GetDefinitionByIndex(num));
					num1 = num + 1;
					strs.Add(num1.ToString(), component);
					num++;
					length = length + str.Length + 1;
				}
			}
			return strs;
		}

		public IHL7Components GetComponents()
		{
			base.Load();
			return new HL7Soup.Components(this.Components.Values.ToList<IHL7Component>());
		}

		public override string GetCurrentPath()
		{
			if (base.CurrentChildPart == null)
			{
				return base.LocationCode;
			}
			return string.Concat(base.LocationCode, ".", base.CurrentChildPart.GetCurrentPath());
		}

		public override string GetDescriptionName()
		{
			if (base.Parent == null)
			{
				return base.Index.ToString();
			}
			int index = base.Index + 1;
			return string.Concat(base.Parent.GetDescriptionName(), ".", index.ToString());
		}

		public override BasePart GetPart(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Component))
			{
				BasePart component = this.GetComponent(path);
				if (path.PathTerminationPoint == PathTypes.Component)
				{
					return component;
				}
				if (component != null)
				{
					BasePart part = component.GetPart(path);
					if (part != null)
					{
						return part;
					}
					if (path.SubComponent == "1")
					{
						return component;
					}
				}
			}
			return null;
		}

		public override string GetPath()
		{
			string path = "";
			if (base.Parent != null)
			{
				path = base.Parent.GetPath();
			}
			return string.Concat(path, "-", base.LocationCode);
		}

		public List<Field> GetRelatedRepeatField()
		{
			return this.GetRelatedRepeatField(true);
		}

		public List<Field> GetRelatedRepeatField(bool excludeCurrent)
		{
			List<Field> fields = new List<Field>();
			if (base.Parent != null && !string.IsNullOrEmpty(this.RepeatGroup))
			{
				foreach (BasePart childPart in base.Parent.ChildParts)
				{
					if (!(((Field)childPart).RepeatGroup == this.RepeatGroup) || excludeCurrent && !(childPart.LocationCode != base.LocationCode))
					{
						continue;
					}
					fields.Add((Field)childPart);
				}
			}
			return fields;
		}

		public ICollection<IHL7Field> GetRelatedRepeatFields()
		{
			return this.GetRelatedRepeatFields(false);
		}

		public ICollection<IHL7Field> GetRelatedRepeatFields(bool excludeCurrent)
		{
			return new Collection<IHL7Field>(this.GetRelatedRepeatField(excludeCurrent).ToArray<IHL7Field>());
		}

		void HL7Soup.Integrations.IHL7Part.SetText(string text)
		{
			this.SetTextQuietly(text);
			this.RebuildParentTextQuietly();
		}

		protected override void LoadChildParts()
		{
			this.Components = this.GetComponents(this);
			base.HasChildren = this.Components.Count > 0;
			base.ChildParts = this.Components.Values.Cast<BasePart>().ToList<BasePart>();
		}

		public override HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path)
		{
			base.Load();
			if (!string.IsNullOrEmpty(path.Component))
			{
				base.CurrentChildPart = this.GetComponent(path);
				if (base.CurrentChildPart != null)
				{
					HL7Soup.PositionInTheDocument positionInTheDocument = base.CurrentChildPart.SetCurrentPath(path);
					if (positionInTheDocument == null)
					{
						return new HL7Soup.PositionInTheDocument(this.CurrentComponent.LocationOfFirstCharacter, this.CurrentComponent.Text.Length);
					}
					HL7Soup.PositionInTheDocument start = positionInTheDocument;
					start.Start = start.Start + this.CurrentComponent.LocationOfFirstCharacter;
					return positionInTheDocument;
				}
			}
			return null;
		}

		public void SetTextEncoded(string text)
		{
			((IHL7Part)this).SetText(Helpers.HL7Encode(text));
		}

		public string[] SplitFieldsIntoComponents(string field)
		{
			return base.Split(field, base.Message.ComponentSeperator);
		}

		public override string ToString()
		{
			string text = "";
			if (this.Components == null)
			{
				text = base.Text;
			}
			else
			{
				foreach (BasePart childPart in base.ChildParts)
				{
					text = (!childPart.HasChildren ? string.Concat(text, childPart.Text, base.Message.ComponentSeperator) : string.Concat(text, childPart.ToString(), base.Message.ComponentSeperator));
				}
			}
			if (text.EndsWith(base.Message.ComponentSeperator))
			{
				text = text.Substring(0, text.Length - 1);
			}
			return text;
		}
	}
}