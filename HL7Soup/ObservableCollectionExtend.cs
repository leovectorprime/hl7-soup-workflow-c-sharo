using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public static class ObservableCollectionExtend
	{
		public static void AddRange<TSource>(this ObservableCollection<TSource> source, IEnumerable<TSource> items)
		{
			foreach (TSource item in items)
			{
				source.Insert(0, item);
			}
		}
	}
}