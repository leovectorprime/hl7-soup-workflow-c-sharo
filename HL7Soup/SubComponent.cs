using HL7Soup.HL7Descriptions;
using HL7Soup.Integrations;
using System;
using System.Diagnostics;

namespace HL7Soup
{
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(SubComponentDebuggingProxy))]
	public class SubComponent : BasePart, IHL7SubComponent, IHL7Part
	{
		private string DebuggerDisplay
		{
			get
			{
				string path = this.GetPath();
				string str = "";
				string str1 = "                                                            ";
				if (base.XPart != null)
				{
					str = Helpers.TruncateString(base.XPart.Description, Math.Max(27 - this.GetPath().Length, 12));
				}
				return string.Concat(new string[] { path, "(", string.Concat(str, ") ", str1).Substring(0, 60 - (int)((double)(path.Length + str.Length) * 0.85)), "  =", base.Text });
			}
		}

		public SubComponent(string text, BasePart parent, string locationCode, int index, int documentLocation, XBasePart xPart) : base(text, parent, locationCode, index, documentLocation, xPart)
		{
			base.LocationOfFirstCharacter = documentLocation;
		}

		public override string GetCurrentPath()
		{
			return base.LocationCode;
		}

		public override string GetDescriptionName()
		{
			if (base.Parent == null)
			{
				return "";
			}
			return base.Parent.GetDescriptionName();
		}

		public override BasePart GetPart(PathSplitter path)
		{
			return null;
		}

		public override string GetPath()
		{
			string path = "";
			if (base.Parent != null)
			{
				path = base.Parent.GetPath();
			}
			return string.Concat(path, ".", base.LocationCode);
		}

		void HL7Soup.Integrations.IHL7Part.SetText(string text)
		{
			this.SetTextQuietly(text);
			this.RebuildParentTextQuietly();
		}

		protected override void LoadChildParts()
		{
		}

		public override BasePart SetCurrentLocation(int location)
		{
			return null;
		}

		public override HL7Soup.PositionInTheDocument SetCurrentPath(PathSplitter path)
		{
			return null;
		}

		public void SetTextEncoded(string text)
		{
			((IHL7Part)this).SetText(Helpers.HL7Encode(text));
		}
	}
}