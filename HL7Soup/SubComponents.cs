using HL7Soup.Integrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HL7Soup
{
	public class SubComponents : Collection<IHL7SubComponent>, IHL7SubComponents, ICollection<IHL7SubComponent>, IEnumerable<IHL7SubComponent>, IEnumerable
	{
		public SubComponents()
		{
		}

		public SubComponents(IList<IHL7SubComponent> subComponents) : base(subComponents)
		{
		}

		public SubComponents(IEnumerable<IHL7SubComponent> subComponents) : base(subComponents.ToList<IHL7SubComponent>())
		{
		}

		public override string ToString()
		{
			return string.Join<IHL7SubComponent>("&", base.Items);
		}
	}
}