using System;

namespace HL7Soup
{
	public enum PathTypes
	{
		None,
		Segment,
		SegmentIndex,
		Field,
		FieldRepeatIndex,
		Component,
		SubComponent,
		Hyphen,
		Dot
	}
}