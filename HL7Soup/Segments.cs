using HL7Soup.Integrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace HL7Soup
{
	public class Segments : Collection<IHL7Segment>, IHL7Segments, ICollection<IHL7Segment>, IEnumerable<IHL7Segment>, IEnumerable
	{
		public Segments()
		{
		}

		public Segments(IList<IHL7Segment> segments) : base(segments)
		{
		}

		public Segments(IEnumerable<IHL7Segment> segments) : base(segments.ToList<IHL7Segment>())
		{
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (IHL7Segment hL7Segment in this)
			{
				stringBuilder.Append(hL7Segment.ToString());
				stringBuilder.Append("\r");
			}
			return stringBuilder.ToString();
		}
	}
}