using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageHighlighters
{
	public class ValidationResult
	{
		public string FieldName
		{
			get;
			set;
		}

		public HL7Soup.MessageHighlighters.MessageHighlighter MessageHighlighter
		{
			get;
			set;
		}

		public BasePart Part
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public bool Result
		{
			get;
			set;
		}

		public ValidationResult()
		{
		}
	}
}