using HL7Soup;
using HL7Soup.MessageFilters;
using HL7Soup.Workflow.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace HL7Soup.MessageHighlighters
{
	public sealed class ValidationManager : INotifyPropertyChanged
	{
		private static volatile ValidationManager instance;

		private static object syncRoot;

		private string highlightersDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\Highlighters\\");

		private HighlightersCollection currentHighlightersCollection;

		public HighlightersCollection CurrentHighlightersCollection
		{
			get
			{
				return this.currentHighlightersCollection;
			}
			set
			{
				this.currentHighlightersCollection = value;
				if (value != null)
				{
					Settings.Default.CurrentMessageHighlighters = value.Name;
					Settings.Default.Save();
					ValidationManager.instance.MessageHighlighters = new ObservableCollection<MessageHighlighter>(value.Highlighters);
					if (this.PropertyChanged != null)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("MessageHighlighters"));
					}
				}
			}
		}

		public ObservableCollection<HighlightersCollection> HighlighterSets
		{
			get;
			set;
		}

		public static ValidationManager Instance
		{
			get
			{
				if (ValidationManager.instance == null)
				{
					lock (ValidationManager.syncRoot)
					{
						if (ValidationManager.instance == null)
						{
							ValidationManager.instance = new ValidationManager();
						}
					}
					ValidationManager.instance.MessageHighlighters = new ObservableCollection<MessageHighlighter>();
					ValidationManager.instance.LoadHighlighters();
				}
				return ValidationManager.instance;
			}
		}

		public ObservableCollection<MessageHighlighter> MessageHighlighters
		{
			get;
			set;
		}

		static ValidationManager()
		{
			ValidationManager.syncRoot = new object();
		}

		private ValidationManager()
		{
		}

		public void CopyCurrentHighlightersToFilePath(string filename)
		{
			this.SaveHighlighters();
			File.Copy(string.Concat(this.highlightersDir, Settings.Default.CurrentMessageHighlighters, ".dat"), filename);
		}

		public static void CreateAppdataDirectory()
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\Highlighters");
			if (!Directory.Exists(str))
			{
				Directory.CreateDirectory(str);
			}
		}

		private HighlightersCollection CreateDefaultHighlightersSet()
		{
			PathSplitter pathSplitter = new PathSplitter("PID-7");
			HighlightersCollection highlightersCollection = new HighlightersCollection();
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("MSH-7");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PID-29");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PID-33");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PD1-17");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("NK1-8");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("NK1-9");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("NK1-16");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV1-25");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV1-30");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV1-35");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV1-44");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV1-45");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBX-12");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBX-14");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBX-19");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("AL1-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("DG1-5");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("DG1-19");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-12");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-13");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-18");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-24");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-26");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("IN1-29");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("ROL-5");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("ROL-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("EVN-2");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("EVN-3");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("EVN-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-7");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-8");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-14");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-22");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("OBR-36");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TXA-4");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TXA-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TXA-7");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TXA-8");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("AIS-4");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("AIG-8");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("AIL-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("AIP-6");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TQ1-7");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("TQ1-8");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV2-9");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV2-14");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV2-17");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			pathSplitter = new PathSplitter("PV2-26");
			highlightersCollection.Highlighters.Add(new MessageHighlighter()
			{
				Path = pathSplitter,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true,
				MessageFilter = new DateMessageFilter()
				{
					Path = pathSplitter.ToString(),
					Comparer = DateMessageFilterComparers.InvalidDate
				}
			});
			highlightersCollection.Description = "Test for invalid dates";
			highlightersCollection.Name = string.Concat("DefaultMessageHighlighters", Guid.NewGuid());
			ValidationManager.instance.CurrentHighlightersCollection = highlightersCollection;
			ValidationManager.instance.SaveHighlighters();
			return highlightersCollection;
		}

		public void DeleteHighlighterSet(HighlightersCollection curItem)
		{
			File.Delete(string.Concat(this.highlightersDir, Settings.Default.CurrentMessageHighlighters, ".dat"));
			ValidationManager.instance.CurrentHighlightersCollection = null;
			ValidationManager.Instance.HighlighterSets.Remove(curItem);
			if (ValidationManager.instance.HighlighterSets.Count > 0)
			{
				ValidationManager.instance.currentHighlightersCollection = ValidationManager.instance.HighlighterSets[0];
			}
		}

		public List<ValidationResult> GetValidationResults(Message message)
		{
			bool flag = true;
			List<ValidationResult> validationResults = new List<ValidationResult>();
			foreach (MessageHighlighter messageHighlighter in ValidationManager.Instance.MessageHighlighters)
			{
				List<BasePart> baseParts = null;
				baseParts = (!messageHighlighter.MessageFilter.IncludesRepeatFields ? new List<BasePart>()
				{
					message.GetPart(messageHighlighter.Path)
				} : message.GetParts(messageHighlighter.Path));
				foreach (BasePart basePart in baseParts)
				{
					ValidationResult validationResult = new ValidationResult()
					{
						MessageHighlighter = messageHighlighter,
						Part = basePart
					};
					if (basePart != null)
					{
						validationResult.FieldName = basePart.Description;
					}
					if (!messageHighlighter.MessageFilter.Compare(basePart))
					{
						continue;
					}
					if (basePart != null)
					{
						validationResult.Path = basePart.GetPath();
					}
					else
					{
						validationResult.Path = messageHighlighter.MessageFilter.Path;
					}
					validationResult.Result = true;
					if (messageHighlighter.InvalidatesMessage)
					{
						flag = false;
					}
					validationResults.Add(validationResult);
				}
			}
			message.IsValid = flag;
			return validationResults;
		}

		public HighlightersCollection ImportHighlighters(string filePath)
		{
			HighlightersCollection highlightersCollection = ValidationManager.LoadHighlighterCollection(filePath);
			ValidationManager.Instance.CurrentHighlightersCollection = highlightersCollection;
			ValidationManager.Instance.HighlighterSets.Add(highlightersCollection);
			ValidationManager.Instance.CurrentHighlightersCollection = highlightersCollection;
			ValidationManager.Instance.SaveHighlighters();
			ValidationManager.instance.MessageHighlighters = new ObservableCollection<MessageHighlighter>(highlightersCollection.Highlighters);
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("MessageHighlighters"));
			}
			Settings.Default.CurrentMessageHighlighters = highlightersCollection.Name;
			this.CurrentHighlightersCollection = highlightersCollection;
			return highlightersCollection;
		}

		public bool IsMessageValid(Message message)
		{
			bool flag;
			using (IEnumerator<MessageHighlighter> enumerator = ValidationManager.Instance.MessageHighlighters.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					MessageHighlighter current = enumerator.Current;
					if (!current.InvalidatesMessage)
					{
						continue;
					}
					BasePart part = message.GetPart(current.Path);
					if (!current.MessageFilter.Compare(part))
					{
						continue;
					}
					flag = false;
					return flag;
				}
				return true;
			}
			return flag;
		}

		public List<HighlightersCollection> LoadAllHighlighterSets()
		{
			ValidationManager.CreateAppdataDirectory();
			bool flag = false;
			List<HighlightersCollection> highlightersCollections = new List<HighlightersCollection>();
			string[] files = Directory.GetFiles(this.highlightersDir);
			for (int i = 0; i < (int)files.Length; i++)
			{
				FileInfo fileInfo = new FileInfo(files[i]);
				HighlightersCollection list = this.LoadHighlighters(fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length));
				if (list != null)
				{
					list.Highlighters = (
						from o in list.Highlighters
						orderby string.Concat(o.Path.ToString(), o.ColorName.StartsWith("Red").ToString())
						select o).ToList<MessageHighlighter>();
					highlightersCollections.Add(list);
					if (list.Name == Settings.Default.CurrentMessageHighlighters)
					{
						flag = true;
					}
				}
			}
			ValidationManager.instance.HighlighterSets = new ObservableCollection<HighlightersCollection>(
				from o in highlightersCollections
				orderby o.Description
				select o);
			if (!flag && highlightersCollections.Count > 0)
			{
				ValidationManager.instance.CurrentHighlightersCollection = highlightersCollections[0];
				Settings.Default.CurrentMessageHighlighters = ValidationManager.instance.CurrentHighlightersCollection.Name;
				Settings.Default.Save();
			}
			return ValidationManager.instance.HighlighterSets.ToList<HighlightersCollection>();
		}

		private static HighlightersCollection LoadHighlighterCollection(string filePath)
		{
			Stream fileStream = null;
			if (File.Exists(filePath))
			{
				fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
				HighlightersCollection highlightersCollection = (HighlightersCollection)((IFormatter)(new BinaryFormatter())).Deserialize(fileStream);
				fileStream.Close();
				return highlightersCollection;
			}
			ValidationManager.Instance.LoadAllHighlighterSets();
			if (ValidationManager.Instance.HighlighterSets.Count <= 0)
			{
				return ValidationManager.instance.CreateDefaultHighlightersSet();
			}
			return ValidationManager.instance.HighlighterSets[0];
		}

		public HighlightersCollection LoadHighlighters()
		{
			HighlightersCollection highlightersCollection = this.LoadHighlighters(Settings.Default.CurrentMessageHighlighters);
			Settings.Default.CurrentMessageHighlighters = highlightersCollection.Name;
			this.CurrentHighlightersCollection = highlightersCollection;
			return highlightersCollection;
		}

		private HighlightersCollection LoadHighlighters(string name)
		{
			ValidationManager.CreateAppdataDirectory();
			return ValidationManager.LoadHighlighterCollection(string.Concat(this.highlightersDir, name, ".dat"));
		}

		public void SaveHighlighters()
		{
			if (ValidationManager.instance.CurrentHighlightersCollection == null)
			{
				this.CurrentHighlightersCollection = ValidationManager.instance.CreateDefaultHighlightersSet();
			}
			foreach (MessageHighlighter messageHighlighter in this.MessageHighlighters)
			{
				messageHighlighter.Path = messageHighlighter.MessageFilter.PathSplitter;
			}
			ValidationManager.instance.CurrentHighlightersCollection.Highlighters = this.MessageHighlighters.ToList<MessageHighlighter>();
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			ValidationManager.CreateAppdataDirectory();
			using (Stream fileStream = new FileStream(string.Concat(this.highlightersDir, Settings.Default.CurrentMessageHighlighters, ".dat"), FileMode.Create, FileAccess.Write, FileShare.None))
			{
				binaryFormatter.Serialize(fileStream, new HighlightersCollection()
				{
					Name = Settings.Default.CurrentMessageHighlighters,
					Description = ValidationManager.instance.CurrentHighlightersCollection.Description,
					Highlighters = ValidationManager.instance.MessageHighlighters.ToList<MessageHighlighter>()
				});
				fileStream.Close();
			}
		}

		public bool ValidateMessage(Message message)
		{
			message.IsValid = this.IsMessageValid(message);
			return message.IsValid;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}