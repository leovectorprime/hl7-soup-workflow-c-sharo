using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup.MessageHighlighters
{
	[Serializable]
	public class HighlightersCollection : INotifyPropertyChanged
	{
		private string description;

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("Description"));
				}
			}
		}

		public List<MessageHighlighter> Highlighters
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public HighlightersCollection()
		{
			this.Highlighters = new List<MessageHighlighter>();
		}

		public override string ToString()
		{
			return this.Description;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}