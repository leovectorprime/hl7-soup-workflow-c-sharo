using HL7Soup;
using HL7Soup.MessageFilters;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace HL7Soup.MessageHighlighters
{
	[Serializable]
	public class MessageHighlighter
	{
		public System.Windows.Media.Color Color
		{
			get
			{
				if (this.ColorName == "Red (Invalid)")
				{
					return System.Drawing.Color.LightPink.ToMediaColor();
				}
				if (this.ColorName == "Blue")
				{
					return System.Drawing.Color.LightSkyBlue.ToMediaColor();
				}
				if (this.ColorName == "Green")
				{
					return System.Drawing.Color.LightGreen.ToMediaColor();
				}
				return System.Drawing.Color.Orange.ToMediaColor();
			}
		}

		public string ColorName
		{
			get;
			set;
		}

		public bool InvalidatesMessage
		{
			get;
			set;
		}

		public string MenuName
		{
			get
			{
				return string.Concat(this.ColorName, " Where");
			}
		}

		public HL7Soup.MessageFilters.MessageFilter MessageFilter
		{
			get;
			set;
		}

		public PathSplitter Path
		{
			get;
			set;
		}

		public string UserText
		{
			get;
			set;
		}

		public MessageHighlighter()
		{
		}

		public MessageHighlighter Clone()
		{
			return new MessageHighlighter()
			{
				ColorName = this.ColorName,
				InvalidatesMessage = this.InvalidatesMessage,
				MessageFilter = this.MessageFilter.Clone(),
				Path = new PathSplitter(this.Path.ToString()),
				UserText = this.UserText
			};
		}

		public string GetResultText()
		{
			return string.Concat(this.ColorName, " when ", this.MessageFilter.GetResultText(), (string.IsNullOrWhiteSpace(this.UserText) ? "" : string.Concat(".  ", this.UserText)));
		}

		public Message Highlight(Message message)
		{
			message.GetPart(this.Path);
			return message;
		}

		public override string ToString()
		{
			return string.Concat(new object[] { "set ", this.Path, " \"", this.ColorName, "\"" });
		}
	}
}