﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("3.2.0.0")]
[assembly: AssemblyProduct("HL7SoupWorkflow")]
[assembly: AssemblyTitle("HL7SoupWorkflow")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("3.2.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: Guid("d323bd47-6f81-4908-85c7-0c89f21aca02")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
